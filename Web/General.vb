﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Web

Namespace Web

    Public Class General


#Region "Debug"

        ''' <summary>
        ''' Debug function to pull session data out into an easily inspected dictionary
        ''' </summary>
        ''' <param name="pSession"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function SessionToDict(Optional pSession As SessionState.HttpSessionState = Nothing) As IDictionary(Of String, Object)
            Dim res As New Dictionary(Of String, Object)
            If ((pSession Is Nothing) AndAlso (HttpContext.Current IsNot Nothing)) Then pSession = HttpContext.Current.Session
            If (pSession IsNot Nothing) Then
                For Each ky As String In pSession.Keys
                    res.Add(ky, pSession.Item(ky))
                Next
                Return res
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Debug function to pull form data out into an easily inspected dictionary
        ''' </summary>
        ''' <param name="pRequest"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormToDict(Optional pRequest As HttpRequest = Nothing) As IDictionary(Of String, Object)
            Dim res As New Dictionary(Of String, Object)
            If ((pRequest Is Nothing) AndAlso (HttpContext.Current IsNot Nothing)) Then pRequest = HttpContext.Current.Request
            If (pRequest IsNot Nothing) Then
                For Each ky As String In pRequest.Form.Keys
                    res.Add(ky, pRequest.Form.Item(ky))
                Next
                Return res
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Debug function to pull url parameters out into an easily inspected dictionary
        ''' </summary>
        ''' <param name="pRequest"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function QueryStringToDict(Optional pRequest As HttpRequest = Nothing) As IDictionary(Of String, Object)
            Dim res As New Dictionary(Of String, Object)
            If ((pRequest Is Nothing) AndAlso (HttpContext.Current IsNot Nothing)) Then pRequest = HttpContext.Current.Request
            If (pRequest IsNot Nothing) Then
                For Each ky As String In pRequest.QueryString.Keys
                    res.Add(ky, pRequest.QueryString.Item(ky))
                Next
                Return res
            Else
                Return Nothing
            End If
        End Function

#End Region

    End Class

End Namespace
