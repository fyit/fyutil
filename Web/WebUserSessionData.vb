﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace Web

    ''' <summary>
    ''' SessionData provider using HttpSessionState
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WebUserSessionData
        Inherits UserSessionData

        Private Const PERSIST_SLOT_NAME = "WebUserSessionData.HttpSessionState"


        ''' <summary>
        ''' Set the current session, for use when there is no HttpContext (e.g. Session_End)
        ''' </summary>
        Public Shared Sub SetHttpSession(pSession As System.Web.SessionState.HttpSessionState)
            'NOTE: Uses SetData instead of LogicalSetData, so it won't pass a thread boundary
            Runtime.Remoting.Messaging.CallContext.SetData(PERSIST_SLOT_NAME, pSession)
        End Sub

        Protected Overrides Function DoGetCurrentSession() As IDictionary(Of String, Object)
            If ((HttpContext.Current IsNot Nothing) AndAlso (HttpContext.Current.Session IsNot Nothing)) Then
                Return New SessionDictionary(HttpContext.Current.Session)
            End If

            'try to get it from CallContext
            Dim ctxSess = DirectCast(Runtime.Remoting.Messaging.CallContext.GetData(PERSIST_SLOT_NAME), System.Web.SessionState.HttpSessionState)
            If (ctxSess IsNot Nothing) Then
                Return New SessionDictionary(ctxSess)
            End If

            Return Nothing
        End Function

        Protected Overrides Sub DoSetCurrentSession(pSession As IDictionary(Of String, Object))
            Dim typSess = DirectCast(pSession, SessionDictionary)
            SetHttpSession(typSess.sessionState)
        End Sub

    End Class

    ''' <summary>
    ''' SessionData provider using HttpSessionState, and fallback to MultiUserSessionData
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WebUserSessionDataWithMultiUserSessionData
        Inherits MultiUserSessionData

        Private Const PERSIST_SLOT_NAME = "WebUserSessionDataWithMultiUserSessionData.HttpSessionState"


        ''' <summary>
        ''' Set the current session, for use when there is no HttpContext (e.g. Session_End)
        ''' </summary>
        Public Shared Sub SetHttpSession(pSession As System.Web.SessionState.HttpSessionState)
            'NOTE: Uses SetData instead of LogicalSetData, so it won't pass a thread boundary
            Runtime.Remoting.Messaging.CallContext.SetData(PERSIST_SLOT_NAME, pSession)
        End Sub

        Protected Overrides Function DoGetCurrentSession() As IDictionary(Of String, Object)
            If ((HttpContext.Current IsNot Nothing) AndAlso (HttpContext.Current.Session IsNot Nothing)) Then
                Return New SessionDictionary(HttpContext.Current.Session)
            End If

            'try to get it from CallContext
            Dim ctxSess = DirectCast(Runtime.Remoting.Messaging.CallContext.GetData(PERSIST_SLOT_NAME), System.Web.SessionState.HttpSessionState)
            If (ctxSess IsNot Nothing) Then
                Return New SessionDictionary(ctxSess)
            End If

            'try to get it from MultiUserSessionData
            Return MyBase.DoGetCurrentSession()
        End Function

        Protected Overrides Sub DoSetCurrentSession(pSession As IDictionary(Of String, Object))
            If (TypeOf pSession Is SessionDictionary) Then
                Dim typSess = DirectCast(pSession, SessionDictionary)
                SetHttpSession(typSess.sessionState)
            Else
                MyBase.DoSetCurrentSession(pSession)
            End If
        End Sub

    End Class






    ''' <summary>
    ''' Manages session data using strongly typed buckets. Includes functions for also saving to cookies.
    ''' </summary>
    ''' <typeparam name="TBucketEnum"></typeparam>
    ''' <remarks></remarks>
    Public Class WebSessionManager(Of TBucketEnum As Structure)
        Inherits FYUtil.SessionManager(Of TBucketEnum)

        Public Sub New(pSessionData As IUserSessionData)
            MyBase.New(pSessionData)
        End Sub


        Public Function ReadCookie(Of T)(pKey As TBucketEnum, pDefault As T) As T
            Dim cky = HttpContext.Current.Request.Cookies.Item(GetBucketKey(pKey))
            Dim dataVal As String = If((cky IsNot Nothing), cky.Value, Nothing)
            If (dataVal Is Nothing) Then Return pDefault
            Try
                Return FYUtil.Serialization.DeserializeObject(Of T)(dataVal, FYUtil.Serialization.eSerializer.Readable)
            Catch
                Return pDefault
            End Try
        End Function

        Public Sub WriteCookie(pKey As TBucketEnum, pData As Object, Optional pExpirationDays As Double = 30)
            Dim cky As New HttpCookie(GetBucketKey(pKey), FYUtil.Serialization.SerializeObject(pData, FYUtil.Serialization.eSerializer.Readable))
            cky.Expires = DateTime.Today.AddDays(pExpirationDays)
            HttpContext.Current.Response.Cookies.Add(cky)
        End Sub


        Public Function ReadSessionAndCookie(Of T)(pKey As TBucketEnum, pDefault As T) As T
            Dim dataVal = ReadSession(pKey, pDefault)
            If (dataVal Is Nothing) Then
                dataVal = ReadCookie(pKey, pDefault)
                If (dataVal IsNot Nothing) Then WriteSession(pKey, dataVal)
            End If
            Return dataVal
        End Function

        Public Sub WriteSessionAndCookie(pKey As TBucketEnum, pData As Object, Optional pExpirationDays As Double = 30)
            WriteSession(pKey, pData)
            WriteCookie(pKey, pData, pExpirationDays)
        End Sub

    End Class

End Namespace
