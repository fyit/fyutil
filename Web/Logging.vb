﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Web
Imports FYUtil.Logging

Namespace Web

    Public Class Logging

        Public Shared Sub DefaultGatherData(pEventData As EventData)
            GatherSimpleData(pEventData)
            If (pEventData.Severity >= FYUtil.Logging.eSeverity.Important) Then
                GatherExtendedData(pEventData)
            End If
        End Sub

        Public Shared Sub GatherSimpleData(pEventData As EventData)
            pEventData.Timestamp = DateTime.UtcNow
            pEventData.TimestampLocal = DateTime.Now

            Try
                pEventData.User = System.Threading.Thread.CurrentPrincipal.Identity.Name
                If (HttpContext.Current IsNot Nothing) Then
                    Dim req = HttpContext.Current.Request
                    If (req IsNot Nothing) Then
                        pEventData.SourceObject = req.Url.AbsolutePath
                    End If
                End If
            Catch
                'pEventData.Message = String.Format("-- Fatal error gathering logging data --    {0}", pEventData.Message)
            End Try
        End Sub

        Public Shared Sub GatherExtendedData(pEventData As EventData)
            Dim extra As New Text.StringBuilder
            If (Not String.IsNullOrEmpty(pEventData.ExtraInfo)) Then extra.AppendLine(pEventData.ExtraInfo)

            Try
                If (HttpContext.Current IsNot Nothing) Then
                    Dim req = HttpContext.Current.Request
                    If (req IsNot Nothing) Then
                        extra.exAppendFormatLine("IP: {0}", req.UserHostAddress)
                        extra.exAppendFormatLine("UserAgent: {0}", req.UserAgent)
                        extra.exAppendFormatLine("Referrer: {0}", req.UrlReferrer)
                        extra.exAppendFormatLine("Method: {0}", req.HttpMethod)
                        extra.exAppendFormatLine("URL Data: {0}", req.QueryString)
                        extra.exAppendFormatLine("Form Data: {0}", PrintFormData(req.Form))
                    End If
                End If
            Catch ex As Exception
                Debug.Print("Error in GatherExtendedData: " + ex.Message)
                'pEventData.Message = String.Format("-- Fatal error gathering logging data --    {0}", pEventData.Message)
            End Try

            If (SiteActivityTracker.Enabled) Then
                Try
                    Dim requests = SiteActivityTracker.GetCurrentRequests()
                    extra.AppendLine("Current Requests:")
                    For Each req In requests
                        extra.exAppendFormatLine("  {0}: {1} {2}", req.Start, req.HttpMethod, req.UrlPath)
                    Next
                Catch ex As Exception
                    Debug.Print("Error in GatherExtendedData: " + ex.Message)
                End Try

                Try
                    Dim sessInfo = SiteActivityTracker.GetCurrentSessionInfo()
                    extra.exAppendFormatLine("Session Start: {0}", sessInfo.Start)
                    extra.exAppendFormatLine("Session Total Time (ms): {0}", sessInfo.TotalTime)
                    extra.AppendLine("Session Page History:")
                    For idx = sessInfo.RequestHistory.Count - 1 To 0 Step -1
                        Dim curPg = sessInfo.RequestHistory.Item(idx)
                        extra.exAppendFormatLine("  {0}: {1} {2}", curPg.Start, curPg.HttpMethod, curPg.UrlPath)
                    Next
                Catch ex As Exception
                    Debug.Print("Error in GatherExtendedData: " + ex.Message)
                End Try
            End If

            pEventData.ExtraInfo = extra.ToString
        End Sub



        Public Shared Function DefaultFormatter(pEventData As EventData) As String
            If (pEventData.Severity >= FYUtil.Logging.eSeverity.Important) Then
                Return ExtendedFormatter(pEventData)
            Else
                Return SimpleFormatter(pEventData)
            End If
        End Function


        Public Shared Function SimpleFormatter(pEventData As EventData) As String
            Return String.Format("{0:yyyyMMdd.HHmmss}: {1}", pEventData.TimestampLocal, pEventData.Message)
        End Function

        Public Shared Function ExtendedFormatter(pEventData As EventData) As String
            Dim message As New Text.StringBuilder()
            message.AppendLine("---------------------------")
            message.exAppendFormatLine("Message: {0}", pEventData.Message)
            message.exAppendFormatLine("Page: {0}", pEventData.SourceObject)
            message.exAppendFormatLine("User: {0}", pEventData.User)
            message.exAppendFormatLine("Date: {0:yyyyMMdd.HHmmss}", pEventData.TimestampLocal)
            If (Not String.IsNullOrEmpty(pEventData.ExtraInfo)) Then
                message.AppendLine("--ExtraInfo")
                message.AppendLine(pEventData.ExtraInfo)
            End If
            If (pEventData.StackTrace IsNot Nothing) Then
                message.Append("Error stack: ")
                message.AppendLine(pEventData.StackTrace)
            End If
            message.Append("---------------------------")
            Return message.ToString()
        End Function


        Public Shared Function PrintFormData(pForm As Collections.Specialized.NameValueCollection) As String
            Static ignoreList As IList(Of String) = {"__VIEWSTATE", "__LASTFOCUS", "__EVENTTARGET", "__EVENTARGUMENT", "__EVENTVALIDATION", "Password", "txtPassword", "TrackData", "ReferenceNo", "ExpireMM", "ExpireYY", "SecurityCode"}
            Dim sb As New Text.StringBuilder
            For Each ky As String In pForm.Keys
                If (Not ignoreList.Contains(ky, StringComparer.OrdinalIgnoreCase)) Then
                    sb.AppendFormat("{0}={1} & ", ky, pForm.Item(ky))
                End If
            Next

            Return sb.ToString
        End Function

    End Class

End Namespace
