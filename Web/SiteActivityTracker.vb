﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace Web

    ''' <summary>
    ''' Tracks activity within a web application. Requires hooks in Global.asax.
    ''' </summary>
    Public Class SiteActivityTracker

        Private Const APPDATA_KEY = "SiteActivityTracker"

        Private Shared _enabled As Boolean = False
        Public Shared Property Enabled() As Boolean
            Get
                Return _enabled
            End Get
            Set(pEnabled As Boolean)
                _enabled = pEnabled
                If (Not _enabled) Then GetTracker().ClearAll()
            End Set
        End Property


        ''' <summary>
        ''' Initialize the tracker on this Application. Should be called from 'Application_Start'
        ''' </summary>
        Public Shared Sub Init()
            Enabled = True
            Dim tracker As New SiteActivityTracker()
            If (HttpContext.Current Is Nothing) Then Throw New Exception("SiteActivityTracker: Not in an HttpContext")
            HttpContext.Current.Application.Item(APPDATA_KEY) = tracker
        End Sub

        ''' <summary>
        ''' Flag the beginning of a request. Should be called from 'Application_PreRequestHandlerExecute'
        ''' </summary>
        Public Shared Sub StartRequest()
            If (Not Enabled) Then Return
            GetTracker().DoStartRequest()
        End Sub

        ''' <summary>
        ''' Flag the end of a request. Should be called from 'Application_PostRequestHandlerExecute'
        ''' </summary>
        Public Shared Function FinishRequest() As RequestInfo
            If (Not Enabled) Then Return Nothing
            Return GetTracker().DoFinishRequest()
        End Function

        ''' <summary>
        ''' Flag the beginning of a session. Should be called from 'Session_Start'
        ''' </summary>
        Public Shared Sub StartSession(pApplication As HttpApplicationState, pSessionId As String)
            If (Not Enabled) Then Return
            GetTracker(pApplication).DoStartSession(pSessionId)
        End Sub

        ''' <summary>
        ''' Flag the end of a session. Should be called from 'Session_End'
        ''' </summary>
        Public Shared Function FinishSession(pApplication As HttpApplicationState, pSessionId As String) As SessionInfo
            If (Not Enabled) Then Return Nothing
            Return GetTracker(pApplication).DoFinishSession(pSessionId)
        End Function

        Public Shared Function GetTracker() As SiteActivityTracker
            If (Not Enabled) Then Throw New Exception("SiteActivityTracker: Not enabled")
            If (HttpContext.Current Is Nothing) Then Throw New Exception("SiteActivityTracker: Not in an HttpContext")
            Return GetTracker(HttpContext.Current.Application)
        End Function

        Private Shared Function GetTracker(pApplication As HttpApplicationState) As SiteActivityTracker
            If (Not Enabled) Then Throw New Exception("SiteActivityTracker: Not enabled")
            Dim tracker = pApplication.Item(APPDATA_KEY)
            Return DirectCast(tracker, SiteActivityTracker)
        End Function

        Public Shared Function GetCurrentSessionInfo() As SessionInfo
            If (Not Enabled) Then Throw New Exception("SiteActivityTracker: Not enabled")
            If ((HttpContext.Current Is Nothing) OrElse (HttpContext.Current.Session Is Nothing)) Then Throw New Exception("SiteActivityTracker: Not in a Session")
            Return GetTracker().GetSessionInfo(HttpContext.Current.Session.SessionID)
        End Function

        Public Shared Function GetCurrentRequests() As IList(Of RequestInfo)
            If ((HttpContext.Current Is Nothing) OrElse (HttpContext.Current.Session Is Nothing)) Then Throw New Exception("SiteActivityTracker: Not in a Session")
            Return GetTracker()._currentRequests.Values.ToList
        End Function








        Private _lockTarget As New Object
        Private _currentRequests As New Dictionary(Of System.Web.HttpRequest, RequestInfo)

        Public Property RequestHistory As New List(Of RequestInfo)
        Public Property Pages As New Dictionary(Of String, PageInfo)
        Public Property Sessions As New Dictionary(Of String, SessionInfo)

        Public Property KeepRequestHistory As Int32 = 100
        Public Property KeepPageRequestHistory As Int32 = 10
        Public Property KeepSessionRequestHistory As Int32 = 10

        Public Class PageInfo
            Public Property PageKey As String
            Public Property UrlPath As String
            Public Property HttpMethod As String
            Public Property MinTime As Int32 = Int32.MaxValue
            Public Property MaxTime As Int32 = Int32.MinValue
            Public Property HitCount As Int32
            Public Property TotalTime As Int64
            Public Property RequestHistory As New List(Of RequestInfo)

            Public ReadOnly Property AverageTime As Int32
                Get
                    If (HitCount = 0) Then Return 0
                    Return Convert.ToInt32(TotalTime / HitCount)
                End Get
            End Property
        End Class

        Public Class RequestInfo
            Public Property UrlPath As String
            Public Property HttpMethod As String
            Public Property UrlParams As String
            Public Property Start As DateTime
            Public Property Elapsed As Int32
            Public Property Username As String
            Public Property IpAddress As String
            Public Property Timer As Stopwatch
            Public Property Session As SessionInfo
        End Class

        Public Class SessionInfo
            Public Property SessionId As String
            Public Property Start As DateTime
            Public Property LastAccess As DateTime
            Public Property TotalTime As Int64
            Public Property Username As String
            Public Property IpAddress As String
            Public Property RequestHistory As New List(Of RequestInfo)
            Public Property ExtraInfo As Object
        End Class



        Private Sub New()
        End Sub

        Public Sub ClearAll()
            RequestHistory = New List(Of RequestInfo)
            Pages = New Dictionary(Of String, PageInfo)
            Sessions = New Dictionary(Of String, SessionInfo)
        End Sub

        Private Function GetSessionInfo(pSessionId As String) As SessionInfo
            If (Not Sessions.ContainsKey(pSessionId)) Then DoStartSession(pSessionId)
            Return Sessions.Item(pSessionId)
        End Function




        Private Sub DoStartRequest()
            If (HttpContext.Current IsNot Nothing) Then
                SyncLock _lockTarget
                    Dim reqInf As New RequestInfo
                    reqInf.Start = DateTime.UtcNow
                    reqInf.Timer = Stopwatch.StartNew()
                    reqInf.UrlPath = HttpContext.Current.Request.Url.AbsolutePath
                    reqInf.HttpMethod = HttpContext.Current.Request.HttpMethod
                    reqInf.UrlParams = HttpContext.Current.Request.Url.Query
                    reqInf.IpAddress = HttpContext.Current.Request.UserHostAddress
                    reqInf.Username = System.Threading.Thread.CurrentPrincipal.Identity.Name
                    _currentRequests.Item(HttpContext.Current.Request) = reqInf
                End SyncLock
            End If
        End Sub

        Private Function DoFinishRequest() As RequestInfo
            If (HttpContext.Current IsNot Nothing) Then
                SyncLock _lockTarget
                    If (_currentRequests.ContainsKey(HttpContext.Current.Request)) Then
                        Dim reqInf = _currentRequests.Item(HttpContext.Current.Request)
                        _currentRequests.Remove(HttpContext.Current.Request)

                        reqInf.Timer.Stop()
                        reqInf.Elapsed = Convert.ToInt32(reqInf.Timer.ElapsedMilliseconds)
                        reqInf.Username = System.Threading.Thread.CurrentPrincipal.Identity.Name
                        RequestHistory.Add(reqInf)
                        While RequestHistory.Count > KeepRequestHistory
                            RequestHistory.RemoveAt(0)
                        End While

                        Dim charsAndSlashesOnly As New System.Text.RegularExpressions.Regex("[^A-Za-z/]")
                        Dim pgKey = charsAndSlashesOnly.Replace(String.Format("{0} {1}", reqInf.HttpMethod, reqInf.UrlPath), String.Empty)
                        Dim pgUrl = charsAndSlashesOnly.Replace(reqInf.UrlPath, String.Empty)

                        If (Not Pages.ContainsKey(pgKey)) Then Pages.Add(pgKey, New PageInfo With {.PageKey = pgKey, .UrlPath = pgUrl, .HttpMethod = reqInf.HttpMethod})
                        Dim pg As PageInfo = Pages.Item(pgKey)
                        If (reqInf.Elapsed > pg.MaxTime) Then pg.MaxTime = reqInf.Elapsed
                        If (reqInf.Elapsed < pg.MinTime) Then pg.MinTime = reqInf.Elapsed
                        pg.HitCount += 1
                        pg.TotalTime += reqInf.Elapsed
                        pg.RequestHistory.Add(reqInf)
                        While pg.RequestHistory.Count > KeepPageRequestHistory
                            pg.RequestHistory.RemoveAt(0)
                        End While
                        Pages.Item(pgKey) = pg

                        If (HttpContext.Current.Session IsNot Nothing) Then
                            Dim sess = HttpContext.Current.Session
                            If (Not Sessions.ContainsKey(sess.SessionID)) Then
                                FYUtil.Logging.LogWarning("SiteActivityTracker: Implicitly creating SessionInfo")
                                Sessions.Item(sess.SessionID) = New SessionInfo With {.SessionId = sess.SessionID, .Start = DateTime.UtcNow}
                            End If
                            reqInf.Session = Sessions.Item(sess.SessionID)
                            reqInf.Session.RequestHistory.Add(reqInf)
                            While reqInf.Session.RequestHistory.Count > KeepSessionRequestHistory
                                reqInf.Session.RequestHistory.RemoveAt(0)
                            End While
                            reqInf.Session.LastAccess = reqInf.Start
                            reqInf.Session.TotalTime += reqInf.Elapsed
                            reqInf.Session.Username = reqInf.Username
                            reqInf.Session.IpAddress = reqInf.IpAddress
                        End If

                        Return reqInf
                    End If
                End SyncLock
            End If

            Return Nothing
        End Function



        Private Sub DoStartSession(pSessionId As String)
            SyncLock _lockTarget
                If (Not Sessions.ContainsKey(pSessionId)) Then
                    Dim sessInf As New SessionInfo With {.SessionId = pSessionId, .Start = DateTime.UtcNow}
                    Sessions.Item(sessInf.SessionId) = sessInf
                Else
                    'was already created implicitly... do nothing
                End If
            End SyncLock
        End Sub

        Private Function DoFinishSession(pSessionId As String) As SessionInfo
            SyncLock _lockTarget
                If (Sessions.ContainsKey(pSessionId)) Then
                    Dim sessInf = Sessions.Item(pSessionId)
                    Sessions.Remove(pSessionId)
                    Return sessInf
                End If
            End SyncLock

            Return Nothing
        End Function

    End Class

End Namespace
