﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace Web

    <Serializable()>
    Public Class SessionDictionary
        Implements IDictionary(Of String, Object)
        Implements IDictionary
        Friend ReadOnly sessionState As System.Web.SessionState.HttpSessionState
        ReadOnly keysAdapter As CollectionAdapter(Of String)
        ReadOnly valuesAdapter As CollectionAdapter(Of Object)

        Public Sub New(sessionState As System.Web.SessionState.HttpSessionState)
            Me.sessionState = sessionState
            keysAdapter = New CollectionAdapter(Of String)(sessionState.Keys)
            valuesAdapter = New CollectionAdapter(Of Object)(sessionState)
        End Sub

        Public Function ContainsKey(key As String) As Boolean Implements IDictionary(Of String, Object).ContainsKey
            Return keysAdapter.Contains(key)
        End Function

        Public Sub Add(name As String, value As Object) Implements IDictionary(Of String, Object).Add
            sessionState.Add(name, value)
        End Sub

        Public Function Remove(name As String) As Boolean Implements IDictionary(Of String, Object).Remove
            If Not ContainsKey(name) Then
                Return False
            End If
            sessionState.Remove(name)
            Return True
        End Function

        Public Function TryGetValue(key As String, ByRef value As Object) As Boolean Implements IDictionary(Of String, Object).TryGetValue
            If ContainsKey(key) Then
                value = Me(key)
                Return True
            End If

            value = Nothing
            Return False
        End Function

        Public Sub Add(item As KeyValuePair(Of String, Object)) Implements IDictionary(Of String, Object).Add
            Add(item.Key, item.Value)
        End Sub

        Public Function Contains(key As Object) As Boolean Implements IDictionary.Contains
            Return ContainsKey(DirectCast(key, String))
        End Function

        Public Sub Add(key As Object, value As Object) Implements IDictionary.Add
            Add(DirectCast(key, String), value)
        End Sub

        Public Sub Clear() Implements IDictionary.Clear, IDictionary(Of String, Object).Clear
            sessionState.Clear()
        End Sub

        Private Function IDictionary_GetEnumerator() As IDictionaryEnumerator Implements IDictionary.GetEnumerator
            Return New DictionaryEnumerator(Of String, Object)(KeyValueEnumerable().GetEnumerator())
        End Function

        Public Sub Remove(key As Object) Implements IDictionary.Remove
            Remove(DirectCast(key, String))
        End Sub

        Private Property IDictionary_Item(key As Object) As Object Implements IDictionary.Item
            Get
                Return Me(DirectCast(key, String))
            End Get
            Set(value As Object)
                Me(DirectCast(key, String)) = value
            End Set
        End Property

        Public Function Contains(item As KeyValuePair(Of String, Object)) As Boolean Implements IDictionary(Of String, Object).Contains
            If Not ContainsKey(item.Key) Then
                Return False
            End If
            Return Equals(Me(item.Key), item.Value)
        End Function

        Public Sub CopyTo(array As KeyValuePair(Of String, Object)(), arrayIndex As Integer) Implements IDictionary(Of String, Object).CopyTo
            CopyTo(DirectCast(array, Array), arrayIndex)
        End Sub

        Public Function Remove(item As KeyValuePair(Of String, Object)) As Boolean Implements IDictionary(Of String, Object).Remove
            If Not Contains(item) Then
                Return False
            End If
            Return Remove(item.Key)
        End Function

        Private Function IEnumerable_GetEnumerator() As IEnumerator(Of KeyValuePair(Of String, Object)) Implements IEnumerable(Of KeyValuePair(Of String, Object)).GetEnumerator
            Return KeyValueEnumerable().GetEnumerator()
        End Function

        Private Function KeyValueEnumerable() As IEnumerable(Of KeyValuePair(Of String, Object))
            Return Keys.[Select](Function(key) New KeyValuePair(Of String, Object)(key, Me(key)))
        End Function

        Public Function GetEnumerator() As IEnumerator Implements IDictionary.GetEnumerator
            Return sessionState.GetEnumerator()
        End Function

        Default Public Property Item(name As String) As Object Implements IDictionary(Of String, Object).Item
            Get
                Return sessionState(name)
            End Get
            Set(value As Object)
                sessionState(name) = value
            End Set
        End Property

        Public Sub CopyTo(array As Array, index As Integer) Implements IDictionary.CopyTo
            sessionState.CopyTo(array, index)
        End Sub

        Public ReadOnly Property Count() As Integer Implements IDictionary.Count, IDictionary(Of String, Object).Count
            Get
                Return sessionState.Count
            End Get
        End Property

        Public ReadOnly Property SyncRoot() As Object Implements IDictionary.SyncRoot
            Get
                Return sessionState.SyncRoot
            End Get
        End Property

        Public ReadOnly Property IsSynchronized() As Boolean Implements IDictionary.IsSynchronized
            Get
                Return sessionState.IsSynchronized
            End Get
        End Property

        Public ReadOnly Property Keys() As ICollection(Of String) Implements IDictionary(Of String, Object).Keys
            Get
                Return keysAdapter
            End Get
        End Property

        Private ReadOnly Property IDictionary_Values() As ICollection Implements IDictionary.Values
            Get
                Return valuesAdapter
            End Get
        End Property

        Private ReadOnly Property IDictionary_Keys() As ICollection Implements IDictionary.Keys
            Get
                Return keysAdapter
            End Get
        End Property

        Public ReadOnly Property Values() As ICollection(Of Object) Implements IDictionary(Of String, Object).Values
            Get
                Return valuesAdapter
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements IDictionary.IsReadOnly, IDictionary(Of String, Object).IsReadOnly
            Get
                Return sessionState.IsReadOnly
            End Get
        End Property

        Public ReadOnly Property IsFixedSize() As Boolean Implements IDictionary.IsFixedSize
            Get
                Return False
            End Get
        End Property
    End Class



    Public Class CollectionAdapter(Of T)
        Implements ICollection(Of T)
        Implements ICollection
        ReadOnly collection As ICollection

        Public Sub CopyTo(array As Array, index As Integer) Implements ICollection.CopyTo
            collection.CopyTo(array, index)
        End Sub

        Public ReadOnly Property SyncRoot() As Object Implements ICollection.SyncRoot
            Get
                Return collection.SyncRoot
            End Get
        End Property

        Public ReadOnly Property IsSynchronized() As Boolean Implements ICollection.IsSynchronized
            Get
                Return collection.IsSynchronized
            End Get
        End Property

        Public Sub New(collection As ICollection)
            Me.collection = collection
        End Sub

        Private Function IEnumerable_GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
            Return collection.Cast(Of T)().GetEnumerator()
        End Function

        Public Function GetEnumerator() As IEnumerator Implements ICollection.GetEnumerator
            Return collection.GetEnumerator()
        End Function

        Public Sub Add(item As T) Implements ICollection(Of T).Add
            Throw New NotSupportedException()
        End Sub

        Public Sub Clear() Implements ICollection(Of T).Clear
            Throw New NotSupportedException()
        End Sub

        Public Function Contains(item As T) As Boolean Implements ICollection(Of T).Contains
            Return collection.Cast(Of T)().Any(Function(x) Equals(x, item))
        End Function

        Public Sub CopyTo(array As T(), arrayIndex As Integer) Implements ICollection(Of T).CopyTo
            collection.CopyTo(array, arrayIndex)
        End Sub

        Public Function Remove(item As T) As Boolean Implements ICollection(Of T).Remove
            Throw New NotSupportedException()
        End Function

        Public ReadOnly Property Count() As Integer Implements ICollection.Count, ICollection(Of T).Count
            Get
                Return collection.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of T).IsReadOnly
            Get
                Return True
            End Get
        End Property
    End Class

    Public Class DictionaryEnumerator(Of TKey, TValue)
        Implements IDictionaryEnumerator
        Implements IDisposable

        ReadOnly enumerator As IEnumerator(Of KeyValuePair(Of TKey, TValue))

        Public Sub New(enumerator As IEnumerator(Of KeyValuePair(Of TKey, TValue)))
            Me.enumerator = enumerator
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            enumerator.Dispose()
        End Sub

        Public Function MoveNext() As Boolean Implements IDictionaryEnumerator.MoveNext
            Return enumerator.MoveNext()
        End Function

        Public Sub Reset() Implements IDictionaryEnumerator.Reset
            enumerator.Reset()
        End Sub

        Public ReadOnly Property Current() As Object Implements IDictionaryEnumerator.Current
            Get
                Return enumerator.Current
            End Get
        End Property

        Public ReadOnly Property Key() As Object Implements IDictionaryEnumerator.Key
            Get
                Return enumerator.Current.Key
            End Get
        End Property

        Public ReadOnly Property Value() As Object Implements IDictionaryEnumerator.Value
            Get
                Return enumerator.Current.Value
            End Get
        End Property

        Public ReadOnly Property Entry() As DictionaryEntry Implements IDictionaryEnumerator.Entry
            Get
                Return New DictionaryEntry(Key, Value)
            End Get
        End Property

    End Class

End Namespace

