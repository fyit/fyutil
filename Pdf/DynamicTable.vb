﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Pdf

    Public Class DynamicTable
        Public Options As Pdf.TableSettings

        Public Class Cell

            Public Enum eDataType
                Html
                Text
                Image
            End Enum

            Public DataType As eDataType
            Public Data As String
            Public Options As Pdf.CellSettings
            Public ImageBytes() As Byte
            Friend Content As iTextSharp.text.Paragraph = Nothing

            Public Sub New(pType As eDataType, pData As String, Optional pOptions As Pdf.CellSettings = Nothing)
                Me.DataType = pType
                Me.Data = pData
                Me.Options = pOptions
            End Sub

            Friend Sub New(pContent As iTextSharp.text.Paragraph)
                Content = pContent
            End Sub

        End Class

        Public Class Row
            Public Property Height As Single?
            Public Cells As New List(Of Cell)
        End Class

        Public Rows As New List(Of Row)
        Public ColumnWidths As List(Of Single)
    End Class

End Class
