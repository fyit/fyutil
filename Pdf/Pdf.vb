﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System
Imports FYUtil.ExtensionMethods
Imports iTextSharp.text
Imports iTextSharp.text.pdf

Public Class Pdf

    Public Enum eFieldType
        NotSet = 0
        TextBox = 1
        CheckBox = 2
        ComboBox = 3
        RadioButton = 4
        Other = 5
    End Enum

    Public Class FormField
        Public Property FieldName As String
        Public Property FieldType As eFieldType
    End Class

    Public Shared Sub FillForm(pData As IDictionary(Of String, String), pSourcePdfPath As String, pDestPdfPath As String)
        If (Not System.IO.File.Exists(pSourcePdfPath)) Then
            Throw New Exception(String.Format("File not found ({0})", pSourcePdfPath))
        End If

        Dim reader As New PdfReader(pSourcePdfPath)
        Dim stamper As New PdfStamper(reader, New System.IO.FileStream(pDestPdfPath, System.IO.FileMode.Create))
        Try
            If (reader.AcroForm Is Nothing) Then Throw New Exception(String.Format("Form fields not found in PDF file ({0})", pSourcePdfPath))
            stamper.FormFlattening = True

            Dim frm As AcroFields = stamper.AcroFields
            If (pData IsNot Nothing) Then
                For i As Int32 = 0 To frm.Fields.Count - 1
                    Dim fieldInfo = DirectCast(reader.AcroForm.Fields(i), iTextSharp.text.pdf.PRAcroForm.FieldInformation)
                    Dim tmpName As String = fieldInfo.Name

                    If (pData.ContainsKey(tmpName)) Then
                        Select Case frm.GetFieldType(tmpName)
                            Case iTextSharp.text.pdf.AcroFields.FIELD_TYPE_CHECKBOX
                                If {"", "Y", "1", "YES", "TRUE"}.Contains(pData.Item(tmpName).exToString().ToUpper()) Then
                                    frm.SetField(tmpName, "Yes")
                                End If
                            Case iTextSharp.text.pdf.AcroFields.FIELD_TYPE_RADIOBUTTON
                                'The value needs to match the "on value" in the field definition for the defined field
                                frm.SetField(tmpName, pData.Item(tmpName))
                            Case Else
                                frm.SetField(tmpName, pData.Item(tmpName))
                        End Select
                        'stamper.PartialFormFlattening(tmpName)
                    End If
                Next
            End If
        Finally
            stamper.Close()
            reader.Close()
        End Try
    End Sub

    Public Shared Function ExtractDataFields(pSourcePdf As String) As IEnumerable(Of FormField)
        If Not System.IO.File.Exists(pSourcePdf) Then Throw New Exception("Cannot locate PDF document '" & pSourcePdf & "'!")
        Dim ret = New List(Of FormField)

        Dim reader As New PdfReader(pSourcePdf)
        If reader.AcroForm IsNot Nothing Then
            For i As Int32 = 0 To reader.AcroForm.Fields.Count - 1
                Dim formField = DirectCast(reader.AcroForm.Fields(i), iTextSharp.text.pdf.PRAcroForm.FieldInformation)

                Dim fieldType As eFieldType
                Select Case reader.AcroFields.GetFieldType(formField.Name)
                    Case iTextSharp.text.pdf.AcroFields.FIELD_TYPE_TEXT : fieldType = eFieldType.TextBox
                    Case iTextSharp.text.pdf.AcroFields.FIELD_TYPE_CHECKBOX : fieldType = eFieldType.CheckBox
                    Case iTextSharp.text.pdf.AcroFields.FIELD_TYPE_COMBO : fieldType = eFieldType.ComboBox
                    Case iTextSharp.text.pdf.AcroFields.FIELD_TYPE_RADIOBUTTON : fieldType = eFieldType.RadioButton
                    Case Else : fieldType = eFieldType.Other
                End Select

                ret.Add(New FormField() With {.FieldName = formField.Name, .FieldType = fieldType})
            Next
        End If
        Return ret
    End Function

    Public Shared Sub CombineMultiple(pSourceFiles As IEnumerable(Of String), pDestFilePath As String)
        Using outputFileStream As New System.IO.FileStream(pDestFilePath, System.IO.FileMode.CreateNew)
            Dim reader As iTextSharp.text.pdf.PdfReader
            Dim numberOfPages As Int32
            Dim currentPageNumber As Int32
            Dim page As iTextSharp.text.pdf.PdfImportedPage
            Dim rotation As Int32
            Dim doc As New iTextSharp.text.Document
            Dim writer As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, outputFileStream)
            doc.Open()
            Try
                Dim cb As iTextSharp.text.pdf.PdfContentByte = writer.DirectContent

                For Each curFile As String In pSourceFiles
                    reader = New iTextSharp.text.pdf.PdfReader(curFile)
                    numberOfPages = reader.NumberOfPages
                    currentPageNumber = 0
                    Do While (currentPageNumber < numberOfPages)
                        currentPageNumber += 1
                        doc.SetPageSize(PageSize.LETTER)
                        doc.NewPage()
                        page = writer.GetImportedPage(reader, currentPageNumber)

                        rotation = reader.GetPageRotation(currentPageNumber)
                        If (rotation = 90) Or (rotation = 270) Then
                            cb.AddTemplate(page, 0, -1.0F, 1.0F, 0, 0, reader.GetPageSizeWithRotation(currentPageNumber).Height)
                        Else
                            cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
                        End If
                    Loop
                Next
            Finally
                doc.Close()
            End Try
            outputFileStream.Close()
        End Using
    End Sub

    Public Shared Sub ExtractPages(pSourcePdfPath As String, pOutputPdfPath As String, pPages As IEnumerable(Of Int32))
        If pSourcePdfPath = pOutputPdfPath Then
            Throw New Exception("For Extracting PDFs the source and output cannot be the same.")
        End If

        Dim document As New Document()
        'Dim reader As New PdfReader(pSourcePdfPath, New System.Text.ASCIIEncoding().GetBytes(pPassword))
        Dim reader As New PdfReader(pSourcePdfPath)
        Using memoryStream As New System.IO.MemoryStream()
            Dim writer As PdfWriter = PdfWriter.GetInstance(document, memoryStream)
            document.Open()
            document.AddDocListener(writer)
            If pPages.Min <= 0 OrElse pPages.Max > reader.NumberOfPages Then
                Throw New Exception("Page range (" & pPages.Min & " - " & pPages.Max & ") exceeds page count in PDF!")
            Else
                For Each page In pPages
                    document.SetPageSize(reader.GetPageSize(page))
                    document.NewPage()
                    Dim cb As PdfContentByte = writer.DirectContent
                    Dim pageImport As PdfImportedPage = writer.GetImportedPage(reader, page)
                    Dim rot As Integer = reader.GetPageRotation(page)
                    If rot = 90 OrElse rot = 270 Then
                        cb.AddTemplate(pageImport, 0, -1.0F, 1.0F, 0, 0, reader.GetPageSizeWithRotation(page).Height)
                    Else
                        cb.AddTemplate(pageImport, 1.0F, 0, 0, 1.0F, 0, 0)
                    End If
                Next
            End If

            reader.Close()
            document.Close()

            System.IO.File.WriteAllBytes(pOutputPdfPath, memoryStream.ToArray())
        End Using
    End Sub

    Public Shared Function PageCount(pSourcePdfPath As String) As Int32
        Dim reader As PdfReader = Nothing
        Try
            reader = New PdfReader(pSourcePdfPath)
            Return reader.NumberOfPages
        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
    End Function

    'Public Shared Sub AddWatermarkImage(ByVal sourceFile As String, ByVal pWatermark As iTextSharp.text.Image)
    '    Dim outputFile As String = "d:\temp\test.pdf"
    '    Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
    '    Dim stamper As iTextSharp.text.pdf.PdfStamper = Nothing
    '    Dim content As iTextSharp.text.pdf.PdfContentByte = Nothing
    '    Dim rect As iTextSharp.text.Rectangle = Nothing
    '    Dim X, Y As Single
    '    Dim pageCount As Integer = 0
    '    Try
    '        reader = New iTextSharp.text.pdf.PdfReader(sourceFile)
    '        rect = reader.GetPageSizeWithRotation(1)
    '        stamper = New iTextSharp.text.pdf.PdfStamper(reader, New System.IO.FileStream(outputFile, IO.FileMode.Create))
    '        If pWatermark.Width > rect.Width OrElse pWatermark.Height > rect.Height Then
    '            pWatermark.ScaleToFit(rect.Width, rect.Height)
    '            X = (rect.Width - pWatermark.ScaledWidth) / 2
    '            Y = (rect.Height - pWatermark.ScaledHeight) / 2
    '        Else
    '            X = (rect.Width - pWatermark.Width) / 2
    '            Y = (rect.Height - pWatermark.Height) / 2
    '        End If
    '        pWatermark.SetAbsolutePosition(X, Y)
    '        pageCount = reader.NumberOfPages()
    '        For i As Integer = 1 To pageCount
    '            content = stamper.GetUnderContent(i)
    '            content.AddImage(pWatermark)
    '            'content = stamper.GetOverContent(i)
    '            'content.AddImage(pWatermark)
    '        Next
    '        stamper.Close()
    '        reader.Close()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


    Public Shared Sub ConvertImageToPdf(pImagePath As String, pDestPdfPath As String)
        Using FS As New System.IO.FileStream(pDestPdfPath, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None)
            Using Doc As New Document(PageSize.LETTER)
                Using Writer = PdfWriter.GetInstance(Doc, FS)
                    Doc.Open()
                    Doc.NewPage()

                    Dim img = iTextSharp.text.Image.GetInstance(pImagePath)
                    'img.ScaleToFit(280.0F, 260.0F)
                    'img.SpacingBefore = 30.0F
                    'img.ScalePercent(24.0F)
                    'img.SpacingAfter = 1.0F
                    'img.Alignment = Element.ALIGN_CENTER
                    'img.SetAbsolutePosition(Doc.PageSize.Width - 36.0F - 72.0F, Doc.PageSize.Height - 36.0F - 216.6F)
                    Doc.Add(img)
                    Doc.Close()
                End Using
            End Using
        End Using
    End Sub

    'Public Shared Sub ResizePdf(pPdf As IO.Stream, Height As Single, Width As Single, outputPath As String)
    '    Dim resizeReader As New PdfReader(pPdf)

    '    Dim newRect As New Rectangle(0, 0, Convert.ToSingle(Width), Convert.ToSingle(Height))
    '    Dim doc As New Document(newRect)
    '    Document.Compress = True

    '    Dim resizeWriter As PdfWriter = PdfWriter.GetInstance(doc, New IO.FileStream(outputPath, IO.FileMode.Create))
    '    doc.Open()

    '    Dim cb As PdfContentByte = resizeWriter.DirectContent

    '    For pageNumber As Integer = 1 To resizeReader.NumberOfPages
    '        Dim page As PdfImportedPage = resizeWriter.GetImportedPage(resizeReader, pageNumber)
    '        cb.AddTemplate(page, newRect.Width / resizeReader.GetPageSize(pageNumber).Width, 0, 0, newRect.Height / resizeReader.GetPageSize(pageNumber).Height, 0, 0)
    '        doc.NewPage()
    '    Next

    '    doc.Close()
    '    doc = Nothing
    'End Sub






    ''' <summary>
    ''' Convert page settings from inch to point, and expanding preset page sizes
    ''' </summary>
    ''' <param name="pOrig"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function ConvertPageSettings(pOrig As PageSettings) As PageSettings
        Dim res As New PageSettings With {
            .Height = Nothing,
            .Width = InchToPoint(pOrig.Width),
            .MarginTop = InchToPoint(pOrig.MarginTop),
            .MarginBottom = InchToPoint(pOrig.MarginBottom),
            .MarginLeft = InchToPoint(pOrig.MarginLeft),
            .MarginRight = InchToPoint(pOrig.MarginRight)
        }
        If (pOrig.Height.HasValue) Then res.Height = InchToPoint(pOrig.Height.Value)

        If (res.PresetSize <> ePageSize.NotSet) Then
            Dim sizeRect = PageSize.LETTER
            Select Case res.PresetSize
                Case ePageSize.Letter : sizeRect = PageSize.LETTER
                Case ePageSize.Letter_Landscape : sizeRect = PageSize.LETTER_LANDSCAPE
            End Select
            res.Width = sizeRect.Width
            res.Height = sizeRect.Height
        End If

        Return res
    End Function

    Public Shared Function InchToPoint(pInches As Single) As Single
        Return pInches * 72
    End Function






    'old html stuff, didn't work well

    'Public Shared Sub HtmlToPdf(pHtml As String, pPageSettings As PageSettings, pDestPdfPath As String)
    '    'Dim pgSettings = ConvertPageSettings(pPageSettings)
    '    Dim pgSettings = pPageSettings

    '    Dim tbl As PdfPTable = HtmlToPdfTable(pHtml, pPageSettings)

    '    Dim effectiveHeight As Single
    '    If (pgSettings.Height.HasValue) Then
    '        effectiveHeight = pgSettings.Height.Value
    '    Else
    '        effectiveHeight = tbl.CalculateHeights() + pgSettings.MarginTop + pgSettings.MarginBottom
    '    End If

    '    Using doc As New Document(New Rectangle(pgSettings.Width, effectiveHeight), pgSettings.MarginLeft, pgSettings.MarginRight, pgSettings.MarginTop, pgSettings.MarginBottom)
    '        Using writer = PdfWriter.GetInstance(doc, New System.IO.FileStream(pDestPdfPath, System.IO.FileMode.Create))
    '            doc.Open()
    '            doc.Add(tbl)
    '            doc.Close()
    '        End Using
    '    End Using
    'End Sub

    'Friend Shared Function HtmlToPdfTable(pHtml As String, pPageSettings As PageSettings) As PdfPTable
    '    Dim tbl As New PdfPTable(1)
    '    tbl.WidthPercentage = 100
    '    tbl.SetTotalWidth({(pPageSettings.Width - pPageSettings.MarginLeft - pPageSettings.MarginRight)})
    '    Dim cell As New PdfPCell()
    '    cell.Border = 0
    '    cell.Padding = 0

    '    'this uses the deprecated HTMLWorker class
    '    'Dim htmElems = html.simpleparser.HTMLWorker.ParseToList(New IO.StringReader(pHtml), Nothing)
    '    'For Each elem In htmElems
    '    '    cell.AddElement(elem)
    '    'Next

    '    Dim htmElems As New ElementHandlerToList()
    '    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(htmElems, New System.IO.StringReader(pHtml))
    '    For Each elem In htmElems.Elements
    '        cell.AddElement(elem)
    '    Next

    '    tbl.AddCell(cell)
    '    Return tbl
    'End Function

    'Private Class ElementHandlerToList
    '    Implements iTextSharp.tool.xml.IElementHandler

    '    Public Elements As New List(Of IElement)

    '    Public Sub Add(w As iTextSharp.tool.xml.IWritable) Implements iTextSharp.tool.xml.IElementHandler.Add
    '        If (TypeOf w Is iTextSharp.tool.xml.pipeline.WritableElement) Then
    '            Elements.AddRange(DirectCast(w, iTextSharp.tool.xml.pipeline.WritableElement).Elements())
    '        End If
    '    End Sub

    'End Class

End Class

