﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.pdf.parser

Public Class DynamicPdf
    Implements IDisposable

    Private _outputFilePath As String
    Private _usedHeight As Single

    Private _sections As New List(Of iTextSharp.text.IElement)

    Private _fixedPositionText As New List(Of FixedPositionTextInfo)
    Private Class FixedPositionTextInfo
        Public Property Text As String
        Public Property FixedPostionInfo As Pdf.FixedPosition
        Public Property FontSize As Int32
        Public Property Font As iTextSharp.text.Font
    End Class

    Public Property PageSettings As Pdf.PageSettings

    Public Sub New(pDestPdfPath As String, pPageSettings As Pdf.PageSettings)
        _outputFilePath = pDestPdfPath
        Me.PageSettings = pPageSettings
    End Sub

    Public Sub AddHorizontalLine(Optional pLineOptions As Pdf.LineSettings = Nothing)
        Dim tbl As New iTextSharp.text.pdf.PdfPTable(3)
        tbl.WidthPercentage = 100

        Dim cell As New iTextSharp.text.pdf.PdfPCell()

        If pLineOptions IsNot Nothing Then
            If pLineOptions.Height.HasValue Then cell.BorderWidthBottom = pLineOptions.Height.Value
            If pLineOptions.SpacingBefore.HasValue Then tbl.SpacingBefore = pLineOptions.SpacingBefore.Value
            If pLineOptions.SpacingAfter.HasValue Then tbl.SpacingAfter = pLineOptions.SpacingAfter.Value
        End If

        Dim row As New PdfPRow({New PdfPCell() With {.Border = 0, .Padding = 0},
                                cell,
                                New PdfPCell() With {.Border = 0, .Padding = 0}})
        tbl.Rows.Add(row)

        'Set the widths
        Dim leftWidth As Single = If(pLineOptions IsNot Nothing AndAlso pLineOptions.LeftOffest.HasValue, pLineOptions.LeftOffest.Value, 0)
        Dim rightWidth As Single = If(pLineOptions IsNot Nothing AndAlso pLineOptions.RightOffset.HasValue, pLineOptions.RightOffset.Value, 0)
        Dim centerWidth As Single = If(pLineOptions IsNot Nothing AndAlso pLineOptions.Width.HasValue, pLineOptions.Width.Value, 100) - (leftWidth + rightWidth)
        tbl.SetWidths({leftWidth, centerWidth, rightWidth})

        _usedHeight += tbl.CalculateHeights() + tbl.SpacingBefore + tbl.SpacingAfter
        _sections.Add(tbl)
    End Sub

    Public Sub AddImage(pImagePath As String, Optional pImageSettings As Pdf.ImageSettings = Nothing)
        If Not System.IO.File.Exists(pImagePath) Then Throw New Exception("The image does not exist: " & pImagePath)

        Dim img = iTextSharp.text.Image.GetInstance(pImagePath)

        If pImageSettings IsNot Nothing Then
            If pImageSettings.Alignment.HasValue Then
                Select Case pImageSettings.Alignment
                    Case Pdf.eHorizontalAlignment.Left : img.Alignment = Element.ALIGN_LEFT
                    Case Pdf.eHorizontalAlignment.Center : img.Alignment = Element.ALIGN_CENTER
                    Case Pdf.eHorizontalAlignment.Right : img.Alignment = Element.ALIGN_RIGHT
                End Select
            End If

            If pImageSettings.ScalePercent.HasValue Then img.ScalePercent(pImageSettings.ScalePercent.Value)
            If pImageSettings.ScaleHeight.HasValue AndAlso pImageSettings.ScaleWidth.HasValue Then
                img.ScaleAbsolute(pImageSettings.ScaleWidth.Value, pImageSettings.ScaleHeight.Value)
            ElseIf pImageSettings.ScaleHeight.HasValue Then
                Dim sw = (pImageSettings.ScaleHeight.Value / img.Height) * img.Width
                img.ScaleAbsolute(sw, pImageSettings.ScaleHeight.Value)
            ElseIf pImageSettings.ScaleWidth.HasValue Then
                Dim sh = (pImageSettings.ScaleWidth.Value / img.Width) * img.Height
                img.ScaleAbsolute(pImageSettings.ScaleWidth.Value, sh)
            End If

            'If pImageSettings.SpacingBefore.HasValue Then img.SpacingBefore = pImageSettings.SpacingBefore.Value
            'If pImageSettings.SpacingAfter.HasValue Then img.SpacingAfter = pImageSettings.SpacingAfter.Value

            If pImageSettings.FixedPositionInfo IsNot Nothing Then
                img.SetAbsolutePosition(pImageSettings.FixedPositionInfo.X, pImageSettings.FixedPositionInfo.Y)
            End If
        End If

        If pImageSettings.FixedPositionInfo Is Nothing Then
            _usedHeight += img.ScaledHeight
        End If
        _sections.Add(img)
    End Sub

    Public Sub AddFixedPositionText(pText As String, pFixedPosition As Pdf.FixedPosition, Optional pFontSettings As Pdf.FontSettings = Nothing)
        If pFixedPosition Is Nothing Then Throw New Exception("Fixed position functions require position information.")

        Dim fnt As Font = Nothing
        If pFontSettings IsNot Nothing AndAlso (Not String.IsNullOrWhiteSpace(pFontSettings.FontPath) OrElse Not String.IsNullOrWhiteSpace(pFontSettings.FontFamily)) Then
            If Not String.IsNullOrWhiteSpace(pFontSettings.FontPath) Then fnt = New Font(BaseFont.CreateFont(pFontSettings.FontPath, BaseFont.CP1252, BaseFont.EMBEDDED))
            If Not String.IsNullOrWhiteSpace(pFontSettings.FontFamily) Then fnt = FontFactory.GetFont(pFontSettings.FontFamily)
        End If
        Dim fntSize As Int32 = 10
        If pFontSettings IsNot Nothing Then fntSize = CInt(pFontSettings.FontSize.Value)

        _fixedPositionText.Add(New FixedPositionTextInfo() With {.Text = pText,
                                                                 .FixedPostionInfo = pFixedPosition,
                                                                 .FontSize = fntSize,
                                                                 .Font = fnt})
    End Sub

    Public Sub AddText(pText As String, Optional pTextSettings As Pdf.TextSettings = Nothing)
        Dim tbl As New PdfPTable(1)
        tbl.WidthPercentage = 100
        tbl.SetTotalWidth({(PageSettings.Width - PageSettings.MarginLeft - PageSettings.MarginRight)})
        Dim pgh As Paragraph
        If pTextSettings IsNot Nothing AndAlso (Not String.IsNullOrWhiteSpace(pTextSettings.FontPath) OrElse Not String.IsNullOrWhiteSpace(pTextSettings.FontFamily)) Then
            Dim fnt As Font = Nothing
            If Not String.IsNullOrWhiteSpace(pTextSettings.FontPath) Then fnt = New Font(BaseFont.CreateFont(pTextSettings.FontPath, BaseFont.CP1252, BaseFont.EMBEDDED))
            If Not String.IsNullOrWhiteSpace(pTextSettings.FontFamily) Then fnt = FontFactory.GetFont(pTextSettings.FontFamily)
            pgh = New Paragraph(pText, fnt)
        Else
            pgh = New Paragraph(pText)
        End If
        If pTextSettings IsNot Nothing AndAlso pTextSettings.FontSize.HasValue Then pgh.Font.Size = pTextSettings.FontSize.Value

        Dim cell As New PdfPCell(pgh)

        If pTextSettings IsNot Nothing Then
            With pTextSettings
                If .FontSize.HasValue Then pgh.Font.Size = pTextSettings.FontSize.Value
                If .SpacingBefore.HasValue Then tbl.SpacingBefore = .SpacingBefore.Value : _usedHeight += .SpacingBefore.Value
                If .SpacingAfter.HasValue Then tbl.SpacingAfter = .SpacingAfter.Value : _usedHeight += .SpacingAfter.Value
                If .HorizontalAlignment.HasValue Then
                    Select Case .HorizontalAlignment
                        Case Pdf.eHorizontalAlignment.Left : cell.HorizontalAlignment = Element.ALIGN_LEFT
                        Case Pdf.eHorizontalAlignment.Center : cell.HorizontalAlignment = Element.ALIGN_CENTER
                        Case Pdf.eHorizontalAlignment.Right : cell.HorizontalAlignment = Element.ALIGN_RIGHT
                    End Select
                End If
            End With
        End If

        cell.Border = 0
        cell.Padding = 0

        tbl.AddCell(cell)
        _usedHeight += tbl.CalculateHeights()
        _sections.Add(tbl)
    End Sub

    Public Sub AddTable(pTable As Pdf.DynamicTable)
        Dim maxCells = (From item In pTable.Rows Select item.Cells.Count).Max

        Dim tbl As PdfPTable
        If pTable.ColumnWidths IsNot Nothing Then
            If (pTable.ColumnWidths.Count < maxCells) Then Throw New Exception("AddTable: Invalid column widths")
            tbl = New PdfPTable(pTable.ColumnWidths.ToArray)
        Else
            tbl = New PdfPTable(maxCells)
        End If

        If pTable.Options IsNot Nothing Then
            If pTable.Options.SpacingBefore.HasValue Then tbl.SpacingBefore = pTable.Options.SpacingBefore.Value
            If pTable.Options.SpacingAfter.HasValue Then tbl.SpacingAfter = pTable.Options.SpacingAfter.Value
        End If

        tbl.WidthPercentage = 100
        tbl.TotalWidth = (PageSettings.Width - PageSettings.MarginLeft - PageSettings.MarginRight)

        For Each row In pTable.Rows
            Dim pdfCells As New List(Of PdfPCell)
            For Each cell In row.Cells
                Dim pdfCell As PdfPCell = Nothing

                If (cell.Content Is Nothing) Then
                    Select Case cell.DataType
                        Case Pdf.DynamicTable.Cell.eDataType.Text
                            Dim pgh As Paragraph

                            If cell.Options IsNot Nothing AndAlso (Not String.IsNullOrWhiteSpace(cell.Options.FontPath) OrElse Not String.IsNullOrWhiteSpace(cell.Options.FontFamily)) Then
                                Dim fnt As Font = Nothing
                                If Not String.IsNullOrWhiteSpace(cell.Options.FontPath) Then fnt = New Font(BaseFont.CreateFont(cell.Options.FontPath, BaseFont.CP1252, BaseFont.EMBEDDED))
                                If Not String.IsNullOrWhiteSpace(cell.Options.FontFamily) Then fnt = FontFactory.GetFont(cell.Options.FontFamily)
                                pgh = New Paragraph(cell.Data, fnt)
                            Else
                                pgh = New Paragraph(cell.Data)
                            End If

                            If cell.Options IsNot Nothing AndAlso cell.Options.FontSize.HasValue Then pgh.Font.Size = cell.Options.FontSize.Value
                            pdfCell = New PdfPCell(pgh)
                        Case Pdf.DynamicTable.Cell.eDataType.Html
                            pdfCell = New PdfPCell(New Paragraph(cell.Data))
                        Case Pdf.DynamicTable.Cell.eDataType.Image
                            pdfCell = New PdfPCell(New iTextSharp.text.ImgWMF(cell.ImageBytes))
                    End Select
                Else
                    pdfCell = New PdfPCell(cell.Content)
                    pdfCell.HorizontalAlignment = cell.Content.Alignment
                End If

                If row.Height.HasValue Then pdfCell.FixedHeight = row.Height.Value

                If pTable.Options IsNot Nothing Then
                    If pTable.Options.Border.HasValue Then
                        Dim brd = pTable.Options.Border.Value
                        pdfCell.BorderWidthTop = brd
                        pdfCell.BorderWidthBottom = brd
                        pdfCell.BorderWidthLeft = brd
                        pdfCell.BorderWidthRight = brd
                    End If
                    If pTable.Options.Padding.HasValue Then pdfCell.Padding = pTable.Options.Padding.Value
                End If

                If cell.Options IsNot Nothing Then
                    If cell.Options.Border.HasValue Then
                        Dim brd = cell.Options.Border.Value
                        pdfCell.BorderWidthTop = brd
                        pdfCell.BorderWidthBottom = brd
                        pdfCell.BorderWidthLeft = brd
                        pdfCell.BorderWidthRight = brd
                    End If
                    If cell.Options.BorderTop.HasValue Then pdfCell.BorderWidthTop = cell.Options.BorderTop.Value
                    If cell.Options.BorderBottom.HasValue Then pdfCell.BorderWidthBottom = cell.Options.BorderBottom.Value
                    If cell.Options.BorderLeft.HasValue Then pdfCell.BorderWidthLeft = cell.Options.BorderLeft.Value
                    If cell.Options.BorderRight.HasValue Then pdfCell.BorderWidthRight = cell.Options.BorderRight.Value
                    If cell.Options.Padding.HasValue Then pdfCell.Padding = cell.Options.Padding.Value

                    If cell.Options.HorizontalAlignment.HasValue Then
                        Select Case cell.Options.HorizontalAlignment
                            Case Pdf.eHorizontalAlignment.Left : pdfCell.HorizontalAlignment = Element.ALIGN_LEFT
                            Case Pdf.eHorizontalAlignment.Center : pdfCell.HorizontalAlignment = Element.ALIGN_CENTER
                            Case Pdf.eHorizontalAlignment.Right : pdfCell.HorizontalAlignment = Element.ALIGN_RIGHT
                        End Select
                    End If
                    If cell.Options.VerticalAlignment.HasValue Then
                        Select Case cell.Options.VerticalAlignment
                            Case Pdf.eVerticalAlignment.Bottom : pdfCell.VerticalAlignment = Element.ALIGN_BOTTOM
                            Case Pdf.eVerticalAlignment.Middle : pdfCell.VerticalAlignment = Element.ALIGN_MIDDLE
                            Case Pdf.eVerticalAlignment.Top : pdfCell.VerticalAlignment = Element.ALIGN_TOP
                        End Select
                    End If
                End If

                pdfCells.Add(pdfCell)
            Next
            While (pdfCells.Count < tbl.NumberOfColumns)
                'if all the cells aren't filled, the row is blank
                Dim pdfCell = New PdfPCell(New Paragraph(" "))
                If row.Height.HasValue Then pdfCell.FixedHeight = row.Height.Value
                If pTable.Options IsNot Nothing Then
                    If pTable.Options.Border.HasValue Then
                        Dim brd = pTable.Options.Border.Value
                        pdfCell.BorderWidthTop = brd
                        pdfCell.BorderWidthBottom = brd
                        pdfCell.BorderWidthLeft = brd
                        pdfCell.BorderWidthRight = brd
                    End If
                    If pTable.Options.Padding.HasValue Then pdfCell.Padding = pTable.Options.Padding.Value
                End If
                pdfCells.Add(pdfCell)
            End While
            tbl.Rows.Add(New PdfPRow(pdfCells.ToArray))
        Next

        _usedHeight += tbl.CalculateHeights() + tbl.SpacingBefore + tbl.SpacingAfter
        _sections.Add(tbl)
    End Sub

    Private Sub CloseDocument()
        Dim effectiveHeight As Single
        If (PageSettings.Height.HasValue) Then
            effectiveHeight = PageSettings.Height.Value
        Else
            effectiveHeight = _usedHeight + PageSettings.MarginTop + PageSettings.MarginBottom
        End If

        Dim doc = New Document(New Rectangle(PageSettings.Width, effectiveHeight, 0), PageSettings.MarginLeft, PageSettings.MarginRight, PageSettings.MarginTop, PageSettings.MarginBottom)
        Dim writer = PdfWriter.GetInstance(doc, New System.IO.FileStream(_outputFilePath, System.IO.FileMode.Create))

        'this should default to "Actual Size"
        writer.AddViewerPreference(PdfName.PRINTSCALING, PdfName.NONE)

        doc.Open()

        For Each item In _fixedPositionText
            Dim content = writer.DirectContent()
            content.BeginText()
            If item.Font IsNot Nothing Then
                content.SetFontAndSize(item.Font.BaseFont, item.FontSize)
            Else
                Dim bf = BaseFont.CreateFont(iTextSharp.text.FontFactory.COURIER, BaseFont.CP1250, False)
                content.SetFontAndSize(bf, item.FontSize)
            End If
            content.SetTextMatrix(item.FixedPostionInfo.X, effectiveHeight - item.FixedPostionInfo.Y)
            content.ShowText(item.Text)
            content.EndText()
        Next

        For Each item In _sections
            doc.Add(item)
        Next

        'prevent the "no pages" error
        If (doc.PageNumber = 0) Then
            doc.Add(New Chunk(" "c))
        End If

        doc.Close()
        writer.Close()

    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).

                CloseDocument()
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
