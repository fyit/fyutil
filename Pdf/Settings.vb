﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Pdf

    Public Enum eHorizontalAlignment
        Left
        Center
        Right
    End Enum

    Public Enum eVerticalAlignment
        Top
        Middle
        Bottom
    End Enum


#Region "Page Settings"

    Public Enum ePageSize
        NotSet
        Letter
        Letter_Landscape
    End Enum

    Public Class PageSettings
        Public Property PresetSize As ePageSize
        Public Property Width As Single
        Public Property Height As Single?
        Public Property MarginRight As Single = 0.5
        Public Property MarginLeft As Single = 0.5
        Public Property MarginTop As Single = 0.5
        Public Property MarginBottom As Single = 0.5
    End Class

#End Region

#Region "Dynamic Table"

    Public Class FontSettings
        Public Property FontPath As String
        Public Property FontFamily As String
        Public Property FontSize As Single?
    End Class

    Public Class TableSettings
        Public Property HorizontalAlignment As eHorizontalAlignment?
        Public Property VerticalAlignment As eVerticalAlignment?
        Public Property SpacingBefore As Single?
        Public Property SpacingAfter As Single?
        Public Property Border As Single?
        Public Property Padding As Single?
    End Class

    Public Class LineSettings
        Public Property Width As Single?
        Public Property Height As Single?
        Public Property SpacingBefore As Single?
        Public Property SpacingAfter As Single?
        Public Property RightOffset As Single?
        Public Property LeftOffest As Single?
    End Class

    Public Class CellSettings
        Inherits FontSettings

        Public Property HorizontalAlignment As eHorizontalAlignment?
        Public Property VerticalAlignment As eVerticalAlignment?
        Public Property Border As Single?
        Public Property BorderTop As Single?
        Public Property BorderBottom As Single?
        Public Property BorderLeft As Single?
        Public Property BorderRight As Single?
        Public Property Padding As Single?
    End Class

    Public Class TextSettings
        Inherits FontSettings

        Public Property SpacingBefore As Single?
        Public Property SpacingAfter As Single?
        Public Property HorizontalAlignment As eHorizontalAlignment?
    End Class

    Public Class ImageSettings
        <Obsolete("This is not currently being used by iTextSharpPDF and is ignored.", True)>
        Public Property SpacingBefore As Single?
        <Obsolete("This is not currently being used by iTextSharpPDF and is ignored.", True)>
        Public Property SpacingAfter As Single?
        Public Property Alignment As eHorizontalAlignment?

        Public Property ScalePercent As Single?
        Public Property ScaleHeight As Single?
        Public Property ScaleWidth As Single?

        Public Property FixedPositionInfo As Pdf.FixedPosition
    End Class

    Public Class FixedPosition
        Public Property X As Int32
        Public Property Y As Int32
    End Class

#End Region

End Class

