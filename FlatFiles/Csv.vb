﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class CsvFile


#Region "Dynamic load of dependent assembly"

    Shared Sub New()
        AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf AssemblyResolve_LumenWorks_Framework_IO
    End Sub

    Private Shared Function AssemblyResolve_LumenWorks_Framework_IO(sender As Object, args As ResolveEventArgs) As System.Reflection.Assembly
        If (args.Name.StartsWith("LumenWorks.Framework.IO", StringComparison.InvariantCultureIgnoreCase)) Then
            Dim rm As New Resources.ResourceManager("FYUtil.FlatFiles.EmbeddedLibraries", GetType(FYUtil.FlatFiles.CsvFile).Assembly)
            Dim bytes As Byte() = DirectCast(rm.GetObject("LumenWorks_Framework_IO"), Byte())
            Return System.Reflection.Assembly.Load(bytes)
        End If
        Return Nothing
    End Function

#End Region


    Public Class ColumnDefinition
        Public Property Name As String
        Public Property DataType As Type
        Public Property AllowEmpty As Boolean
        Public Property DefaultValue As Object
    End Class

    Public Enum ColumnIdentificationType
        ByName
        ByPosition
    End Enum


    Private _ColumnPositions As New List(Of String)

    Public Property ColumnDefinitions As New Dictionary(Of String, ColumnDefinition)
    Public Property MissingColumns As New Dictionary(Of String, ColumnDefinition)
    Public Property ColumnIdentification As ColumnIdentificationType
    Public Property AllowUndefinedColumns As Boolean
    Public Property AllowMissingColumns As Boolean
    Public Property HasHeader As Boolean
    Public Property TrimValues As Boolean

    Public Sub New()
        ColumnIdentification = ColumnIdentificationType.ByName
        AllowUndefinedColumns = True
        AllowMissingColumns = False
        HasHeader = True
        TrimValues = True
    End Sub

    Private Function DefaultDefaultValue(dataType As Type) As Object
        If dataType = GetType(String) Then
            Return ""
        ElseIf dataType = GetType(Integer) Then
            Return 0
        ElseIf dataType = GetType(Double) Then
            Return 0.0
        ElseIf dataType = GetType(Decimal) Then
            Return 0.0
        ElseIf dataType = GetType(Boolean) Then
            Return False
        ElseIf dataType = GetType(DateTime) Then
            Return DateTime.MinValue
        Else
            Return DBNull.Value
        End If
    End Function

    Public Sub AddColumnDefinition(name As String, dataType As Type, allowEmpty As Boolean, defaultValue As Object)
        Dim def As New ColumnDefinition With {
            .Name = name,
            .DataType = dataType,
            .AllowEmpty = allowEmpty,
            .DefaultValue = defaultValue
        }
        AddColumnDefinition(def)
    End Sub

    Public Sub AddColumnDefinition(name As String, dataType As Type, allowEmpty As Boolean)
        AddColumnDefinition(name, dataType, allowEmpty, DefaultDefaultValue(dataType))
    End Sub

    Public Sub AddColumnDefinition(name As String, dataType As Type)
        AddColumnDefinition(name, dataType, True)
    End Sub

    Public Sub AddColumnDefinition(name As String)
        AddColumnDefinition(name, GetType(String), True)
    End Sub

    Public Sub AddColumnDefinition(pDef As ColumnDefinition)
        If (pDef.DefaultValue Is Nothing) Then pDef.DefaultValue = DBNull.Value
        If (Not {GetType(String), GetType(Int32), GetType(Double), GetType(Decimal), GetType(Boolean), GetType(DateTime)}.Contains(pDef.DataType)) Then
            Throw New Exception(String.Format("Invalid column type ({0}, {1})", pDef.Name, pDef.DataType.Name))
        End If
        Dim key As String = pDef.Name.ToLower()
        ColumnDefinitions.Add(key, pDef)
        _ColumnPositions.Add(key)
    End Sub

    Public Sub AddColumnDefinition(pDataColumn As DataColumn)
        Dim colDef As New ColumnDefinition
        colDef.DataType = pDataColumn.DataType
        colDef.Name = pDataColumn.ColumnName
        colDef.DefaultValue = pDataColumn.DefaultValue
        'colDef.AllowEmpty = col.AllowDBNull 'not quite correct
        colDef.AllowEmpty = True
        AddColumnDefinition(colDef)
    End Sub

    Public Sub AddColumnDefinitions(pDataTable As DataTable)
        For Each col As DataColumn In pDataTable.Columns
            AddColumnDefinition(col)
        Next
    End Sub

    ''' <summary>
    ''' Convert a CSV file to a datatable
    ''' </summary>
    ''' <param name="pStream"></param>
    ''' <returns></returns>
    Public Function ToDataTable(pStream As IO.TextReader) As DataTable
        Dim res As New DataTable("result")
        Using csv As New LumenWorks.Framework.IO.Csv.CsvReader(pStream, HasHeader)

            Dim colKeys = GetColumnKeys(csv)
            For Each colKey In colKeys
                res.Columns.Add(ColumnDefinitions(colKey).Name, ColumnDefinitions(colKey).DataType)
            Next
            FillDataTableImpl(csv, colKeys, res)

        End Using

        Return res
    End Function

    Public Function ToDataTable(pFilename As String) As DataTable
        Using sr As New IO.StreamReader(pFilename)
            Return ToDataTable(sr)
        End Using
    End Function


    ''' <summary>
    ''' Fill a datatable with CSV data
    ''' </summary>
    ''' <param name="pStream"></param>
    Public Sub FillDataTable(pStream As IO.TextReader, pResult As DataTable)
        Using csv As New LumenWorks.Framework.IO.Csv.CsvReader(pStream, HasHeader)

            Dim colKeys = GetColumnKeys(csv)
            For Each colKey In colKeys
                If (Not pResult.Columns.Contains(colKey)) Then
                    pResult.Columns.Add(ColumnDefinitions(colKey).Name, ColumnDefinitions(colKey).DataType)
                End If
            Next
            FillDataTableImpl(csv, colKeys, pResult)

        End Using
    End Sub

    Public Sub FillDataTable(pFilename As String, pResult As DataTable)
        Using sr As New IO.StreamReader(pFilename)
            FillDataTable(sr, pResult)
        End Using
    End Sub


    Private Function GetColumnKeys(pCsv As LumenWorks.Framework.IO.Csv.CsvReader) As List(Of String)
        Dim headers As List(Of String)
        If (ColumnIdentification = ColumnIdentificationType.ByPosition) Then
            headers = New List(Of String)(_ColumnPositions)
        ElseIf ((ColumnIdentification = ColumnIdentificationType.ByName) AndAlso HasHeader) Then
            Dim temp As String() = pCsv.GetFieldHeaders()
            headers = New List(Of String)(temp)
        Else
            Throw New Exception("CsvFile : Unable to identify columns")
        End If

        Dim fieldCount As Integer = pCsv.FieldCount
        Dim colKeys As New List(Of String)()
        Dim unusedCols As New Dictionary(Of String, ColumnDefinition)(ColumnDefinitions)
        For i As Integer = 0 To fieldCount - 1
            Dim colKey As String
            If (ColumnIdentification = ColumnIdentificationType.ByName) Then
                colKey = headers(i).Trim().ToLower()
            Else
                If (i < _ColumnPositions.Count) Then
                    colKey = _ColumnPositions(i)
                Else
                    colKey = "xxxUNDEFINEDxxx"
                End If
            End If

            If (unusedCols.ContainsKey(colKey)) Then
                unusedCols.Remove(colKey)
            ElseIf (AllowUndefinedColumns) Then
                Dim colName = String.Format("Column{0}", i)
                If (HasHeader) Then
                    colName = headers(i).Trim()
                End If
                AddColumnDefinition(colName)
                colKey = colName.ToLower()
            Else
                Throw New Exception(String.Format("CsvFile : Invalid column ({0})", colKey))
            End If

            colKeys.Add(colKey)
        Next
        If (unusedCols.Count > 0) Then
            MissingColumns = unusedCols
            If (AllowMissingColumns) Then
                For Each col In MissingColumns
                    If (ColumnDefinitions.ContainsKey(col.Key)) Then ColumnDefinitions.Remove(col.Key)
                Next
            Else
                Dim colList = String.Join(",", (From c In unusedCols Select c.Value.Name))
                Throw New Exception(String.Format("CsvFile : Missing columns ({0})", colList))
            End If
        End If

        Return colKeys
    End Function

    Private Sub FillDataTableImpl(pCsv As LumenWorks.Framework.IO.Csv.CsvReader, pColumnKeys As List(Of String), pResult As DataTable)
        Dim fieldCount As Integer = pCsv.FieldCount
        Dim rowNum = 1
        While pCsv.ReadNextRecord()
            Dim newRow As DataRow = pResult.NewRow()
            For colNum As Integer = 0 To fieldCount - 1
                Dim colDef As ColumnDefinition = ColumnDefinitions(pColumnKeys(colNum))
                Dim colName As String = colDef.Name
                Dim colDat As String = pCsv(colNum)
                Try
                    If TrimValues Then
                        colDat = colDat.Trim()
                    End If

                    If colDat.Equals(String.Empty) Then
                        If colDef.AllowEmpty Then
                            newRow(colName) = colDef.DefaultValue
                        Else
                            Throw New Exception("Empty not allowed")
                        End If
                    Else
                        If colDef.DataType = GetType(String) Then
                            'no validation
                            newRow(colName) = colDat
                        ElseIf colDef.DataType = GetType(Integer) Then
                            Dim dat As Integer = Convert.ToInt32(colDat)
                            newRow(colName) = dat
                        ElseIf colDef.DataType = GetType(Double) Then
                            Dim dat As Double = Convert.ToDouble(colDat)
                            newRow(colName) = dat
                        ElseIf colDef.DataType = GetType(Decimal) Then
                            Dim dat As Decimal = Convert.ToDecimal(colDat)
                            newRow(colName) = dat
                        ElseIf colDef.DataType = GetType(Boolean) Then
                            Dim dat As Boolean = Convert.ToBoolean(colDat)
                            newRow(colName) = dat
                        ElseIf colDef.DataType = GetType(DateTime) Then
                            Dim dat As DateTime = Convert.ToDateTime(colDat)
                            newRow(colName) = dat
                        Else
                            Throw New Exception("Invalid column type")
                        End If
                    End If
                Catch ex As Exception
                    Throw New Exception(String.Format("{0}, row {1}, column {2} ({3})", ex.Message, rowNum, colNum, colDef.Name))
                End Try
            Next
            pResult.Rows.Add(newRow)
            rowNum += 1
        End While
    End Sub


    ''' <summary>
    ''' Converts the passed in data table to a CSV-style string.
    ''' </summary>
    ''' <returns>Resulting CSV-style string</returns>
    Public Shared Function DataTableToCsv(pTable As DataTable, Optional pIncludeHeader As Boolean = True, Optional pColumnList As IEnumerable(Of String) = Nothing) As String
        Dim delimiter = ""
        Dim result As New Text.StringBuilder()

        Dim allColumns As New List(Of String)
        For Each column As DataColumn In pTable.Columns
            allColumns.Add(column.ColumnName)
        Next

        If (pColumnList Is Nothing) Then
            pColumnList = allColumns
        Else
            If (Not allColumns.exContainsAll(pColumnList)) Then Throw New Exception("DataTableToCsv: Invalid column list")
        End If

        If (pIncludeHeader) Then
            For Each colName In pColumnList
                result.Append(delimiter)
                result.Append(colName)
                delimiter = ","
            Next
            result.AppendLine()
        End If

        For Each row As DataRow In pTable.Rows
            delimiter = ""
            For Each colName In pColumnList
                result.Append(delimiter)
                AppendCsvFieldData(result, row.Item(colName))
                delimiter = ","
            Next
            result.AppendLine()
        Next

        Return result.ToString()
    End Function

    Public Shared Sub DataTableToCsvFile(pFilename As String, pTable As DataTable, Optional pIncludeHeader As Boolean = True, Optional pColumnList As IEnumerable(Of String) = Nothing)
        Dim data As String = DataTableToCsv(pTable, pIncludeHeader, pColumnList)
        Using fil As New IO.StreamWriter(pFilename)
            fil.Write(data)
            fil.Close()
        End Using
    End Sub


    ''' <summary>
    ''' Fill a predefined DataTable with the data from a CSV file. Must have header row.
    ''' </summary>
    Public Shared Sub CsvFillDataTable(pStream As IO.TextReader, pResult As DataTable)
        Dim csvFile As New CsvFile With {
            .HasHeader = True,
            .TrimValues = True,
            .AllowUndefinedColumns = False,
            .AllowMissingColumns = False,
            .ColumnIdentification = ColumnIdentificationType.ByName
        }
        csvFile.AddColumnDefinitions(pResult)

        csvFile.FillDataTable(pStream, pResult)
    End Sub

    ''' <summary>
    ''' Fill a predefined DataTable with the data from a CSV file. Must have header row.
    ''' </summary>
    Public Shared Sub CsvFileFillDataTable(pFileName As String, pResult As DataTable)
        Using sr As New IO.StreamReader(pFileName)
            CsvFillDataTable(sr, pResult)
        End Using
    End Sub


    ''' <summary>
    ''' Fill a predefined DataTable with the data from a CSV file. Must have header row. Allows missing columns, and returns the list of missing columns.
    ''' </summary>
    Public Shared Sub CsvFillDataTablePartial(pStream As IO.TextReader, pResult As DataTable, ByRef poMissingColumns As IList(Of DataColumn))
        Dim csvFile As New CsvFile With {
            .HasHeader = True,
            .TrimValues = True,
            .AllowUndefinedColumns = False,
            .AllowMissingColumns = True,
            .ColumnIdentification = ColumnIdentificationType.ByName
        }
        csvFile.AddColumnDefinitions(pResult)

        csvFile.FillDataTable(pStream, pResult)

        poMissingColumns = New List(Of DataColumn)
        For Each col In csvFile.MissingColumns
            poMissingColumns.Add(pResult.Columns.Item(col.Key))
        Next
    End Sub

    ''' <summary>
    ''' Fill a predefined DataTable with the data from a CSV file. Must have header row. Allows missing columns, and returns the list of missing columns.
    ''' </summary>
    Public Shared Sub CsvFileFillDataTablePartial(pFileName As String, pResult As DataTable, ByRef poMissingColumns As IList(Of DataColumn))
        Using sr As New IO.StreamReader(pFileName)
            CsvFillDataTablePartial(sr, pResult, poMissingColumns)
        End Using
    End Sub


    ''' <summary>
    ''' Saves the datareader to a CSV file
    ''' </summary>
    Public Shared Sub DataReaderToCsvFile(data As IDataReader, includeHeader As Boolean, fileName As String, Optional delimiter As String = ",")
        Dim aLine As New Text.StringBuilder()

        Using fil As New IO.StreamWriter(fileName)
            If includeHeader Then
                For colNum As Integer = 0 To data.FieldCount - 1
                    If (colNum > 0) Then aLine.Append(delimiter)
                    aLine.Append(data.GetName(colNum))
                Next
                fil.WriteLine(aLine.ToString())
                aLine.Clear()
            End If

            While data.Read()
                For colNum As Integer = 0 To data.FieldCount - 1
                    If (colNum > 0) Then aLine.Append(delimiter)
                    AppendCsvFieldData(aLine, data.GetValue(colNum))
                Next
                fil.WriteLine(aLine.ToString())
                aLine.Clear()
            End While

            fil.Close()
        End Using
    End Sub

    Private Shared Sub AppendCsvFieldData(sb As Text.StringBuilder, fieldData As Object)
        If TypeOf fieldData Is System.DBNull Then
            'don't add anything
        Else
            sb.Append("""")

            If (TypeOf fieldData Is Byte()) Then
                sb.Append("0x")
                sb.Append(FYUtil.Utilities.ByteArrayToHexString(DirectCast(fieldData, Byte())))
            Else
                sb.Append(fieldData.ToString().Replace("""", """"""))
            End If

            sb.Append("""")
        End If
    End Sub

End Class
