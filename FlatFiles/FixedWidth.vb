﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 




Public Class FixedWidthFile
    Public Property ColumnWidths As List(Of Int32) = Nothing
    Public Property ColumnNames As List(Of String) = Nothing

    Public Property Rows As List(Of Row)

    Public Property MaxRowErrors As Int32 = 100
    Public Property Errors As List(Of String)

    Public Class Row
        Public Elements() As String

        Public Sub New(ByVal pData() As String)
            Elements = pData
        End Sub

        Public Function GetElement(ByVal pIndex As Int32) As String
            If Elements IsNot Nothing AndAlso Elements.Length > pIndex Then
                Return Elements(pIndex)
            Else
                Return Nothing
            End If
        End Function

        Public Function GetString(ByVal pIndex As Int32, Optional ByVal pDefault As String = "") As String
            Dim data = GetElement(pIndex)
            If String.IsNullOrWhiteSpace(data) Then Return pDefault Else Return data
        End Function

        Public Function GetInt32(ByVal pIndex As Int32, Optional ByVal pDefault As Int32? = Nothing) As Int32?
            Dim data = GetElement(pIndex)
            If IsNumeric(data) Then Return CInt(data) Else Return pDefault
        End Function

        Public Function GetBool(ByVal pIndex As Int32, Optional ByVal pDefault As Boolean? = Nothing, Optional ByVal pTrueValue As String = Nothing, Optional ByVal pFalseValue As String = Nothing) As Boolean?
            Dim data = GetElement(pIndex)
            If (pTrueValue IsNot Nothing AndAlso data = pTrueValue) OrElse {"Y", "1"}.Contains(CStr(data).ToUpper.Substring(0, 1)) Then Return True
            If (pFalseValue IsNot Nothing AndAlso data = pFalseValue) OrElse {"N", "0"}.Contains(CStr(data).ToUpper.Substring(0, 1)) Then Return False
            Return pDefault
        End Function

        Public Function GetGuid(ByVal pIndex As Int32, Optional ByVal pDefault As Guid? = Nothing) As Guid?
            Dim data = GetElement(pIndex)
            Dim newGuid As Guid
            If Guid.TryParse(data, newGuid) Then Return newGuid Else Return pDefault
        End Function

        Public Function GetDateTime(ByVal pIndex As Int32, Optional ByVal pDefault As DateTime? = Nothing) As DateTime?
            Dim data = GetElement(pIndex)
            Dim newDate As DateTime
            If Date.TryParse(data, newDate) Then Return newDate Else Return pDefault
        End Function

    End Class

    Public Sub Import(ByVal pFilename As String, Optional ByVal pDefaultEncoding As Text.Encoding = Nothing)
        If pDefaultEncoding Is Nothing Then pDefaultEncoding = Text.Encoding.ASCII
        Using Reader As New Microsoft.VisualBasic.FileIO.TextFieldParser(pFilename, pDefaultEncoding)
            Call ProcessReader(Reader)
        End Using
    End Sub

    Public Sub Import(ByVal pStream As IO.Stream, Optional ByVal pDefaultEncoding As Text.Encoding = Nothing)
        If pDefaultEncoding Is Nothing Then pDefaultEncoding = Text.Encoding.ASCII
        Using Reader As New Microsoft.VisualBasic.FileIO.TextFieldParser(pStream, pDefaultEncoding)
            Call ProcessReader(Reader)
        End Using
    End Sub

    Private Sub ProcessReader(ByVal pReader As FileIO.TextFieldParser)
        pReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.FixedWidth
        pReader.SetFieldWidths(ColumnWidths.ToArray)

        Me.Rows = New List(Of Row)
        While Not pReader.EndOfData
            Try
                Me.Rows.Add(New Row(pReader.ReadFields()))
            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                If Errors Is Nothing Then Errors = New List(Of String)
                Errors.Add(ex.Message)
                If Errors.Count >= Me.MaxRowErrors - 1 Then
                    Me.Errors.Add("Import Aborted because maximum number of import row errors was exceeded!")
                    Exit While
                End If
            End Try
        End While
    End Sub

    Public Function AddColumn(ByVal pWidth As Int32, Optional ByVal pName As String = Nothing) As Int32
        If Me.ColumnWidths Is Nothing Then Me.ColumnWidths = New List(Of Int32)
        Me.ColumnWidths.Add(pWidth)

        If pName IsNot Nothing Then
            If Me.ColumnNames Is Nothing Then Me.ColumnNames = New List(Of String)
            Me.ColumnNames.Add(pName)
        End If

        Return Me.ColumnWidths.Count - 1
    End Function

    Public Function GetElement(ByVal pRowIndex As Int32, ByVal pColumnName As String) As String
        Dim colIndex As Int32 = ColumnNames.IndexOf(pColumnName)
        If colIndex > -1 Then
            If pRowIndex > -1 AndAlso pRowIndex < Me.Rows.Count Then
                Dim row = Me.Rows.ElementAt(pRowIndex)
                If row IsNot Nothing Then
                    If row.Elements IsNot Nothing AndAlso row.Elements.Length > colIndex Then Return row.Elements(colIndex)
                End If
            End If
        End If
        Return Nothing
    End Function

    Public Function GetElement(ByVal pRow As Row, ByVal pColumnName As String) As String
        If Me.ColumnNames Is Nothing OrElse Me.ColumnNames.Count = 0 Then Throw New Exception("You cannot reference columns by name without initializing the property ColumnNames!")

        Dim colIndex As Int32 = ColumnNames.IndexOf(pColumnName)
        If pRow.Elements IsNot Nothing AndAlso pRow.Elements.Length > colIndex Then Return pRow.Elements(colIndex) Else Return Nothing
    End Function

End Class
