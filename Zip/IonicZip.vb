﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports Ionic
Imports Ionic.Zip

Namespace Zip

    Public Class IonicZip
        Private Shared _busy As Boolean = False

        Public Shared Event DataProgress(ByVal Cur As Long, ByVal Total As Long)
        Public Shared LastUpdate As Date = Now()

        Public Shared Sub HandleExtractProgress(ByVal sender As Object, ByVal e As ExtractProgressEventArgs)
            If Now.Subtract(LastUpdate).TotalMilliseconds < 250 Then Exit Sub
            Select Case e.EventType
                Case ZipProgressEventType.Extracting_EntryBytesWritten
                    RaiseEvent DataProgress(e.BytesTransferred, e.TotalBytesToTransfer)
                    LastUpdate = Now
            End Select
        End Sub

        Public Shared Sub HandleSaveProgress(ByVal sender As Object, ByVal e As SaveProgressEventArgs)
            If Now.Subtract(LastUpdate).TotalMilliseconds < 250 Then Exit Sub
            Select Case e.EventType
                Case ZipProgressEventType.Saving_EntryBytesRead
                    RaiseEvent DataProgress(e.BytesTransferred, e.TotalBytesToTransfer)
                    LastUpdate = Now
            End Select
        End Sub


        Public Shared Sub Extract(ByVal pZipFilename As String, Optional ByVal pDestDir As String = "")
            If IsNothing(pDestDir) OrElse pDestDir = "" Then
                pDestDir = pZipFilename.Substring(0, pZipFilename.LastIndexOf("\"))
            End If

            Using tmp As New Ionic.Zip.ZipFile(pZipFilename)
                AddHandler tmp.ExtractProgress, AddressOf HandleExtractProgress
                tmp.ExtractAll(pDestDir, ExtractExistingFileAction.OverwriteSilently)
                tmp.Dispose()
            End Using
        End Sub

        Public Shared Sub ZipFile(ByVal pFilename As String, Optional ByRef pZipFile As String = Nothing)
            AddToZip(pFilename, pZipFile, False)
        End Sub

        Public Shared Sub AddToZip(ByVal pFilename As String, Optional ByVal pRetainDirectoryStructure As Boolean = True)
            AddToZip(pFilename, GetZipFileName(pFilename), pRetainDirectoryStructure)
        End Sub

        Public Shared Sub AddToZip(ByVal pFilename As String, ByRef pZipFile As String, Optional ByVal pRetainDirectoryStructure As Boolean = True)
            If IsNothing(pZipFile) OrElse pZipFile = "" Then
                pZipFile = GetZipFileName(pFilename)
            End If

            Using tmp = New Ionic.Zip.ZipFile(pZipFile)
                Try
                    AddHandler tmp.SaveProgress, AddressOf HandleSaveProgress
                    If tmp.EntryFileNames.Contains(FilenameOnly(pFilename)) Then Call tmp.RemoveEntry(FilenameOnly(pFilename)) 'If Exists

                    If pRetainDirectoryStructure Then
                        tmp.AddFile(pFilename)
                    Else
                        tmp.AddFile(pFilename, "")
                    End If

                    tmp.Save(pZipFile)

                Finally
                    tmp.Dispose()
                End Try
            End Using
        End Sub

        Public Shared Function ZipContainsFile(ByVal pZipFilename As String, ByVal pFilename As String) As Boolean
            Using tmp = New Ionic.Zip.ZipFile(pZipFilename)
                Dim response = ZipContainsFile(tmp, pFilename)
                tmp.Dispose()
                Return response
            End Using
        End Function

        Friend Shared Function ZipContainsFile(ByRef pZipFile As Ionic.Zip.ZipFile, ByVal pFilename As String) As Boolean
            Dim BaseFilename As String = FilenameOnly(pFilename)
            Return pZipFile.EntryFileNames.Contains(BaseFilename)
        End Function

        Public Shared Function isZipFile(ByVal pFilename As String) As Boolean
            Return Ionic.Zip.ZipFile.IsZipFile(pFilename)
        End Function

        Public Shared Function hasZipExtension(ByVal pFilename As String) As Boolean
            Return InStr(pFilename.Substring(pFilename.Length - 4).ToUpper(), ".ZIP") > 0
        End Function

        Public Shared Function GetZipFileName(ByVal pFilename As String) As String
            If pFilename.Contains(".") Then
                Dim index = pFilename.LastIndexOf(".")
                Return pFilename.Substring(0, index + 1) & "zip"
            Else
                Return pFilename & ".zip"
            End If
        End Function

        Private Shared Function FilenameOnly(ByVal pFilename As String) As String
            If pFilename.LastIndexOf("\") > 0 Then
                Return pFilename.Substring(pFilename.LastIndexOf("\") + 1)
            Else
                Return pFilename
            End If
        End Function

        '------------- Async Code

        Private Structure ExtractParams
            Dim ZipFilename As String
            Dim DestDir As String
        End Structure

        Private Structure AddToZipParams
            Dim Filename As String
            Dim ZipFilename As String
            Dim RetainDirectoryStructure As Boolean
        End Structure

        Public Shared Sub ExtractAsync(ByVal pZipFilename As String, Optional ByVal pDestDir As String = "")
            Dim workerThread As New Threading.Thread(New Threading.ParameterizedThreadStart(AddressOf delegateExtract))
            Dim myParams As ExtractParams
            myParams.ZipFilename = pZipFilename
            myParams.DestDir = pDestDir

            _busy = True
            workerThread.Start(myParams)

            While _busy
                System.Threading.Thread.Sleep(100)
                Application.DoEvents()
            End While
        End Sub

        Private Shared Sub delegateExtract(ByVal pParams As Object)
            Try
                With DirectCast(pParams, ExtractParams)
                    Call Extract(.ZipFilename, .DestDir)
                End With
            Finally
                _busy = False
            End Try
        End Sub

        Public Shared Sub AddToZipAsync(ByVal pFilename As String, Optional ByRef pZipFile As String = "", Optional ByVal pRetainDirectoryStructure As Boolean = True)
            Dim workerThread As New Threading.Thread(New Threading.ParameterizedThreadStart(AddressOf delegateAddToZip))
            Dim myParams As AddToZipParams
            myParams.Filename = pFilename
            myParams.ZipFilename = pZipFile
            myParams.RetainDirectoryStructure = pRetainDirectoryStructure

            _busy = True
            workerThread.Start(myParams)

            While _busy
                System.Threading.Thread.Sleep(100)
                Application.DoEvents()
            End While
        End Sub

        Private Shared Sub delegateAddToZip(ByVal pParams As Object)
            Try
                With DirectCast(pParams, AddToZipParams)
                    Call AddToZip(.Filename, .ZipFilename, .RetainDirectoryStructure)
                End With
            Finally
                _busy = False
            End Try
        End Sub

    End Class

End Namespace
