﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Text.RegularExpressions

Public Class CommandLine

    Public Shared Function GetArguments(pArgsArray As String()) As Dictionary(Of String, String)
        'http://www.codeproject.com/KB/recipes/command_line.aspx

        Dim argsList As New Dictionary(Of String, String)
        Dim Spliter As New Regex("^-{1,2}|^/|=|:", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Dim Remover As New Regex("^['""]?(.*?)['""]?$", RegexOptions.IgnoreCase Or RegexOptions.Compiled)

        Dim Parameter As String = Nothing
        Dim Parts As String()

        ' Valid parameters forms:
        ' {-,/,--}param{ ,=,:}((",')value(",'))

        ' Examples: 
        ' -param1 value1 --param2 /param3:"Test-:-work" 
        '   /param4=happy -param5 '--=nice=--'

        For Each Txt As String In pArgsArray
            ' Look for new parameters (-,/ or --) and a
            ' possible enclosed value (=,:)

            Parts = Spliter.Split(Txt, 3)

            Select Case Parts.Length
                Case 1
                    ' Found a value (for the last parameter 
                    ' found (space separator))
                    If Parameter IsNot Nothing Then
                        If (Not argsList.ContainsKey(Parameter)) Then
                            Parts(0) = Remover.Replace(Parts(0), "$1")
                            argsList.Add(Parameter, Parts(0))
                        End If
                        Parameter = Nothing
                    End If
                    ' else Error: no parameter waiting for a value (skipped)

                    Exit Select
                Case 2
                    ' Found just a parameter
                    ' The last parameter is still waiting. 
                    ' With no value, set it to true.

                    If (Parameter IsNot Nothing) Then
                        If (Not argsList.ContainsKey(Parameter)) Then
                            argsList.Add(Parameter, "true")
                        End If
                    End If
                    Parameter = Parts(1)
                    Exit Select
                Case 3
                    ' Parameter with enclosed value
                    ' The last parameter is still waiting. 
                    ' With no value, set it to true.

                    If (Parameter IsNot Nothing) Then
                        If (Not argsList.ContainsKey(Parameter)) Then
                            argsList.Add(Parameter, "true")
                        End If
                    End If

                    Parameter = Parts(1)

                    ' Remove possible enclosing characters (",')
                    If (Not argsList.ContainsKey(Parameter)) Then
                        Parts(2) = Remover.Replace(Parts(2), "$1")
                        argsList.Add(Parameter, Parts(2))
                    End If

                    Parameter = Nothing
                    Exit Select
            End Select
        Next

        ' In case a parameter is still waiting
        If (Parameter IsNot Nothing) Then
            If (Not argsList.ContainsKey(Parameter)) Then
                argsList.Add(Parameter, "true")
            End If
        End If

        Return argsList
    End Function

End Class
