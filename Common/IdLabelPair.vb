﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class IdLabelPair
    Public Property Id As Int32
    Public Property Label As String

    Sub New(ByVal pId As Int32, ByVal pLabel As String)
        Id = pId
        Label = pLabel
    End Sub

    Public Overrides Function ToString() As String
        Return Label
    End Function

    Public Shared Function GetId(pILPair As Object) As Int32?
        If ((pILPair Is Nothing) OrElse (Not (TypeOf pILPair Is IdLabelPair))) Then Return Nothing

        Dim ilp = DirectCast(pILPair, IdLabelPair)
        Return ilp.Id
    End Function

    Public Shared Function FindInList(pCollection As IEnumerable, pIdVal As Int32?) As IdLabelPair
        If (Not pIdVal.HasValue) Then Return Nothing
        For Each curObj As Object In pCollection
            If (TypeOf curObj Is IdLabelPair) Then
                Dim val As Int32? = GetId(curObj)
                If (val.Equals(pIdVal)) Then
                    Return DirectCast(curObj, IdLabelPair)
                End If
            End If
        Next
        Return Nothing
    End Function

End Class
