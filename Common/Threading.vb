﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Threading

Public Class Threading

    Private Class DelayDoObj
        Public Property MyAction As Action
        Public Property Delay As TimeSpan
    End Class

    Public Shared Sub DelayDo(pAction As Action, pDelay As TimeSpan)
        Dim obj As New DelayDoObj
        obj.MyAction = pAction
        obj.Delay = pDelay
        Dim doThrd As New Thread(New ParameterizedThreadStart(AddressOf DelayDoImpl))
        doThrd.Priority = ThreadPriority.BelowNormal
        doThrd.Start(obj)
    End Sub

    Private Shared Sub DelayDoImpl(pDelayDoObj As Object)
        Dim args = DirectCast(pDelayDoObj, DelayDoObj)
        Thread.Sleep(CInt(args.Delay.TotalMilliseconds))
        args.MyAction.Invoke()
    End Sub





    ''' <summary>
    ''' Base class for a safer thread to do ongoing work.
    ''' Will not overlap work or wait less than the specified interval between attempts.
    ''' Supports safe shutdown.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class WorkThread

        Private _workingLock As New Object
        Private _workThread As Thread
        Private _workThreadExit As ManualResetEventSlim
        Private _started As Boolean = False

        Public Property Interval As TimeSpan = TimeSpan.FromSeconds(60)

        Protected Sub New()
        End Sub

        Public Sub Startup()
            If (_started) Then Return

            Setup()
            _workThread = New Thread(New ThreadStart(AddressOf ThreadMain))
            _workThread.Priority = ThreadPriority.BelowNormal
            _workThreadExit = New ManualResetEventSlim(initialState:=False)
            _workThread.Start()
            _started = True
        End Sub

        Public Sub Shutdown(Optional ByVal pTimeoutMs As Int32 = 5000)
            If (Not _started) Then Return
            _started = False

            _workThreadExit.Set()
            Dim waited As Int32 = 0
            While ((_workThread.ThreadState <> ThreadState.Stopped) AndAlso (waited < pTimeoutMs))
                Thread.Sleep(10)
                waited += 10
            End While
            If (_workThread.ThreadState <> ThreadState.Stopped) Then
                'stop processing anyway
                _workThread.Abort()
                System.Threading.Thread.Sleep(1000)
                FYUtil.Logging.LogWarning("WorkThread: Timeout in Shutdown")
            End If

            Teardown()
        End Sub

        Private Sub ThreadMain()
            _workThreadExit.Wait(Interval)
            While Not _workThreadExit.IsSet
                If (Not _started) Then Return
                If (Monitor.TryEnter(_workingLock)) Then
                    Try
                        'do work in here
                        DoWork()
                    Finally
                        Monitor.Exit(_workingLock)
                    End Try
                Else
                    FYUtil.Logging.LogWarning("WorkThread: Lock in use")
                End If
                _workThreadExit.Wait(Interval)
            End While
        End Sub

        Protected MustOverride Sub DoWork()

        Protected Overridable Sub Setup()
        End Sub

        Protected Overridable Sub Teardown()
        End Sub

    End Class





    ''' <summary>
    ''' Simple class for adding timer-type behavior to a function.
    ''' Supports safe shutdown.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WorkThreadBehavior
        Inherits WorkThread

        Public Delegate Sub DoWorkDelegate()

        Private _myDoWork As DoWorkDelegate

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pDoWorkDelegate">Method to call at the specified interval</param>
        ''' <remarks></remarks>
        Public Sub New(pDoWorkDelegate As DoWorkDelegate)
            _myDoWork = pDoWorkDelegate
            Interval = TimeSpan.FromSeconds(60)
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pDoWorkDelegate">Method to call at the specified interval</param>
        ''' <param name="pInterval">Repeat interval</param>
        ''' <remarks></remarks>
        Public Sub New(pDoWorkDelegate As DoWorkDelegate, pInterval As TimeSpan)
            _myDoWork = pDoWorkDelegate
            Interval = pInterval
        End Sub

        Protected Overrides Sub DoWork()
            _myDoWork()
        End Sub

    End Class


End Class
