﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Configuration

Public Class AppSettingsHelper

    Private Shared Function GetValue(pKeyName As String) As String
        Return ConfigurationManager.AppSettings(pKeyName)
    End Function

    Public Shared Function GetString(pKeyName As String, Optional pDefault As String = "") As String
        Dim valStr = GetValue(pKeyName)
        If (valStr Is Nothing) Then
            Return pDefault
        Else
            Return valStr
        End If
    End Function

#Region "Int32"

    Public Shared Function GetInt32(pKeyName As String) As Int32?
        Return GetInt32Impl(pKeyName, Nothing)
    End Function

    Public Shared Function GetInt32(pKeyName As String, pDefault As Int32) As Int32
        Return GetInt32Impl(pKeyName, pDefault).Value
    End Function

    Private Shared Function GetInt32Impl(pKeyName As String, pDefault As Int32?) As Int32?
        Dim valStr = GetValue(pKeyName)
        If ((valStr Is Nothing) OrElse (Not IsNumeric(valStr))) Then
            Return pDefault
        Else
            Try
                Return Convert.ToInt32(valStr)
            Catch ex As Exception
                Return pDefault
            End Try
        End If
    End Function

#End Region

#Region "Double"

    Public Shared Function GetDouble(pKeyName As String) As Double?
        Return GetDoubleImpl(pKeyName, Nothing)
    End Function

    Public Shared Function GetDouble(pKeyName As String, pDefault As Double) As Double
        Return GetDoubleImpl(pKeyName, pDefault).Value
    End Function

    Private Shared Function GetDoubleImpl(pKeyName As String, pDefault As Double?) As Double?
        Dim valStr = GetValue(pKeyName)
        If ((valStr Is Nothing) OrElse (Not IsNumeric(valStr))) Then
            Return pDefault
        Else
            Try
                Return Convert.ToDouble(valStr)
            Catch ex As Exception
                Return pDefault
            End Try
        End If
    End Function

#End Region

#Region "Boolean"

    Public Shared Function GetBoolean(pKeyName As String) As Boolean?
        Return GetBooleanImpl(pKeyName, Nothing)
    End Function

    Public Shared Function GetBoolean(pKeyName As String, pDefault As Boolean) As Boolean
        Return GetBooleanImpl(pKeyName, pDefault).Value
    End Function

    Private Shared Function GetBooleanImpl(pKeyName As String, pDefault As Boolean?) As Boolean?
        Dim valStr = GetValue(pKeyName)
        If (valStr Is Nothing) Then
            Return pDefault
        Else
            valStr = valStr.ToLower()
            If ({"yes", "true", "1", "y", "t"}.Contains(valStr, StringComparer.OrdinalIgnoreCase)) Then
                Return True
            ElseIf ({"no", "false", "0", "n", "f"}.Contains(valStr, StringComparer.OrdinalIgnoreCase)) Then
                Return False
            Else
                Return pDefault
            End If
        End If
    End Function

#End Region

#Region "Enum"

    Public Shared Function GetEnum(Of TEnum As Structure)(pKeyName As String) As TEnum?
        Return GetEnumImpl(Of TEnum)(pKeyName, Nothing)
    End Function

    Public Shared Function GetEnum(Of TEnum As Structure)(pKeyName As String, pDefault As TEnum) As TEnum
        Return GetEnumImpl(Of TEnum)(pKeyName, pDefault).Value
    End Function

    Private Shared Function GetEnumImpl(Of TEnum As Structure)(pKeyName As String, pDefault As TEnum?) As TEnum?
        Dim valStr = GetValue(pKeyName)
        If (valStr Is Nothing) Then
            Return pDefault
        Else
            Try
                Dim res As TEnum = DirectCast(System.Enum.Parse(GetType(TEnum), valStr, ignoreCase:=True), TEnum)
                Return res
            Catch ex As Exception
                Return pDefault
            End Try
        End If
    End Function

#End Region




    Public Class WriteManager

        Private _myConfig As Configuration

        Public Sub New()
            _myConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        End Sub

        Public Sub Save()
            _myConfig.Save(ConfigurationSaveMode.Modified)
            ConfigurationManager.RefreshSection("appSettings")
        End Sub


        Public Sub SetValue(pKeyName As String, pValue As Object)
            Dim strVal As String = Nothing
            If (pValue Is Nothing) Then
                strVal = ""
            Else
                strVal = pValue.ToString
            End If
            If (_myConfig.AppSettings.Settings(pKeyName) IsNot Nothing) Then
                _myConfig.AppSettings.Settings(pKeyName).Value = strVal
            Else
                _myConfig.AppSettings.Settings.Add(pKeyName, strVal)
            End If
        End Sub

    End Class


End Class
