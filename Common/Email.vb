﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Net

Public Class Email

    Public Shared Sub Send(ByVal pSmtpServer As String, ByVal pFrom As String, ByVal pTo As String, ByVal pSubject As String, ByVal pBody As String, Optional pCC As String = Nothing, Optional pBCC As String = Nothing, Optional pReplyTo As String = Nothing, Optional pAttachments As IEnumerable(Of Mail.Attachment) = Nothing, Optional pHtmlBody As Boolean = False)
        Using EMail = New Mail.MailMessage(pFrom, pTo.Replace(";", ","))
            EMail.Subject = pSubject
            EMail.IsBodyHtml = pHtmlBody
            If pHtmlBody Then
                EMail.Body = Replace(pBody, vbCrLf, "<br>")
            Else
                EMail.Body = pBody
            End If
            If Not String.IsNullOrWhiteSpace(pCC) Then
                For Each cc In pCC.Replace(";", ",").Split(","c)
                    EMail.CC.Add(cc.Trim())
                Next
            End If
            If Not String.IsNullOrWhiteSpace(pBCC) Then
                For Each bcc In pBCC.Replace(";", ",").Split(","c)
                    EMail.Bcc.Add(bcc.Trim())
                Next
            End If
            If Not String.IsNullOrWhiteSpace(pReplyTo) Then
                For Each rt In pReplyTo.Replace(";", ",").Split(","c)
                    EMail.ReplyToList.Add(rt.Trim())
                Next
            End If
            If pAttachments IsNot Nothing AndAlso pAttachments.Count > 0 Then
                For Each att In pAttachments
                    EMail.Attachments.Add(att)
                Next
            End If

            Dim tmp As New Mail.SmtpClient(pSmtpServer)
            tmp.Send(EMail)
        End Using
    End Sub

#Region "MX"

    Private Declare Unicode Function DnsQuery Lib "dnsapi" Alias "DnsQuery_W" (<Runtime.InteropServices.MarshalAs(Runtime.InteropServices.UnmanagedType.VBByRefStr)> ByRef pszName As String, wType As QueryTypes, options As QueryOptions, aipServers As Integer, ByRef ppQueryResults As IntPtr, pReserved As Integer) As Integer

    <Runtime.InteropServices.DllImport("dnsapi", CharSet:=Runtime.InteropServices.CharSet.Auto, SetLastError:=True)> _
    Private Shared Sub DnsRecordListFree(pRecordList As IntPtr, FreeType As Integer)
    End Sub

    Public Shared Function GetMxRecords(domain As String) As IEnumerable(Of String)
        Dim ptr1 As IntPtr = IntPtr.Zero
        Dim ptr2 As IntPtr = IntPtr.Zero
        Dim recMx As MXRecord
        If Environment.OSVersion.Platform <> PlatformID.Win32NT Then
            Throw New NotSupportedException()
        End If
        Dim list1 As New List(Of String)
        Dim num1 As Integer = DnsQuery(domain, QueryTypes.DNS_TYPE_MX, QueryOptions.DNS_QUERY_BYPASS_CACHE, 0, ptr1, 0)
        If num1 <> 0 Then
            Throw New ComponentModel.Win32Exception(num1)
        End If
        ptr2 = ptr1
        While Not ptr2.Equals(IntPtr.Zero)
            recMx = DirectCast(Runtime.InteropServices.Marshal.PtrToStructure(ptr2, GetType(MXRecord)), MXRecord)
            If recMx.wType = 15 Then
                Dim text1 As String = Runtime.InteropServices.Marshal.PtrToStringAuto(recMx.pNameExchange)
                list1.Add(text1)
            End If
            ptr2 = recMx.pNext
        End While
        DnsRecordListFree(ptr1, 0)
        Return list1
    End Function

    Private Enum QueryOptions
        DNS_QUERY_ACCEPT_TRUNCATED_RESPONSE = 1
        DNS_QUERY_BYPASS_CACHE = 8
        DNS_QUERY_DONT_RESET_TTL_VALUES = &H100000
        DNS_QUERY_NO_HOSTS_FILE = &H40
        DNS_QUERY_NO_LOCAL_NAME = &H20
        DNS_QUERY_NO_NETBT = &H80
        DNS_QUERY_NO_RECURSION = 4
        DNS_QUERY_NO_WIRE_QUERY = &H10
        DNS_QUERY_RESERVED = -16777216
        DNS_QUERY_RETURN_MESSAGE = &H200
        DNS_QUERY_STANDARD = 0
        DNS_QUERY_TREAT_AS_FQDN = &H1000
        DNS_QUERY_USE_TCP_ONLY = 2
        DNS_QUERY_WIRE_ONLY = &H100
    End Enum

    Private Enum QueryTypes
        DNS_TYPE_MX = 15
    End Enum

    <Runtime.InteropServices.StructLayout(Runtime.InteropServices.LayoutKind.Sequential)> _
    Private Structure MXRecord
        Public pNext As IntPtr
        Public pName As String
        Public wType As Short
        Public wDataLength As Short
        Public flags As Integer
        Public dwTtl As Integer
        Public dwReserved As Integer
        Public pNameExchange As IntPtr
        Public wPreference As Short
        Public Pad As Short
    End Structure

#End Region

End Class