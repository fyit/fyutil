﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class FileTypes


    Private Shared _contentTypeMap As New Dictionary(Of String, eFileType)(StringComparer.OrdinalIgnoreCase)
    Private Shared _altContentTypeMap As New Dictionary(Of String, eFileType)(StringComparer.OrdinalIgnoreCase)
    Private Shared _extensionMap As New Dictionary(Of String, eFileType)(StringComparer.OrdinalIgnoreCase)
    Private Shared _altExtensionMap As New Dictionary(Of String, eFileType)(StringComparer.OrdinalIgnoreCase)

    Shared Sub New()
        _contentTypeMap.Add("image/png", eFileType.PngImage)
        _contentTypeMap.Add("image/jpeg", eFileType.JpgImage)
        _contentTypeMap.Add("image/gif", eFileType.GifImage)
        _contentTypeMap.Add("image/tiff", eFileType.TifImage)
        _contentTypeMap.Add("image/bmp", eFileType.BmpImage)
        _contentTypeMap.Add("application/pdf", eFileType.PdfDocument)
        _contentTypeMap.Add("application/msword", eFileType.WordDocument)
        _contentTypeMap.Add("application/vnd.openxmlformats-officedocument.wordprocessingml.document", eFileType.WordXDocument)
        _contentTypeMap.Add("application/vnd.ms-excel", eFileType.ExcelDocument)
        _contentTypeMap.Add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", eFileType.ExcelXDocument)
        _contentTypeMap.Add("text/plain", eFileType.Text)
        _contentTypeMap.Add("text/html", eFileType.HtmlFile)
        _contentTypeMap.Add("text/css", eFileType.CssFile)
        _contentTypeMap.Add("text/csv", eFileType.CSV)
        _contentTypeMap.Add("text/xml", eFileType.Xml)

        'nonstandard aliases
        _altContentTypeMap.Add("image/pjpeg", eFileType.JpgImage)
        _altContentTypeMap.Add("image/x-png", eFileType.PngImage)

        _extensionMap.Add("gif", eFileType.GifImage)
        _extensionMap.Add("jpg", eFileType.JpgImage)
        _extensionMap.Add("png", eFileType.PngImage)
        _extensionMap.Add("tif", eFileType.TifImage)
        _extensionMap.Add("bmp", eFileType.BmpImage)
        _extensionMap.Add("pdf", eFileType.PdfDocument)
        _extensionMap.Add("doc", eFileType.WordDocument)
        _extensionMap.Add("docx", eFileType.WordXDocument)
        _extensionMap.Add("xls", eFileType.ExcelDocument)
        _extensionMap.Add("xlsx", eFileType.ExcelXDocument)
        _extensionMap.Add("css", eFileType.CssFile)
        _extensionMap.Add("txt", eFileType.Text)
        _extensionMap.Add("htm", eFileType.HtmlFile)
        _extensionMap.Add("html", eFileType.HtmlFile)
        _extensionMap.Add("csv", eFileType.CSV)
        _extensionMap.Add("xml", eFileType.XML)

        'nonstandard extensions
        _altExtensionMap.Add("jpeg", eFileType.JpgImage)
        _altExtensionMap.Add("tiff", eFileType.TifImage)
    End Sub






    Public Enum eFileType
        <FYUtil.CodedEnum.CodedEnumValue("", "")>
        Unknown = 0

        <FYUtil.CodedEnum.CodedEnumValue("PNG")>
        PngImage = 10

        <FYUtil.CodedEnum.CodedEnumValue("JPG")>
        JpgImage = 20

        <FYUtil.CodedEnum.CodedEnumValue("GIF")>
        GifImage = 30

        <FYUtil.CodedEnum.CodedEnumValue("TIF")>
        TifImage = 40

        <FYUtil.CodedEnum.CodedEnumValue("BMP")>
        BmpImage = 50

        <FYUtil.CodedEnum.CodedEnumValue("PDF")>
        PdfDocument = 60

        <FYUtil.CodedEnum.CodedEnumValue("DOC")>
        WordDocument = 70

        <FYUtil.CodedEnum.CodedEnumValue("DOCX")>
        WordXDocument = 80

        <FYUtil.CodedEnum.CodedEnumValue("XLS")>
        ExcelDocument = 90

        <FYUtil.CodedEnum.CodedEnumValue("XLSX")>
        ExcelXDocument = 100

        <FYUtil.CodedEnum.CodedEnumValue("HTM")>
        HtmlFile = 110

        <FYUtil.CodedEnum.CodedEnumValue("CSS")>
        CssFile = 120

        <FYUtil.CodedEnum.CodedEnumValue("TXT")>
        Text = 130

        <FYUtil.CodedEnum.CodedEnumValue("CSV")>
        CSV = 140

        <FYUtil.CodedEnum.CodedEnumValue("XML")>
        Xml = 140

    End Enum

    Private Shared _images As IEnumerable(Of eFileType) = {eFileType.BmpImage, eFileType.GifImage, eFileType.JpgImage, eFileType.PngImage, eFileType.TifImage}



    Public Shared Function GetCode(pFileType As eFileType) As String
        Return FYUtil.CodedEnum.GetCode(pFileType)
    End Function

    Public Shared Function GetByCode(pFileTypeCode As String) As eFileType
        Return FYUtil.CodedEnum.GetByCode(Of eFileType)(pFileTypeCode, eFileType.Unknown)
    End Function

    Public Shared Function GetByFileExtension(pFilename As String) As eFileType
        Dim ext = pFilename
        If (pFilename.Contains(".")) Then ext = IO.Path.GetExtension(pFilename).TrimStart("."c)

        If (_extensionMap.ContainsKey(ext)) Then
            Return _extensionMap.Item(ext)
        ElseIf (_altExtensionMap.ContainsKey(ext)) Then
            Return _altExtensionMap.Item(ext)
        Else
            Return eFileType.Unknown
        End If
    End Function

    Public Shared Function GetExtension(pFileType As eFileType) As String
        Return (From x In _extensionMap Where x.Value = pFileType Select x.Key).FirstOrDefault
    End Function

    Public Shared Function GetByContentType(pContentType As String) As eFileType
        If (_contentTypeMap.ContainsKey(pContentType)) Then
            Return _contentTypeMap.Item(pContentType)
        ElseIf (_altContentTypeMap.ContainsKey(pContentType)) Then
            Return _altContentTypeMap.Item(pContentType)
        Else
            Return eFileType.Unknown
        End If
    End Function

    Public Shared Function GetContentType(pFileType As eFileType) As String
        Return (From x In _contentTypeMap Where x.Value = pFileType Select x.Key).FirstOrDefault
    End Function


    Public Shared Function IsImage(pFileType As eFileType) As Boolean
        Return _images.Contains(pFileType)
    End Function

End Class
