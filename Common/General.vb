﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class General


    ''' <summary>
    ''' Will look in the AppSettings section of the config file for App.Environment
    ''' </summary>
    Public Shared Function DetermineEnvironment() As eEnvironment
        Return DetermineEnvironment(FYUtil.AppSettingsHelper.GetString("App.Environment"))
    End Function

    Public Shared Function DetermineEnvironment(pEnvironmentString As String) As eEnvironment
        Try
            Dim res As eEnvironment = DirectCast(System.Enum.Parse(GetType(eEnvironment), pEnvironmentString, ignoreCase:=True), eEnvironment)
            Return res
        Catch ex As Exception
            If ((Not String.IsNullOrWhiteSpace(pEnvironmentString)) AndAlso (pEnvironmentString.Length >= 3)) Then
                Select Case pEnvironmentString.ToUpper.Substring(0, 3)
                    Case "DEV" : Return eEnvironment.Dev
                    Case "TES" : Return eEnvironment.Test
                    Case "CER" : Return eEnvironment.Cert
                    Case "DEM" : Return eEnvironment.Demo
                    Case "PRO" : Return eEnvironment.Prod
                End Select
            End If
        End Try
        Return eEnvironment.NotSet
    End Function

End Class
