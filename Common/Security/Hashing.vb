﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Security.Cryptography

Namespace Security

	''' <summary>
	''' One-way cryptographic hash algorithms. Note: Recommend adding salt to the hash payload to avoid dictionary attacks
	''' </summary>
	''' <remarks></remarks>
	Public Class Hashing

        Public Shared Function HashSha512(ByVal pToHash As Byte()) As Byte()
            Using crypto As New SHA512Managed()
                Return crypto.ComputeHash(pToHash)
            End Using
        End Function

        ''' <summary>
        ''' Hash a string using SHA512 and then encode in HEX
        ''' </summary>
        Public Shared Function HashSha512ToHex(ByVal pToHash As String) As String
            Return Utilities.ByteArrayToHexString(HashSha512(Utilities.StringToByteArray(pToHash)))
        End Function

        ''' <summary>
        ''' Hash a string using SHA512 and then encode in Base64
        ''' </summary>
        Public Shared Function HashSha512ToBase64(ByVal pToHash As String) As String
            Return Convert.ToBase64String(HashSha512(Utilities.StringToByteArray(pToHash)))
        End Function



        Public Shared Function HashSha1(ByVal pToHash As Byte()) As Byte()
            Using crypto As New SHA1Managed()
                Return crypto.ComputeHash(pToHash)
            End Using
        End Function

        ''' <summary>
        ''' Hash a string using SHA1 and then encode in HEX
        ''' </summary>
        Public Shared Function HashSha1ToHex(ByVal pToHash As String) As String
            Return Utilities.ByteArrayToHexString(HashSha1(Utilities.StringToByteArray(pToHash)))
        End Function

        ''' <summary>
        ''' Hash a string using SHA1 and then encode in Base64
        ''' </summary>
        Public Shared Function HashSha1ToBase64(ByVal pToHash As String) As String
            Return Convert.ToBase64String(HashSha1(Utilities.StringToByteArray(pToHash)))
        End Function



		Public Shared Function HashMd5(ByVal pToHash As Byte()) As Byte()
			Using crypto As New MD5CryptoServiceProvider()
				Return crypto.ComputeHash(pToHash)
			End Using
		End Function

		''' <summary>
		''' Hash a string using MD5. Returns string with HEX representation.
		''' </summary>
		Public Shared Function HashMd5ToHex(ByVal pToHash As String) As String
			Return Utilities.ByteArrayToHexString(HashMd5(Utilities.StringToByteArray(pToHash)))
		End Function

		''' <summary>
		''' Hash a string using MD5. Returns string in Base64
		''' </summary>
		Public Shared Function HashMd5ToBase64(ByVal pToHash As String) As String
			Return Convert.ToBase64String(HashMd5(Utilities.StringToByteArray(pToHash)))
		End Function

		''' <summary>
		''' Hash a file using MD5.  Returns string with HEX representation
		''' </summary>
		Public Shared Function HashMd5File(ByVal pFilepath As String) As String
			Using reader As New System.IO.FileStream(pFilepath, IO.FileMode.Open, IO.FileAccess.Read)
				Using md5 As New System.Security.Cryptography.MD5CryptoServiceProvider
					Dim hash() As Byte = md5.ComputeHash(reader)
                    Return FYUtil.Utilities.ByteArrayToHexString(hash)
				End Using
			End Using
		End Function

        Public Shared Function CRC32(ByVal pFileName As String) As String
            If (Not IO.File.Exists(pFileName)) Then Throw New ArgumentNullException("File does not exist")
            Dim FS As IO.FileStream = Nothing
            Try
                FS = New IO.FileStream(pFileName, IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.Read, 8192)

                Dim CRC32Result As Long = &HFFFFFFFF
                Dim Buffer(4096) As Byte
                Dim ReadSize As Integer = 4096
                Dim Count As Integer = FS.Read(Buffer, 0, ReadSize)
                Dim CRC32Table(256) As Long
                Dim DWPolynomial As Integer = &HEDB88320
                Dim DWCRC As Long
                Dim i As Integer, j As Long, n As Long

                'Create CRC32 Table
                For i = 0 To 255
                    DWCRC = i
                    For j = 8 To 1 Step -1
                        If CBool(DWCRC And 1) Then
                            DWCRC = ((DWCRC And &HFFFFFFFE) \ 2&) And &H7FFFFFFF
                            DWCRC = DWCRC Xor DWPolynomial
                        Else
                            DWCRC = ((DWCRC And &HFFFFFFFE) \ 2&) And &H7FFFFFFF
                        End If
                    Next j
                    CRC32Table(i) = DWCRC
                Next i

                'Calcualting CRC32 Hash
                Do While (Count > 0)
                    For i = 0 To Count - 1
                        n = (CRC32Result And &HFF) Xor Buffer(i)
                        CRC32Result = ((CRC32Result And &HFFFFFF00) \ &H100) And &HFFFFFF
                        CRC32Result = CRC32Result Xor CRC32Table(CInt(n))
                    Next i
                    Count = FS.Read(Buffer, 0, ReadSize)
                Loop

                Return Hex(Not (CRC32Result))

            Catch ex As Exception
                Return ""
            Finally
                FS.Dispose()
            End Try
        End Function

	End Class

End Namespace
