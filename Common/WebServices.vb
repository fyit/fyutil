﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.ServiceModel

Public Class WebServices

    Public Enum eServiceSecurityMode
        NoEncryptionWithNoAuth = 10
        SslEncryptionWithNoAuth = 20
        SslEncryptionWithUsernameAuth = 30
    End Enum

    Public Class BindingOptions
        ''' <summary>MaxClockSkew is used as a security measure against replay attacks. Extend this timespan to be more tolerant of clock variation</summary>
        Public Property MaxClockSkew As TimeSpan = TimeSpan.FromMinutes(15)

        ''' <summary>Maximum size in bytes of messages that can be received by this host</summary>
        Public Property MaxReceivedMessageSize As Int32 = 1048576 '1 meg

        ''' <summary>Maximum length of arrays that can be processed</summary>
        Public Property MaxArrayLength As Int32 = 16384 'system default

        ''' <summary>Maximum length of strings that can be processed</summary>
        Public Property MaxStringContentLength As Int32 = 8192 'system default

        ''' <summary>Maximum characters allowed in a Name Table</summary>
        Public Property MaxNameTableCharCount As Int32 = 16384

        ''' <summary>Maximum nested node depth</summary>
        Public Property MaxDepth As Int32 = 32

        ''' <summary>Maximum bytes allowed returned for each read</summary>
        Public Property MaxBytesPerRead As Int32 = 8192

    End Class

    Protected Shared Function CreateStandardBinding(pSecurityMode As eServiceSecurityMode, pOptions As BindingOptions) As ServiceModel.Channels.Binding
        If (pOptions Is Nothing) Then pOptions = New BindingOptions

        Dim bnd As ServiceModel.Channels.Binding
        Dim readerQuotas As New Xml.XmlDictionaryReaderQuotas

        Select Case pSecurityMode
            Case eServiceSecurityMode.NoEncryptionWithNoAuth
                'add insecure http binding
                Dim bndBasic = New BasicHttpBinding(BasicHttpSecurityMode.None)
                bndBasic.MaxReceivedMessageSize = pOptions.MaxReceivedMessageSize
                bndBasic.ReaderQuotas.MaxArrayLength = pOptions.MaxArrayLength
                bndBasic.ReaderQuotas.MaxStringContentLength = pOptions.MaxStringContentLength
                bndBasic.ReaderQuotas.MaxNameTableCharCount = pOptions.MaxNameTableCharCount
                bndBasic.ReaderQuotas.MaxDepth = pOptions.MaxDepth
                bndBasic.ReaderQuotas.MaxBytesPerRead = pOptions.MaxBytesPerRead
                bnd = bndBasic

            Case eServiceSecurityMode.SslEncryptionWithNoAuth
                'add https binding, no client auth
                Dim bndWs As New WSHttpBinding(SecurityMode.Transport)
                bndWs.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
                bndWs.MaxReceivedMessageSize = pOptions.MaxReceivedMessageSize
                bndWs.ReaderQuotas.MaxArrayLength = pOptions.MaxArrayLength
                bndWs.ReaderQuotas.MaxStringContentLength = pOptions.MaxStringContentLength
                bndWs.ReaderQuotas.MaxNameTableCharCount = pOptions.MaxNameTableCharCount
                bndWs.ReaderQuotas.MaxDepth = pOptions.MaxDepth
                bndWs.ReaderQuotas.MaxBytesPerRead = pOptions.MaxBytesPerRead
                bnd = bndWs

            Case eServiceSecurityMode.SslEncryptionWithUsernameAuth
                'add https binding, client auth in message
                Dim bndWs As New WSHttpBinding(SecurityMode.TransportWithMessageCredential)
                bndWs.Security.Message.ClientCredentialType = MessageCredentialType.UserName
                bndWs.MaxReceivedMessageSize = pOptions.MaxReceivedMessageSize
                bndWs.ReaderQuotas.MaxArrayLength = pOptions.MaxArrayLength
                bndWs.ReaderQuotas.MaxStringContentLength = pOptions.MaxStringContentLength
                bndWs.ReaderQuotas.MaxNameTableCharCount = pOptions.MaxNameTableCharCount
                bndWs.ReaderQuotas.MaxDepth = pOptions.MaxDepth
                bndWs.ReaderQuotas.MaxBytesPerRead = pOptions.MaxBytesPerRead
                bnd = bndWs

            Case Else
                Throw New NotImplementedException("Unknown ServiceSecurityMode")
        End Select

        If (Not pOptions.MaxClockSkew.Equals(TimeSpan.FromMinutes(5))) Then
            '5 minutes is the default, don't do this unless it's different
            'this extends the clock skew tolerance, but also disables the security context (token auth). this makes client incompatible
            'TODO: supposedly there is a way to do this without disabling the security context by modifying the "bootstrap" parameters
            If (bnd.GetType() = GetType(WSHttpBinding)) Then
                DirectCast(bnd, WSHttpBinding).Security.Message.EstablishSecurityContext = False
            End If
            Dim bndCustom As New Channels.CustomBinding(bnd)
            Dim elSec As Channels.TransportSecurityBindingElement = bndCustom.Elements.Find(Of Channels.TransportSecurityBindingElement)()
            If (elSec IsNot Nothing) Then
                elSec.LocalClientSettings.MaxClockSkew = pOptions.MaxClockSkew
                elSec.LocalServiceSettings.MaxClockSkew = pOptions.MaxClockSkew
            End If

            Return bndCustom
        End If

        Return bnd
    End Function

#Region "Client"

    ''' <summary>
    ''' Globally disables all client certificate validation
    ''' </summary>
    Public Shared Sub DisableClientCertificationValidation()
        System.Net.ServicePointManager.ServerCertificateValidationCallback = (Function(sender, certificate, chain, sslPolicyErrors) True)
    End Sub

    Public Shared Function CreateClientBinding(pSecurityMode As eServiceSecurityMode, Optional pOptions As BindingOptions = Nothing) As ServiceModel.Channels.Binding
        Return CreateStandardBinding(pSecurityMode, pOptions)
    End Function

#End Region


#Region "Server"

    Public Enum eAuthProvider
        None = 0
        MembershipProvider = 10
    End Enum

    Public Shared Sub ConfigureServiceHost(pioHost As ServiceHost, pServiceContract As Type, pSecurityMode As eServiceSecurityMode, Optional pOptions As BindingOptions = Nothing, Optional pAuthProvider As eAuthProvider = eAuthProvider.None)
        ConfigureServiceHostImpl(pioHost, pServiceContract, pSecurityMode, pOptions, pAuthProvider, pDebug:=False)
    End Sub

    Public Shared Sub ConfigureDebugServiceHost(pioHost As ServiceHost, pServiceContract As Type, pSecurityMode As eServiceSecurityMode, Optional pOptions As BindingOptions = Nothing, Optional pAuthProvider As eAuthProvider = eAuthProvider.None)
        ConfigureServiceHostImpl(pioHost, pServiceContract, pSecurityMode, pOptions, pAuthProvider, pDebug:=True)
        FYUtil.Logging.LogWarning("DebugServiceHost created")
    End Sub





    Protected Shared Sub ConfigureServiceHostImpl(pioHost As ServiceHost, pServiceContract As Type, pSecurityMode As eServiceSecurityMode, pOptions As BindingOptions, pAuthProvider As eAuthProvider, pDebug As Boolean)
        If (pDebug) Then
            'add mex endpoint
            Dim behMeta As Description.ServiceMetadataBehavior = pioHost.Description.Behaviors.OfType(Of Description.ServiceMetadataBehavior)().FirstOrDefault
            If (behMeta Is Nothing) Then
                behMeta = New Description.ServiceMetadataBehavior()
                pioHost.Description.Behaviors.Add(behMeta)
            End If
            behMeta.HttpGetEnabled = True

            'TODO: this might blow up if default endpoints are configured including mex
            Dim bndMex = Description.MetadataExchangeBindings.CreateMexHttpBinding()
            pioHost.AddServiceEndpoint(GetType(Description.IMetadataExchange), bndMex, "mex")

            'enable exception details
            Dim behDebug As Description.ServiceDebugBehavior = pioHost.Description.Behaviors.OfType(Of Description.ServiceDebugBehavior).First
            behDebug.IncludeExceptionDetailInFaults = True

            Dim behAudit As Description.ServiceSecurityAuditBehavior = pioHost.Description.Behaviors.OfType(Of Description.ServiceSecurityAuditBehavior)().FirstOrDefault
            If (behAudit Is Nothing) Then
                behAudit = New Description.ServiceSecurityAuditBehavior()
                pioHost.Description.Behaviors.Add(behAudit)
            End If
            behAudit.AuditLogLocation = AuditLogLocation.Application
            behAudit.MessageAuthenticationAuditLevel = AuditLevel.Failure
            behAudit.ServiceAuthorizationAuditLevel = AuditLevel.Failure
            behAudit.SuppressAuditFailure = True
        End If

        Dim bnd As Channels.Binding = CreateStandardBinding(pSecurityMode, pOptions)

        ConfigureServiceHostAuthProvider(pioHost, pAuthProvider)

        'create the endpoint
        Dim ep = pioHost.AddServiceEndpoint(pServiceContract, bnd, "")

    End Sub

    Protected Shared Sub ConfigureServiceHostAuthProvider(pioHost As ServiceHost, pAuthProvider As eAuthProvider)
        Select Case pAuthProvider
            Case eAuthProvider.MembershipProvider
                'configure host to use the configured RoleProvider and MembershipProvider from web.config
                Dim behAuth As Description.ServiceAuthorizationBehavior = pioHost.Description.Behaviors.OfType(Of Description.ServiceAuthorizationBehavior).First
                behAuth.PrincipalPermissionMode = Description.PrincipalPermissionMode.UseAspNetRoles
                pioHost.Credentials.UserNameAuthentication.UserNamePasswordValidationMode = ServiceModel.Security.UserNamePasswordValidationMode.MembershipProvider
            Case eAuthProvider.None
                'do nothing
            Case Else
                Throw New NotImplementedException("Unknown AuthProvider")
        End Select
    End Sub

#End Region

End Class
