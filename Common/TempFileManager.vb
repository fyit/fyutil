﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class TempFileManager
    Implements IDisposable

    Private _basePath As String
    Private _dirsCreated As New List(Of String)
    Private _filesCreated As New List(Of String)

    ''' <summary>
    ''' Creates a new manager to handle creation and removal of temporary files
    ''' </summary>
    ''' <param name="pBasePath">Root directory to store temporary files. If not specified, uses system temp directory.</param>
    ''' <param name="pSubDir">Subdirectory under the root to store files.</param>
    ''' <remarks></remarks>
    Public Sub New(Optional pBasePath As String = Nothing, Optional pSubDir As String = Nothing)
        _basePath = pBasePath
        If (String.IsNullOrEmpty(_basePath)) Then _basePath = IO.Path.GetTempPath()
        If (Not IO.Path.IsPathRooted(_basePath)) Then Throw New ArgumentException("TempFileManager: Invalid root path")
        If (Not String.IsNullOrEmpty(pSubDir)) Then _basePath = IO.Path.Combine(_basePath, pSubDir)
        If (Not _basePath.EndsWith("\")) Then _basePath += "\"
        If (Not IO.Directory.Exists(_basePath)) Then IO.Directory.CreateDirectory(_basePath)
    End Sub

    Public ReadOnly Property BasePath As String
        Get
            Return _basePath
        End Get
    End Property

    ''' <summary>
    ''' Number of random characters to append to a TempFilename
    ''' </summary>
    Public Property RandomStringLength As Int32 = 8

    ''' <summary>
    ''' Add a new file to the collection of managed files
    ''' </summary>
    ''' <param name="pTempFilename"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AddTempFilename(pTempFilename As String) As String
        If (Not IO.Path.IsPathRooted(pTempFilename)) Then pTempFilename = IO.Path.Combine(_basePath, pTempFilename)
        If (Not pTempFilename.StartsWith(_basePath)) Then Throw New ArgumentException("TempFileManager: File must be stored under BasePath")

        _filesCreated.Add(pTempFilename)
        Return pTempFilename
    End Function

    ''' <summary>
    ''' Create a unique filename and add to the collection of managed files
    ''' </summary>
    ''' <param name="pBaseName"></param>
    ''' <param name="pExt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTempFilename(Optional pBaseName As String = "temp", Optional pExt As String = "tmp") As String
        If (String.IsNullOrEmpty(pBaseName)) Then pBaseName = "temp"
        If (String.IsNullOrEmpty(pExt)) Then pExt = "tmp"
        If (pExt.StartsWith(".")) Then pExt = pExt.TrimStart("."c)

        Dim fname As String = ""
        Dim fpath As String = ""
        Do
            fname = String.Format("{0}_{1}.{2}", pBaseName, Utilities.RandomString(RandomStringLength, Utilities.CHARSET_UPPERALPHANOVOWELS), pExt)
            fpath = IO.Path.Combine(_basePath, fname)
        Loop Until ((Not IO.File.Exists(fpath)) AndAlso (Not _filesCreated.Contains(fpath)))

        _filesCreated.Add(fpath)

        Return fpath
    End Function

    Public Sub DeleteTempFiles()
        For Each tmpFilename In _filesCreated
            Try
                If (IO.File.Exists(tmpFilename)) Then IO.File.Delete(tmpFilename)
            Catch ex As Exception
                'ignore
            End Try
        Next

        For Each tmpDir In _dirsCreated
            Try
                IO.Directory.Delete(tmpDir, True)
            Catch ex As Exception
                'ignore
            End Try
        Next
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            Call DeleteTempFiles()
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(False)
        MyBase.Finalize()
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
