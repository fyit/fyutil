﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Configuration
Imports System.Diagnostics
Imports System.Net.Mail
Imports System.Web
Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Text

''' <summary>
''' Logging utility
''' </summary>
''' 

Public Class Logging

    Public Class EventData
        Public Severity As eSeverity
        Public Category As String
        Public Message As String
        Public Timestamp As DateTime
        Public TimestampLocal As DateTime
        Public Source As String
        Public SourceObject As String
        Public SourceMethod As String
        Public SourceFileLocation As String
        Public StackTrace As String
        Public User As String
        Public ExtraInfo As String
    End Class

    Public Enum eSeverity
        NotSet = 0
        Trace = 10
        Debug = 20
        Info = 40
        Important = 50
        Warning = 70
        [Error] = 80
        Fatal = 90
    End Enum

    Public Enum eLogLevel
        All = 0
        Trace = 10
        Debug = 20
        Info = 40
        Important = 50
        Warning = 70
        [Error] = 80
        Fatal = 90
        Off = 99
    End Enum

    Public Enum eFileCreationMode
        OneFile = 1
        OneFilePerRun = 2
        OneFilePerDay = 3
    End Enum

    ''' <summary>Event thrown when logged message is higher severity than MasterLoggingLevel. Includes entire EventData object</summary>
    Public Shared Event LogEvent(pEventData As EventData)
    ''' <summary>Event thrown when logged message is higher severity than MasterLoggingLevel. Includes only the Message from the EventData object</summary>
    Public Shared Event LogMessageEvent(pMessage As String)

    ''' <summary></summary>
    Public Delegate Sub GatherDataDelegate(pEventData As EventData)
    ''' <summary></summary>
    Public Delegate Function EventDataFormatterDelegate(pEventData As EventData) As String
    ''' <summary></summary>
    Public Delegate Sub EventDataTransmitterDelegate(pEventData As EventData)

    ''' <summary></summary>
    Public Shared MasterLoggingLevel As eLogLevel = eLogLevel.Error

    ''' <summary>Always include extended data (class, file, line, etc) in log messages</summary>
    Public Shared IncludeExtendedData As Boolean = False

    ''' <summary>Ignore any settings from appSettings in the config file. Only use this option to disallow user configuration of settings. Has no effect after Init is called</summary>
    Public Shared IgnoreAppSettings As Boolean = False

    ''' <summary>Block duplicate messages for the specified time period</summary>
    Public Shared EmailFloodProtectionBlockPeriod As TimeSpan? = TimeSpan.FromMinutes(1)

    ''' <summary>Keep unused log files for this time period</summary>
    Public Shared FileArchivePeriod As TimeSpan? = TimeSpan.FromDays(90)

    ''' <summary>Naming convention for log files</summary>
    Public Shared FileCreationMode As eFileCreationMode = eFileCreationMode.OneFile

    ''' <summary>Name of the app, for documentation</summary>
    Public Shared AppName As String

    ''' <summary>Environment of the app, for documentation</summary>
    Public Shared AppEnvironment As String

    ''' <summary>Method to gather data into an EventData object</summary>
    Public Shared GatherDataHelper As GatherDataDelegate = AddressOf DefaultGatherData

    ''' <summary>Method to format an EventData object as a string</summary>
    Public Shared FormatEventHelper As EventDataFormatterDelegate = AddressOf DefaultFormatter


    Protected Shared _lockTarget As New Object()
    Protected Shared _started As Boolean = False

    Protected Shared _enableFileSink As Boolean = False
    Protected Shared _fileLoggingLevel As eLogLevel? = Nothing
    Protected Shared _logFile As String

    Protected Shared _enableEmailSink As Boolean = False
    Protected Shared _emailLoggingLevel As eLogLevel? = eLogLevel.Off
    Protected Shared _emailTo As String
    Protected Shared _emailFrom As String
    Protected Shared _smtpServer As String
    Protected Shared _emailTransmitter As EventDataTransmitterDelegate = AddressOf SendEmail

    Private Sub New()
        'no instantiation
    End Sub

    Protected Shared Sub InitImpl(pImplicit As Boolean)
        SyncLock _lockTarget
            If (Not _started) Then
                If (AppName Is Nothing) Then AppName = FYUtil.AppSettingsHelper.GetString("App.Name", "UntitledApp")
                If (AppEnvironment Is Nothing) Then AppEnvironment = FYUtil.AppSettingsHelper.GetString("App.Environment", "UnknownEnvironment")

                If (Not IgnoreAppSettings) Then
                    Dim lvl = AppSettingsHelper.GetString("Logging.LogLevel")
                    MasterLoggingLevel = ParseLoggingLevel(lvl, MasterLoggingLevel).Value

                    'enable logfile logging if log filename set in config
                    Dim logFn = AppSettingsHelper.GetString("Logging.LogFile")
                    If (Not String.IsNullOrEmpty(logFn)) Then
                        EnableFileLogSink(Nothing, logFn)
                    End If

                    'override logfile level if set in config file
                    Dim logFl = AppSettingsHelper.GetString("Logging.LogFileLevel")
                    If (Not String.IsNullOrEmpty(logFl)) Then
                        _fileLoggingLevel = ParseLoggingLevel(logFl, MasterLoggingLevel)
                    End If

                    'enable email logging if "to" set in config file
                    Dim emTo = AppSettingsHelper.GetString("Logging.LogEmailTo")
                    If (Not String.IsNullOrEmpty(emTo)) Then
                        Dim emFrom As String = AppSettingsHelper.GetString("Logging.LogEmailFrom")
                        Dim emSvr As String = AppSettingsHelper.GetString("Logging.SmtpServer")
                        EnableEmailLogSink(Nothing, emTo, emFrom, emSvr)
                    End If

                    'override email level if set in config file
                    Dim emFl = AppSettingsHelper.GetString("Logging.LogEmailLevel")
                    If (Not String.IsNullOrEmpty(emFl)) Then
                        _emailLoggingLevel = ParseLoggingLevel(emFl, _emailLoggingLevel)
                    End If
                End If

                If (Not _fileLoggingLevel.HasValue) Then
                    'if no level set, set to the master level
                    _fileLoggingLevel = MasterLoggingLevel
                End If

                'If (_enableFileSink AndAlso (MasterLoggingLevel < eLogLevel.Off) AndAlso (_fileLoggingLevel < eLogLevel.Off)) Then
                '    Try
                '        Using log As New StreamWriter(GetLogFilePath, True)
                '            log.WriteLine(String.Format("{0:yyyyMMdd.HHmmss}: Logging started at {1} level", DateTime.Now, _fileLoggingLevel))
                '        End Using
                '    Catch ex As Exception
                '    End Try
                'End If


                _started = True
            Else
                'this catches an explicit call to Init after Init has already been called (explicitly or implicitly)
                If (Not pImplicit) Then Throw New Exception("FYUtil.Logging: Duplicate call to Init()")
            End If
        End SyncLock
    End Sub




    ''' <summary>Enable sending log messages to a log file, supports custom formatting</summary>
    Public Shared Sub EnableFileLogSink(pMinLogLevel As eLogLevel?, pFilePath As String)
        If (pMinLogLevel.HasValue) Then _fileLoggingLevel = pMinLogLevel
        _enableFileSink = True

        _logFile = pFilePath
        If (Not _logFile.EndsWith(".log")) Then _logFile += ".log"
        If (Not IO.Path.IsPathRooted(_logFile)) Then
            _logFile = IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), _logFile)
        End If
    End Sub

    ''' <summary>Enable sending log messages to email, supports custom formatting</summary>
    Public Shared Sub EnableEmailLogSink(pMinLogLevel As eLogLevel?, pToAddress As String, pFromAddress As String, Optional pServer As String = Nothing)
        If (pMinLogLevel.HasValue) Then _emailLoggingLevel = pMinLogLevel
        _enableEmailSink = True

        _emailTo = pToAddress
        _emailFrom = If(pFromAddress, "error@FYUtil.Logging")
        _smtpServer = pServer
    End Sub

    Protected Shared Function ParseLoggingLevel(pLevelString As String, pDefault As eLogLevel?) As eLogLevel?
        If (Not String.IsNullOrEmpty(pLevelString)) Then
            Dim res As eLogLevel
            If ([Enum].TryParse(pLevelString, res)) Then
                Return res
            Else
                pLevelString = pLevelString.ToUpper
                If (pLevelString.StartsWith("A")) Then
                    Return eLogLevel.All
                ElseIf (pLevelString.StartsWith("T")) Then
                    Return eLogLevel.Trace
                ElseIf (pLevelString.StartsWith("D")) Then
                    Return eLogLevel.Debug
                ElseIf (pLevelString.StartsWith("IN")) Then
                    Return eLogLevel.Info
                ElseIf (pLevelString.StartsWith("IM")) Then
                    Return eLogLevel.Important
                ElseIf (pLevelString.StartsWith("W")) Then
                    Return eLogLevel.Warning
                ElseIf (pLevelString.StartsWith("E")) Then
                    Return eLogLevel.Error
                ElseIf (pLevelString.StartsWith("F")) Then
                    Return eLogLevel.Fatal
                ElseIf (pLevelString.StartsWith("O")) Then
                    Return eLogLevel.Off
                ElseIf (pLevelString.StartsWith("V")) Then
                    'handle the old "verbose" level
                    Return eLogLevel.Debug
                End If
            End If
        End If

        Return pDefault
    End Function

    Protected Enum eBypassLoggingLevel
        None = 0
        ForEvents = 10
        ForEventsAndFile = 20
        ForEventsAndFileAndEmail = 30
        All = 100
    End Enum

    ''' <summary>Logs a EventData object</summary>
    Protected Shared Sub LogMessage(pEventData As EventData, Optional pBypassLoggingLevel As eBypassLoggingLevel = eBypassLoggingLevel.None, Optional pGatherExtendedData As Boolean? = Nothing)
        If (Not _started) Then InitImpl(pImplicit:=True)

        If ((pEventData.Severity < MasterLoggingLevel) AndAlso (pBypassLoggingLevel = eBypassLoggingLevel.None)) Then Exit Sub

        'force extended on error
        If (pEventData.Severity >= eSeverity.Error) Then pGatherExtendedData = True

        If (GatherDataHelper IsNot Nothing) Then GatherDataHelper(pEventData)
        If (pGatherExtendedData = True) Then
            GatherExtendedData(pEventData)
        ElseIf ((pGatherExtendedData Is Nothing) AndAlso IncludeExtendedData) Then
            GatherExtendedData(pEventData)
            'remove stacktrace, clutters the log
            pEventData.StackTrace = Nothing
        End If

        RaiseEvent LogEvent(pEventData)

        If (LogMessageEventEvent IsNot Nothing) Then
            RaiseEvent LogMessageEvent(If(FormatEventHelper IsNot Nothing, FormatEventHelper(pEventData), pEventData.Message))
        End If

        If (_enableFileSink AndAlso ((pEventData.Severity >= _fileLoggingLevel) OrElse (pBypassLoggingLevel >= eBypassLoggingLevel.ForEventsAndFile))) Then
            If System.Threading.Monitor.TryEnter(_lockTarget, 1000) Then
                Try
                    Using log As New StreamWriter(GetLogFilePath, True)
                        log.WriteLine(If(FormatEventHelper IsNot Nothing, FormatEventHelper(pEventData), pEventData.Message))
                    End Using
                Catch
                Finally
                    System.Threading.Monitor.Exit(_lockTarget)
                End Try
            Else
                Debug.WriteLine("FYUtil.Logging.LogMessage: Timeout acquiring lock")
            End If
        End If

        If (_enableEmailSink AndAlso ((pEventData.Severity >= _emailLoggingLevel) OrElse (pBypassLoggingLevel >= eBypassLoggingLevel.ForEventsAndFileAndEmail))) Then
            _emailTransmitter.BeginInvoke(pEventData, Nothing, Nothing)
            'SendEmail(pEventData)
        End If

        'EventLog.WriteEntry(AppName, message, EventLogEntryType.Error);

        'write to EntLib logging system
        'Microsoft.Practices.EnterpriseLibrary.Logging.LogEntry logent = new Microsoft.Practices.EnterpriseLibrary.Logging.LogEntry();
        'logent.Message = entry.Message;
        'logent.Categories.Add(entry.Category);
        'Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(logent);

        'write to .net trace.axd
        'System.Web.HttpContext.Current.Trace.Write(entry.Category, entry.Message)
    End Sub

    ''' <summary>Logs a message at the specified severity</summary>
    Public Shared Sub LogMessage(pMessage As String, pSeverity As eSeverity, Optional pCategory As String = Nothing)
        'since this function can exit before logging, need to make sure logging is started
        If (Not _started) Then InitImpl(pImplicit:=True)

        If (pSeverity < MasterLoggingLevel) Then Exit Sub
        Dim evt As New EventData()
        evt.Message = pMessage
        evt.Severity = pSeverity
        evt.Category = pCategory
        LogMessage(evt)
    End Sub

    ''' <summary>Logs a message at the specified severity, bypassing LogLevel filters. NOTE: does not bypass email LogLevel</summary>
    Public Shared Sub LogUnconditional(pMessage As String, Optional pSeverity As eSeverity = eSeverity.Important, Optional pCategory As String = Nothing)
        Dim evt As New EventData()
        evt.Message = pMessage
        evt.Severity = pSeverity
        evt.Category = pCategory
        LogMessage(evt, pBypassLoggingLevel:=eBypassLoggingLevel.ForEventsAndFile)
    End Sub

    ''' <summary>Logs a message at the specified severity, bypassing LogLevel filters, including email LogLevel</summary>
    Public Shared Sub LogUnconditionalEmail(pMessage As String, Optional pSeverity As eSeverity = eSeverity.Important, Optional pCategory As String = Nothing)
        Dim evt As New EventData()
        evt.Message = pMessage
        evt.Severity = pSeverity
        evt.Category = pCategory
        LogMessage(evt, pBypassLoggingLevel:=eBypassLoggingLevel.All)
    End Sub

    ''' <summary>Logs a message at the error level</summary>
    Public Shared Sub LogError(pMessage As String, Optional pCategory As String = Nothing)
        'since this function can exit before logging, need to make sure logging is started
        If (Not _started) Then InitImpl(pImplicit:=True)

        If (eSeverity.Error < MasterLoggingLevel) Then Exit Sub
        Dim evt As New EventData()
        evt.Message = pMessage
        evt.Severity = eSeverity.Error
        evt.Category = pCategory
        LogMessage(evt, pGatherExtendedData:=True)
    End Sub

    ''' <summary>Logs an exception as an error</summary>
    Public Shared Sub LogError(pEx As Exception, Optional pAddlMessage As String = Nothing, Optional pCategory As String = Nothing)
        'since this function can exit before logging, need to make sure logging is started
        If (Not _started) Then InitImpl(pImplicit:=True)

        If (eSeverity.Error < MasterLoggingLevel) Then Exit Sub
        Dim evt As New EventData()
        Dim exMessage = pEx.Message
        Dim innerEx = pEx.InnerException
        While (innerEx IsNot Nothing)
            exMessage = String.Format("{0} (inner: {1})", exMessage, innerEx.Message)
            innerEx = innerEx.InnerException
        End While
        If (pAddlMessage Is Nothing) Then
            evt.Message = exMessage
        Else
            evt.Message = String.Format("{0} ({1})", exMessage, pAddlMessage)
        End If
        evt.Severity = eSeverity.Error
        evt.Category = pCategory
        evt.Source = pEx.Source
        evt.StackTrace = pEx.StackTrace
        LogMessage(evt, pGatherExtendedData:=True)
    End Sub

    ''' <summary>Logs a message at the warning level</summary>
    Public Shared Sub LogWarning(pMessage As String, Optional pCategory As String = Nothing)
        LogMessage(pMessage, eSeverity.Warning, pCategory)
    End Sub

    ''' <summary>Logs a message at the important level</summary>
    Public Shared Sub LogImportant(pMessage As String, Optional pCategory As String = Nothing)
        LogMessage(pMessage, eSeverity.Important, pCategory)
    End Sub

    ''' <summary>Logs a message at the informational level</summary>
    Public Shared Sub LogInformational(pMessage As String, Optional pCategory As String = Nothing)
        LogMessage(pMessage, eSeverity.Info, pCategory)
    End Sub

    ''' <summary>Logs a message at the debug level</summary>
    Public Shared Sub LogDebug(pMessage As String, Optional pCategory As String = Nothing)
        LogMessage(pMessage, eSeverity.Debug, pCategory)
    End Sub

    ''' <summary>Logs a message at the debug level, with extended information</summary>
    Public Shared Sub LogDebugFull(pMessage As String, Optional pCategory As String = Nothing)
        'since this function can exit before logging, need to make sure logging is started
        If (Not _started) Then InitImpl(pImplicit:=True)

        If (eSeverity.Debug < MasterLoggingLevel) Then Exit Sub
        Dim evt As New EventData()
        evt.Message = pMessage
        evt.Severity = eSeverity.Debug
        evt.Category = pCategory
        LogMessage(evt, pGatherExtendedData:=True)
    End Sub

    ''' <summary>Logs a message at the trace level</summary>
    Public Shared Sub LogTrace(pMessage As String, Optional pCategory As String = Nothing)
        LogMessage(pMessage, eSeverity.Trace, pCategory)
    End Sub

    ''' <summary>Constructs a trace message including the calling class and method</summary>
    Public Shared Function CreateTraceMessage(pMessage As String) As String
        'go back one to whatever called this function
        Dim callerFrame As New StackFrame(1, fNeedFileInfo:=True)
        Dim meth As Reflection.MethodBase = callerFrame.GetMethod()
        Return String.Format("{0}.{1} ({2}): {3}", meth.ReflectedType.Name, meth.Name, callerFrame.GetFileLineNumber, pMessage)
    End Function

    ''' <summary>Determine if logging is currently enabled at the specified level or above</summary>
    Public Shared Function IsLevel(pSeverity As eSeverity) As Boolean
        Return (pSeverity >= MasterLoggingLevel)
    End Function

    ''' <summary>Determine if logging is currently enabled at the trace level or above</summary>
    Public Shared Function IsTraceLevel() As Boolean
        Return (eSeverity.Trace >= MasterLoggingLevel)
    End Function

    ''' <summary>Determine if logging is currently enabled at the debug level or above</summary>
    Public Shared Function IsDebugLevel() As Boolean
        Return (eSeverity.Debug >= MasterLoggingLevel)
    End Function

    Public Shared Function GetExceptionDetails(pException As Exception, Optional pIncludeStackTrace As Boolean = False, Optional pMaxLevels As Int32 = 5) As String
        Dim sb As New Text.StringBuilder
        Dim level As Int32 = 0
        Dim curException As Exception = pException
        Do
            sb.Append(curException.Message)
            If pIncludeStackTrace Then sb.AppendLine(" " & curException.StackTrace)
            sb.Append(vbCrLf)
            curException = curException.InnerException
            level += 1
        Loop Until curException Is Nothing OrElse level > pMaxLevels
        Return sb.ToString
    End Function





    Protected Shared Sub DefaultGatherData(pData As EventData)
        pData.Timestamp = DateTime.UtcNow
        pData.TimestampLocal = DateTime.Now
    End Sub

    Protected Shared Sub GatherExtendedData(pData As EventData)
        Dim st As Diagnostics.StackTrace = Nothing
        If (String.IsNullOrEmpty(pData.StackTrace)) Then
            If (st Is Nothing) Then st = GetBackStackTrace()
            If (st IsNot Nothing) Then
                pData.StackTrace = st.ToString
            End If
        End If

        If (String.IsNullOrEmpty(pData.SourceObject)) Then
            If (st Is Nothing) Then st = GetBackStackTrace()
            If (st IsNot Nothing) Then
                Dim callerFrame As StackFrame = st.GetFrame(0)
                pData.SourceObject = callerFrame.GetMethod().ReflectedType.Name
                pData.SourceMethod = callerFrame.GetMethod().Name
                pData.SourceFileLocation = String.Format("{0}:{1}", callerFrame.GetFileName, callerFrame.GetFileLineNumber)
            End If
        End If

        If (String.IsNullOrEmpty(pData.User)) Then
            Try
                Dim user As String = System.Threading.Thread.CurrentPrincipal.Identity.Name
                pData.User = user
            Catch
            End Try
        End If
    End Sub

    Protected Shared Function GetBackStackTrace() As Diagnostics.StackTrace
        Dim st As New Diagnostics.StackTrace(fNeedFileInfo:=True)
        'walk back the stack until we get out of this class
        For i As Int32 = 0 To st.FrameCount
            If (st.GetFrame(i).GetMethod().ReflectedType <> GetType(FYUtil.Logging)) Then Return New Diagnostics.StackTrace(i, fNeedFileInfo:=True)
        Next
        Return Nothing
    End Function


    Protected Shared Sub SendEmail(pEventData As EventData)
        If (String.IsNullOrWhiteSpace(_emailTo)) Then Return
        If (IsDuplicateEmailMessage(pEventData.Message, EmailFloodProtectionBlockPeriod)) Then Return

        Dim svr As String = _smtpServer
        If (String.IsNullOrWhiteSpace(svr)) Then
            Try
                Dim addr As New MailAddress(_emailTo)
                Dim mxs As IEnumerable(Of String) = Email.GetMxRecords(addr.Host)
                If (mxs.Count > 0) Then svr = mxs(0)
            Catch ex As Exception
                svr = ""
            End Try
        End If
        If (String.IsNullOrWhiteSpace(svr)) Then svr = "127.0.0.1"

        Try
            Dim subject As String = String.Format("{0} message from {1}.{2}", pEventData.Severity.ToString, AppName, AppEnvironment)

            Dim smtp As New SmtpClient(svr)
            Dim mm As New MailMessage()
            mm.From = New MailAddress(_emailFrom)
            mm.Subject = subject
            mm.Body = If(FormatEventHelper IsNot Nothing, FormatEventHelper(pEventData), pEventData.Message)
            For Each item In _emailTo.Replace(",", ";").Split(";"c)
                mm.To.Add(item)
            Next
            smtp.Send(mm)
        Catch ex As Exception
            If System.Threading.Monitor.TryEnter(_lockTarget, 1000) Then
                Try
                    Using log As New StreamWriter(GetLogFilePath, True)
                        log.WriteLine(String.Format("{0:yyyyMMdd.HHmmss}: [FYUtil.Logging] Error sending email notification: {1}", DateTime.Now, ex.Message))
                    End Using
                Catch
                    'can't do anything here...
                Finally
                    System.Threading.Monitor.Exit(_lockTarget)
                End Try
            Else
                Debug.WriteLine("FYUtil.Logging.LogMessage: Timeout acquiring lock")
            End If
        End Try
    End Sub


    Private Shared _emailDuplicateCheck As New Dictionary(Of String, DateTime)
    Private Shared _lastEmailTime As DateTime = DateTime.MinValue
    Protected Shared Function IsDuplicateEmailMessage(pMessage As String, pBlockTime As TimeSpan?) As Boolean
        If (pBlockTime.HasValue) Then
            SyncLock _emailDuplicateCheck
                Dim dtNow = DateTime.Now
                If (dtNow < _lastEmailTime.Add(pBlockTime.Value)) Then
                    'within block period, check
                    If (_emailDuplicateCheck.ContainsKey(pMessage)) Then
                        If (dtNow < _emailDuplicateCheck.Item(pMessage).Add(pBlockTime.Value)) Then
                            'previous message is within block time, block it
                            Return True
                        Else
                            'previous message is outside block time, remove record
                            _emailDuplicateCheck.Remove(pMessage)
                        End If
                    End If
                ElseIf (_emailDuplicateCheck.Count > 0) Then
                    'outside of the block period, can clear the list
                    _emailDuplicateCheck.Clear()
                End If

                _emailDuplicateCheck.Add(pMessage, dtNow)
                _lastEmailTime = dtNow
                Return False
            End SyncLock
        Else
            Return False
        End If
    End Function

    Protected Shared Function DefaultFormatter(pEventData As EventData) As String
        Dim message As New StringBuilder()
        message.Append(String.Format("{0:yyyyMMdd.HHmmss}: ", pEventData.TimestampLocal))
        message.AppendFormat("[{0}] ", pEventData.Severity.ToString)
        If (pEventData.Category IsNot Nothing) Then
            message.AppendFormat("[{0}] ", pEventData.Category)
        End If
        If (pEventData.SourceObject IsNot Nothing) Then
            message.AppendFormat("[{0}.{1} ({2})] ", pEventData.SourceObject, pEventData.SourceMethod, pEventData.SourceFileLocation)
        End If
        message.Append(pEventData.Message)
        If (pEventData.Source IsNot Nothing) Then
            message.AppendLine()
            message.AppendFormat("Error source: {0}", pEventData.Source)
        End If
        If (pEventData.StackTrace IsNot Nothing) Then
            message.AppendLine()
            message.AppendLine("Stacktrace:")
            message.AppendLine(pEventData.StackTrace)
            message.Append("-----------------------------------------------------------------------")
        End If
        Return message.ToString()
    End Function

    Protected Shared Function DefaultEmailFormatter(pEventData As EventData) As String
        Dim message As New StringBuilder()
        message.AppendFormat("Message: {0}", pEventData.Message)
        message.AppendLine()
        message.AppendFormat("Date: {0:yyyyMMdd.HHmmss}", pEventData.TimestampLocal)
        message.AppendLine()
        message.AppendFormat("Severity: {0}", pEventData.Severity.ToString)
        message.AppendLine()
        If (pEventData.Category IsNot Nothing) Then
            message.AppendFormat("Category: {0}", pEventData.Category)
            message.AppendLine()
        End If
        If (pEventData.SourceObject IsNot Nothing) Then
            message.AppendFormat("Source: {0}.{1} ({2})", pEventData.SourceObject, pEventData.SourceMethod, pEventData.SourceFileLocation)
            message.AppendLine()
        End If
        If (pEventData.Source IsNot Nothing) Then
            message.AppendFormat("Error source: {0}", pEventData.Source)
            message.AppendLine()
        End If
        If (pEventData.StackTrace IsNot Nothing) Then
            message.AppendLine("Stacktrace:")
            message.AppendLine(pEventData.StackTrace)
        End If
        Return message.ToString()
    End Function

    Protected Shared _currentLogFilepath As String
    Public Shared Function GetLogFilePath() As String
        Dim newFn As String
        If (FileCreationMode = eFileCreationMode.OneFilePerDay) Then
            'file name can change during run
            Dim filePathPattern = CreateFilepathsetPattern(_logFile)
            newFn = String.Format(filePathPattern, DateTime.Now.ToString("yyyyMMdd"))
        Else
            'file name fixed during run
            If (String.IsNullOrEmpty(_currentLogFilepath)) Then
                If (FileCreationMode = eFileCreationMode.OneFilePerRun) Then
                    Dim filePathPattern = CreateFilepathsetPattern(_logFile)
                    newFn = String.Format(filePathPattern, DateTime.Now.ToString("yyyyMMdd-hhmmss"))
                Else
                    'one file, fixed filename
                    newFn = _logFile
                End If
            Else
                newFn = _currentLogFilepath
            End If
        End If

        If (Not newFn.Equals(_currentLogFilepath)) Then
            'if filename has changed, run cleanup procedure
            CleanupFiles(_logFile)

            _currentLogFilepath = newFn
        End If

        Return _currentLogFilepath
    End Function

    Protected Shared Sub CleanupFiles(pBaseFilename As String)
        Dim filePattern = CreateFilenamesetPattern(pBaseFilename)
        For Each fn As String In IO.Directory.GetFiles(IO.Path.GetDirectoryName(pBaseFilename), String.Format(filePattern, "*"))
            Dim fi As New FileInfo(fn)
            If (DateTime.Now > fi.LastWriteTime.Add(FileArchivePeriod.Value)) Then
                Try
                    fi.Delete()
                Catch
                End Try
            End If
        Next
    End Sub

    Protected Shared Function CreateFilenamesetPattern(pBaseFilepath As String) As String
        Return IO.Path.GetFileNameWithoutExtension(pBaseFilepath) + ".{0}" + IO.Path.GetExtension(pBaseFilepath)
    End Function

    Protected Shared Function CreateFilepathsetPattern(pBaseFilepath As String) As String
        Return IO.Path.Combine(IO.Path.GetDirectoryName(pBaseFilepath), IO.Path.GetFileNameWithoutExtension(pBaseFilepath)) + ".{0}" + IO.Path.GetExtension(pBaseFilepath)
    End Function





    Public Class ExecutionTimer
        Implements IDisposable

        Protected _name As String
        Protected _group As String
        Protected _timer As Stopwatch
        Protected _disabled As Boolean = False

        Public Sub New(pName As String, Optional pGroup As String = NO_GROUP_GROUP)
            If (ExecutionTimer._enableTraceLogging OrElse ExecutionTimer._enableAggregation) Then
                _name = pName
                _group = pGroup
                _timer = New Stopwatch()
                _timer.Start()
                If (ExecutionTimer._enableTraceLogging AndAlso (Logging.MasterLoggingLevel <= eLogLevel.Trace)) Then
                    Logging.LogTrace(String.Format("ExecutionTimer: + Begin '{0}'", _name), "Performance")
                End If
            Else
                _disabled = True
            End If
        End Sub

        Protected Sub Close()
            If (Not _disabled) Then
                _timer.Stop()
                If (ExecutionTimer._enableTraceLogging AndAlso (Logging.MasterLoggingLevel <= eLogLevel.Trace)) Then
                    Logging.LogTrace(String.Format("ExecutionTimer: - End   '{0}'  ({1} ms)", _name, _timer.ElapsedMilliseconds), "Performance")
                End If
                If (ExecutionTimer._enableAggregation) Then
                    SyncLock ExecutionTimer._lockTarget
                        Dim agg = ExecutionTimer.GetAggregator(_name, _group)
                        Dim elapsedMs As Int64 = _timer.ElapsedMilliseconds
                        agg.TotalMilliseconds += elapsedMs
                        agg.Count += 1
                        If (agg.MaxMilliseconds < elapsedMs) Then agg.MaxMilliseconds = elapsedMs
                        If (agg.MinMilliseconds > elapsedMs) Then agg.MinMilliseconds = elapsedMs
                        If (ExecutionTimer._keepHistoryCount > 0) Then
                            If (ExecutionTimer._keepHistoryMode = eKeepHistoryMode.KeepLast) Then
                                While (agg.History.Count >= ExecutionTimer._keepHistoryCount)
                                    agg.History.Dequeue()
                                End While
                                agg.History.Enqueue(New Tuple(Of Date, Int64)(DateTime.Now, elapsedMs))
                            ElseIf (agg.History.Count < ExecutionTimer._keepHistoryCount) Then
                                agg.History.Enqueue(New Tuple(Of Date, Int64)(DateTime.Now, elapsedMs))
                            End If
                        End If
                    End SyncLock
                End If
            End If
        End Sub






        Public Class TimerAggregator
            Public Property Name As String
            Public Property Group As String
            Public Property TotalMilliseconds As Int64 = 0
            Public Property Count As Int64 = 0
            Public Property MinMilliseconds As Int64 = Int64.MaxValue
            Public Property MaxMilliseconds As Int64 = Int64.MinValue
            Public Property History As New Queue(Of Tuple(Of DateTime, Int64))

            Public ReadOnly Property AverageMilliseconds As Double
                Get
                    Return TotalMilliseconds / Count
                End Get
            End Property

            Friend Function Clone() As TimerAggregator
                Return DirectCast(MemberwiseClone(), TimerAggregator)
            End Function

        End Class

        Public Enum eKeepHistoryMode
            KeepFirst = 1
            KeepLast = 2
        End Enum




        Protected Const NO_GROUP_GROUP = "NONE"
        Protected Shared _lockTarget As New Object
        Protected Shared _aggregateGroups As New Dictionary(Of String, Dictionary(Of String, TimerAggregator))

        Protected Shared _enableTraceLogging As Boolean = False
        Public Shared Property EnableTraceLogging() As Boolean
            Get
                Return _enableTraceLogging
            End Get
            Set(ByVal value As Boolean)
                SyncLock _lockTarget
                    _enableTraceLogging = value
                End SyncLock
            End Set
        End Property

        Protected Shared _enableAggregation As Boolean = False
        Public Shared Property EnableAggregation() As Boolean
            Get
                Return _enableAggregation
            End Get
            Set(ByVal value As Boolean)
                SyncLock _lockTarget
                    _enableAggregation = value
                    If (Not _enableAggregation) Then
                        _aggregateGroups.Clear()
                    End If
                End SyncLock
            End Set
        End Property

        Protected Shared _keepHistoryCount As Int32 = 0
        Public Shared Property KeepHistoryCount() As Int32
            Get
                Return _keepHistoryCount
            End Get
            Set(ByVal value As Int32)
                SyncLock _lockTarget
                    _keepHistoryCount = value
                End SyncLock
            End Set
        End Property

        Protected Shared _keepHistoryMode As eKeepHistoryMode = eKeepHistoryMode.KeepFirst
        Public Shared Property KeepHistoryMode() As eKeepHistoryMode
            Get
                Return _keepHistoryMode
            End Get
            Set(ByVal value As eKeepHistoryMode)
                SyncLock _lockTarget
                    _keepHistoryMode = value
                End SyncLock
            End Set
        End Property

        Public Shared Function GetAggregate(pName As String, Optional pGroupName As String = NO_GROUP_GROUP) As TimerAggregator
            If (_aggregateGroups.ContainsKey(pGroupName)) Then
                Dim grp = _aggregateGroups.Item(pGroupName)
                If (grp.ContainsKey(pName)) Then
                    Return grp.Item(pName).Clone
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function GetAggregates(Optional pGroupName As String = NO_GROUP_GROUP) As ICollection(Of TimerAggregator)
            If (_aggregateGroups.ContainsKey(pGroupName)) Then
                Dim grp = _aggregateGroups.Item(pGroupName)
                Return (From a In grp.Values Select a.Clone).ToList
            Else
                Return New List(Of TimerAggregator)
            End If
        End Function

        Public Shared Function GetAllAggregates() As ICollection(Of TimerAggregator)
            Return (From grp In _aggregateGroups.Values From a In grp.Values Select a.Clone).ToList
        End Function

        Public Shared Sub ClearAggregate(pName As String, Optional pGroupName As String = NO_GROUP_GROUP)
            SyncLock _lockTarget
                If (_aggregateGroups.ContainsKey(pGroupName)) Then
                    Dim grp = _aggregateGroups.Item(pGroupName)
                    If (grp.ContainsKey(pName)) Then
                        grp.Remove(pName)
                    End If
                End If
            End SyncLock
        End Sub

        Public Shared Sub ClearAggregates(Optional pGroupName As String = NO_GROUP_GROUP)
            SyncLock _lockTarget
                If (_aggregateGroups.ContainsKey(pGroupName)) Then
                    _aggregateGroups.Remove(pGroupName)
                End If
            End SyncLock
        End Sub

        Public Shared Sub ClearAllAggregates()
            SyncLock _lockTarget
                _aggregateGroups.Clear()
            End SyncLock
        End Sub





        'private function also creates path if it doesn't exist
        Protected Shared Function GetAggregator(pName As String, pGroupName As String) As TimerAggregator
            If (pGroupName Is Nothing) Then pGroupName = NO_GROUP_GROUP
            Dim grp As Dictionary(Of String, TimerAggregator)
            If (_aggregateGroups.ContainsKey(pGroupName)) Then
                grp = _aggregateGroups.Item(pGroupName)
            Else
                grp = New Dictionary(Of String, TimerAggregator)
                _aggregateGroups.Add(pGroupName, grp)
            End If

            Dim agg As TimerAggregator
            If (grp.ContainsKey(pName)) Then
                agg = grp.Item(pName)
            Else
                agg = New TimerAggregator
                agg.Name = pName
                agg.Group = pGroupName
                grp.Add(pName, agg)
            End If

            Return agg
        End Function


#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    Close()
                End If
            End If
            Me.disposedValue = True
        End Sub


        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Class
