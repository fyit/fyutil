﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class CodedEnum

    Public Shared Function GetCode(pEnumVal As System.Enum) As String
        Return GetCodedValuesByName(pEnumVal.GetType).Item(pEnumVal.ToString).Code
    End Function

    Public Shared Function GetCode(Of TEnum As Structure)(pEnumVal As TEnum) As String
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Return GetCodedValuesByName(GetType(TEnum)).Item(pEnumVal.ToString).Code
    End Function

    Public Shared Function GetLabel(pEnumVal As System.Enum) As String
        Return GetCodedValuesByName(pEnumVal.GetType).Item(pEnumVal.ToString).Label
    End Function

    Public Shared Function GetLabel(Of TEnum As Structure)(pCode As String) As String
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Dim cvs As IDictionary(Of String, CodedValues) = GetCodedValuesByCode(GetType(TEnum))
        If ((pCode IsNot Nothing) AndAlso cvs.ContainsKey(pCode)) Then
            Return cvs.Item(pCode).Label
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetByCode(Of TEnum As Structure)(pCode As String) As TEnum?
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Dim cvs As IDictionary(Of String, CodedValues) = GetCodedValuesByCode(GetType(TEnum))
        If ((pCode IsNot Nothing) AndAlso cvs.ContainsKey(pCode)) Then
            Return DirectCast(DirectCast(cvs.Item(pCode).EnumValue, Object), TEnum)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetByCode(Of TEnum As Structure)(pCode As String, pDefault As TEnum) As TEnum
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Dim cvs As IDictionary(Of String, CodedValues) = GetCodedValuesByCode(GetType(TEnum))
        If ((pCode IsNot Nothing) AndAlso cvs.ContainsKey(pCode)) Then
            Return DirectCast(DirectCast(cvs.Item(pCode).EnumValue, Object), TEnum)
        Else
            Return pDefault
        End If
    End Function

    Public Shared Function GetCodeLabelList(Of TEnum As Structure)(Optional pCategory As String = Nothing, Optional pIncludeHidden As Boolean = False) As IList(Of KeyValuePair(Of String, String))
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Dim res As New List(Of KeyValuePair(Of String, String))
        Dim cvs As IDictionary(Of String, CodedValues) = GetCodedValuesByName(GetType(TEnum))
        For Each cv As CodedValues In cvs.Values
            If ((Not cv.Hidden) OrElse pIncludeHidden) Then
                If ((pCategory Is Nothing) OrElse (cv.Categories.Contains(pCategory))) Then
                    res.Add(New KeyValuePair(Of String, String)(cv.Code, cv.Label))
                End If
            End If
        Next
        Return res
    End Function

    Public Shared Function GetValueLabelList(Of TEnum As Structure)(Optional pCategory As String = Nothing, Optional pIncludeHidden As Boolean = False) As IList(Of KeyValuePair(Of TEnum, String))
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Dim res As New List(Of KeyValuePair(Of TEnum, String))
        Dim cvs As IDictionary(Of String, CodedValues) = GetCodedValuesByName(GetType(TEnum))
        For Each cv As CodedValues In cvs.Values
            If ((Not cv.Hidden) OrElse pIncludeHidden) Then
                If ((pCategory Is Nothing) OrElse (cv.Categories.Contains(pCategory))) Then
                    res.Add(New KeyValuePair(Of TEnum, String)(DirectCast(DirectCast(cv.EnumValue, Object), TEnum), cv.Label))
                End If
            End If
        Next
        Return res
    End Function

    Public Shared Function GetObjectLabelList(Of TEnum As Structure)(Optional pCategory As String = Nothing, Optional pIncludeHidden As Boolean = False) As IList(Of ObjectLabelPair)
        If (Not GetType(TEnum).IsEnum) Then Throw New Exception("TEnum must be an Enum")
        Dim res As New List(Of ObjectLabelPair)
        Dim cvs As IDictionary(Of String, CodedValues) = GetCodedValuesByName(GetType(TEnum))
        For Each cv As CodedValues In cvs.Values
            If ((Not cv.Hidden) OrElse pIncludeHidden) Then
                If ((pCategory Is Nothing) OrElse (cv.Categories.Contains(pCategory))) Then
                    res.Add(New ObjectLabelPair(cv.EnumValue, cv.Label))
                End If
            End If
        Next
        Return res
    End Function

    Public Shared Function InCategory(pEnumVal As System.Enum, pCategory As String) As Boolean
        Return GetCodedValuesByName(pEnumVal.GetType).Item(pEnumVal.ToString).Categories.Contains(pCategory)
    End Function



#Region "Attributes"

    <AttributeUsage(AttributeTargets.Field)>
    Public Class CodedEnumValueAttribute
        Inherits Attribute

        Public ReadOnly Code As String
        Public ReadOnly Label As String

        Public Sub New(pCode As String, pLabel As String)
            Code = pCode
            Label = pLabel
        End Sub

        Public Sub New(pCode As String)
            Code = pCode
            Label = Nothing
        End Sub

    End Class

    <AttributeUsage(AttributeTargets.Field)>
    Public Class CategoryAttribute
        Inherits Attribute

        Public ReadOnly Categories As New List(Of String)

        Public Sub New(pCategory As String)
            Categories.Add(pCategory)
        End Sub

        Public Sub New(pCategories As String())
            Categories.AddRange(pCategories)
        End Sub

    End Class

    <AttributeUsage(AttributeTargets.Field)>
    Public Class HiddenAttribute
        Inherits Attribute

        Public ReadOnly Hidden As Boolean

        Public Sub New(Optional pHidden As Boolean = True)
            Hidden = pHidden
        End Sub

    End Class

#End Region



#Region "Load Coded Values"

    Private Class CodedValues
        Public Property EnumValue As System.Enum
        Public Property IntValue As Int32
        Public Property Name As String
        Public Property Code As String
        Public Property Label As String
        Public Property Hidden As Boolean
        Public Property Categories As New List(Of String)
    End Class

    Private Shared _allCodedValuesByName As New Dictionary(Of Type, Dictionary(Of String, CodedValues))
    Private Shared _allCodedValuesByCode As New Dictionary(Of Type, Dictionary(Of String, CodedValues))

    Private Shared Function GetCodedValuesByName(pEnumType As Type) As Dictionary(Of String, CodedValues)
        If (Not _allCodedValuesByName.ContainsKey(pEnumType)) Then LoadCodedValues(pEnumType)
        Return DirectCast(_allCodedValuesByName.Item(pEnumType), Dictionary(Of String, CodedValues))
    End Function

    Private Shared Function GetCodedValuesByCode(pEnumType As Type) As Dictionary(Of String, CodedValues)
        If (Not _allCodedValuesByName.ContainsKey(pEnumType)) Then LoadCodedValues(pEnumType)
        Return DirectCast(_allCodedValuesByCode.Item(pEnumType), Dictionary(Of String, CodedValues))
    End Function

    Private Shared Sub LoadCodedValues(pEnumType As Type)
        If (Not _allCodedValuesByName.ContainsKey(pEnumType)) Then
            SyncLock _allCodedValuesByName
                Dim cvByName As New Dictionary(Of String, CodedValues)(StringComparer.OrdinalIgnoreCase)
                Dim cvByCode As New Dictionary(Of String, CodedValues)(StringComparer.OrdinalIgnoreCase)

                For Each fld As Reflection.FieldInfo In pEnumType.GetFields(Reflection.BindingFlags.Static Or Reflection.BindingFlags.GetField Or Reflection.BindingFlags.Public)
                    Dim cv As New CodedValues
                    cv.EnumValue = DirectCast(fld.GetValue(Nothing), System.Enum)
                    cv.IntValue = Convert.ToInt32(cv.EnumValue)
                    cv.Name = fld.Name
                    Dim cvattrs = fld.GetCustomAttributes(GetType(CodedEnumValueAttribute), False)
                    If ((cvattrs IsNot Nothing) AndAlso (cvattrs.Length > 0)) Then
                        Dim cvattr = DirectCast(cvattrs(0), CodedEnumValueAttribute)
                        cv.Code = cvattr.Code
                        cv.Label = cvattr.Label
                    End If
                    Dim hidattrs = fld.GetCustomAttributes(GetType(HiddenAttribute), False)
                    If ((hidattrs IsNot Nothing) AndAlso (hidattrs.Length > 0)) Then
                        Dim hidattr = DirectCast(hidattrs(0), HiddenAttribute)
                        cv.Hidden = hidattr.Hidden
                    End If
                    Dim catattrs = fld.GetCustomAttributes(GetType(CategoryAttribute), False)
                    If ((catattrs IsNot Nothing) AndAlso (catattrs.Length > 0)) Then
                        Dim catattr = DirectCast(catattrs(0), CategoryAttribute)
                        cv.Categories.AddRange(catattr.Categories)
                    End If
                    If (cv.Label Is Nothing) Then
                        'try to get it from another attribute
                        Dim descattrs = fld.GetCustomAttributes(GetType(System.ComponentModel.DescriptionAttribute), False)
                        If ((descattrs IsNot Nothing) AndAlso (descattrs.Length > 0)) Then
                            Dim descattr = DirectCast(descattrs(0), System.ComponentModel.DescriptionAttribute)
                            cv.Label = descattr.Description
                        End If
                    End If
                    If (cv.Categories.Count = 0) Then cv.Categories.Add("")

                    'if Code or Label were not available as attributes, set to enum item Name
                    If (cv.Code Is Nothing) Then cv.Code = cv.Name
                    If (cv.Label Is Nothing) Then cv.Label = cv.Name

                    cvByName.Add(cv.Name, cv)

                    If (cvByCode.ContainsKey(cv.Code)) Then
                        Throw New Exception(String.Format("CodedEnum: Duplicate code in {0}, {1} = {2}", pEnumType.Name, cv.Name, cv.Code))
                    End If
                    cvByCode.Add(cv.Code, cv)
                Next
                _allCodedValuesByName.Item(pEnumType) = cvByName
                _allCodedValuesByCode.Item(pEnumType) = cvByCode
            End SyncLock
        End If
    End Sub

#End Region


End Class
