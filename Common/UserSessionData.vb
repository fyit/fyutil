﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Interface IUserSessionData

    Function Read(pKey As String) As Object
    Function Read(Of T)(pKey As String, pDefault As T) As T
    Sub Write(pKey As String, pData As Object)
    Sub Delete(pKey As String)
    Function Exists(pKey As String) As Boolean

End Interface

Public MustInherit Class UserSessionData
    Implements IUserSessionData

    Private Const PERSIST_SLOT_NAME = "UserSessionData.CurrentSession"

    Public Property IgnoreMissingSession As Boolean = False

    Public Overridable Function Read(pKey As String) As Object Implements IUserSessionData.Read
        Dim session = CurrentSession()
        If (session Is Nothing) Then Return Nothing
        If (session.ContainsKey(pKey)) Then Return session.Item(pKey)
        Return Nothing
    End Function

    Public Overridable Sub Write(pKey As String, pData As Object) Implements IUserSessionData.Write
        Dim session = CurrentSession()
        If (session Is Nothing) Then Return
        session.Item(pKey) = pData
    End Sub

    Public Overridable Sub Delete(pKey As String) Implements IUserSessionData.Delete
        Dim session = CurrentSession()
        If (session Is Nothing) Then Return
        If (session.ContainsKey(pKey)) Then session.Remove(pKey)
    End Sub

    Public Overridable Function Exists(pKey As String) As Boolean Implements IUserSessionData.Exists
        Return (Read(pKey) IsNot Nothing)
    End Function

    Public Overridable Function Read(Of T)(pKey As String, pDefault As T) As T Implements IUserSessionData.Read
        Try
            Dim data = Read(pKey)
            If (data Is Nothing) Then Return pDefault
            Return DirectCast(data, T)
        Catch ex As Exception
            Return pDefault
        End Try
    End Function

    ''' <summary>
    ''' Save the current session to logical CallContext, for transfer through a thread boundary
    ''' </summary>
    Public Sub SaveToContext()
        'uses logical slot to pass thread boundary
        Runtime.Remoting.Messaging.CallContext.LogicalSetData(PERSIST_SLOT_NAME, DoGetCurrentSession())
    End Sub

    ''' <summary>
    ''' Save the current session of the specified SessionData object to logical CallContext, for transfer through a thread boundary
    ''' </summary>
    Public Shared Sub SaveToContext(pSessionData As IUserSessionData)
        If (TypeOf pSessionData Is UserSessionData) Then
            DirectCast(pSessionData, UserSessionData).SaveToContext()
        Else
            Throw New NotSupportedException("SaveToContext is only supported on a UserSessionData object")
        End If
    End Sub

    ''' <summary>
    ''' Load the current session from logical CallContext, after transfer through a thread boundary
    ''' </summary>
    Public Sub LoadFromContext()
        Dim sess = DirectCast(Runtime.Remoting.Messaging.CallContext.LogicalGetData(PERSIST_SLOT_NAME), IDictionary(Of String, Object))
        If (sess IsNot Nothing) Then DoSetCurrentSession(sess)

        'clear the slot
        'NOTE: If this is not done, have run into problems with ReportViewer unable to load the web assemblies when trying to deserialize
        Runtime.Remoting.Messaging.CallContext.LogicalSetData(PERSIST_SLOT_NAME, Nothing)
    End Sub

    ''' <summary>
    ''' Load the current session of the specified SessionData object from logical CallContext, after transfer through a thread boundary
    ''' </summary>
    Public Shared Sub LoadFromContext(pSessionData As IUserSessionData)
        If (TypeOf pSessionData Is UserSessionData) Then
            DirectCast(pSessionData, UserSessionData).LoadFromContext()
        Else
            Throw New NotSupportedException("LoadFromContext is only supported on a UserSessionData object")
        End If
    End Sub

    Protected Function CurrentSession() As IDictionary(Of String, Object)
        Dim sess = DoGetCurrentSession()
        If (sess Is Nothing) Then
            'try to get it from context
            LoadFromContext()
            sess = DoGetCurrentSession()
        End If
        If ((sess Is Nothing) AndAlso (Not IgnoreMissingSession)) Then Throw New Exception("Unable to locate session store")
        Return sess
    End Function

    Protected MustOverride Function DoGetCurrentSession() As IDictionary(Of String, Object)
    Protected MustOverride Sub DoSetCurrentSession(pSession As IDictionary(Of String, Object))

End Class


''' <summary>
''' Simple class to implement IUserSessionData. Does not provide multi-user sessions.
''' </summary>
''' <remarks></remarks>
Public Class SimpleUserSessionData
    Inherits UserSessionData

    Private _data As New Dictionary(Of String, Object)

    Protected Overrides Function DoGetCurrentSession() As IDictionary(Of String, Object)
        Return _data
    End Function

    Protected Overrides Sub DoSetCurrentSession(pSession As IDictionary(Of String, Object))
        'do nothing
    End Sub

End Class


''' <summary>
''' Class to implement IUserSessionData for multi users. Sessions are accessed with a session ID key. Saves current session to CallContext for multithreading.
''' </summary>
''' <remarks></remarks>
Public Class MultiUserSessionData
    Inherits FYUtil.UserSessionData

    Private Const PERSIST_SLOT_NAME = "MultiUserSessionData.SessionId"

    Private _lockTarget As New Object()
    Private _sessions As New Dictionary(Of String, Dictionary(Of String, Object))

    Public Sub AddSession(pSessionId As String)
        SyncLock _lockTarget
            If (Not _sessions.ContainsKey(pSessionId)) Then
                Dim sess = New Dictionary(Of String, Object)
                sess.Item("SESSIONID") = pSessionId
                _sessions.Add(pSessionId, sess)
            Else
                Throw New Exception("Session already exists")
            End If
        End SyncLock
    End Sub

    Public Sub RemoveSession(pSessionId As String)
        SyncLock _lockTarget
            If (_sessions.ContainsKey(pSessionId)) Then
                _sessions.Remove(pSessionId)
            Else
                Throw New Exception("Session does not exist")
            End If
        End SyncLock
    End Sub

    Public Sub SetCurrentSession(pSessionId As String)
        'NOTE: Uses SetData instead of LogicalSetData, so it won't pass a thread boundary
        SyncLock _lockTarget
            If (pSessionId Is Nothing) Then
                Runtime.Remoting.Messaging.CallContext.SetData(PERSIST_SLOT_NAME, Nothing)
            ElseIf (_sessions.ContainsKey(pSessionId)) Then
                Runtime.Remoting.Messaging.CallContext.SetData(PERSIST_SLOT_NAME, pSessionId)
            Else
                Throw New Exception("Session does not exist")
            End If
        End SyncLock
    End Sub



    Protected Overrides Function DoGetCurrentSession() As IDictionary(Of String, Object)
        Dim sessId = DirectCast(Runtime.Remoting.Messaging.CallContext.GetData(PERSIST_SLOT_NAME), String)
        If (sessId Is Nothing) Then Return Nothing
        Return _sessions.Item(sessId)
    End Function

    Protected Overrides Sub DoSetCurrentSession(pSession As IDictionary(Of String, Object))
        SetCurrentSession(pSession.Item("SESSIONID").ToString)
    End Sub

End Class



''' <summary>
''' Manages session data using strongly typed buckets.
''' </summary>
''' <typeparam name="TBucketEnum"></typeparam>
''' <remarks></remarks>
Public Class SessionManager(Of TBucketEnum As Structure)

    Private _sessionData As IUserSessionData
    Public ReadOnly Property SessionData() As IUserSessionData
        Get
            Return _sessionData
        End Get
    End Property


    Public Sub New(pSessionData As IUserSessionData)
        _sessionData = pSessionData
    End Sub



    Private Sub WriteSessionBase(pKey As String, pData As Object)
        _sessionData.Write(pKey, pData)
    End Sub

    Private Function ReadSessionBase(Of TPayload)(pKey As String, pDefault As TPayload) As TPayload
        Dim dataVal = _sessionData.Read(pKey)
        If (dataVal Is Nothing) Then Return pDefault
        Try
            'JWM: changed directcast to ctype.  http://stackoverflow.com/questions/3056514/difference-between-directcast-and-ctype-in-vb-net
            Return CType(FYUtil.Utilities.ChangeType(dataVal, GetType(TPayload)), TPayload)
        Catch
            FYUtil.Logging.LogWarning(String.Format("ReadSessionBase: Unable to cast value '{0}' in '{1}' to '{2}'", dataVal, pKey, GetType(TPayload).Name))
            Return pDefault
        End Try

        'NOTE: DirectCast does not work for nullable value types?
        'Try
        '    Return DirectCast(dataVal, TPayload)
        'Catch
        '    Return pDefault
        'End Try
    End Function

    Private Sub ClearSessionBase(pKey As String)
        _sessionData.Delete(pKey)
    End Sub

    Public Function ReadSession(Of T)(pKey As TBucketEnum, pDefault As T) As T
        Return ReadSessionBase(GetBucketKey(pKey), pDefault)
    End Function

    Public Sub WriteSession(pKey As TBucketEnum, pData As Object)
        WriteSessionBase(GetBucketKey(pKey), pData)
    End Sub

    Public Sub ClearSession(pKey As TBucketEnum)
        ClearSessionBase(GetBucketKey(pKey))
    End Sub

    Public Function ReadSession(Of T)(pKey As TBucketEnum, pIndex As String, pDefault As T) As T
        Return ReadSessionBase(GetBucketKey(pKey, pIndex), pDefault)
    End Function

    Public Sub WriteSession(pKey As TBucketEnum, pIndex As String, pData As Object)
        WriteSessionBase(GetBucketKey(pKey, pIndex), pData)
    End Sub

    Public Sub ClearSession(pKey As TBucketEnum, pIndex As String)
        ClearSessionBase(GetBucketKey(pKey, pIndex))
    End Sub






    Protected Function GetBucketKey(pKey As TBucketEnum) As String
        Return String.Format("SessionManager.{0}", pKey)
    End Function

    Protected Function GetBucketKey(pKey As TBucketEnum, pIndex As String) As String
        Return String.Format("SessionManager.{0}.{1}", pKey, pIndex)
    End Function

End Class
