﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Class TimeZoneCodes

    Public Enum eTimeZone
        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("UTC", "UTC Time")>
        UTC

        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("ET", "Eastern Time")>
        Eastern

        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("CT", "Central Time")>
        Central

        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("MT", "Mountain Time")>
        Mountain

        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("PT", "Pacific Time")>
        Pacific

        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("AKT", "Alaskan Time")>
        Alaskan

        <FYUtil.CodedEnum.Category("Common")>
        <FYUtil.CodedEnum.CodedEnumValue("HT", "Hawaiian Time")>
        Hawaiian

        <FYUtil.CodedEnum.CodedEnumValue("AT", "Atlantic Time")>
        Atlantic

        <FYUtil.CodedEnum.CodedEnumValue("ETND", "Eastern Time (No DST)")>
        Eastern_ND

        <FYUtil.CodedEnum.CodedEnumValue("CTND", "Central Time (No DST)")>
        Central_ND

        <FYUtil.CodedEnum.CodedEnumValue("MTND", "Mountain Time (No DST)")>
        Mountain_ND

        <FYUtil.CodedEnum.CodedEnumValue("PTND", "Pacific Time (No DST)")>
        Pacific_ND
    End Enum

    Public Enum eTimeZoneCategory
        All = 0
        Common = 1
    End Enum

    Private Class TZMap
        Public Property CodeEnum As eTimeZone
        Public Property Code As String
        Public Property Description As String
        Public Property TZInfo As TimeZoneInfo

        Public Sub New(pCodeEnum As eTimeZone, pSystemId As String, Optional pRemoveDst As Boolean = False)
            Me.CodeEnum = pCodeEnum
            Me.Code = FYUtil.CodedEnum.GetCode(pCodeEnum)
            Me.Description = FYUtil.CodedEnum.GetLabel(pCodeEnum)
            If (pRemoveDst) Then
                Dim baseTz = TimeZoneInfo.FindSystemTimeZoneById(pSystemId)
                Me.TZInfo = TimeZoneInfo.CreateCustomTimeZone(baseTz.Id, baseTz.BaseUtcOffset, baseTz.DisplayName, baseTz.StandardName, baseTz.StandardName, baseTz.GetAdjustmentRules(), disableDaylightSavingTime:=True)
            Else
                Me.TZInfo = TimeZoneInfo.FindSystemTimeZoneById(pSystemId)
            End If
        End Sub
    End Class

    Private Shared _fullList As New List(Of TZMap)
    Private Shared _findByCode As New Dictionary(Of String, TZMap)
    Private Shared _findByTZInfo As New Dictionary(Of TimeZoneInfo, TZMap)

    Shared Sub New()
        _fullList.Add(New TZMap(eTimeZone.UTC, "UTC"))
        _fullList.Add(New TZMap(eTimeZone.Eastern, "Eastern Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Central, "Central Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Mountain, "Mountain Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Pacific, "Pacific Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Alaskan, "Alaskan Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Hawaiian, "Hawaiian Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Atlantic, "Atlantic Standard Time"))
        _fullList.Add(New TZMap(eTimeZone.Eastern_ND, "Eastern Standard Time", pRemoveDst:=True))
        _fullList.Add(New TZMap(eTimeZone.Central_ND, "Central Standard Time", pRemoveDst:=True))
        _fullList.Add(New TZMap(eTimeZone.Mountain_ND, "Mountain Standard Time", pRemoveDst:=True))
        _fullList.Add(New TZMap(eTimeZone.Pacific_ND, "Pacific Standard Time", pRemoveDst:=True))

        For Each mp In _fullList
            _findByCode.Add(mp.Code, mp)
            _findByTZInfo.Add(mp.TZInfo, mp)
        Next
    End Sub

    Public Shared Function GetTimeZoneInfo(pCode As String) As TimeZoneInfo
        If Not String.IsNullOrEmpty(pCode) AndAlso (_findByCode.ContainsKey(pCode)) Then
            Return _findByCode.Item(pCode).TZInfo
        End If
        Return Nothing
    End Function

    Public Shared Function GetTimeZoneInfo(pCodeEnum As eTimeZone) As TimeZoneInfo
        Return GetTimeZoneInfo(FYUtil.CodedEnum.GetCode(pCodeEnum))
    End Function

    Public Shared Function GetCode(pTzInfo As TimeZoneInfo) As String
        If ((pTzInfo IsNot Nothing) AndAlso (_findByTZInfo.ContainsKey(pTzInfo))) Then
            Return _findByTZInfo.Item(pTzInfo).Code
        End If
        Return Nothing
    End Function

    Public Shared Function GetCode(pCodeEnum As eTimeZone) As String
        Return FYUtil.CodedEnum.GetCode(pCodeEnum)
    End Function

    Public Shared Function GetDescription(pCode As String) As String
        If Not String.IsNullOrEmpty(pCode) AndAlso (_findByCode.ContainsKey(pCode)) Then
            Return _findByCode.Item(pCode).Description
        End If
        Return Nothing
    End Function


    Public Shared Function GetCodeDescriptionList(Optional pCategory As eTimeZoneCategory = eTimeZoneCategory.All) As IDictionary(Of String, String)
        Dim res As New Dictionary(Of String, String)
        For Each mp In _fullList
            If ((pCategory = eTimeZoneCategory.All) OrElse (FYUtil.CodedEnum.InCategory(mp.CodeEnum, pCategory.ToString))) Then
                res.Add(mp.Code, mp.Description)
            End If
        Next
        Return res
    End Function

End Class
