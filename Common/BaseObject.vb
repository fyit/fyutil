﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Runtime.Serialization
Imports System.Reflection

<Serializable()>
Public MustInherit Class ChangeHistoryBaseObject
    Private _previousState As ChangeHistoryBaseObject

    Public Sub SaveCurrentState()
        _previousState = CopyObject()
    End Sub

    Public Function GetSavedState() As ChangeHistoryBaseObject
        Return _previousState
    End Function

    Public Function CopyObject() As ChangeHistoryBaseObject
        Return DirectCast(Me.MemberwiseClone(), ChangeHistoryBaseObject)
    End Function

    Public Class UpdatedDataElement
        Public Property PropertyName As String
        Public Property Before As Object
        Public Property After As Object

        Friend Sub New(pPropertyName As String, pBefore As Object, pAfter As Object)
            PropertyName = pPropertyName
            Before = pBefore
            After = pAfter
        End Sub
    End Class

    Public Function GetUpdatedDataElements() As List(Of UpdatedDataElement)
        Dim updatedElems As New List(Of UpdatedDataElement)
        For Each prop As PropertyInfo In Me.GetType().GetProperties()
            Dim curVal As Object = prop.GetValue(Me, Nothing)
            If (_previousState Is Nothing) Then
                updatedElems.Add(New UpdatedDataElement(prop.Name, Nothing, curVal))
            Else
                Dim prevVal As Object = prop.GetValue(_previousState, Nothing)
                If (Not (curVal Is Nothing AndAlso prevVal Is Nothing)) Then
                    If (prevVal Is Nothing OrElse curVal Is Nothing OrElse (Not prevVal.Equals(curVal))) Then
                        updatedElems.Add(New UpdatedDataElement(prop.Name, prevVal, curVal))
                    End If
                End If
            End If
        Next
        Return updatedElems
    End Function

End Class

#Region "Original Untyped Base Object"

<Serializable()>
Public MustInherit Class StandardBaseObject
    Inherits ChangeHistoryBaseObject
    Implements IStandardDictionaryObject

    MustOverride Property UniqueId As Integer Implements IStandardDictionaryObject.UniqueId

    ''' <summary>
    ''' This property should be overridden to provide a general description of the object, rather than the standard "ObjectType" as a string that is defaulted.  It may eventually be "MustOverride"
    ''' </summary>
    Public Overridable ReadOnly Property DisplayValue As String Implements IStandardDictionaryObject.DisplayValue
        Get
            Return Me.exToString
        End Get
    End Property
End Class

''' <summary>
''' This interface is only needed for the dictionary class below
''' </summary>
Friend Interface IStandardDictionaryObject
    Property UniqueId As Int32
    ReadOnly Property DisplayValue As String
End Interface

<Serializable()>
Public Class StandardBaseObjectDict(Of itemType As StandardBaseObject)
    Inherits Generic.Dictionary(Of Int32, itemType)

    Public Sub New()
    End Sub

    Public Sub New(ByVal pList As IEnumerable(Of itemType))
        If pList IsNot Nothing Then Me.Consume(pList)
    End Sub

    ''' <summary>
    ''' Returns a a base object dictionary containing clones of the original objects
    ''' </summary>
    Public Function GetCopies() As StandardBaseObjectDict(Of itemType)
        Return New StandardBaseObjectDict(Of itemType)(From item In Me.Values Select DirectCast(item.CopyObject, itemType))
    End Function

    Public Overloads Sub Add(ByVal pValue As itemType, ByVal pAddAsClone As Boolean)
        If pAddAsClone Then
            MyBase.Add(pValue.UniqueId, DirectCast(pValue.CopyObject(), itemType))
        Else
            Me.Add(pValue)
        End If
    End Sub

    Public Overloads Sub Add(ByVal pValue As itemType)
        MyBase.Add(pValue.UniqueId, pValue)
    End Sub

    Public Sub Consume(ByVal pList As IEnumerable(Of itemType))
        For Each tmpItem As itemType In pList
            Me.Add(tmpItem.UniqueId, tmpItem)
        Next
    End Sub

End Class

#End Region

#Region "Typed Base Object"

<Serializable()>
Public MustInherit Class StandardBaseObject(Of TKey)
    Inherits ChangeHistoryBaseObject
    Implements IStandardDictionaryObject(Of TKey)

    MustOverride Property UniqueId As TKey Implements IStandardDictionaryObject(Of TKey).UniqueId

    ''' <summary>
    ''' This property should be overridden to provide a general description of the object, rather than the standard "ObjectType" as a string that is defaulted.  It may eventually be "MustOverride"
    ''' </summary>
    Public Overridable ReadOnly Property DisplayValue As String Implements IStandardDictionaryObject(Of TKey).DisplayValue
        Get
            Return Me.exToString
        End Get
    End Property
End Class

Public MustInherit Class IntKeyBaseObject
    Inherits StandardBaseObject(Of Int32)

End Class

''' <summary>
''' This interface is only needed for the dictionary class below
''' </summary>
Friend Interface IStandardDictionaryObject(Of Tkey)
    Property UniqueId As Tkey
    ReadOnly Property DisplayValue As String
End Interface

<Serializable()>
Public Class StandardBaseObjectDict(Of keyType, itemType As StandardBaseObject(Of keyType))
    Inherits Generic.Dictionary(Of keyType, itemType)

    Public Sub New()
    End Sub

    Public Sub New(ByVal pList As IEnumerable(Of itemType))
        If pList IsNot Nothing Then Me.Consume(pList)
    End Sub

    ''' <summary>
    ''' Returns a a base object dictionary containing clones of the original objects
    ''' </summary>
    Public Function GetCopies() As StandardBaseObjectDict(Of keyType, itemType)
        Return New StandardBaseObjectDict(Of keyType, itemType)(From item In Me.Values Select DirectCast(item.CopyObject, itemType))
    End Function

    Public Overloads Sub Add(ByVal pValue As itemType, ByVal pAddAsClone As Boolean)
        If pAddAsClone Then
            MyBase.Add(pValue.UniqueId, DirectCast(pValue.CopyObject(), itemType))
        Else
            Me.Add(pValue)
        End If
    End Sub

    Public Overloads Sub Add(ByVal pValue As itemType)
        MyBase.Add(pValue.UniqueId, pValue)
    End Sub

    Public Sub Consume(ByVal pList As IEnumerable(Of itemType))
        For Each tmpItem As itemType In pList
            Me.Add(tmpItem.UniqueId, tmpItem)
        Next
    End Sub

End Class

#End Region
