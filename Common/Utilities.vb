﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Text

Public Class Utilities

    Public Const CHARSET_ALPHA As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    Public Const CHARSET_UPPERALPHA As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    Public Const CHARSET_LOWERALPHA As String = "abcdefghijklmnopqrstuvwxyz"
    Public Const CHARSET_ALPHANUMERIC As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    Public Const CHARSET_DIGITS As String = "1234567890"

    ''' <summary>This charset is good for generating random strings that can't be words</summary>
    Public Const CHARSET_UPPERALPHANOVOWELS As String = "BCDFGHJKLMNPQRSTVWXYZ"

    ''' <summary>Charset used for generating random passwords, etc, with no easily confused characters</summary>
    Public Const CHARSET_SAFEREAD As String = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789"

    ''' <summary>Charset used for generating random passwords, etc, with no easily confused characters. Includes no vowels so it can't create words</summary>
    Public Const CHARSET_SAFEREADNOVOWELS As String = "bcdfghjkmnpqrstvwxyzBCDFGHJKMNPQRSTVWXYZ23456789"

    Public Const REGEX_EMAILADDRESS As String = "^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"
    Public Const REGEX_VARIABLENAME As String = "^[a-zA-Z_][a-zA-Z_0-9]*$"

    ''' <summary>
    ''' Convert a string object to a byte array using UTF8 encoding
    ''' </summary>
    Public Shared Function StringToByteArray(pStr As String) As Byte()
        Dim encoding As New System.Text.UTF8Encoding()
        Return encoding.GetBytes(pStr)
    End Function

    ''' <summary>
    ''' Convert a byte array to a string object assuming UTF8 encoding
    ''' </summary>
    Public Shared Function ByteArrayToString(pArr As Byte()) As String
        Dim enc As New System.Text.UTF8Encoding()
        Return enc.GetString(pArr)
    End Function

    ''' <summary>
    ''' Convert a byte array to a string object containing the HEX representation
    ''' </summary>
    Public Shared Function ByteArrayToHexString(pArr As Byte()) As String
        Dim sb As New StringBuilder(pArr.Length * 2)
        For Each byt In pArr
            sb.AppendFormat("{0:x2}", byt)
        Next
        Return sb.ToString()

        'this works too, all caps
        'Return BitConverter.ToString(pArr).Replace("-", "")
    End Function

    ''' <summary>
    ''' Convert a string containing the HEX representation of binary data into a byte array
    ''' </summary>
    Public Shared Function HexStringToByteArray(pStr As String) As Byte()
        If (String.IsNullOrWhiteSpace(pStr)) Then Return Nothing

        Try
            Dim ln As Integer = Convert.ToInt32(pStr.Length / 2)
            Dim b(ln - 1) As Byte
            For i As Integer = 0 To ln - 1
                b(i) = Convert.ToByte(pStr.Substring(i * 2, 2), 16)
            Next
            Return b
        Catch ex As Exception
            Throw New System.FormatException("The provided string does not appear to be Hex encoded:" & pStr, ex)
        End Try
    End Function

    Public Shared Function StreamToByteArray(pInput As IO.Stream) As Byte()
        Dim buffer As Byte() = New Byte(16 * 1024 - 1) {}
        Using ms As New IO.MemoryStream()
            Dim bytesRead = 0
            Do
                bytesRead = pInput.Read(buffer, 0, buffer.Length)
                ms.Write(buffer, 0, bytesRead)
            Loop Until bytesRead = 0
            Return ms.ToArray()
        End Using
    End Function


    Public Shared Function HashMd5(pToHash As Byte()) As Byte()
        Using crypto As New System.Security.Cryptography.MD5CryptoServiceProvider()
            Return crypto.ComputeHash(pToHash)
        End Using
    End Function

    ''' <summary>
    ''' Hash a string using MD5. Returns string with HEX representation.
    ''' </summary>
    Public Shared Function HashMd5ToHex(pToHash As String) As String
        Return ByteArrayToHexString(HashMd5(StringToByteArray(pToHash)))
    End Function

    ''' <summary>
    ''' Hash a string using MD5. Returns string in Base64
    ''' </summary>
    Public Shared Function HashMd5ToBase64(pToHash As String) As String
        Return Convert.ToBase64String(HashMd5(StringToByteArray(pToHash)))
    End Function

    ''' <summary>
    ''' Check whether a string contains only characters from the specified list of characters
    ''' </summary>
    ''' <param name="pToCheck">string to check for validity</param>
    ''' <param name="pAllowChars">string of all the valid characters. CHARSET_xxx constants are available</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function StringContainsOnly(pToCheck As String, pAllowChars As String) As Boolean
        If (String.IsNullOrEmpty(pToCheck)) Then Return True
        If (String.IsNullOrEmpty(pAllowChars)) Then Return False
        'is there a faster way to do this?
        For x As Int32 = 0 To pToCheck.Length - 1
            If (Not pAllowChars.Contains(pToCheck.Substring(x, 1))) Then Return False
        Next
        Return True
    End Function

    ''' <summary>
    ''' Ensure that a string contains only characters from the specified list of characters by removing all others
    ''' </summary>
    ''' <param name="pToCheck">string to modify</param>
    ''' <param name="pAllowChars">string of all the valid characters. CHARSET_xxx constants are available</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function StringEnsureContainsOnly(pToCheck As String, pAllowChars As String) As String
        If (String.IsNullOrEmpty(pToCheck)) Then Return pToCheck
        If (String.IsNullOrEmpty(pAllowChars)) Then Return String.Empty

        Dim sb As New System.Text.StringBuilder()
        For ix As Integer = 0 To pToCheck.Length - 1
            If (pAllowChars.IndexOf(pToCheck(ix)) >= 0) Then
                sb.Append(pToCheck(ix))
            End If
        Next

        Return sb.ToString()
    End Function

    ''' <summary>
    ''' Check whether the entire string matches the regular expression. REGEX_xxx constants are available
    ''' </summary>
    Public Shared Function RegexMatchExact(pToCheck As String, pPattern As String) As Boolean
        'this forces RegEx to match the entire string rather than a substring
        If (Not pPattern.StartsWith("^")) Then
            pPattern = "^" + pPattern
        End If
        If (Not pPattern.EndsWith("$")) Then
            pPattern += "$"
        End If

        Return Text.RegularExpressions.Regex.IsMatch(pToCheck, pPattern)
    End Function

    Public Shared Function AlphaNumeric(pData As String) As String
        Dim nonNumericCharacters As New System.Text.RegularExpressions.Regex("[^A-Za-z0-9]")
        Return nonNumericCharacters.Replace(pData, String.Empty)
    End Function

    Public Shared Function CharsOnly(pData As String) As String
        Dim nonNumericCharacters As New System.Text.RegularExpressions.Regex("[^A-Za-z]")
        Return nonNumericCharacters.Replace(pData, String.Empty)
    End Function

    Public Shared Function NumbersOnly(pData As String) As String
        Dim nonNumericCharacters As New System.Text.RegularExpressions.Regex("[^0-9]")
        Return nonNumericCharacters.Replace(pData, String.Empty)
    End Function

#Region "Random"

    Private Shared _randomSeed As New Random()  'single seed value prevents duplicate random chains when done in quick succession

    ''' <summary>
    ''' Generates a random string of the specified length, optionally with a limited character set
    ''' </summary>
    ''' <param name="pLength"></param>
    ''' <param name="pAllowChars">string containing the allowed characters, defaults to CHARSET_ALPHANUMERIC</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function RandomString(pLength As Integer, Optional pAllowChars As String = CHARSET_ALPHANUMERIC) As String
        Dim sb As New Text.StringBuilder()
        Dim nextChar As String
        Dim maxInd As Int32 = pAllowChars.Length - 1

        For i As Int32 = 0 To pLength - 1
            nextChar = pAllowChars.Substring(_randomSeed.Next(0, maxInd), 1)
            sb.Append(nextChar)
        Next i

        Return sb.ToString()
    End Function

    Public Shared Function RandomNumber(pMin As Int32, pMax As Int32) As Int32
        Return _randomSeed.Next(pMin, pMax)
    End Function

    ''' <summary>
    ''' Generates a random password of the specified length. Character set is limited for readability
    ''' </summary>
    ''' <param name="pLength"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function RandomPassword(pLength As Integer) As String
        Dim sb As New Text.StringBuilder()
        Dim nextChar As String
        Dim allowChars As String = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789!@#$%*."
        Dim maxInd As Int32 = allowChars.Length - 1

        For i As Int32 = 0 To pLength - 1
            nextChar = allowChars.Substring(_randomSeed.Next(0, maxInd), 1)
            sb.Append(nextChar)
        Next i

        Return sb.ToString()
    End Function

    ''' <summary>
    ''' Pick a random entry from the list of items passed
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="pList"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function RandomFromList(Of T)(pList As IEnumerable(Of T)) As T
        If ((pList Is Nothing) OrElse (pList.Count = 0)) Then Return Nothing
        Return pList.ElementAt(_randomSeed.Next(0, pList.Count - 1))
    End Function

#End Region

#Region "Enums"

    Public Shared Function EnumToDict(pEnumType As Type) As Dictionary(Of Int32, String)
        Dim res As New Dictionary(Of Int32, String)
        Dim names() As String = [Enum].GetNames(pEnumType)
        Dim vals As Array = [Enum].GetValues(pEnumType)
        For x As Int32 = 0 To names.Length - 1
            res.Add(DirectCast(vals.GetValue(x), Int32), names(x))
        Next

        Return res
    End Function

#End Region

#Region "Reflection"

    Public Shared Function ChangeType(value As Object, type As Type) As Object
        If value Is Nothing AndAlso type.IsGenericType Then
            Return Activator.CreateInstance(type)
        End If
        If value Is Nothing Then
            Return Nothing
        End If
        If type = value.[GetType]() Then
            Return value
        End If
        If type.IsEnum Then
            If TypeOf value Is String Then
                Return [Enum].Parse(type, TryCast(value, String))
            Else
                Return [Enum].ToObject(type, value)
            End If
        End If
        If Not type.IsInterface AndAlso type.IsGenericType Then
            Dim innerType As Type = type.GetGenericArguments()(0)
            Dim innerValue As Object = Convert.ChangeType(value, innerType)
            Return Activator.CreateInstance(type, New Object() {innerValue})
        End If
        If TypeOf value Is String AndAlso type = GetType(Guid) Then
            Return New Guid(TryCast(value, String))
        End If
        If TypeOf value Is String AndAlso type = GetType(Version) Then
            Return New Version(TryCast(value, String))
        End If
        If Not (TypeOf value Is IConvertible) Then
            Return value
        End If
        Return Convert.ChangeType(value, type)
    End Function


    ''' <summary>
    ''' Returns a dictionary containing an item for each property of the object
    ''' </summary>
    ''' <param name="pObject"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ObjectToDictionary(pObject As Object) As IDictionary(Of String, Object)
        If (pObject IsNot Nothing) Then
            Dim values = New Dictionary(Of String, Object)
            Dim props As ComponentModel.PropertyDescriptorCollection = ComponentModel.TypeDescriptor.GetProperties(pObject)
            For Each prop As ComponentModel.PropertyDescriptor In props
                values.Add(prop.Name, prop.GetValue(pObject))
            Next

            Return values
        Else
            Return Nothing
        End If
    End Function


    ''' <summary>
    ''' Gets the value of a property (or field) on an object using reflection
    ''' </summary>
    ''' <param name="pObject">object to examine</param>
    ''' <param name="pPropertyName">name of property to get</param>
    ''' <returns>value of the property</returns>
    ''' <remarks></remarks>
    Public Shared Function GetObjectProperty(pObject As Object, pPropertyName As String) As Object
        If (pObject Is Nothing) Then Return Nothing
        Dim pinfo As System.Reflection.PropertyInfo = pObject.GetType().GetProperty(pPropertyName)
        If (pinfo IsNot Nothing) Then
            Return pinfo.GetValue(pObject, Nothing)
        Else
            'failsafe for member variables
            Dim finfo As System.Reflection.FieldInfo = pObject.GetType().GetField(pPropertyName)
            If (finfo Is Nothing) Then Throw New Exception(String.Format("GetObjectProperty: Property or Field '{0}' not found", pPropertyName))
            Return finfo.GetValue(pObject)
        End If
    End Function

    ''' <summary>
    ''' Gets the value of a property (or field) on an object using reflection
    ''' </summary>
    ''' <param name="pObject">object to examine</param>
    ''' <param name="pPropertyName">name of property to get</param>
    ''' <returns>value of the property</returns>
    ''' <remarks></remarks>
    Public Shared Function GetObjectProperty(Of TResult)(pObject As Object, pPropertyName As String) As TResult
        Return DirectCast(GetObjectProperty(pObject, pPropertyName), TResult)
    End Function

    ''' <summary>
    ''' Calls a method on an object using reflection
    ''' </summary>
    ''' <param name="pObject">object to examine</param>
    ''' <param name="pMethodName">name of method to call</param>
    ''' <param name="pArgs">arguments to the method</param>
    ''' <returns>return value of the method, if applicable</returns>
    ''' <remarks></remarks>
    Public Shared Function CallObjectMethod(pObject As Object, pMethodName As String, pArgs As IEnumerable(Of Object)) As Object
        If (pObject Is Nothing) Then Return Nothing
        Dim args As Object() = Nothing
        If (pArgs IsNot Nothing) Then args = pArgs.ToArray
        Dim typ As System.Type = pObject.GetType()
        Return typ.InvokeMember(pMethodName, Reflection.BindingFlags.InvokeMethod, Nothing, pObject, args)
        'Dim minfo As System.Reflection.MethodInfo = pObject.GetType().GetMethod(pMethodName)
        'Return minfo.Invoke(pObject, args)
    End Function

    ''' <summary>
    ''' Calls a method on an object using reflection
    ''' </summary>
    ''' <param name="pObject">object to examine</param>
    ''' <param name="pMethodName">name of method to call</param>
    ''' <param name="pArgs">arguments to the method</param>
    ''' <returns>return value of the method, if applicable</returns>
    ''' <remarks></remarks>
    Public Shared Function CallObjectMethod(Of TResult)(pObject As Object, pMethodName As String, pArgs As IEnumerable(Of Object)) As TResult
        Return DirectCast(CallObjectMethod(pObject, pMethodName, pArgs), TResult)
    End Function

    ''' <summary>
    ''' Gets the value of a property (or field) on an object or subobject using reflection
    ''' </summary>
    ''' <param name="pObject">object to examine</param>
    ''' <param name="pPropertyPath">property path to get (in dotted notation, e.g. "Address.Line1")</param>
    ''' <returns>value of the property</returns>
    ''' <remarks></remarks>
    Public Shared Function GetObjectPropertyPath(pObject As Object, pPropertyPath As String) As Object
        If (Not pPropertyPath.Contains(".")) Then Return GetObjectProperty(pObject, pPropertyPath)

        Dim subObjectNames() As String = Split(pPropertyPath, ".")
        Dim curObject As Object = pObject

        For i As Integer = 0 To subObjectNames.Length - 2
            curObject = GetObjectProperty(curObject, subObjectNames(i))
        Next

        Return GetObjectProperty(curObject, subObjectNames(subObjectNames.Length - 1))
    End Function

    ''' <summary>
    ''' Sets a property (or field) on an object using reflection
    ''' </summary>
    ''' <param name="pObject">object to modify</param>
    ''' <param name="pPropertyName">property to modify</param>
    ''' <param name="pValue">value to assign to property</param>
    ''' <remarks></remarks>
    Public Shared Sub SetObjectProperty(pObject As Object, pPropertyName As String, pValue As Object)
        Dim pinfo As System.Reflection.PropertyInfo = pObject.GetType().GetProperty(pPropertyName)
        If (pinfo IsNot Nothing) Then
            If (pinfo.PropertyType <> pValue.GetType()) Then Throw New Exception(String.Format("SetObjectProperty: Type of Property '{0}' does not match value", pPropertyName))
            pinfo.SetValue(pObject, pValue, Nothing)
        Else
            'failsafe for member variables
            Dim finfo As System.Reflection.FieldInfo = pObject.GetType().GetField(pPropertyName)
            If (finfo Is Nothing) Then Throw New Exception(String.Format("SetObjectProperty: Property or Field '{0}' not found", pPropertyName))
            If (finfo.FieldType <> pValue.GetType()) Then Throw New Exception(String.Format("SetObjectProperty: Type of Field '{0}' does not match value", pPropertyName))
            finfo.SetValue(pObject, pValue)
        End If
    End Sub

    ''' <summary>
    ''' Sets a property (or field) on an object or subobject using reflection
    ''' </summary>
    ''' <param name="pObject">object to modify</param>
    ''' <param name="pPropertyPath">property path to modify (in dotted notation, e.g. "Address.Line1")</param>
    ''' <param name="pValue">value to assign to property</param>
    ''' <remarks></remarks>
    Public Shared Sub SetObjectPropertyPath(pObject As Object, pPropertyPath As String, pValue As Object)
        If (Not pPropertyPath.Contains(".")) Then Call SetObjectProperty(pObject, pPropertyPath, pValue)

        Dim subObjectNames() As String = Split(pPropertyPath, ".")
        Dim curObject As Object = pObject

        For i As Integer = 0 To subObjectNames.Length - 2
            curObject = GetObjectProperty(curObject, subObjectNames(i))
        Next

        Call SetObjectProperty(curObject, subObjectNames(subObjectNames.Length - 1), pValue)
    End Sub

    Public Enum eHandleRefTypes
        ThrowError
        CopyReference
        SetNull
        Ignore
    End Enum

    ''' <summary>
    ''' Does a shallow copy of the source object properties, returning a new object of the same underlying type
    ''' </summary>
    Public Shared Function CopyObject(Of T)(pSource As T, Optional pHandleRefTypes As eHandleRefTypes = eHandleRefTypes.ThrowError) As T
        'this would create an object of the advertised type of pSource, not the actual type
        'Dim dest As T = Activator.CreateInstance(Of T)()
        Dim dest As Object = Activator.CreateInstance(pSource.GetType())
        CopyObjectInto(pSource, dest, pHandleRefTypes)
        Return DirectCast(dest, T)
    End Function


    ''' <summary>
    ''' Does a shallow copy of the source object properties into the destination object, matching by property name and type
    ''' </summary>
    Public Shared Sub CopyObjectInto(pSource As Object, pDest As Object, Optional pHandleRefTypes As eHandleRefTypes = eHandleRefTypes.ThrowError)
        For Each destProp As Reflection.PropertyInfo In pDest.GetType().GetProperties()
            Dim sourceProp As Reflection.PropertyInfo = pSource.GetType().GetProperty(destProp.Name, Reflection.BindingFlags.IgnoreCase Or Reflection.BindingFlags.Public Or Reflection.BindingFlags.Instance)
            If ((sourceProp IsNot Nothing) AndAlso (sourceProp.PropertyType = destProp.PropertyType) AndAlso destProp.CanWrite) Then
                If ((destProp.PropertyType.exIsScalarType) OrElse (pHandleRefTypes = eHandleRefTypes.CopyReference)) Then
                    destProp.SetValue(pDest, sourceProp.GetValue(pSource, Nothing), Nothing)
                ElseIf (pHandleRefTypes = eHandleRefTypes.SetNull) Then
                    destProp.SetValue(pDest, Nothing, Nothing)
                ElseIf (pHandleRefTypes = eHandleRefTypes.ThrowError) Then
                    Throw New Exception(String.Format("CopyObject: Unable to copy reference type ({0})", destProp.Name))
                Else
                    'ignore
                End If
            End If
        Next
    End Sub



    Public Shared Function CopyObjectGraph(Of T)(pSource As T) As T
        'TODO: get to this when we need it..
        Throw New NotImplementedException
    End Function

    Public Shared Sub CopyObjectGraphInto(pSource As Object, pDest As Object)
        'TODO: get to this when we need it..
        Throw New NotImplementedException
    End Sub




    Public Class TypeSummaryInfo
        Public Property IsNumeric As Boolean = False
        Public Property IsInteger As Boolean = False
        Public Property IsFloatingPoint As Boolean = False
        Public Property IsNullable As Boolean = False
        Public Property IsImmutable As Boolean = False
        Public Property IsValueType As Boolean = False
        Public Property IsStringType As Boolean = False
        Public Property IsEnum As Boolean = False
    End Class

    Public Shared Function GetTypeSummaryInfo(pType As Type) As TypeSummaryInfo
        If (pType Is Nothing) Then Throw New ArgumentException("Missing type")
        Dim res As New TypeSummaryInfo

        Dim baseType As Type = pType
        If (Nullable.GetUnderlyingType(pType) IsNot Nothing) Then
            baseType = Nullable.GetUnderlyingType(pType)
            res.IsNullable = True
        End If

        Select Case Type.GetTypeCode(baseType)
            Case TypeCode.Boolean
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Byte
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Char
                res.IsImmutable = True
                res.IsValueType = True
                res.IsStringType = True
            Case TypeCode.DateTime
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.DBNull
                res.IsImmutable = True
            Case TypeCode.Decimal
                res.IsNumeric = True
                res.IsFloatingPoint = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Double
                res.IsNumeric = True
                res.IsFloatingPoint = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Empty
                res.IsImmutable = True
            Case TypeCode.Int16
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Int32
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Int64
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Object
                res.IsNullable = True
            Case TypeCode.SByte
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.Single
                res.IsNumeric = True
                res.IsFloatingPoint = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.String
                res.IsNullable = True
                res.IsImmutable = True
                res.IsStringType = True
            Case TypeCode.UInt16
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.UInt32
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case TypeCode.UInt64
                res.IsNumeric = True
                res.IsInteger = True
                res.IsImmutable = True
                res.IsValueType = True
            Case Else
                Throw New ArgumentException("Unknown type")
        End Select

        If (baseType.IsEnum) Then res.IsEnum = True

        Return res
    End Function

    Public Class PropertyDataChanged
        Public Property [Property] As String
        Public Property Original As Object
        Public Property Current As Object
    End Class

    Public Shared Function CompareObjectProperties(pOriginal As Object, pCurrent As Object) As IList(Of PropertyDataChanged)
        If pOriginal Is Nothing Then Throw New FYUtil.UserException("The original object is Nothing")
        If pCurrent Is Nothing Then Throw New FYUtil.UserException("The compare object is Nothing")
        If Not pOriginal.GetType().Equals(pCurrent.GetType) Then Throw New FYUtil.UserException("Unable to comare objects that are not the same type.")

        Dim differences = New List(Of PropertyDataChanged)

        Dim props = pOriginal.GetType().GetProperties
        For Each prop In props
            If prop.CanWrite Then
                Dim origData = prop.GetValue(pOriginal, Nothing)
                Dim curData = prop.GetValue(pCurrent, Nothing)

                If prop.PropertyType.IsByRef Then
                    Dim results = CompareObjectProperties(origData, curData)
                    For Each item In results
                        item.[Property] = prop.Name & "." & item.[Property]
                    Next
                    differences.AddRange(results)
                Else
                    Dim origDataString As String = FYUtil.exToString(origData)
                    Dim curDataString As String = FYUtil.exToString(curData)
                    If (String.Compare(origDataString, curDataString, ignoreCase:=True, culture:=System.Globalization.CultureInfo.InvariantCulture) <> 0) Then
                        differences.Add(New PropertyDataChanged() With {.[Property] = prop.Name,
                                                                         .Original = origData,
                                                                         .Current = curData})
                    End If
                End If
            End If
        Next

        Return differences
    End Function

#End Region


#Region "DateTime"

    ''' <summary>
    ''' Utility function to translate a range of dates or date/times to a consistent format, including timezone. Result should be used in a *left-inclusive* range comparison (i.e. (dt GTE d1 AND dt LT d2))
    ''' </summary>
    ''' <param name="pDateFrom">The start date/time of the range</param>
    ''' <param name="pDateTo">The start date/time of the range</param>
    ''' <param name="pDateOnly">Interpret source date/times as date-only</param>
    ''' <param name="pSourceTimeZone">If timezone conversion is required, source timezone. Defaults to UTC</param>
    ''' <param name="pDestTimeZone">If timezone conversion is required, destination timezone. Defaults to UTC</param>
    ''' <remarks></remarks>
    Public Shared Function TranslateDateRange(pDateFrom As DateTime?, pDateTo As DateTime?, Optional pDateOnly As Boolean = False, Optional pSourceTimeZone As TimeZoneInfo = Nothing, Optional pDestTimeZone As TimeZoneInfo = Nothing) As Tuple(Of DateTime?, DateTime?)
        If (pSourceTimeZone Is Nothing) Then pSourceTimeZone = TimeZoneInfo.Utc
        If (pDestTimeZone Is Nothing) Then pDestTimeZone = TimeZoneInfo.Utc

        If (pDateFrom.HasValue AndAlso pDateTo.HasValue AndAlso (pDateFrom.Value > pDateTo.Value)) Then Dim tmp = pDateFrom : pDateFrom = pDateTo : pDateTo = tmp

        If (pDateFrom.HasValue) Then
            If (pDateOnly) Then pDateFrom = pDateFrom.Value.Date
            pDateFrom = ConvertDateTime(pDateFrom.Value, pSourceTimeZone, pDestTimeZone)
        End If
        If (pDateTo.HasValue) Then
            If (pDateOnly) Then pDateTo = pDateTo.Value.Date.AddDays(1)
            pDateTo = ConvertDateTime(pDateTo.Value, pSourceTimeZone, pDestTimeZone)
        End If

        Return New Tuple(Of DateTime?, DateTime?)(pDateFrom, pDateTo)
    End Function

    Public Shared Function ConvertDateTime(pDate As DateTime, pSourceTimeZone As TimeZoneInfo, pDestTimeZone As TimeZoneInfo) As DateTime
        If (pSourceTimeZone.Equals(pDestTimeZone)) Then Return pDate
        If (pSourceTimeZone.Equals(TimeZoneInfo.Utc)) Then
            Return TimeZoneInfo.ConvertTimeFromUtc(pDate, pDestTimeZone)
        ElseIf (pDestTimeZone.Equals(TimeZoneInfo.Utc)) Then
            If (pDate.Kind = DateTimeKind.Local) Then pDate = DateTime.SpecifyKind(pDate, DateTimeKind.Unspecified)
            Return TimeZoneInfo.ConvertTimeToUtc(pDate, pSourceTimeZone)
        Else
            If (pDate.Kind = DateTimeKind.Local) Then pDate = DateTime.SpecifyKind(pDate, DateTimeKind.Unspecified)
            Return TimeZoneInfo.ConvertTime(pDate, pSourceTimeZone, pDestTimeZone)
        End If
    End Function

    Public Shared Function DateTimeRangesOverlap(pRange1Start As DateTime, pRange1End As DateTime, pRange2Start As DateTime, pRange2End As DateTime) As Boolean
        If (pRange1Start > pRange1End) Then Throw New ArgumentException("DateTimeRangesOverlap: Invalid range 1")
        If (pRange2Start > pRange2End) Then Throw New ArgumentException("DateTimeRangesOverlap: Invalid range 2")

        If ((pRange2Start >= pRange1Start AndAlso pRange2Start < pRange1End) OrElse
            (pRange2End > pRange1Start AndAlso pRange2End <= pRange1End) OrElse
            (pRange1Start >= pRange2Start AndAlso pRange1End <= pRange2End)) Then
            Return True
        Else
            Return False
        End If

        'This method fails the case where endtime1 = starttime2. This may be technically correct, but people don't think that way.
        'If ((Date.Compare(pRange1Start, pRange2End) <= 0) AndAlso (Date.Compare(pRange1End, pRange2Start) >= 0)) Then
        '    Return True
        'End If
        'Return False
    End Function

    Public Shared Function TimeSpanRangesOverlap(pRange1Start As TimeSpan, pRange1End As TimeSpan, pRange2Start As TimeSpan, pRange2End As TimeSpan) As Boolean
        If (pRange1Start > pRange1End) Then Throw New ArgumentException("TimeSpanRangesOverlap: Invalid range 1")
        If (pRange2Start > pRange2End) Then Throw New ArgumentException("TimeSpanRangesOverlap: Invalid range 2")

        If ((pRange2Start >= pRange1Start AndAlso pRange2Start < pRange1End) OrElse
            (pRange2End > pRange1Start AndAlso pRange2End <= pRange1End) OrElse
            (pRange1Start >= pRange2Start AndAlso pRange1End <= pRange2End)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function UserFriendlyDuration(pDate As DateTime, Optional pCompareDate As DateTime? = Nothing, Optional pMaxParts As Int16 = 2) As String
        If Not pCompareDate.HasValue Then pCompareDate = DateTime.Now
        Dim ts = pCompareDate.Value.Subtract(pDate)
        Return UserFriendlyDuration(ts, pMaxParts)
    End Function

    Public Shared Function UserFriendlyDuration(pTimespan As TimeSpan, Optional pMaxDepth As Int32 = 2) As String
        Return UserFriendlyDuration(pTimespan, pMaxDepth, 1)
    End Function

    Private Shared Function UserFriendlyDuration(pTimespan As TimeSpan, pMaxParts As Int32, ByRef pDepth As Int32) As String
        Const SECOND As Integer = 1
        Const MINUTE As Integer = 60 * SECOND
        Const HOUR As Integer = 60 * MINUTE
        Const DAY As Integer = 24 * HOUR

        Dim delta = Math.Abs(pTimespan.TotalSeconds)

        If delta >= 365 * DAY Then
            Dim years = CDbl(pTimespan.Days) / 365
            Return If(years <= 1, "1 year", Math.Round(years, 1) & " years")
        End If
        If delta >= 30 * DAY Then
            Dim months = CInt(pTimespan.Days / 30)
            Dim remaining = pTimespan.Subtract(New TimeSpan(months * 30, 0, 0, 0))
            If remaining.Days < 0 Then
                'If the conversion from months to days results in a negative remainder, adjust the months/remaining days accordingly 
                months = months - 1
                remaining = remaining.Add(New TimeSpan(30, 0, 0, 0))
            End If
            Dim response = months & " month" & If(months = 1, "", "s")
            If pDepth < pMaxParts Then response &= ", " & UserFriendlyDuration(remaining, pMaxParts, pDepth + 1)
            Return response
        End If
        If delta >= 24 * HOUR Then
            Dim response = pTimespan.Days & " day" & If(pTimespan.Days = 1, "", "s")
            Dim remaining = pTimespan.Subtract(New TimeSpan(pTimespan.Days, 0, 0, 0))
            If pDepth < pMaxParts Then response &= ", " & UserFriendlyDuration(remaining, pMaxParts, pDepth + 1)
            Return response
        End If
        If delta >= 60 * MINUTE Then
            Dim response = pTimespan.Hours & " hour" & If(pTimespan.Hours = 1, "", "s")
            Dim remaining = pTimespan.Subtract(New TimeSpan(pTimespan.Hours, 0, 0))
            If pDepth < pMaxParts Then response &= ", " & UserFriendlyDuration(remaining, pMaxParts, pDepth + 1)
            Return response
        End If
        If delta >= 1 * MINUTE Then
            Dim response = pTimespan.Minutes & " minute" & If(pTimespan.Minutes = 1, "", "s")
            Dim remaining = pTimespan.Subtract(New TimeSpan(0, pTimespan.Minutes, 0))
            If pDepth < pMaxParts Then response &= ", " & UserFriendlyDuration(remaining, pMaxParts, pDepth + 1)
            Return response
        End If
        Return pTimespan.Seconds & " second" & If(pTimespan.Seconds = 1, "", "s")
    End Function

    Public Shared Function ApproximateDuration(pDate As DateTime, Optional pCompareDate As DateTime? = Nothing) As String
        If Not pCompareDate.HasValue Then pCompareDate = DateTime.Now
        Dim ts = pCompareDate.Value.Subtract(pDate)
        Return ApproximateDuration(ts)
    End Function

    Public Shared Function ApproximateDuration(pTimespan As TimeSpan) As String
        Const SECOND As Integer = 1
        Const MINUTE As Integer = 60 * SECOND
        Const HOUR As Integer = 60 * MINUTE
        Const DAY As Integer = 24 * HOUR
        Const MONTH As Integer = 30 * DAY

        Dim delta = Math.Abs(pTimespan.TotalSeconds)

        If delta < 45 * SECOND Then
            Return If(pTimespan.Seconds = 1, "one second", pTimespan.Seconds & " seconds")
        End If
        If delta < 75 * SECOND Then
            Return "about a minute"
        End If
        If delta < 150 * SECOND Then
            Return "about 2 minutes"
        End If
        If delta < 60 * MINUTE Then
            Return pTimespan.Minutes & " minutes"
        End If
        If delta < 90 * MINUTE Then
            Return "about an hour"
        End If
        If delta < 2.5 * HOUR Then
            Return pTimespan.Hours & " a couple hours"
        End If
        If delta < 24 * HOUR Then
            Return pTimespan.Hours & " hours"
        End If
        If delta < 30 * DAY Then
            Return pTimespan.Days & " days"
        End If
        If delta < 12 * MONTH Then
            Dim months As Integer = Convert.ToInt32(Math.Floor(CDbl(pTimespan.Days) / 30))
            Return If(months <= 1, "one month", months & " months")
        Else
            Dim years As Integer = Convert.ToInt32(Math.Floor(CDbl(pTimespan.Days) / 365))
            Return If(years <= 1, "one year", years & " years")
        End If
    End Function

#End Region


#Region "General Formatting"

    Public Enum eNameFormat
        ''' <summary>Prefix First Middle Last Suffix</summary>
        Standard
        ''' <summary>Last, First Middle Suffix</summary>
        LastFirstFull
        ''' <summary>First Last</summary>
        FirstLastOnly
    End Enum

    Public Shared Function FormatName(pFormat As eNameFormat, pFirstName As String, pLastName As String, pMiddleName As String, Optional pPrefix As String = Nothing, Optional pSuffix As String = Nothing) As String
        Dim res As New Text.StringBuilder

        Select Case pFormat
            Case eNameFormat.LastFirstFull
                res.Append(pLastName)
                If (Not String.IsNullOrEmpty(pFirstName)) Then
                    If (res.Length > 0) Then res.Append(", ")
                    res.Append(pFirstName)
                End If
                If (Not String.IsNullOrEmpty(pMiddleName)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pMiddleName)
                End If
                If (Not String.IsNullOrEmpty(pSuffix)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pSuffix)
                End If
            Case eNameFormat.FirstLastOnly
                res.Append(pFirstName)
                If (Not String.IsNullOrEmpty(pLastName)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pLastName)
                End If
            Case eNameFormat.Standard
                res.Append(pPrefix)
                If (Not String.IsNullOrEmpty(pFirstName)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pFirstName)
                End If
                If (Not String.IsNullOrEmpty(pMiddleName)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pMiddleName)
                End If
                If (Not String.IsNullOrEmpty(pLastName)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pLastName)
                End If
                If (Not String.IsNullOrEmpty(pSuffix)) Then
                    If (res.Length > 0) Then res.Append(" ")
                    res.Append(pSuffix)
                End If
            Case Else
                Throw New NotImplementedException
        End Select

        Return res.ToString
    End Function

    Public Shared Function FormatAddress(pLine1 As String, pLine2 As String, pCity As String, pState As String, pZip As String) As String
        Return FormatAddress("US", pLine1, pLine2, Nothing, Nothing, pCity, pState, pZip, Nothing)
    End Function

    Public Shared Function FormatAddress(pCountryCode As String, pLine1 As String, pLine2 As String, pLine3 As String, pLine4 As String, pCity As String, pState As String, pZip As String, Optional pCountryPostalCode As String = Nothing) As String
        Dim res As New Text.StringBuilder

        If (Not String.IsNullOrEmpty(pLine1)) Then res.AppendLine(pLine1)
        If (Not String.IsNullOrEmpty(pLine2)) Then res.AppendLine(pLine2)
        If String.IsNullOrWhiteSpace(pCountryCode) OrElse pCountryCode = "US" Then
            Dim lenStart = res.Length
            If (Not String.IsNullOrEmpty(pCity)) Then
                res.Append(pCity)
            End If
            If (Not String.IsNullOrEmpty(pState)) Then
                If (res.Length > lenStart) Then res.Append(", ")
                res.Append(pState)
            End If
            If (Not String.IsNullOrEmpty(pZip)) Then
                If (res.Length > lenStart) Then res.Append(" ")
                res.Append(pZip)
            End If
        Else
            If (Not String.IsNullOrEmpty(pLine3)) Then res.AppendLine(pLine3)
            If (Not String.IsNullOrEmpty(pLine4)) Then res.AppendLine(pLine4)
        End If
        If (Not String.IsNullOrEmpty(pCountryPostalCode)) Then res.AppendLine(pCountryPostalCode)

        Return res.ToString
    End Function

    Public Shared Function SerializeAddress(pAddressCountry As String, pAddressLine1 As String, pAddressLine2 As String, pAddressLine3 As String, pAddressLine4 As String, pAddressCity As String, pAddressState As String, pAddressZip As String) As String
        Dim addr = New With {.Country = pAddressCountry,
                             .AddressLine1 = pAddressLine1,
                             .AddressLine2 = pAddressLine2,
                             .AddressLine3 = pAddressLine3,
                             .AddressLine4 = pAddressLine4,
                             .AddressCity = pAddressCity,
                             .AddressState = pAddressState,
                             .AddressZip = pAddressZip}
        Return FYUtil.Serialization.SerializeObject(addr, FYUtil.Serialization.eSerializer.JsonNet)
    End Function


#End Region


End Class
