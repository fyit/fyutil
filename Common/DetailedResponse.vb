﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Xml.Serialization
Imports System.Xml

<Serializable()>
Public Class DetailedResponse(Of payloadType)
    Inherits DetailedResponse

    Protected _payload As payloadType

    Public Sub SetReturnObject(pReturnObject As payloadType)
        _payload = pReturnObject
    End Sub

    Public ReadOnly Property ReturnObject As payloadType
        Get
            Return _payload
        End Get
    End Property

    Public Sub New()
    End Sub

    Public Sub New(pInitialStatus As eStatusType, Optional pMessage As String = Nothing)
        MyBase.new(pInitialStatus, pMessage)
    End Sub

    Public Sub New(pPayload As payloadType, Optional pMessage As String = Nothing)
        MyBase.New(eStatusType.Ok, pMessage)
        _payload = pPayload
    End Sub

End Class

<Serializable()>
Public Class DetailedResponse
    Implements Xml.Serialization.IXmlSerializable

    Public Enum eStatusType As Int32
        NotSet = 0
        Ok = 1
        Warning = 2
        [Error] = 3
    End Enum

    Public Class DetailedResponseMessage
        Public Property Status As eStatusType
        Public Property Message As String
    End Class

    Protected _status As eStatusType = eStatusType.NotSet
    Protected _messages As New List(Of DetailedResponseMessage)

    Public Sub New()
    End Sub

    Public Sub New(pInitialStatus As eStatusType, Optional pMessage As String = Nothing)
        _status = pInitialStatus
        If Not String.IsNullOrWhiteSpace(pMessage) Then AddMessage(pMessage)
    End Sub

    ''' <summary>
    ''' Will add the message and update the status, assuming the status passed is higher in escalation than the current status.
    ''' </summary>
    Public Sub AddMessage(pMessage As String, pStatus As eStatusType, Optional pAtTop As Boolean = False)
        If pStatus > _status Then _status = pStatus
        Dim msg As New DetailedResponseMessage With {.Status = pStatus, .Message = pMessage}
        If pAtTop Then
            _messages.Insert(0, msg)
        Else
            _messages.Add(msg)
        End If
    End Sub

    Public Sub AddMessage(pMessage As String)
        Call AddMessage(pMessage, eStatusType.NotSet)
    End Sub

    ''' <summary>
    ''' Will update the status, regardless of the existing status
    ''' </summary>
    Public Sub SetStatus(pStatus As eStatusType)
        _status = pStatus
    End Sub

    Public ReadOnly Property Status() As eStatusType
        Get
            Return _status
        End Get
    End Property

    Public ReadOnly Property MessageCount() As Int32
        Get
            Return _messages.Count
        End Get
    End Property

    Public ReadOnly Property Messages() As IEnumerable(Of String)
        Get
            Return (From msg In _messages Select msg.Message).ToList
        End Get
    End Property

    Public ReadOnly Property Message() As String
        Get
            Dim sb As New Text.StringBuilder()
            Dim delim As String = ""
            For Each msg In _messages
                sb.Append(delim + msg.Message)
                delim = vbCrLf
            Next
            Return sb.ToString
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return Status.ToString() & vbCrLf & vbCrLf & Message()
    End Function

    Public Sub Consume(pResponse As DetailedResponse, Optional pHeader As String = Nothing)
        If pResponse._status <> eStatusType.NotSet Then Call SetStatus(pResponse._status)
        If pResponse.MessageCount > 0 Then
            If pHeader IsNot Nothing Then AddMessage(pHeader)
            For Each msg In pResponse._messages
                AddMessage(CStr(If(pHeader Is Nothing, "", " - ") + msg.Message), msg.Status)
            Next
        End If
    End Sub

#Region "Serialization Code"

    Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function

    Public Sub ReadXml(reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        While reader.Read()
            If reader.NodeType = XmlNodeType.Element Then
                If reader.Name = "StatusCode" Then Me._status = CType(reader.ReadInnerXml, eStatusType)
                If reader.Name = "Message" Then AddMessage(reader.ReadInnerXml)
            End If
        End While
    End Sub

    Public Sub WriteXml(writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        writer.WriteElementString("StatusCode", CInt(Me.Status).ToString)
        writer.WriteElementString("Status", Me.Status.ToString)
        writer.WriteElementString("Message", Me.Message)
    End Sub

#End Region

End Class
