﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Reflection
Imports System.Runtime.Serialization

<Serializable()>
Public Class EnumString(Of valType As {EnumString(Of valType), New})
    Implements Xml.Serialization.IXmlSerializable

    Private Class EnumNameObject
        Public EnumData As EnumString(Of valType)
        Public NameData As String
        Public Description As String

        Public Sub New(ByVal pEnumData As EnumString(Of valType))
            Me.EnumData = pEnumData
            Me.NameData = Nothing
            Me.Description = Nothing
        End Sub
    End Class

    Private Shared _values As New Dictionary(Of String, EnumNameObject)

#Region "Xml Serialization Code"

    Public Sub ReadXml(ByVal reader As System.Xml.XmlReader) Implements System.Xml.Serialization.IXmlSerializable.ReadXml
        While reader.Read()
            If reader.NodeType = Xml.XmlNodeType.Element Then
                If reader.Name = "Code" Then Me.Code = reader.ReadInnerXml
            End If
        End While
    End Sub

    Public Sub WriteXml(ByVal writer As System.Xml.XmlWriter) Implements System.Xml.Serialization.IXmlSerializable.WriteXml
        writer.WriteElementString("Code", Me.Code)
        writer.WriteElementString("Name", Me.Name)
        writer.WriteElementString("Description", Me.Description)
        writer.WriteElementString("Mapped", If(ContainsCode(Me.Code), "1", "0"))
    End Sub

    Public Function GetSchema() As System.Xml.Schema.XmlSchema Implements System.Xml.Serialization.IXmlSerializable.GetSchema
        Return Nothing
    End Function

#End Region

    Protected Sub New()
    End Sub

    Protected Sub New(ByVal pCode As String, Optional ByVal pDescription As String = Nothing)
        If pCode Is Nothing Then Throw New Exception("Code cannot be nothing.")
        _code = pCode

        If _values.ContainsKey(pCode) Then Throw New Exception(String.Format("The string value '{0}' already exists in EnumString {1}!", pCode, Me.GetType().Name))
        Dim tmpStruct As New EnumNameObject(Me)
        tmpStruct.Description = pDescription
        _values.Add(Me.Code, tmpStruct)
    End Sub

    Private _code As String = Nothing
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property

    Public ReadOnly Property Mapped As Boolean
        Get
            Return ContainsCode(_code)
        End Get
    End Property

    Public ReadOnly Property Description As String
        Get
            With _values
                If .ContainsKey(_code) Then
                    If Not String.IsNullOrWhiteSpace(.Item(_code).Description) Then Return .Item(_code).Description Else Return Me.Name
                Else
                    Return String.Format("'{0}' Not Mapped", _code)
                End If
            End With
        End Get
    End Property

    Private _name As String
    Private _nameLookedUp As Boolean
    Public ReadOnly Property Name As String
        Get
            If Not _nameLookedUp Then
                If _values.ContainsKey(_code) Then _name = GetName(_code) Else _name = Nothing
                _nameLookedUp = True
            End If
            Return _name
        End Get
    End Property

    Public Shared Function GetName(ByVal pCode As String) As String
        'If it's already looked up - just return it using the dictionary 
        With _values
            If .ContainsKey(pCode) AndAlso .Item(pCode).NameData IsNot Nothing Then Return .Item(pCode).NameData
        End With

        'Otherwise loop through the methods (default implementation) on this element and find the one that matches and set it up 
        For Each meth In GetType(valType).GetFields()
            If pCode = DirectCast(meth.GetValue(Nothing), EnumString(Of valType))._code Then
                Dim curItem = _values.Item(pCode)
                curItem.NameData = meth.Name
                Return curItem.NameData
            End If
        Next

        'Otherwise loop through the properties (alternate implementation) on this element and find the one that matches and set it up 
        Dim properties = GetType(valType).GetProperties()
        For Each prop In properties
            If pCode = DirectCast(prop.GetValue(Nothing, Nothing), valType).Code Then
                Dim curItem = _values.Item(pCode)
                curItem.NameData = prop.Name
                Return curItem.NameData
            End If
        Next

        Return Nothing
    End Function

    Public Overrides Function ToString() As String
        Return Me.Description
    End Function

    Private Shared Sub Init()
        Try
            SyncLock _values
                'Check again to ensure nobody else populated it since you locked it.
                If _values.Count = 0 Then
                    System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(GetType(valType).TypeHandle)
                End If
            End SyncLock

        Catch ex As Exception
            Dim msg As String = "There was an error initializing the EnumString " & GetType(valType).Name & ".  The values can only be initialized once (each must be unique) and they must be defined as Public Shared member variables in the EnumString class!"
            Throw New Exception(msg & vbCrLf & vbCrLf & FYUtil.Logging.GetExceptionDetails(ex))
        End Try
    End Sub

    Public Shared Function GetByCode(ByVal pCode As String, Optional ByVal pDefault As valType = Nothing) As valType
        If pCode Is Nothing Then Return Nothing
        If (_values.Count = 0) Then Init()
        If _values.ContainsKey(pCode) Then
            Return New valType With {._code = pCode, ._name = GetName(pCode)}
        ElseIf (Not _values.ContainsKey(pCode)) AndAlso (pDefault IsNot Nothing) Then
            Return pDefault
        Else
            'Unmapped
            Return New valType With {._code = pCode, ._name = Nothing}
        End If
    End Function

    ''' <summary>
    ''' Protected so it's used through the extension function (to force user to know it's less efficient than looking up by code)
    ''' </summary>
    Public Shared Function LookupByNameOrDescription(pData As String, Optional ByVal pDefault As valType = Nothing) As valType
        If _values.Count = 0 Then Init()
        Return DirectCast((From item In _values
                           Where UCase(GetName(item.Key)) = pData.ToUpper OrElse (item.Value IsNot Nothing AndAlso UCase(item.Value.Description) = pData.ToUpper)
                           Select item.Value.EnumData).FirstOrDefault, valType)
    End Function

    ''' <summary>
    ''' Protected so it's used through the extension function (to force user to know it's less efficient than looking up by code)
    ''' </summary>
    Public Shared Function LookupByAny(pData As String, Optional ByVal pDefault As valType = Nothing) As valType
        Dim tmp = GetByCode(pData, pDefault)
        If tmp Is Nothing OrElse Not tmp.Mapped Then tmp = LookupByNameOrDescription(pData, pDefault)
        Return tmp
    End Function

    Public Shared Function ContainsCode(ByVal pCode As String) As Boolean
        If _values.Count = 0 Then Init()
        Return _values.ContainsKey(pCode)
    End Function

    Public Shared Operator =(ByVal value1 As EnumString(Of valType), ByVal value2 As EnumString(Of valType)) As Boolean
        Return (StringValueOrNothing(value1) = StringValueOrNothing(value2))
    End Operator

    Public Shared Operator <>(ByVal value1 As EnumString(Of valType), ByVal value2 As EnumString(Of valType)) As Boolean
        Return StringValueOrNothing(value1) <> StringValueOrNothing(value2)
    End Operator


    Public Shared Operator =(ByVal value1 As EnumString(Of valType), ByVal value2 As KeyValuePair(Of String, String)) As Boolean
        Return (StringValueOrNothing(value1) = value2.Key)
    End Operator

    Public Shared Operator <>(ByVal value1 As EnumString(Of valType), ByVal value2 As KeyValuePair(Of String, String)) As Boolean
        Return StringValueOrNothing(value1) <> value2.Key
    End Operator


    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return StringValueOrNothing(Me) = StringValueOrNothing(CType(obj, EnumString(Of valType)))
    End Function

    Public Overloads Overrides Function GetHashCode() As Integer
        Return Code.GetHashCode
    End Function

    Private Shared Function StringValueOrNothing(ByVal value As EnumString(Of valType)) As String
        If value Is Nothing Then Return String.Empty Else Return value.Code
    End Function

    ''' <summary>
    ''' Returns a zero based index, based on the order of entry of the elements defined in the Enum String
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function NumericOrder(ByVal value As EnumString(Of valType)) As Int32
        If _values.Count = 0 Then Init()
        Return _values.Keys.ToList.IndexOf(StringValueOrNothing(value))
    End Function

    ''' <summary>
    ''' Will compare the values as per their order of entry 
    ''' </summary>
    ''' <param name="value1"></param>
    ''' <param name="value2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Operator >(ByVal value1 As EnumString(Of valType), ByVal value2 As EnumString(Of valType)) As Boolean
        Return NumericOrder(value1) > NumericOrder(value2)
    End Operator

    ''' <summary>
    ''' Will compare the values as per their order of entry
    ''' </summary>
    ''' <param name="value1"></param>
    ''' <param name="value2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Operator <(ByVal value1 As EnumString(Of valType), ByVal value2 As EnumString(Of valType)) As Boolean
        Return NumericOrder(value1) < NumericOrder(value2)
    End Operator

    ''' <summary>
    ''' Will compare the values first, and if not equal - then by their order of entry
    ''' </summary>
    ''' <param name="value1"></param>
    ''' <param name="value2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Operator >=(ByVal value1 As EnumString(Of valType), ByVal value2 As EnumString(Of valType)) As Boolean
        If value1 = value2 Then Return True Else Return value1 > value2
    End Operator

    ''' <summary>
    ''' Will compare the values first, and if not equal - then by their order of entry
    ''' </summary>
    ''' <param name="value1"></param>
    ''' <param name="value2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Operator <=(ByVal value1 As EnumString(Of valType), ByVal value2 As EnumString(Of valType)) As Boolean
        If value1 = value2 Then Return True Else Return value1 < value2
    End Operator

    Public Shared Function ToBindableList() As List(Of KeyValuePair(Of String, String))
        If _values.Count = 0 Then Init()
        Dim tmpKVPair As New List(Of KeyValuePair(Of String, String))
        For Each curItem In _values.Values
            tmpKVPair.Add(New KeyValuePair(Of String, String)(curItem.EnumData.Code, If(curItem.Description, curItem.EnumData.Description)))
        Next
        Return tmpKVPair
    End Function

End Class

