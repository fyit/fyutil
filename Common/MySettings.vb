﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Configuration

Public Class MySettings

    Public Enum eSource
        AppConfig
        UserThenAppConfig
    End Enum

    Private _myConfig As Configuration
    Private _mySection As AppSettingsSection

    Public Sub New(pSource As eSource, Optional pSectionName As String = "MySettings")
        If (pSource = eSource.AppConfig) Then
            _myConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        ElseIf (pSource = eSource.UserThenAppConfig) Then
            _myConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal)
        Else
            Throw New NotImplementedException
        End If
        _mySection = DirectCast(_myConfig.GetSection(pSectionName), AppSettingsSection)
        If (_mySection Is Nothing) Then Throw New Exception("MySettings: Config section not found")
    End Sub

    Public Sub New(pFile As String, Optional pSectionName As String = "MySettings")
        If (String.IsNullOrWhiteSpace(pFile)) Then Throw New ArgumentException("MySettings: File required")
        If (Not IO.Path.IsPathRooted(pFile)) Then
            Dim asm = System.Reflection.Assembly.GetCallingAssembly()
            pFile = IO.Path.Combine(IO.Path.GetDirectoryName(asm.Location), pFile)
        End If
        If (Not IO.File.Exists(pFile)) Then Throw New Exception("MySettings: File not found")

        Dim map = New ExeConfigurationFileMap With {.ExeConfigFilename = pFile}
        _myConfig = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None)

        _mySection = DirectCast(_myConfig.GetSection(pSectionName), AppSettingsSection)
        If (_mySection Is Nothing) Then Throw New Exception("MySettings: Config section not found")
    End Sub


    Private Function GetValue(pKeyName As String) As String
        Dim item = _mySection.Settings.Item(pKeyName)
        If (item IsNot Nothing) Then
            Return item.Value
        Else
            Return Nothing
        End If
    End Function



#Region "Write"

    Public Sub SetValue(pKeyName As String, pValue As Object)
        Dim strVal As String = Nothing
        If (pValue Is Nothing) Then
            strVal = ""
        Else
            strVal = pValue.ToString
        End If
        If (_mySection.Settings(pKeyName) IsNot Nothing) Then
            _mySection.Settings(pKeyName).Value = strVal
        Else
            _mySection.Settings.Add(pKeyName, strVal)
        End If
    End Sub

    Public Sub Save()
        _myConfig.Save(ConfigurationSaveMode.Modified)
    End Sub

#End Region




#Region "String"

    Public Function GetString(pKeyName As String, Optional pDefault As String = "") As String
        Dim valStr = GetValue(pKeyName)
        If (valStr Is Nothing) Then
            Return pDefault
        Else
            Return valStr
        End If
    End Function

#End Region

#Region "Int32"

    Public Function GetInt32(pKeyName As String) As Int32?
        Return GetInt32Impl(pKeyName, Nothing)
    End Function

    Public Function GetInt32(pKeyName As String, pDefault As Int32) As Int32
        Return GetInt32Impl(pKeyName, pDefault).Value
    End Function

    Private Function GetInt32Impl(pKeyName As String, pDefault As Int32?) As Int32?
        Dim valStr = GetValue(pKeyName)
        If ((valStr Is Nothing) OrElse (Not IsNumeric(valStr))) Then
            Return pDefault
        Else
            Try
                Return Convert.ToInt32(valStr)
            Catch ex As Exception
                Return pDefault
            End Try
        End If
    End Function

#End Region

#Region "Double"

    Public Function GetDouble(pKeyName As String) As Double?
        Return GetDoubleImpl(pKeyName, Nothing)
    End Function

    Public Function GetDouble(pKeyName As String, pDefault As Double) As Double
        Return GetDoubleImpl(pKeyName, pDefault).Value
    End Function

    Private Function GetDoubleImpl(pKeyName As String, pDefault As Double?) As Double?
        Dim valStr = GetValue(pKeyName)
        If ((valStr Is Nothing) OrElse (Not IsNumeric(valStr))) Then
            Return pDefault
        Else
            Try
                Return Convert.ToDouble(valStr)
            Catch ex As Exception
                Return pDefault
            End Try
        End If
    End Function

#End Region

#Region "Boolean"

    Public Function GetBoolean(pKeyName As String) As Boolean?
        Return GetBooleanImpl(pKeyName, Nothing)
    End Function

    Public Function GetBoolean(pKeyName As String, pDefault As Boolean) As Boolean
        Return GetBooleanImpl(pKeyName, pDefault).Value
    End Function

    Private Function GetBooleanImpl(pKeyName As String, pDefault As Boolean?) As Boolean?
        Dim valStr = GetValue(pKeyName)
        If (valStr Is Nothing) Then
            Return pDefault
        Else
            valStr = valStr.ToLower()
            If ({"yes", "true", "1", "y", "t"}.Contains(valStr, StringComparer.OrdinalIgnoreCase)) Then
                Return True
            ElseIf ({"no", "false", "0", "n", "f"}.Contains(valStr, StringComparer.OrdinalIgnoreCase)) Then
                Return False
            Else
                Return pDefault
            End If
        End If
    End Function

#End Region

#Region "Enum"

    Public Function GetEnum(Of TEnum As Structure)(pKeyName As String) As TEnum?
        Return GetEnumImpl(Of TEnum)(pKeyName, Nothing)
    End Function

    Public Function GetEnum(Of TEnum As Structure)(pKeyName As String, pDefault As TEnum) As TEnum
        Return GetEnumImpl(Of TEnum)(pKeyName, pDefault).Value
    End Function

    Private Function GetEnumImpl(Of TEnum As Structure)(pKeyName As String, pDefault As TEnum?) As TEnum?
        Dim valStr = GetValue(pKeyName)
        If (valStr Is Nothing) Then
            Return pDefault
        Else
            Try
                Dim res As TEnum = DirectCast(System.Enum.Parse(GetType(TEnum), valStr, ignoreCase:=True), TEnum)
                Return res
            Catch ex As Exception
                Return pDefault
            End Try
        End If
    End Function

#End Region


End Class
