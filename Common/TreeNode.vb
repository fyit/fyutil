﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Collections
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text

'adapted from http://www.siepman.nl/blog/post/2013/07/30/tree-node-nodes-descendants-ancestors.aspx

Public Class TreeNode(Of T)
    Implements IEqualityComparer
    'Implements IEnumerable(Of T)
    Implements IEnumerable(Of TreeNode(Of T))

    Private m_Value As T
    Private m_Parent As TreeNode(Of T)
    Private ReadOnly _children As New List(Of TreeNode(Of T))()

    Public Property Parent() As TreeNode(Of T)
        Get
            Return m_Parent
        End Get
        Private Set(value As TreeNode(Of T))
            m_Parent = value
        End Set
    End Property

    Public Property Value() As T
        Get
            Return m_Value
        End Get
        Set(value As T)
            m_Value = value
        End Set
    End Property

    Public Sub New(value As T)
        m_Value = value
    End Sub

    Default Public ReadOnly Property Item(index As Integer) As TreeNode(Of T)
        Get
            Return _children(index)
        End Get
    End Property

    Public Function Add(value As T, Optional index As Integer = -1) As TreeNode(Of T)
        Dim childNode = New TreeNode(Of T)(value)
        Add(childNode, index)
        Return childNode
    End Function

    Public Sub Add(childNode As TreeNode(Of T), Optional index As Integer = -1)
        If index < -1 Then
            Throw New ArgumentException("The index can not be lower then -1")
        End If
        If index > Children.Count() - 1 Then
            Throw New ArgumentException(String.Format("The index ({0}) can not be higher then index of the last iten. Use the AddChild() method without an index to add at the end", index))
        End If
        If Not childNode.IsRoot Then
            Throw New ArgumentException(String.Format("The child node with value [{0}] can not be added because it is not a root node.", childNode.Value))
        End If

        If Root Is childNode Then
            Throw New ArgumentException(String.Format("The child node with value [{0}] is the rootnode of the parent.", childNode.Value))
        End If

        If childNode.SelfAndDescendants.Any(Function(n) Me = n) Then
            Throw New ArgumentException(String.Format("The childnode with value [{0}] can not be added to itself or its descendants.", childNode.Value))
        End If
        childNode.Parent = Me
        If index = -1 Then
            _children.Add(childNode)
        Else
            _children.Insert(index, childNode)
        End If
    End Sub

    Public Function AddFirstChild(value As T) As TreeNode(Of T)
        Dim childNode = New TreeNode(Of T)(value)
        AddFirstChild(childNode)
        Return childNode
    End Function

    Public Sub AddFirstChild(childNode As TreeNode(Of T))
        Add(childNode, 0)
    End Sub

    Public Function AddFirstSibling(value As T) As TreeNode(Of T)
        Dim childNode = New TreeNode(Of T)(value)
        AddFirstSibling(childNode)
        Return childNode
    End Function

    Public Sub AddFirstSibling(childNode As TreeNode(Of T))
        Parent.AddFirstChild(childNode)
    End Sub

    Public Function AddLastSibling(value As T) As TreeNode(Of T)
        Dim childNode = New TreeNode(Of T)(value)
        AddLastSibling(childNode)
        Return childNode
    End Function

    Public Sub AddLastSibling(childNode As TreeNode(Of T))
        Parent.Add(childNode)
    End Sub

    Public Function AddParent(value As T) As TreeNode(Of T)
        Dim newNode = New TreeNode(Of T)(value)
        AddParent(newNode)
        Return newNode
    End Function

    Public Sub AddParent(parentNode As TreeNode(Of T))
        If Not IsRoot Then
            Throw New ArgumentException(String.Format("This node [{0}] already has a parent", Value), "parentNode")
        End If
        parentNode.Add(Me)
    End Sub

    Public ReadOnly Property Ancestors() As IEnumerable(Of TreeNode(Of T))
        Get
            If IsRoot Then
                Return Enumerable.Empty(Of TreeNode(Of T))()
            End If
            Return {Parent}.Concat(Parent.Ancestors)
        End Get
    End Property

    Public ReadOnly Property Descendants() As IEnumerable(Of TreeNode(Of T))
        Get
            Return SelfAndDescendants.Skip(1)
        End Get
    End Property

    Public ReadOnly Property Children() As IEnumerable(Of TreeNode(Of T))
        Get
            Return _children
        End Get
    End Property

    Public ReadOnly Property Siblings() As IEnumerable(Of TreeNode(Of T))
        Get

            Return SelfAndSiblings.Where(AddressOf Other)
        End Get
    End Property

    Private Function Other(node As TreeNode(Of T)) As Boolean
        Return Not ReferenceEquals(node, Me)
    End Function

    Public ReadOnly Property SelfAndChildren() As IEnumerable(Of TreeNode(Of T))
        Get
            Return {Me}.Concat(Children)
        End Get
    End Property

    Public ReadOnly Property SelfAndAncestors() As IEnumerable(Of TreeNode(Of T))
        Get
            Return {Me}.Concat(Ancestors)
        End Get
    End Property

    Public ReadOnly Property SelfAndDescendants() As IEnumerable(Of TreeNode(Of T))
        Get
            Return {Me}.Concat(Children.SelectMany(Function(c) c.SelfAndDescendants))
        End Get
    End Property

    Public ReadOnly Property SelfAndSiblings() As IEnumerable(Of TreeNode(Of T))
        Get
            If IsRoot Then
                Return {Me}
            End If

            Return Parent.Children
        End Get
    End Property

    Public ReadOnly Property All() As IEnumerable(Of TreeNode(Of T))
        Get
            Return Root.SelfAndDescendants
        End Get
    End Property


    Public ReadOnly Property SameLevel() As IEnumerable(Of TreeNode(Of T))
        Get

            Return SelfAndSameLevel.Where(AddressOf Other)
        End Get
    End Property

    Public ReadOnly Property Level() As Integer
        Get
            Return Ancestors.Count()
        End Get
    End Property

    Public ReadOnly Property SelfAndSameLevel() As IEnumerable(Of TreeNode(Of T))
        Get
            Return GetNodesAtLevel(Level)
        End Get
    End Property

    Public Function GetNodesAtLevel(level As Integer) As IEnumerable(Of TreeNode(Of T))
        Return Root.GetNodesAtLevelInternal(level)
    End Function

    Private Function GetNodesAtLevelInternal(lvl As Integer) As IEnumerable(Of TreeNode(Of T))
        If lvl = Level Then
            Return {Me}
        End If
        Return Children.SelectMany(Function(c) c.GetNodesAtLevelInternal(lvl))
    End Function

    Public ReadOnly Property Root() As TreeNode(Of T)
        Get
            Return SelfAndAncestors.Last()
        End Get
    End Property

    Public Sub Disconnect()
        If IsRoot Then
            Throw New InvalidOperationException(String.Format("The root node [{0}] can not get disconnected from a parent.", Value))
        End If
        Parent._children.Remove(Me)
        Parent = Nothing
    End Sub

    Public ReadOnly Property IsRoot() As Boolean
        Get
            Return Parent Is Nothing
        End Get
    End Property

    'Private Function IEnumerableT_GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
    '    Return _children.Values().GetEnumerator()
    'End Function

    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return _children.GetEnumerator()
    End Function

    Public Function GetEnumerator() As IEnumerator(Of TreeNode(Of T)) Implements IEnumerable(Of TreeNode(Of T)).GetEnumerator
        Return _children.GetEnumerator()
    End Function

    Public Overrides Function ToString() As String
        Return Value.ToString()
    End Function

    Public Shared Function CreateTree(Of TId As Structure)(values As IEnumerable(Of T), idSelector As Func(Of T, TId), parentIdSelector As Func(Of T, System.Nullable(Of TId))) As IEnumerable(Of TreeNode(Of T))
        Dim valuesCache = values.ToList()
        If Not valuesCache.Any() Then
            Return Enumerable.Empty(Of TreeNode(Of T))()
        End If
        Dim itemWithIdAndParentIdIsTheSame As T = valuesCache.FirstOrDefault(Function(v) IsSameId(idSelector(v), parentIdSelector(v)))
        If itemWithIdAndParentIdIsTheSame IsNot Nothing Then
            Throw New ArgumentException(String.Format("At least one value has the samen Id and parentId [{0}]", itemWithIdAndParentIdIsTheSame))
        End If

        Dim nodes = valuesCache.[Select](Function(v) New TreeNode(Of T)(v))
        Return CreateTree(nodes, idSelector, parentIdSelector)

    End Function

    Public Shared Function CreateTree(Of TId As Structure)(rootNodes As IEnumerable(Of TreeNode(Of T)), idSelector As Func(Of T, TId), parentIdSelector As Func(Of T, System.Nullable(Of TId))) As IEnumerable(Of TreeNode(Of T))

        Dim rootNodesCache = rootNodes.ToList()
        'Dim duplicates = rootNodesCache.Duplicates(Function(n) n).ToList()
        'If duplicates.Any() Then
        '    Throw New ArgumentException(String.Format("One or more values contains {0} duplicate keys. The first duplicate is: [{1}]", duplicates.Count, duplicates(0)))
        'End If

        For Each rootNode In rootNodesCache
            Dim parentId = parentIdSelector(rootNode.Value)
            Dim parent = rootNodesCache.FirstOrDefault(Function(n) IsSameId(idSelector(n.Value), parentId))

            If parent IsNot Nothing Then
                parent.Add(rootNode)
            ElseIf parentId IsNot Nothing Then
                Throw New ArgumentException(String.Format("A value has the parent ID [{0}] but no other nodes has this ID", parentId.Value))
            End If
        Next
        Dim result = rootNodesCache.Where(Function(n) n.IsRoot)
        Return result
    End Function

    Public Sub Traverse(pAction As Action(Of TreeNode(Of T)))
        pAction(Me)
        For Each child In _children
            child.Traverse(pAction)
        Next
    End Sub

    Public Sub TraverseValues(pAction As Action(Of T))
        pAction(Value)
        For Each child In _children
            child.TraverseValues(pAction)
        Next
    End Sub

    Public Sub TraverseDepthFirst(pAction As Action(Of TreeNode(Of T)))
        For Each child In _children
            child.TraverseDepthFirst(pAction)
        Next
        pAction(Me)
    End Sub

    Public Sub TraverseValuesDepthFirst(pAction As Action(Of T))
        For Each child In _children
            child.TraverseValuesDepthFirst(pAction)
        Next
        pAction(Value)
    End Sub

    Private Shared Function IsSameId(Of TId As Structure)(id As TId, parentId As System.Nullable(Of TId)) As Boolean
        Return parentId IsNot Nothing AndAlso id.Equals(parentId.Value)
    End Function

#Region "Equals en =="

    Public Shared Operator =(value1 As TreeNode(Of T), value2 As TreeNode(Of T)) As Boolean
        If DirectCast(value1, Object) Is Nothing AndAlso DirectCast(value2, Object) Is Nothing Then
            Return True
        End If
        Return ReferenceEquals(value1, value2)
    End Operator

    Public Shared Operator <>(value1 As TreeNode(Of T), value2 As TreeNode(Of T)) As Boolean
        Return Not (value1 = value2)
    End Operator

    Public Overrides Function Equals(anderePeriode As [Object]) As Boolean
        Dim valueThisType = TryCast(anderePeriode, TreeNode(Of T))
        Return Me Is valueThisType
    End Function

    Public Overloads Function Equals(value As TreeNode(Of T)) As Boolean
        Return Me Is value
    End Function

    Public Overloads Function Equals(value1 As TreeNode(Of T), value2 As TreeNode(Of T)) As Boolean
        Return value1 Is value2
    End Function

    Private Function IEqualityComparer_Equals(value1 As Object, value2 As Object) As Boolean Implements IEqualityComparer.Equals
        Dim valueThisType1 = TryCast(value1, TreeNode(Of T))
        Dim valueThisType2 = TryCast(value2, TreeNode(Of T))

        Return Equals(valueThisType1, valueThisType2)
    End Function

    Public Overloads Function GetHashCode(obj As Object) As Integer Implements IEqualityComparer.GetHashCode
        Return GetHashCode(TryCast(obj, TreeNode(Of T)))
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return GetHashCode(Me)
    End Function

    Public Overloads Function GetHashCode(value As TreeNode(Of T)) As Integer
        Return MyBase.GetHashCode()
    End Function

#End Region

End Class
