﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Security

<Serializable()>
Public Class CustomIdentity
	Inherits System.Security.Principal.GenericIdentity

    Private _payload As Object

    Public Sub New(pName As String, pPayload As Object)
        MyBase.New(pName)
        _payload = pPayload
    End Sub

    Public ReadOnly Property Payload As Object
        Get
            Return _payload
        End Get
    End Property

    Public Shared Function GetPayload(Of TPayload)(pGenericIdentity As Principal.IIdentity) As TPayload
        Dim ident As CustomIdentity = TryCast(pGenericIdentity, CustomIdentity)
        If ident IsNot Nothing Then Return DirectCast(ident.Payload, TPayload) Else Return Nothing
    End Function

    Public Shared Function GetPayload(Of TPayload)() As TPayload
        Return GetPayload(Of TPayload)(System.Threading.Thread.CurrentPrincipal.Identity)
    End Function

	Public Shared Sub EnsureCustomIdentityOnThread(pBuilder As Func(Of String, Object))
		If System.Threading.Thread.CurrentPrincipal IsNot Nothing AndAlso System.Threading.Thread.CurrentPrincipal.Identity IsNot Nothing Then
			Dim ci As CustomIdentity = TryCast(System.Threading.Thread.CurrentPrincipal.Identity, CustomIdentity)
			If ci Is Nothing Then
				ci = New CustomIdentity(System.Threading.Thread.CurrentPrincipal.Identity.Name, pBuilder.Invoke(System.Threading.Thread.CurrentPrincipal.Identity.Name))
				System.Threading.Thread.CurrentPrincipal = New System.Security.Principal.GenericPrincipal(ci, Nothing)
			End If
		End If
    End Sub

End Class
