﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



''' <summary>
''' Create a temporary directory for storing work files. All files placed under this directory will be deleted on Dispose. Be sure all file locks are closed before disposing this object.
''' </summary>
''' <remarks></remarks>
Public Class TempDirectory
    Implements IDisposable

    Private _path As String

    Public ReadOnly Property Path() As String
        Get
            Return _path
        End Get
    End Property

    ''' <summary>
    ''' Create the temp directory within the specified directory. Optionally with the specified subdirectory name.
    ''' </summary>
    ''' <param name="pParentPath">Path under which to create a new folder. If unspecified, use system or profile temp folder</param>
    ''' <param name="pSubDirBaseName">Name of subdir to create. If this name exists, a new name will be generated based on this name.</param>
    ''' <remarks></remarks>
    Sub New(Optional pParentPath As String = Nothing, Optional pSubDirBaseName As String = Nothing)
        If (String.IsNullOrEmpty(pParentPath)) Then pParentPath = IO.Path.GetTempPath()
        If (Not IO.Directory.Exists(pParentPath)) Then IO.Directory.CreateDirectory(pParentPath)
        If (String.IsNullOrEmpty(pSubDirBaseName)) Then pSubDirBaseName = "Temp"
        Dim subDir = pSubDirBaseName
        While (IO.Directory.Exists(IO.Path.Combine(pParentPath, subDir)))
            subDir = String.Format("{0}.{1}", pSubDirBaseName, Utilities.RandomString(10, Utilities.CHARSET_UPPERALPHA))
        End While

        _path = IO.Path.Combine(pParentPath, subDir)
        If (Not IO.Directory.Exists(_path)) Then IO.Directory.CreateDirectory(_path)
    End Sub


    ''' <summary>
    ''' Create a unique filename within the temp directory
    ''' </summary>
    ''' <param name="pBaseName"></param>
    ''' <param name="pExt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTempFilename(Optional pBaseName As String = "temp", Optional pExt As String = "tmp") As String
        If (String.IsNullOrEmpty(pBaseName)) Then pBaseName = "temp"
        If (String.IsNullOrEmpty(pExt)) Then pExt = "tmp"
        If (pExt.StartsWith(".")) Then pExt = pExt.TrimStart("."c)

        Dim fname As String = ""
        Dim fpath As String = ""
        Do
            fname = String.Format("{0}_{1}.{2}", pBaseName, Utilities.RandomString(10, Utilities.CHARSET_UPPERALPHANOVOWELS), pExt)
            fpath = IO.Path.Combine(_path, fname)
        Loop Until (Not IO.File.Exists(fpath))

        Return fpath
    End Function

    Public Function IsEmpty() As Boolean
        If (IO.Directory.GetFiles(_path).Count > 0) Then Return False
        If (IO.Directory.GetDirectories(_path).Count > 0) Then Return False
        Return True
    End Function

    Public Sub DeleteContents()
        'delete what you can, ignore errors
        Dim fils As String() = IO.Directory.GetFiles(_path, "*", IO.SearchOption.AllDirectories)
        For Each fil As String In fils
            Try
                IO.File.Delete(fil)
            Catch
            End Try
        Next
        Dim dirs As String() = IO.Directory.GetDirectories(_path)
        For Each dir As String In dirs
            Try
                IO.Directory.Delete(dir)
            Catch
            End Try
        Next
    End Sub

    Public Sub Delete()
        DeleteContents()
        Try
            IO.Directory.Delete(_path, recursive:=True)
        Catch
        End Try
    End Sub




#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' dispose managed state (managed objects).
            End If

            ' free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' set large fields to null.

            Delete()

        End If
        Me.disposedValue = True
    End Sub

    ' override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(False)
        MyBase.Finalize()
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region


End Class
