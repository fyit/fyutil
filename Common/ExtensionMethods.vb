﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Runtime.CompilerServices

Public Module ExtensionMethods

#Region "Object"

    <Extension()>
    Public Function exToString(ByVal pItem As Object) As String
        If pItem Is Nothing OrElse String.IsNullOrWhiteSpace(pItem.ToString) Then Return "" Else Return pItem.ToString
    End Function

    <Extension()>
    Public Function exIsScalarType(pType As Type) As Boolean
        If (pType.IsValueType) Then
            Return True
        ElseIf (pType Is GetType(String)) Then
            Return True
        End If
        Return False
    End Function

    <Extension()>
    Public Function exIsNullableType(pType As Type) As Boolean
        Return (pType.IsGenericType) AndAlso (pType.GetGenericTypeDefinition() Is GetType(Nullable(Of )))
    End Function

    <Extension()>
    Public Function exGetNaturalType(pType As Type) As Type
        If (pType.exIsNullableType) Then
            Return Nullable.GetUnderlyingType(pType)
        Else
            Return pType
        End If
    End Function


#End Region


#Region "String"

    <Extension>
    Public Function exTrim(pString As String) As String
        If pString IsNot Nothing Then Return pString.Trim Else Return Nothing
    End Function

    <Extension()>
    Public Function exFirstChars(pString As String, pLength As Int32, Optional pOffset As Int32 = 0, Optional pAddEllipsis As Boolean = False) As String
        If pString Is Nothing Then Return ""
        Dim ret As String = Nothing
        If pString.Length > pLength Then
            ret = pString.Substring(pOffset, pLength)
        Else
            ret = pString.Substring(pOffset)
        End If
        If pAddEllipsis Then Return ret & "..."
        Return ret
    End Function

    <Extension()>
    Public Function exToAlphaNumeric(pData As String) As String
        If pData Is Nothing Then Return Nothing Else Return Utilities.AlphaNumeric(pData)
    End Function

    <Extension()>
    Public Function exToNumbersOnly(pData As String) As String
        If pData Is Nothing Then Return Nothing Else Return Utilities.NumbersOnly(pData)
    End Function

    <Extension()>
    Public Function exToCharsOnly(pData As String) As String
        If pData Is Nothing Then Return Nothing Else Return Utilities.CharsOnly(pData)
    End Function

#End Region


#Region "Dictionary"

    <Extension()>
    Public Function exValueOrDefault(Of TKey, TValue)(ByVal pDict As IDictionary(Of TKey, TValue), ByVal pKey As TKey) As TValue
        If (pDict.ContainsKey(pKey)) Then
            Return pDict.Item(pKey)
        Else
            If (GetType(TValue) = GetType(String)) Then
                'return empty string because we like that better
                Dim res As Object = String.Empty
                Return DirectCast(res, TValue)
                'ElseIf (GetType(TValue).GetConstructor({}) IsNot Nothing) Then
                '    'if type has a parameterless constructor, create a new object
            Else
                'returns nothing for object types, default values for others
                Dim res As TValue
                Return res
            End If
        End If
    End Function

    <Extension()>
    Public Function exValueOrDefault(Of TKey, TValue)(ByVal pDict As IDictionary(Of TKey, TValue), ByVal pKey As TKey, ByVal pDefaultValue As TValue) As TValue
        If (pDict.ContainsKey(pKey)) Then
            Return pDict.Item(pKey)
        Else
            Return pDefaultValue
        End If
    End Function


    Public Enum eDupMergeStyle
        ThrowError = 0
        IgnoreDups = 1
        OverwriteDups = 2
    End Enum

    <Extension()>
    Public Sub exConsume(Of TKey, TValue)(ByVal pDict1 As IDictionary(Of TKey, TValue), ByVal pDict2 As IDictionary(Of TKey, TValue), ByVal pMergeType As eDupMergeStyle)
        For Each tmpKey In pDict2.Keys
            If Not pDict1.ContainsKey(tmpKey) Then
                pDict1.Add(tmpKey, pDict2.Item(tmpKey))
            Else
                Select Case pMergeType
                    Case eDupMergeStyle.IgnoreDups ' Do nothing 
                    Case eDupMergeStyle.OverwriteDups
                        pDict1.Item(tmpKey) = pDict2.Item(tmpKey)
                    Case eDupMergeStyle.ThrowError
                        If Not pDict1.Item(tmpKey).Equals(pDict2.Item(tmpKey)) Then Throw New Exception(String.Format("Dictionary Values for key {0} do not match {1} vs {2}", tmpKey, pDict1(tmpKey), pDict2(tmpKey)))
                End Select
            End If
        Next
    End Sub

    <Extension()>
    Public Function exMerge(Of TKey, TValue)(ByVal pDict1 As IDictionary(Of TKey, TValue), ByVal pDict2 As IDictionary(Of TKey, TValue), ByVal pMergeType As eDupMergeStyle) As Dictionary(Of TKey, TValue)
        Dim tmp As New Dictionary(Of TKey, TValue)
        tmp.exConsume(pDict1, pMergeType)
        tmp.exConsume(pDict2, pMergeType)
        Return tmp
    End Function

#End Region


#Region "StringBuilder"

    <Extension()>
    Public Sub exAppendFormatLine(ByVal pSb As System.Text.StringBuilder, ByVal format As String, ByVal Arg0 As Object, Optional ByVal Arg1 As Object = Nothing, Optional ByVal Arg2 As Object = Nothing, Optional ByVal Arg3 As Object = Nothing, Optional ByVal Arg4 As Object = Nothing)
        pSb.AppendFormat(format, Arg0, Arg1, Arg2, Arg3, Arg4)
        pSb.AppendLine()
    End Sub

#End Region


#Region "DateTime"

    <Extension()>
    Public Function exAddWeekdays(ByVal pDate As DateTime, ByVal pDays As Int32) As DateTime
        Dim sign = If(pDays < 0, -1, 1)
        Dim unsignedDays = Math.Abs(pDays)
        Dim weekdaysAdded = 0
        Dim res As DateTime = pDate
        While weekdaysAdded < unsignedDays
            res = res.AddDays(sign)
            If ((res.DayOfWeek <> DayOfWeek.Saturday) AndAlso (res.DayOfWeek <> DayOfWeek.Sunday)) Then
                weekdaysAdded += 1
            End If
        End While

        Return res
    End Function


    <Extension()>
    Public Function exSetTime(ByVal pDate As DateTime, ByVal pHour As Int32) As DateTime
        Return exSetTime(pDate, pHour, 0, 0, 0)
    End Function

    <Extension()>
    Public Function exSetTime(ByVal pDate As DateTime, ByVal pHour As Int32, ByVal pMinute As Int32) As DateTime
        Return exSetTime(pDate, pHour, pMinute, 0, 0)
    End Function

    <Extension()>
    Public Function exSetTime(ByVal pDate As DateTime, ByVal pHour As Int32, ByVal pMinute As Int32, ByVal pSecond As Int32) As DateTime
        Return exSetTime(pDate, pHour, pMinute, pSecond, 0)
    End Function

    <Extension()>
    Public Function exSetTime(ByVal pDate As DateTime, ByVal pHour As Int32, ByVal pMinute As Int32, ByVal pSecond As Int32, ByVal pMillisecond As Int32) As DateTime
        Return New DateTime(pDate.Year, pDate.Month, pDate.Day, pHour, pMinute, pSecond, pMillisecond)
    End Function


    <Extension()>
    Public Function exFirstDayOfMonth(ByVal pDate As DateTime) As DateTime
        Return New DateTime(pDate.Year, pDate.Month, 1)
    End Function

    <Extension()>
    Public Function exLastDayOfMonth(ByVal pDate As DateTime) As DateTime
        Return New DateTime(pDate.Year, pDate.Month, DateTime.DaysInMonth(pDate.Year, pDate.Month))
    End Function


    <Extension()>
    Public Function exToString(ByVal pDate As DateTime?) As String
        Return pDate.exToString(Nothing, Globalization.DateTimeFormatInfo.CurrentInfo)
    End Function

    <Extension()>
    Public Function exToString(ByVal pDate As DateTime?, ByVal pFormat As String) As String
        Return pDate.exToString(pFormat, Globalization.DateTimeFormatInfo.CurrentInfo)
    End Function

    <Extension()>
    Public Function exToString(ByVal pDate As DateTime?, ByVal pProvider As IFormatProvider) As String
        Return pDate.exToString(Nothing, pProvider)
    End Function

    <Extension()>
    Public Function exToString(ByVal pDate As DateTime?, ByVal pFormat As String, ByVal pProvider As IFormatProvider) As String
        If (pDate.HasValue) Then
            Return pDate.Value.ToString(pFormat, pProvider)
        Else
            Return String.Empty
        End If
    End Function


    <Extension()>
    Public Function exToRelativeDateString(ByVal pDate As DateTime) As String
        Return GetRelativeDateValue(pDate, DateTime.Now)
    End Function

    <Extension()>
    Public Function exToRelativeDateStringUtc(ByVal pDate As DateTime) As String
        Return GetRelativeDateValue(pDate, DateTime.UtcNow)
    End Function

    Private Function GetRelativeDateValue(ByVal pDate As DateTime, ByVal pCompareTo As DateTime) As String
        If (pDate > pCompareTo) Then
            Dim diff As TimeSpan = pCompareTo.Subtract(pDate)
            If (diff.TotalDays >= 365) Then
                Return pDate.ToString("MMMM d, yyyy")
            End If
            If (diff.TotalDays >= 7) Then
                Return pDate.ToString("MMMM d")
            ElseIf (diff.TotalDays > 1) Then
                Return String.Format("{0:N0} days ago", diff.TotalDays)
            ElseIf (diff.TotalDays = 1) Then
                Return "yesterday"
            ElseIf (diff.TotalHours >= 2) Then
                Return String.Format("{0:N0} hours ago", diff.TotalHours)
            ElseIf (diff.TotalMinutes >= 60) Then
                Return "more than an hour ago"
            ElseIf (diff.TotalMinutes >= 5) Then
                Return String.Format("{0:N0} minutes ago", diff.TotalMinutes)
            End If
            If (diff.TotalMinutes >= 1) Then
                Return "a few minutes ago"
            Else
                Return "less than a minute ago"
            End If
        Else
            Return pDate.ToString("MMMM d, yyyy")
        End If
    End Function


    <Extension()>
    Public Function exGetPriorDay(pDate As DateTime, pDay As DayOfWeek) As Date
        Return GetPriorDay(pDate, pDay)
    End Function

    <Extension()>
    Public Function exGetNextDay(pDate As DateTime, pDay As DayOfWeek, Optional pSetTimeTo2359599999 As Boolean = False) As Date
        Return GetNextDay(pDate, pDay, pSetTimeTo2359599999)
    End Function

    Private Function GetPriorDay(pDate As DateTime, pDay As DayOfWeek) As Date
        Dim diff = CInt(pDay) - CInt(pDate.DayOfWeek)
        If diff >= 0 Then diff -= 7
        Return CDate(pDate.AddDays(diff).ToShortDateString)
    End Function

    Private Function GetNextDay(pDate As DateTime, pDay As DayOfWeek, Optional pSetTimeTo2359599999 As Boolean = False) As Date
        Dim diff = CInt(pDay) - CInt(pDate.DayOfWeek)
        If diff <= 0 Then diff += 7
        Return CDate(pDate.AddDays(diff).ToShortDateString & If(pSetTimeTo2359599999, " 23:59:59.9999", ""))
    End Function

#End Region


#Region "EnumString"

    <Extension()>
    Public Function exToCode(Of valType As {EnumString(Of valType), New})(ByVal pEnumString As valType) As String
        If pEnumString Is Nothing Then Return "" Else Return pEnumString.Code
    End Function

#End Region


#Region "CodedEnum"

    <Extension()>
    Public Function exCode(pEnumVal As System.Enum) As String
        Return FYUtil.CodedEnum.GetCode(pEnumVal)
    End Function

    <Extension()>
    Public Function exLabel(pEnumVal As System.Enum) As String
        Return FYUtil.CodedEnum.GetLabel(pEnumVal)
    End Function

#End Region


#Region "BaseObject"

    <Extension()>
    Public Function exToBaseObjectDict(Of entityType As StandardBaseObject)(ByVal pList As IEnumerable(Of entityType)) As StandardBaseObjectDict(Of entityType)
        Return New StandardBaseObjectDict(Of entityType)(pList)
    End Function

    <Extension()>
    Public Function exId(ByVal pItem As StandardBaseObject) As Int32
        If IsNothing(pItem) Then Return 0 Else Return pItem.UniqueId
    End Function

    <Extension()>
    Public Function exId(Of keyType)(ByVal pItem As StandardBaseObject(Of keyType)) As keyType
        If IsNothing(pItem) Then Return Nothing Else Return pItem.UniqueId
    End Function

#End Region


#Region "IEnumerable"

    ''' <summary>
    ''' Returns true if pSet1 contains all elements of pSet2. Returns false if any element of pSet2 is not in pSet1.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="pSet1"></param>
    ''' <param name="pSet2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function exContainsAll(Of T)(pSet1 As IEnumerable(Of T), pSet2 As IEnumerable(Of T)) As Boolean
        If (Not pSet2.Except(pSet1).Any()) Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Returns true if pSet1 contains only elements of pSet2. Returns false if any element of pSet1 is not contained in pSet2.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="pSet1"></param>
    ''' <param name="pSet2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function exContainsOnly(Of T)(pSet1 As IEnumerable(Of T), pSet2 As IEnumerable(Of T)) As Boolean
        If (Not pSet1.Except(pSet2).Any()) Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Returns true if pSet1 and pSet2 contain all the same elements.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="pSet1"></param>
    ''' <param name="pSet2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function exContainsSame(Of T)(pSet1 As IEnumerable(Of T), pSet2 As IEnumerable(Of T)) As Boolean
        If (pSet1.exContainsAll(pSet2) AndAlso pSet1.exContainsOnly(pSet2)) Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

End Module
