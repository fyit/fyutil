﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Public Structure CodeLabelPair
    Public Property Code As String
    Public Property Label As String

    Sub New(ByVal pCode As String, ByVal pLabel As String)
        Code = pCode
        Label = pLabel
    End Sub

    Public Overrides Function ToString() As String
        Return Label
    End Function

    Public Shared Function GetCode(pCLPair As Object) As String
        If ((pCLPair Is Nothing) OrElse (Not (TypeOf pCLPair Is CodeLabelPair))) Then Return Nothing

        Dim clp = DirectCast(pCLPair, CodeLabelPair)
        Return clp.Code
    End Function

    Public Shared Function FindInList(pCollection As IEnumerable, pCodeVal As String) As CodeLabelPair
        If (pCodeVal Is Nothing) Then Return Nothing
        For Each curObj As Object In pCollection
            If (TypeOf curObj Is CodeLabelPair) Then
                Dim val As String = GetCode(curObj)
                If (pCodeVal.Equals(val)) Then
                    Return DirectCast(curObj, CodeLabelPair)
                End If
            End If
        Next
        Return Nothing
    End Function

End Structure
