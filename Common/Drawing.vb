﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Drawing


Public Class Drawing

    ''' <summary>
    ''' Create a PNG image containing the specified text in the specified font
    ''' </summary>
    ''' <param name="pText"></param>
    ''' <param name="pFontName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateImageFromText(pText As String, pFontName As String, pFontSize As Single) As Byte()
        Dim fnt As New Font(pFontName, pFontSize)
        Dim textSize As SizeF
        Dim backColor = Color.White
        Dim foreColor = Color.Black

        'create dummy object to measure needed size
        Using img As Image = New Bitmap(1, 1)
            Using gph As Graphics = Graphics.FromImage(img)
                textSize = gph.MeasureString(pText, fnt)
            End Using
        End Using

        'create a new image of the right size
        Using img As Image = New Bitmap(CInt(textSize.Width), CInt(textSize.Height))
            Using gph As Graphics = Graphics.FromImage(img)
                gph.Clear(backColor)
                Using textBrush As Brush = New SolidBrush(foreColor)
                    gph.DrawString(pText, fnt, textBrush, 0, 0)
                    Using ms As New IO.MemoryStream()
                        img.Save(ms, Imaging.ImageFormat.Png)
                        Return ms.GetBuffer()
                    End Using
                End Using
            End Using
        End Using
    End Function


End Class
