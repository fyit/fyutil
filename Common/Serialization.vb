﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Json
Imports System.Xml.Serialization

Public Class Serialization

#Region "Dynamic load of dependent assembly"

    Shared Sub New()
        AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf AssemblyResolve_Newtonsoft_Json
    End Sub

    Private Shared Function AssemblyResolve_Newtonsoft_Json(sender As Object, args As ResolveEventArgs) As System.Reflection.Assembly
        If (args.Name.StartsWith("Newtonsoft.Json", StringComparison.InvariantCultureIgnoreCase)) Then
            Dim bytes = FYUtil.EmbeddedResources.GetResourceData("Newtonsoft.Json.4.5.8.dll")
            Return System.Reflection.Assembly.Load(bytes)
        End If
        Return Nothing
    End Function

#End Region


    Public Enum eSerializer
        ''' <summary>XML serialization with the classic .NET serialization (not recommended)</summary>
        XmlClassic = 10

        ''' <summary>XML serialization with DataContractSerializer</summary>
        XmlContract = 20

        ''' <summary>JSON serialization with DataContractSerializer</summary>
        JsonContract = 30

        ''' <summary>JSON serialization with JSON.NET</summary>
        JsonNet = 40

        ''' <summary>JSON serialization with custom Readable serializer</summary>
        Readable = 50
    End Enum


#Region "Specify serializer"

    ''' <summary>
    ''' Serialize an object using the specified serializer
    ''' </summary>
    ''' <param name="pObj">object to serialize</param>
    Public Shared Function SerializeObject(pObj As Object, pSerializer As eSerializer) As String
        Select Case pSerializer
            Case eSerializer.XmlClassic : Return SerializeObjectXmlClassic(pObj)
            Case eSerializer.XmlContract : Return SerializeObjectXmlContract(pObj)
            Case eSerializer.JsonContract : Return SerializeObjectJsonContract(pObj)
            Case eSerializer.JsonNet : Return SerializeObjectJsonNet(pObj)
            Case eSerializer.Readable : Return SerializeObjectReadable(pObj)
            Case Else : Throw New ArgumentException("Invalid serializer")
        End Select
    End Function

    ''' <summary>
    ''' Deserialize an object using the specified serializer. Throws error if unable to deserialize
    ''' </summary>
    Public Shared Function DeserializeObject(pSerializedString As String, pType As System.Type, pSerializer As eSerializer) As Object
        If (String.IsNullOrEmpty(pSerializedString)) Then Return Nothing
        Dim dat As Object
        Try
            Select Case pSerializer
                Case eSerializer.XmlClassic : dat = DeserializeObjectXmlClassic(pSerializedString, pType)
                Case eSerializer.XmlContract : dat = DeserializeObjectXmlContract(pSerializedString, pType)
                Case eSerializer.JsonContract : dat = DeserializeObjectJsonContract(pSerializedString, pType)
                Case eSerializer.JsonNet : dat = DeserializeObjectJsonNet(pSerializedString, pType)
                Case eSerializer.Readable : dat = DeserializeObjectReadable(pSerializedString, pType)
                Case Else : Throw New ArgumentException("Invalid serializer")
            End Select
            Return dat
        Catch ex As Exception
            Throw New Exception(String.Format("DeserializeObject: Error in deserialization ({0})", ex.Message))
        End Try
    End Function

    ''' <summary>
    ''' Deserialize an object using the specified serializer. Throws error if unable to deserialize
    ''' </summary>
    ''' <typeparam name="T">Type of object to deserialize into</typeparam>
    Public Shared Function DeserializeObject(Of T)(pSerializedString As String, pSerializer As eSerializer) As T
        Return DirectCast(DeserializeObject(pSerializedString, GetType(T), pSerializer), T)
    End Function

    ''' <summary>
    ''' Deserialize an object using the specified serializer. Returns pDefault if unable to deserialize
    ''' </summary>
    ''' <typeparam name="T">Type of object to deserialize into</typeparam>
    Public Shared Function DeserializeObject(Of T)(pSerializedString As String, pDefault As T, pSerializer As eSerializer) As T
        If (String.IsNullOrEmpty(pSerializedString)) Then Return pDefault
        Try
            Return DeserializeObject(Of T)(pSerializedString, pSerializer)
        Catch ex As Exception
            Return pDefault
        End Try
    End Function

    ''' <summary>
    ''' Deserialize an object using the specified serializer. Returns pDefault if unable to deserialize
    ''' </summary>
    Public Shared Function DeserializeObject(pSerializedString As String, pType As System.Type, pDefault As Object, pSerializer As eSerializer) As Object
        If (String.IsNullOrEmpty(pSerializedString)) Then Return pDefault
        Try
            Return DeserializeObject(pSerializedString, pType, pSerializer)
        Catch ex As Exception
            Return pDefault
        End Try
    End Function

    ''' <summary>
    ''' Deserialize an object using the specified serializer into an existing object. Throws error if unable to deserialize
    ''' </summary>
    Public Shared Sub DeserializeInto(pSerializedString As String, pTarget As Object, pSerializer As eSerializer)
        If (pTarget Is Nothing) Then Throw New NullReferenceException("DeserializeInto: Target required")
        If (String.IsNullOrEmpty(pSerializedString)) Then Return
        Try
            Select Case pSerializer
                Case eSerializer.XmlClassic : Throw New NotImplementedException
                Case eSerializer.XmlContract : Throw New NotImplementedException
                Case eSerializer.JsonContract : Throw New NotImplementedException
                Case eSerializer.JsonNet : DeserializeIntoJsonNet(pSerializedString, pTarget)
                Case eSerializer.Readable : DeserializeIntoReadable(pSerializedString, pTarget)
                Case Else : Throw New ArgumentException("Invalid serializer")
            End Select
        Catch ex As Exception
            Throw New Exception(String.Format("DeserializeInto: Error in deserialization ({0})", ex.Message))
        End Try
    End Sub

#End Region




#Region "XmlContract serialization (DataContractSerializer)"

    Private Shared Function SerializeObjectXmlContract(pObj As Object) As String
        If (pObj Is Nothing) Then Return ""
        Dim ser As New DataContractSerializer(pObj.GetType())

        Using ms As New MemoryStream()
            ser.WriteObject(ms, pObj)
            Using rdr As New IO.StreamReader(ms, System.Text.Encoding.UTF8)
                ms.Position = 0
                Return rdr.ReadToEnd()
            End Using
        End Using
    End Function


    Private Shared Function DeserializeObjectXmlContract(pXmlStr As String, pType As System.Type) As Object
        If (String.IsNullOrEmpty(pXmlStr)) Then Return Nothing
        Dim ser As New DataContractSerializer(pType)
        Using sr As New StringReader(pXmlStr)
            Using xr = Xml.XmlReader.Create(sr)
                Return ser.ReadObject(xr)
            End Using
        End Using
    End Function

#End Region


#Region "XmlClassic serialization"

    Private Shared Function SerializeObjectXmlClassic(pObj As Object) As String
        If (pObj Is Nothing) Then Return ""

        Dim ser As New XmlSerializer(pObj.GetType())
        Using sw As New StringWriter()
            ser.Serialize(sw, pObj)
            Return sw.ToString()
        End Using
    End Function

    Private Shared Function DeserializeObjectXmlClassic(pXmlStr As String, pType As System.Type) As Object
        If (String.IsNullOrEmpty(pXmlStr)) Then Return Nothing

        Dim ser As New XmlSerializer(pType)
        Using sr As New StringReader(pXmlStr)
            Return ser.Deserialize(sr)
        End Using
    End Function

#End Region


#Region "JsonContract serialization"

    Private Shared Function SerializeObjectJsonContract(pObj As Object) As String
        If (pObj Is Nothing) Then Return ""
        Dim ser As New DataContractJsonSerializer(pObj.GetType())

        Using ms As New MemoryStream()
            ser.WriteObject(ms, pObj)
            Using rdr As New IO.StreamReader(ms, System.Text.Encoding.UTF8)
                ms.Position = 0
                Return rdr.ReadToEnd()
            End Using
        End Using
    End Function


    Private Shared Function DeserializeObjectJsonContract(pJsonStr As String, pType As System.Type) As Object
        If (String.IsNullOrEmpty(pJsonStr)) Then Return Nothing
        Dim ser As New DataContractJsonSerializer(pType)
        Using ms As New MemoryStream(Text.Encoding.Unicode.GetBytes(pJsonStr))
            Return ser.ReadObject(ms)
        End Using
    End Function

#End Region



#Region "JsonNet serialization"

    Private Shared Function SerializeObjectJsonNet(pObj As Object) As String
        If (pObj Is Nothing) Then Return ""
        Return Newtonsoft.Json.JsonConvert.SerializeObject(pObj)
    End Function


    Private Shared Function DeserializeObjectJsonNet(pJsonStr As String, pType As System.Type) As Object
        If (String.IsNullOrEmpty(pJsonStr)) Then Return Nothing
        Return Newtonsoft.Json.JsonConvert.DeserializeObject(pJsonStr, pType)
    End Function

    Private Shared Sub DeserializeIntoJsonNet(pJsonStr As String, pTarget As Object)
        If (String.IsNullOrEmpty(pJsonStr)) Then Return
        Newtonsoft.Json.JsonConvert.PopulateObject(pJsonStr, pTarget)
    End Sub

#End Region



#Region "Readable serialization"

    Private Shared Function GetCustomReadableSettings(Optional pIndented As Boolean = False) As Newtonsoft.Json.JsonSerializerSettings
        Dim settings As New Newtonsoft.Json.JsonSerializerSettings()
        'indented is readable, but causes problems in cookies..
        If (pIndented) Then settings.Formatting = Newtonsoft.Json.Formatting.Indented
        settings.Converters.Add(New Newtonsoft.Json.Converters.StringEnumConverter)
        Return settings
    End Function

    ''' <summary>
    ''' Serialize an object using custom Readable serializer
    ''' </summary>
    Public Shared Function SerializeObjectReadable(pObj As Object, Optional pIndented As Boolean = False) As String
        If (pObj Is Nothing) Then Return ""
        Dim typ = pObj.GetType()
        If (typ = GetType(String)) Then
            Return pObj.ToString
        ElseIf (typ.IsEnum) Then
            Return pObj.ToString
        ElseIf (typ = GetType(DateTime)) Then
            Return pObj.ToString
        Else
            Return Newtonsoft.Json.JsonConvert.SerializeObject(pObj, GetCustomReadableSettings(pIndented))
        End If
    End Function

    ''' <summary>
    ''' Deserializes an object using custom Readable serializer. Throws error if unable to deserialize
    ''' </summary>
    Public Shared Function DeserializeObjectReadable(pSerializedString As String, pType As System.Type) As Object
        If (String.IsNullOrEmpty(pSerializedString)) Then Return Nothing
        Dim typ = pType.exGetNaturalType()
        If (typ = GetType(String)) Then
            Return pSerializedString
        ElseIf (typ.IsEnum) Then
            Return [Enum].Parse(typ, pSerializedString)
        ElseIf (typ = GetType(DateTime)) Then
            Return DateTime.Parse(pSerializedString)
        Else
            Return Newtonsoft.Json.JsonConvert.DeserializeObject(pSerializedString, pType, GetCustomReadableSettings())
        End If
    End Function

    Private Shared Sub DeserializeIntoReadable(pSerializedString As String, pTarget As Object)
        If (String.IsNullOrEmpty(pSerializedString)) Then Return
        Newtonsoft.Json.JsonConvert.PopulateObject(pSerializedString, pTarget, GetCustomReadableSettings())
    End Sub





    ''' <summary>
    ''' Deserializes an object using custom Readable serializer. Throws error if unable to deserialize
    ''' </summary>
    ''' <typeparam name="T">Type of object to deserialize into</typeparam>
    Public Shared Function DeserializeObjectReadable(Of T)(pSerializedString As String) As T
        'just a shortcut
        Return DeserializeObject(Of T)(pSerializedString, eSerializer.Readable)
    End Function

    ''' <summary>
    ''' Deserializes an object using custom Readable serializer. Returns default if unable to deserialize
    ''' </summary>
    ''' <typeparam name="T">Type of object to deserialize into</typeparam>
    Public Shared Function DeserializeObjectReadable(Of T)(pSerializedString As String, pDefault As T) As T
        'just a shortcut
        Return DeserializeObject(Of T)(pSerializedString, pDefault, eSerializer.Readable)
    End Function

#End Region



End Class
