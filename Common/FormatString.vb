﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports System.Text

Public Class FormatString

    'modified from FormatWith (http://james.newtonking.com/archive/2008/03/29/formatwith-2-0-string-formatting-with-named-variables.aspx)

    ''' <summary>
    ''' Replaces keys in a string with the values of matching object properties. Like String.Format but using named placeholders.
    ''' <remarks>Uses <see cref="String.Format()"/> internally; custom formats should match those used for that method.</remarks>
    ''' </summary>
    ''' <param name="pFormat">The format string, containing keys like {foo} and {foo:SomeFormat}.</param>
    ''' <param name="pData">Dictionary of named values that should be injected in the string</param>
    ''' <returns>A version of the format string with keys replaced by (formatted) key values.</returns>
    Public Shared Function Format(pFormat As String, pData As IDictionary(Of String, Object)) As String
        Return FormatImpl(pFormat, pData)
    End Function



    ''' <summary>
    ''' Replaces keys in a string with the values of matching object properties. Like String.Format but using named placeholders.
    ''' <remarks>Uses <see cref="String.Format()"/> internally; custom formats should match those used for that method.</remarks>
    ''' </summary>
    ''' <param name="pFormat">The format string, containing keys like {foo} and {foo:SomeFormat}.</param>
    ''' <param name="pData">The object whose properties should be injected in the string</param>
    ''' <returns>A version of the format string with keys replaced by (formatted) key values.</returns>
    Public Shared Function Format(pFormat As String, pData As Object) As String
        Return FormatImpl(pFormat, Utilities.ObjectToDictionary(pData))
    End Function



    ''' <summary>
    ''' Parse a format string and return a list of required placeholder data
    ''' </summary>
    ''' <param name="pFormat">Format string to examine</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetPlaceholders(pFormat As String) As IList(Of String)
        Dim placeholders As List(Of String) = Nothing
        Dim newFormat As String = Nothing
        RewriteFormatString(pFormat, poNewFormat:=newFormat, poPlaceholders:=placeholders)
        Return placeholders
    End Function



    ''' <summary>
    ''' Validate a format string for proper structure. Cannot validate placeholder custom subformats.
    ''' </summary>
    ''' <param name="pFormat"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Validate(pFormat As String) As FYUtil.SuccessAndDescription
        Try
            Dim phs = GetPlaceholders(pFormat)
            Dim dat = phs.ToDictionary(Function(e) e, Function(e) Nothing)
            Dim str = Format(pFormat, dat)
        Catch ex As Exception
            Return FYUtil.SuccessAndDescription.NotOk(ex.Message)
        End Try
        Return FYUtil.SuccessAndDescription.Ok
    End Function





    Private Shared Function FormatImpl(pFormat As String, pData As IDictionary(Of String, Object)) As String
        If (pFormat Is Nothing) Then Throw New ArgumentNullException("pFormat")

        Dim placeholders As List(Of String) = Nothing
        Dim newFormat As String = Nothing
        RewriteFormatString(pFormat, poNewFormat:=newFormat, poPlaceholders:=placeholders)
        Dim values As New List(Of Object)
        For Each placeholder In placeholders
            If (Not pData.ContainsKey(placeholder)) Then Throw New Exception(String.Format("FormatString: Data for placeholder '{0}' not found", placeholder))
            values.Add(pData.Item(placeholder))
        Next

        Return String.Format(newFormat, values.ToArray())
    End Function

    Private Shared Sub RewriteFormatString(pFormat As String, ByRef poNewFormat As String, ByRef poPlaceholders As List(Of String))
        Dim placeholders As New List(Of String)
        Dim newFormat As String = RegularExpressions.Regex.Replace(
            pFormat, "(?<start>\{)+(?<property>[\w\.\[\]]+)(?<format>:[^}]+)?(?<end>\})+",
            Function(m As RegularExpressions.Match) As String
                Dim startGroup = m.Groups("start")
                Dim propertyGroup = m.Groups("property")
                Dim formatGroup = m.Groups("format")
                Dim endGroup = m.Groups("end")

                Dim openings = startGroup.Captures.Count
                Dim closings = endGroup.Captures.Count
                If ((closings Mod 2 = 1) AndAlso (openings Mod 2 = 1)) Then
                    Dim placeholderIndex = -1
                    If (Not placeholders.Contains(propertyGroup.Value)) Then
                        placeholderIndex = placeholders.Count
                        placeholders.Add(propertyGroup.Value)
                    Else
                        placeholderIndex = placeholders.IndexOf(propertyGroup.Value)
                    End If
                    Return New String("{"c, openings) & (placeholderIndex) & formatGroup.Value & New String("}"c, closings)
                Else
                    'braces not matched, give up
                    Return m.Value
                End If
            End Function,
            RegularExpressions.RegexOptions.Compiled Or RegularExpressions.RegexOptions.CultureInvariant Or RegularExpressions.RegexOptions.IgnoreCase
            )

        poNewFormat = newFormat
        poPlaceholders = placeholders
    End Sub

End Class
