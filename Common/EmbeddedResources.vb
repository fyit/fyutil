﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 


Imports System.Reflection


Public Class EmbeddedResources

    Public Shared Function GetResourceNames() As IList(Of String)
        Return GetResourceNames(Assembly.GetCallingAssembly())
    End Function

    Public Shared Sub ExtractResourceToFile(pResourceName As String, pFilepath As String)
        Dim assbly = Reflection.Assembly.GetCallingAssembly()
        ExtractResourceToFile(assbly, pResourceName, pFilepath)
    End Sub

    Public Shared Function ExtractResourceToAssemblyPath(pResourceName As String) As String
        Dim assbly = Reflection.Assembly.GetCallingAssembly()
        Dim filePath = IO.Path.Combine(IO.Path.GetDirectoryName(assbly.Location), pResourceName)
        ExtractResourceToFile(assbly, pResourceName, filePath)
        Return filePath
    End Function

    Public Shared Function GetResourceText(pResourceName As String) As String
        Dim assbly = Reflection.Assembly.GetCallingAssembly()
        Dim resourceName = FindResourceName(assbly, pResourceName)
        Using strm = assbly.GetManifestResourceStream(resourceName)
            Using reader = New IO.StreamReader(assbly.GetManifestResourceStream(resourceName))
                Return reader.ReadToEnd()
            End Using
        End Using
    End Function

    Public Shared Function GetResourceData(pResourceName As String) As Byte()
        Dim assbly = Reflection.Assembly.GetCallingAssembly()
        Dim resourceName = FindResourceName(assbly, pResourceName)
        Using strm = assbly.GetManifestResourceStream(resourceName)
            Return Utilities.StreamToByteArray(strm)
        End Using
    End Function

    Private Shared Function GetResourceNames(pAssembly As Assembly) As IList(Of String)
        Dim resources = pAssembly.GetManifestResourceNames()
        Return resources.ToList
    End Function

    Private Shared Function FindResourceName(pAssembly As Assembly, pName As String) As String
        Dim resources = GetResourceNames(pAssembly)
        If (resources.Contains(pName)) Then
            Return pName
        Else
            Dim matching = (From r In resources Where r.ToLower = pName.ToLower OrElse r.ToLower.EndsWith("." + pName.ToLower) Select r)
            If (matching.Count = 1) Then
                Return matching.Single
            Else
                Throw New Exception("Unable to locate resource")
            End If
        End If
    End Function

    Private Shared Sub ExtractResourceToFile(pAssembly As Assembly, pResourceName As String, pFilepath As String)
        Dim resourceName = FindResourceName(pAssembly, pResourceName)
        Using newFile = New IO.FileStream(pFilepath, IO.FileMode.Create)
            Using strm = pAssembly.GetManifestResourceStream(resourceName)
                strm.CopyTo(newFile)
            End Using
        End Using
    End Sub

End Class
