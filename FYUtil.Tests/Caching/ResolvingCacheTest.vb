﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil.Caching



'''<summary>
'''This is a test class for ResolvingCacheTest and is intended
'''to contain all ResolvingCacheTest Unit Tests
'''</summary>
<TestClass()> _
Public Class ResolvingCacheTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    <TestInitialize()> _
    Public Sub MyTestInitialize()
        _repository = New Dictionary(Of Int32, MyPayload)
        _repository.Add(1, New MyPayload With {.Id = 1, .Name = "Test1"})
        _repository.Add(2, New MyPayload With {.Id = 2, .Name = "Test2"})
        _repository.Add(3, New MyPayload With {.Id = 3, .Name = "Test3"})
        _repository.Add(4, New MyPayload With {.Id = 4, .Name = "Test4"})
        _repository.Add(5, New MyPayload With {.Id = 5, .Name = "Test5"})
        _repository.Add(6, New MyPayload With {.Id = 6, .Name = "Test6"})
        _repository.Add(7, New MyPayload With {.Id = 7, .Name = "Test7"})
        _repository.Add(8, New MyPayload With {.Id = 8, .Name = "Test8"})
        _repository.Add(9, New MyPayload With {.Id = 9, .Name = "Test9"})
        _repository.Add(10, New MyPayload With {.Id = 10, .Name = "Test10"})
    End Sub

    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    Private Class MyPayload
        Public Property Id As Int32
        Public Property Name As String
        Public Property RepoHits As Int32 = 0
    End Class


    Private _repository As Dictionary(Of Int32, MyPayload)

    Private Function SingleResolver(pId As Int32) As MyPayload
        If (_repository.ContainsKey(pId)) Then
            Dim curVal As MyPayload = _repository.Item(pId)
            curVal.RepoHits += 1
            Return curVal
        Else
            Return Nothing
        End If
    End Function

    Private Function MultiResolver(pIds As IEnumerable(Of Int32)) As IDictionary(Of Int32, MyPayload)
        Dim res As New Dictionary(Of Int32, MyPayload)
        For Each id In pIds
            If (_repository.ContainsKey(id)) Then
                Dim curVal As MyPayload = _repository.Item(id)
                curVal.RepoHits += 1
                res.Add(id, curVal)
            End If
        Next
        Return res
    End Function


    '''<summary>
    '''Using the single item resolver
    '''</summary>
    <TestMethod()> _
    Public Sub SingleLookupTest()
        Dim cache1 As New FYUtil.Caching.ResolvingCache(Of Int32, MyPayload)("test", AddressOf SingleResolver, Nothing, TimeSpan.FromMinutes(1))

        With Nothing
            Dim val1 = cache1.Item(1)
            Assert.AreEqual(1, val1.Id, "Bad id for val 1")
            Assert.AreEqual(1, val1.RepoHits, "Bad hits for val 1")
        End With

        With Nothing
            Dim vals123 = cache1.Items({1, 2, 3})
            Assert.AreEqual(1, vals123.Item(1).Id, "Bad id for val 1")
            Assert.AreEqual(1, vals123.Item(1).RepoHits, "Bad hits for val 1")
            Assert.AreEqual(2, vals123.Item(2).Id, "Bad id for val 2")
            Assert.AreEqual(1, vals123.Item(2).RepoHits, "Bad hits for val 2")
        End With

        With Nothing
            Dim vals9_99 = cache1.Items({9, 99})
            Assert.AreEqual(9, vals9_99.Item(9).Id)
            Assert.AreEqual(1, vals9_99.Item(9).RepoHits)
            Assert.IsNull(vals9_99.Item(99), "val 99 should be null")
        End With


        'wait 30 seconds
        System.Threading.Thread.Sleep(30000)


        'get id 4
        With Nothing
            Dim val4 = cache1.Item(4)
            Assert.AreEqual(4, val4.Id, "Bad id for val 4")
            Assert.AreEqual(1, val4.RepoHits, "Bad hits for val 4")
        End With


        'wait 40 seconds
        System.Threading.Thread.Sleep(40000)


        With Nothing
            Dim val1 = cache1.Item(1)
            Assert.AreEqual(1, val1.Id)
            Assert.AreEqual(2, val1.RepoHits)
        End With

        With Nothing
            Dim val4 = cache1.Item(4)
            Assert.AreEqual(4, val4.Id, "Bad id for val 4")
            Assert.AreEqual(1, val4.RepoHits, "Bad hits for val 4")
        End With

        With Nothing
            Dim val5 = cache1.Item(5)
            Assert.AreEqual(5, val5.Id, "Bad id for val 5")
            Assert.AreEqual(1, val5.RepoHits, "Bad hits for val 5")
        End With
    End Sub


    '''<summary>
    '''Using the multiple item resolver
    '''</summary>
    <TestMethod()> _
    Public Sub MultiLookupTest()
        Dim cache1 As New FYUtil.Caching.ResolvingCache(Of Int32, MyPayload)("test", AddressOf MultiResolver, Nothing, TimeSpan.FromMinutes(1))
        With Nothing
            Dim val1 = cache1.Item(1)
            Assert.AreEqual(1, val1.Id, "Bad id for val 1")
            Assert.AreEqual(1, val1.RepoHits, "Bad hits for val 1")
        End With

        With Nothing
            Dim vals123 = cache1.Items({1, 2, 3})
            Assert.AreEqual(1, vals123.Item(1).Id, "Bad id for val 1")
            Assert.AreEqual(1, vals123.Item(1).RepoHits, "Bad hits for val 1")
            Assert.AreEqual(2, vals123.Item(2).Id, "Bad id for val 2")
            Assert.AreEqual(1, vals123.Item(2).RepoHits, "Bad hits for val 2")
        End With

        With Nothing
            Dim vals9_99 = cache1.Items({9, 99})
            Assert.AreEqual(9, vals9_99.Item(9).Id)
            Assert.AreEqual(1, vals9_99.Item(9).RepoHits)
            Assert.IsNull(vals9_99.Item(99), "val 99 should be null")
        End With


        'wait 30 seconds
        System.Threading.Thread.Sleep(30000)


        'get id 4
        With Nothing
            Dim val4 = cache1.Item(4)
            Assert.AreEqual(4, val4.Id, "Bad id for val 4")
            Assert.AreEqual(1, val4.RepoHits, "Bad hits for val 4")
        End With


        'wait 40 seconds
        System.Threading.Thread.Sleep(40000)


        With Nothing
            Dim val1 = cache1.Item(1)
            Assert.AreEqual(1, val1.Id)
            Assert.AreEqual(2, val1.RepoHits)
        End With

        With Nothing
            Dim val4 = cache1.Item(4)
            Assert.AreEqual(4, val4.Id, "Bad id for val 4")
            Assert.AreEqual(1, val4.RepoHits, "Bad hits for val 4")
        End With

        With Nothing
            Dim val5 = cache1.Item(5)
            Assert.AreEqual(5, val5.Id, "Bad id for val 5")
            Assert.AreEqual(1, val5.RepoHits, "Bad hits for val 5")
        End With

    End Sub


End Class
