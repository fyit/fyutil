﻿Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil.FlatFiles



'''<summary>
'''This is a test class for CsvFileTest and is intended
'''to contain all CsvFileTest Unit Tests
'''</summary>
<TestClass()> _
Public Class CsvFileTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for ToDataTable
    '''</summary>
    <TestMethod()> _
    Public Sub ToDataTableTest()
        Dim csvFile As New CsvFile()
        csvFile.HasHeader = True
        csvFile.TrimValues = True
        csvFile.AllowUndefinedColumns = False
        csvFile.ColumnIdentification = FYUtil.FlatFiles.CsvFile.ColumnIdentificationType.ByName
        csvFile.AddColumnDefinition("Id")
        csvFile.AddColumnDefinition("FirstName")
        csvFile.AddColumnDefinition("LastName")

        Dim sbMultiline As New Text.StringBuilder
        sbMultiline.AppendLine("multi-line string")
        sbMultiline.AppendLine("line 2")
        sbMultiline.Append("line 3")

        Dim sb As New Text.StringBuilder
        sb.AppendLine("FirstName, Id, LastName")
        sb.AppendLine("bill,smith,0")
        sb.AppendLine("""tom"",""1"",""jones""")
        sb.AppendLine("sam,2,""spade""")
        sb.AppendLine(",,")
        sb.AppendLine("robert,4,""" + sbMultiline.ToString + """")
        sb.AppendLine("suzy,5,test")
        'sb.AppendLine("short,6")    'invalid CSV exception
        'sb.AppendLine("long,7,name,,,")    'invalid CSV exception

        Dim sr As New IO.StringReader(sb.ToString)
        Dim dt As DataTable = csvFile.ToDataTable(sr)

        'test right number of rows
        Assert.IsTrue(dt.Rows.Count = 6)
        'get by number
        Assert.AreEqual("bill", dt.Rows(0)(0).ToString)
        'get by name
        Assert.AreEqual("bill", dt.Rows(0)("FirstName").ToString)

        'quoted string
        Assert.AreEqual("jones", dt.Rows(1)("LastName").ToString)
        'multiline string
        Assert.AreEqual(sbMultiline.ToString, dt.Rows(4)("LastName").ToString)
        'short line
        Assert.AreEqual("short", dt.Rows(6)("FirstName").ToString)
    End Sub
End Class
