﻿Imports System.Collections.Generic

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil



'''<summary>
'''This is a test class for ExtensionMethodsTest and is intended
'''to contain all ExtensionMethodsTest Unit Tests
'''</summary>
<TestClass()> _
Public Class ExtensionMethodsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for exContainsAll
    '''</summary>
    <TestMethod()> _
    Public Sub exContainsAllTest()
        Dim emptyColl As New List(Of String)

        Assert.IsTrue(emptyColl.exContainsAll({}))
        Assert.IsTrue({"a", "b", "c"}.exContainsAll({}))
        Assert.IsTrue({"a", "b", "c"}.exContainsAll({"a"}))
        Assert.IsTrue({"a", "b", "c"}.exContainsAll({"a", "b", "c"}))
        Assert.IsTrue({"a", "b", "c"}.exContainsAll({"b", "c", "a"}))
        Assert.IsTrue({"a", "b", "c"}.exContainsAll({"a", "a"}))
        Assert.IsTrue({"a", "a"}.exContainsAll({"a", "a"}))
        Assert.IsTrue({"a", "a"}.exContainsAll({"a"}))
        Assert.IsTrue({"a"}.exContainsAll({"a", "a"}))

        Assert.IsFalse(emptyColl.exContainsAll({"a"}))
        Assert.IsFalse({"a", "b", "c"}.exContainsAll({"d"}))
        Assert.IsFalse({"a", "b", "c"}.exContainsAll({"a", "b", "c", "d"}))
        Assert.IsFalse({"a", "b", "c"}.exContainsAll({"d", "d"}))

        Assert.IsFalse({"A"}.exContainsAll({"a"}))
    End Sub

    '''<summary>
    '''A test for exContainsOnly
    '''</summary>
    <TestMethod()> _
    Public Sub exContainsOnlyTest()
        Dim emptyColl As New List(Of String)

        Assert.IsTrue(emptyColl.exContainsOnly({}))
        Assert.IsTrue(emptyColl.exContainsOnly({"a"}))
        Assert.IsTrue({"a"}.exContainsOnly({"a"}))
        Assert.IsTrue({"a"}.exContainsOnly({"a", "b"}))
        Assert.IsTrue({"a"}.exContainsOnly({"b", "a"}))
        Assert.IsTrue({"a", "a"}.exContainsOnly({"a"}))
        Assert.IsTrue({"a", "a"}.exContainsOnly({"a", "a"}))
        Assert.IsTrue({"a"}.exContainsOnly({"a", "a"}))

        Assert.IsFalse({"a"}.exContainsOnly({}))
        Assert.IsFalse({"a"}.exContainsOnly({"b"}))
        Assert.IsFalse({"a", "b"}.exContainsOnly({"b"}))

        Assert.IsFalse({"a"}.exContainsOnly({"A"}))
    End Sub

    '''<summary>
    '''A test for exContainsSame
    '''</summary>
    <TestMethod()> _
    Public Sub exContainsSameTest()
        Dim emptyColl As New List(Of String)

        Assert.IsTrue(emptyColl.exContainsSame({}))
        Assert.IsTrue({"a"}.exContainsSame({"a"}))
        Assert.IsTrue({"a", "a"}.exContainsSame({"a"}))
        Assert.IsTrue({"a"}.exContainsSame({"a", "a"}))
        Assert.IsTrue({"a", "b"}.exContainsSame({"a", "b"}))
        Assert.IsTrue({"a", "b"}.exContainsSame({"b", "a"}))

        Assert.IsFalse(emptyColl.exContainsSame({"a"}))
        Assert.IsFalse({"a"}.exContainsSame({}))
        Assert.IsFalse({"a", "a"}.exContainsSame({"a", "b"}))
        Assert.IsFalse({"a", "b"}.exContainsSame({"a"}))
        Assert.IsFalse({"a"}.exContainsSame({"a", "b"}))
        Assert.IsFalse({"a"}.exContainsSame({"b"}))

        Assert.IsFalse({"a"}.exContainsSame({"A"}))
    End Sub

End Class
