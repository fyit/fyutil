﻿Imports System.Collections.Generic

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil



'''<summary>
'''This is a test class for ChangeHistoryBaseObjectTest and is intended
'''to contain all ChangeHistoryBaseObjectTest Unit Tests
'''</summary>
<TestClass()> _
Public Class ChangeHistoryBaseObjectTest


    Private Class SampleClass
        Inherits ChangeHistoryBaseObject

        Public Property StrTest As String
        Public Property BoolTest As Boolean
        Public Property DateTest As DateTime
        Public Property IntTest As Int32

        Private _roTest As String
        Public ReadOnly Property ROTest() As String
            Get
                Return _roTest
            End Get
        End Property

        Public Sub SetRO(pVal As String)
            _roTest = pVal
        End Sub



        Public FieldTest As String

        Private Property PrivTest As String

        Public Sub SetPrivate(pVal As String)
            PrivTest = pVal
        End Sub

    End Class


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for GetUpdatedDataElements
    '''</summary>
    <TestMethod()> _
    Public Sub GetUpdatedDataElementsTest()
        Using New Scoper
            Dim testObj As New SampleClass
            testObj.SaveCurrentState()
            testObj.StrTest = "blah"
            testObj.DateTest = DateTime.Today

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(2, changes.Count, "Should be 2 changes")
            Assert.IsTrue(FYUtil.exContainsSame({"StrTest", "DateTest"}, (From ch In changes Select ch.PropertyName)), "Should be StrTest and DateTest")
        End Using

        Using New Scoper
            Dim testObj As New SampleClass
            testObj.SaveCurrentState()
            testObj.BoolTest = Not testObj.BoolTest
            testObj.IntTest = 500

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(2, changes.Count, "Should be 2 changes")
            Assert.IsTrue(FYUtil.exContainsSame({"BoolTest", "IntTest"}, (From ch In changes Select ch.PropertyName)), "Should be BoolTest and IntTest")
        End Using

        'test before/after value
        Using New Scoper
            Dim testObj As New SampleClass
            testObj.SaveCurrentState()
            testObj.StrTest = "newval"

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(1, changes.Count, "Should be 1 change")
            Dim chg = changes.Item(0)
            Assert.AreEqual(Nothing, chg.Before, "Should be null before")
            Assert.AreEqual("newval", chg.After, "Should be 'newval' after")
        End Using

        'test changing a value to the same thing
        Using New Scoper
            Dim testObj As New SampleClass
            testObj.StrTest = "blah"
            testObj.DateTest = DateTime.Today
            testObj.IntTest = 20
            testObj.BoolTest = False
            testObj.SaveCurrentState()
            testObj.StrTest = "blah"
            testObj.DateTest = DateTime.Today
            testObj.IntTest = 20
            testObj.BoolTest = False

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(0, changes.Count, "Should not be any changes")
        End Using

        'test a private property
        Using New Scoper
            Dim testObj As New SampleClass
            testObj.SaveCurrentState()
            testObj.SetPrivate("junk")

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(0, changes.Count, "Should not show changes for private")
        End Using

        'test a public field
        Using New Scoper
            Dim testObj As New SampleClass
            testObj.SaveCurrentState()
            testObj.FieldTest = "asdf"

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(0, changes.Count, "Should not show changes for field")
        End Using

        'test a readonly property
        Using New Scoper
            Dim testObj As New SampleClass
            testObj.SetRO("asdf")
            testObj.SaveCurrentState()
            testObj.SetRO("newval")

            Dim changes = testObj.GetUpdatedDataElements()
            Assert.AreEqual(1, changes.Count, "Should show changes for RO")
        End Using
    End Sub

End Class
