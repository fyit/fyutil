﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil
Imports FYUtil.ExtensionMethods



'''<summary>
'''This is a test class for CodedEnumTest and is intended
'''to contain all CodedEnumTest Unit Tests
'''</summary>
<TestClass()> _
Public Class CodedEnumTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    Enum eGender
        <FYUtil.CodedEnum.CodedEnumValue("", "")>
        NotSet

        <FYUtil.CodedEnum.CodedEnumValue("M")>
        Male

        <FYUtil.CodedEnum.CodedEnumValue("F")>
        Female

        <FYUtil.CodedEnum.CodedEnumValue("R", "Refused to report")>
        Refused
    End Enum

    Enum eStandardEnum
        val1
        val2
        val3 = 99
    End Enum

    Enum eDupsEnum1
        DupCodeAndName

        <FYUtil.CodedEnum.CodedEnumValue("DupCodeAndName")>
        DupCodeAndName2
    End Enum

    Enum eDupsEnum2
        <FYUtil.CodedEnum.CodedEnumValue("DC")>
        DupCode1

        <FYUtil.CodedEnum.CodedEnumValue("DC")>
        DupCode2
    End Enum


    Enum eHidden
        Visible1

        <FYUtil.CodedEnum.Hidden(False)>
        Visible2

        <FYUtil.CodedEnum.Hidden()>
        Hidden1

        <FYUtil.CodedEnum.Hidden(True)>
        Hidden2
    End Enum


    Enum eCategories
        <FYUtil.CodedEnum.Category("Cat1")>
        Cat1_1

        <FYUtil.CodedEnum.Category("Cat1")>
        Cat1_2

        <FYUtil.CodedEnum.Category("Cat2")>
        Cat2_1

        <FYUtil.CodedEnum.Category("Cat2")>
        Cat2_2

        <FYUtil.CodedEnum.Category({"Cat1", "Cat2"})>
        BothCat_1

        <FYUtil.CodedEnum.Category({"Cat1", "Cat2"})>
        BothCat_2

        NoCat_1
        NoCat_2
    End Enum

    '''<summary>
    '''Test a matching code lookup
    '''</summary>
    <TestMethod()> _
    Public Sub SuccessfulGetByCodeTest()
        'check that code lookup works
        Dim fem = FYUtil.CodedEnum.GetByCode(Of eGender)("F", eGender.NotSet)
        Assert.AreEqual(eGender.Female, fem)

        Dim ml = FYUtil.CodedEnum.GetByCode(Of eGender)("M")
        Assert.AreEqual(eGender.Male, ml)

        Dim ns = FYUtil.CodedEnum.GetByCode(Of eGender)("")
        Assert.AreEqual(eGender.NotSet, ns)
    End Sub


    '''<summary>
    '''Test a non-matching code lookup
    '''</summary>
    <TestMethod()> _
    Public Sub FailGetByCodeTest()
        Dim ns = FYUtil.CodedEnum.GetByCode(Of eGender)("aklsjdfhlksadhf", eGender.NotSet)
        Assert.AreEqual(eGender.NotSet, ns)

        Dim ml = FYUtil.CodedEnum.GetByCode(Of eGender)("aklsjdfhlksadhf", eGender.Male)
        Assert.AreEqual(eGender.Male, ml)

        Dim unk = FYUtil.CodedEnum.GetByCode(Of eGender)("sdfghsdft")
        Assert.AreEqual(Nothing, unk)

        Dim unk2 = FYUtil.CodedEnum.GetByCode(Of eGender)(Nothing)
        Assert.AreEqual(Nothing, unk2)

        Dim fem = FYUtil.CodedEnum.GetByCode(Of eGender)(Nothing, eGender.Female)
        Assert.AreEqual(eGender.Female, fem)
    End Sub


    '''<summary>
    '''Test getting the code from the enum
    '''</summary>
    <TestMethod()> _
    Public Sub GetCodeTest()
        Dim cd As String = ""

        cd = FYUtil.CodedEnum.GetCode(eGender.Male)
        Assert.AreEqual("M", cd)

        cd = FYUtil.CodedEnum.GetCode(eGender.Female)
        Assert.AreEqual("F", cd)

        cd = FYUtil.CodedEnum.GetCode(eGender.NotSet)
        Assert.AreEqual("", cd)
    End Sub

    '''<summary>
    '''Test getting the description from the enum
    '''</summary>
    <TestMethod()> _
    Public Sub GetDescTest()
        Dim desc As String = ""

        desc = FYUtil.CodedEnum.GetLabel(eGender.Male)
        Assert.AreEqual("Male", desc)

        desc = FYUtil.CodedEnum.GetLabel(eGender.Female)
        Assert.AreEqual("Female", desc)

        desc = FYUtil.CodedEnum.GetLabel(eGender.NotSet)
        Assert.AreEqual("", desc)

        desc = FYUtil.CodedEnum.GetLabel(eGender.Refused)
        Assert.AreEqual("Refused to report", desc)
    End Sub

    '''<summary>
    '''Test working with an enum with no attributes
    '''</summary>
    <TestMethod()> _
    Public Sub StandardEnumTest()
        Dim cd = FYUtil.CodedEnum.GetCode(eStandardEnum.val2)
        Assert.AreEqual("val2", cd)

        cd = FYUtil.CodedEnum.GetCode(eStandardEnum.val3)
        Assert.AreNotEqual("99", cd)
        Assert.AreEqual("val3", cd)

        Dim desc = FYUtil.CodedEnum.GetLabel(eStandardEnum.val3)
        Assert.AreEqual("val3", desc)

        desc = FYUtil.CodedEnum.GetLabel(eStandardEnum.val1)
        Assert.AreEqual("val1", desc)
    End Sub

    '''<summary>
    '''Test enums with duplicate codes
    '''</summary>
    <TestMethod()> _
    Public Sub DuplicatesTest()
        Try
            Dim cd = FYUtil.CodedEnum.GetCode(eDupsEnum1.DupCodeAndName)
            Assert.Fail()
        Catch ex As Exception
            Assert.IsTrue(ex.Message.Contains("Duplicate code"))
        End Try

        Try
            Dim cd = FYUtil.CodedEnum.GetCode(eDupsEnum2.DupCode2)
            Assert.Fail()
        Catch ex As Exception
            Assert.IsTrue(ex.Message.Contains("Duplicate code"))
        End Try
    End Sub

    '''<summary>
    '''Test the hidden tag
    '''</summary>
    <TestMethod()> _
    Public Sub HiddenTest()
        With Nothing
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eHidden)(pIncludeHidden:=True) Select kvp.Key).ToList
            Assert.AreEqual(4, lst.Count)
            Assert.IsTrue(exContainsSame(lst, {eHidden.Visible1, eHidden.Visible2, eHidden.Hidden1, eHidden.Hidden2}))
        End With

        With Nothing
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eHidden)(pIncludeHidden:=False) Select kvp.Key).ToList
            Assert.AreEqual(2, lst.Count)
            Assert.IsTrue(exContainsSame(lst, {eHidden.Visible1, eHidden.Visible2}))
        End With
    End Sub


    '''<summary>
    '''Test the categories
    '''</summary>
    <TestMethod()> _
    Public Sub CategoryTest()
        With Nothing
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eCategories)(pCategory:="Cat1") Select kvp.Key).ToList
            Assert.AreEqual(4, lst.Count)
            Assert.IsTrue(exContainsSame(lst, {eCategories.Cat1_1, eCategories.Cat1_2, eCategories.BothCat_1, eCategories.BothCat_2}))
        End With

        With Nothing
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eCategories)(pCategory:="Cat2") Select kvp.Key).ToList
            Assert.AreEqual(4, lst.Count)
            Assert.IsTrue(exContainsSame(lst, {eCategories.Cat2_1, eCategories.Cat2_2, eCategories.BothCat_1, eCategories.BothCat_2}))
        End With

        With Nothing
            'case sensitive, gets nothing
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eCategories)(pCategory:="CAT1") Select kvp.Key).ToList
            Assert.AreEqual(0, lst.Count)
        End With

        With Nothing
            'does no category filter, gets everything
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eCategories)(pCategory:=Nothing) Select kvp.Key).ToList
            Assert.AreEqual(8, lst.Count)
        End With

        With Nothing
            'empty string, gets ones with no category
            Dim lst = (From kvp In FYUtil.CodedEnum.GetValueLabelList(Of eCategories)(pCategory:="") Select kvp.Key).ToList
            Assert.AreEqual(2, lst.Count)
            Assert.IsTrue(exContainsSame(lst, {eCategories.NoCat_1, eCategories.NoCat_2}))
        End With

    End Sub



End Class
