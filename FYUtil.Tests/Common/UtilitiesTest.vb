﻿Imports System.Collections.Generic

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil



'''<summary>
'''This is a test class for UtilitiesTest and is intended
'''to contain all UtilitiesTest Unit Tests
'''</summary>
<TestClass()> _
Public Class UtilitiesTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for ByteArrayToHexString
    '''</summary>
    <TestMethod()> _
    Public Sub ByteArrayToHexStringTest()
        Dim pArr() As Byte = Nothing ' TODO: Initialize to an appropriate value
        Dim expected As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim actual As String
        actual = Utilities.ByteArrayToHexString(pArr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for ByteArrayToString
    '''</summary>
    <TestMethod()> _
    Public Sub ByteArrayToStringTest()
        Dim pArr() As Byte = Nothing ' TODO: Initialize to an appropriate value
        Dim expected As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim actual As String
        actual = Utilities.ByteArrayToString(pArr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    Private Enum eTest
        NotSet = 0
        junk = 1
        test = 3
    End Enum

    '''<summary>
    '''A test for EnumToDict
    '''</summary>
    <TestMethod()> _
    Public Sub EnumToDictTest()
        Dim pEnumType As Type = GetType(eTest)
        Dim dict As Dictionary(Of Integer, String)
        Dim expected As String = "0:NotSet,1:junk,3:test,"
        Dim actual As String = ""
        dict = Utilities.EnumToDict(pEnumType)
        For Each itm In dict
            actual += String.Format("{0}:{1},", itm.Key, itm.Value)
        Next
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for HashMd5
    '''</summary>
    <TestMethod()> _
    Public Sub HashMd5ToHexStringTest()
        Dim pToHash As String = "test string"
        Dim expected As String = "6f8db599de986fab7a21625b7916589c"
        Dim actual As String
        actual = Utilities.HashMd5ToHex(pToHash)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for HexStringToByteArray
    '''</summary>
    <TestMethod(), _
     DeploymentItem("FYUtil.Common.dll")> _
    Public Sub HexStringToByteArrayTest()
        Dim pStr As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim expected() As Byte = Nothing ' TODO: Initialize to an appropriate value
        Dim actual() As Byte
        actual = Utilities.HexStringToByteArray(pStr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for RandomNumber
    '''</summary>
    <TestMethod()> _
    Public Sub RandomNumberTest()
        Dim pMin As Integer = 0
        Dim pMax As Integer = 5
        Dim actual As Integer = Utilities.RandomNumber(pMin, pMax)
        Assert.IsTrue((actual >= pMin) AndAlso (actual <= pMax))
    End Sub

    '''<summary>
    '''A test for RandomString
    '''</summary>
    <TestMethod()> _
    Public Sub RandomStringTest()
        Dim pLength As Integer = 5
        Dim pAllowChars As String = "abc"
        Dim actual As String = Utilities.RandomString(pLength, pAllowChars)
        Assert.IsTrue(actual.Length = 5)
        Dim isok As Boolean = True
        For x As Int32 = 0 To actual.Length - 1
            If (Not pAllowChars.Contains(actual.Substring(x, 1))) Then isok = False
        Next
        Assert.IsTrue(isok)

        pAllowChars = "x"
        actual = Utilities.RandomString(5, pAllowChars)
        Assert.AreEqual("xxxxx", actual)
    End Sub

    '''<summary>
    '''A test for StringToByteArray
    '''</summary>
    <TestMethod()> _
    Public Sub StringToByteArrayTest()
        Dim pStr As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim expected() As Byte = Nothing ' TODO: Initialize to an appropriate value
        Dim actual() As Byte
        actual = Utilities.StringToByteArray(pStr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for DateTimeRangesOverlap
    '''</summary>
    <TestMethod()> _
    Public Sub DateTimeRangesOverlapTest()
        Dim refDay As New DateTime(2000, 1, 15)
        Dim refRangeStart = refDay.AddHours(3)
        Dim refRangeEnd = refDay.AddHours(6)

        With Nothing
            Dim compRangeStart = refDay.AddHours(2)
            Dim compRangeEnd = refDay.AddHours(5)
            Assert.IsTrue(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Around start")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(3)
            Dim compRangeEnd = refDay.AddHours(5)
            Assert.IsTrue(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Inside against start")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(4)
            Dim compRangeEnd = refDay.AddHours(5)
            Assert.IsTrue(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Inside")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(4)
            Dim compRangeEnd = refDay.AddHours(6)
            Assert.IsTrue(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Inside against end")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(4)
            Dim compRangeEnd = refDay.AddHours(7)
            Assert.IsTrue(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Around end")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(2)
            Dim compRangeEnd = refDay.AddHours(7)
            Assert.IsTrue(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Around")
        End With



        'these should be non-overlapping

        With Nothing
            Dim compRangeStart = refDay.AddHours(1)
            Dim compRangeEnd = refDay.AddHours(2)
            Assert.IsFalse(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside before")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(2)
            Dim compRangeEnd = refDay.AddHours(3)
            Assert.IsFalse(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside against start")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(6)
            Dim compRangeEnd = refDay.AddHours(7)
            Assert.IsFalse(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside against end")
        End With

        With Nothing
            Dim compRangeStart = refDay.AddHours(7)
            Dim compRangeEnd = refDay.AddHours(8)
            Assert.IsFalse(FYUtil.Utilities.DateTimeRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside after")
        End With
    End Sub

    '''<summary>
    '''A test for TimeSpanRangesOverlap
    '''</summary>
    <TestMethod()> _
    Public Sub TimeSpanRangesOverlapTest()
        Dim refRangeStart = TimeSpan.FromHours(3)
        Dim refRangeEnd = TimeSpan.FromHours(6)

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(2)
            Dim compRangeEnd = TimeSpan.FromHours(5)
            Assert.IsTrue(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Around start")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(3)
            Dim compRangeEnd = TimeSpan.FromHours(5)
            Assert.IsTrue(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Inside against start")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(4)
            Dim compRangeEnd = TimeSpan.FromHours(5)
            Assert.IsTrue(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Inside")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(4)
            Dim compRangeEnd = TimeSpan.FromHours(6)
            Assert.IsTrue(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Inside against end")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(4)
            Dim compRangeEnd = TimeSpan.FromHours(7)
            Assert.IsTrue(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Around end")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(2)
            Dim compRangeEnd = TimeSpan.FromHours(7)
            Assert.IsTrue(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Around")
        End With



        'these should be non-overlapping

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(1)
            Dim compRangeEnd = TimeSpan.FromHours(2)
            Assert.IsFalse(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside before")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(2)
            Dim compRangeEnd = TimeSpan.FromHours(3)
            Assert.IsFalse(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside against start")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(6)
            Dim compRangeEnd = TimeSpan.FromHours(7)
            Assert.IsFalse(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside against end")
        End With

        With Nothing
            Dim compRangeStart = TimeSpan.FromHours(7)
            Dim compRangeEnd = TimeSpan.FromHours(8)
            Assert.IsFalse(FYUtil.Utilities.TimeSpanRangesOverlap(refRangeStart, refRangeEnd, compRangeStart, compRangeEnd), "Outside after")
        End With
    End Sub


    '''<summary>
    '''A test for TypeSummaryInfo
    '''</summary>
    <TestMethod()> _
    Public Sub TypeSummaryInfoTest()
        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(String))
            Assert.IsTrue(tsi.IsStringType, "String is string type")
            Assert.IsTrue(tsi.IsImmutable, "String is immutable")
            Assert.IsFalse(tsi.IsNumeric, "String is not numeric")
            Assert.IsTrue(tsi.IsNullable, "String is nullable")
        End With

        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(Int32))
            Assert.IsFalse(tsi.IsStringType, "Int32 is not string type")
            Assert.IsTrue(tsi.IsImmutable, "Int32 is immutable")
            Assert.IsTrue(tsi.IsNumeric, "Int32 is numeric")
            Assert.IsFalse(tsi.IsNullable, "Int32 is not nullable")
        End With

        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(Int32?))
            Assert.IsFalse(tsi.IsStringType, "Int32? is not string type")
            Assert.IsTrue(tsi.IsImmutable, "Int32? is immutable")
            Assert.IsTrue(tsi.IsNumeric, "Int32? is numeric")
            Assert.IsTrue(tsi.IsNullable, "Int32? is nullable")
        End With

        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(DateTime))
            Assert.IsFalse(tsi.IsStringType, "DateTime is not string type")
            Assert.IsTrue(tsi.IsImmutable, "DateTime is immutable")
            Assert.IsFalse(tsi.IsNumeric, "DateTime is not numeric")
            Assert.IsFalse(tsi.IsNullable, "DateTime is not nullable")
        End With

        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(DateTime?))
            Assert.IsFalse(tsi.IsStringType, "DateTime? is not string type")
            Assert.IsTrue(tsi.IsImmutable, "DateTime? is immutable")
            Assert.IsFalse(tsi.IsNumeric, "DateTime? is not numeric")
            Assert.IsTrue(tsi.IsNullable, "DateTime? is nullable")
        End With

        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(eTest))
            Assert.IsFalse(tsi.IsStringType, "eTest is not string type")
            Assert.IsTrue(tsi.IsImmutable, "eTest is immutable")
            Assert.IsTrue(tsi.IsNumeric, "eTest is numeric")
            Assert.IsFalse(tsi.IsNullable, "eTest is not nullable")
            Assert.IsTrue(tsi.IsEnum, "eTest is enum")
        End With

        With Nothing
            Dim tsi = Utilities.GetTypeSummaryInfo(GetType(eTest?))
            Assert.IsFalse(tsi.IsStringType, "eTest? is not string type")
            Assert.IsTrue(tsi.IsImmutable, "eTest? is immutable")
            Assert.IsTrue(tsi.IsNumeric, "eTest? is numeric")
            Assert.IsTrue(tsi.IsNullable, "eTest? is nullable")
            Assert.IsTrue(tsi.IsEnum, "eTest? is enum")
        End With

    End Sub

End Class
