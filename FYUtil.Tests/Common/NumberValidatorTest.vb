﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil



'''<summary>
'''This is a test class for NumberValidatorTest and is intended
'''to contain all NumberValidatorTest Unit Tests
'''</summary>
<TestClass()> _
Public Class NumberValidatorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()> _
    Public Sub ValidateTest()
        Dim testval As NumberValidator
        testval = New NumberValidator(1, 10)
        Assert.AreEqual(True, testval.Validate("1").Success)
        Assert.AreEqual(True, testval.Validate("10").Success)
        Assert.AreEqual(False, testval.Validate("0").Success)
        Assert.AreEqual(False, testval.Validate("11").Success)
        Assert.AreEqual(False, testval.Validate("5.9").Success)
        Assert.AreEqual(True, testval.Validate("5.0").Success)

        testval = New NumberValidator(-5, Nothing)
        Assert.AreEqual(True, testval.Validate("-1").Success)
        Assert.AreEqual(True, testval.Validate("11").Success)
        Assert.AreEqual(False, testval.Validate("-6").Success)
        Assert.AreEqual(True, testval.Validate("1101095").Success)
        Assert.AreEqual(False, testval.Validate("5.9").Success)
        Assert.AreEqual(True, testval.Validate("5.0").Success)

        testval = New NumberValidator(1.0, 5.0)
        testval.DecimalDigits = 3
        Assert.AreEqual(True, testval.Validate("2").Success)
        Assert.AreEqual(True, testval.Validate("2.0").Success)
        Assert.AreEqual(True, testval.Validate("2.967").Success)
        Assert.AreEqual(False, testval.Validate("-2.3").Success)
        Assert.AreEqual(False, testval.Validate("2.3345").Success)
    End Sub
End Class
