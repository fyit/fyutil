﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil



'''<summary>
'''This is a test class for DateValidatorTest and is intended
'''to contain all DateValidatorTest Unit Tests
'''</summary>
<TestClass()> _
Public Class DateValidatorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()> _
    Public Sub ValidateTest()
        Dim testval As DateValidator
        testval = New DateValidator(DateTime.Parse("1/1/2000"), DateTime.Parse("1/1/2005"))
        Assert.AreEqual(True, testval.Validate("1/1/2000").Success)
        Assert.AreEqual(True, testval.Validate("1/1/2005").Success)
        Assert.AreEqual(False, testval.Validate("12/31/1999").Success)
        Assert.AreEqual(False, testval.Validate("1/2/2005").Success)
        Assert.AreEqual(True, testval.Validate("3/1/2000 4:51 PM").Success)

        testval = New DateValidator(DateTime.Parse("1/1/2000"), DateTime.Parse("1/1/2005"), pDateOnly:=True)
        Assert.AreEqual(True, testval.Validate("1/1/2000").Success)
        Assert.AreEqual(True, testval.Validate("1/1/2005").Success)
        Assert.AreEqual(False, testval.Validate("12/31/1999").Success)
        Assert.AreEqual(False, testval.Validate("1/2/2005").Success)
        Assert.AreEqual(False, testval.Validate("3/1/2000 4:51 PM").Success)
    End Sub
End Class
