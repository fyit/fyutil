﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports FYUtil



'''<summary>
'''This is a test class for StringValidatorTest and is intended
'''to contain all StringValidatorTest Unit Tests
'''</summary>
<TestClass()> _
Public Class StringValidatorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()> _
    Public Sub ValidateTest()
        Dim testval As StringValidator
        testval = New StringValidator(3, 6)
        Assert.AreEqual(True, testval.Validate("abc").Success)
        Assert.AreEqual(True, testval.Validate("abcdef").Success)
        Assert.AreEqual(False, testval.Validate("ab").Success)
        Assert.AreEqual(False, testval.Validate("abcdefg").Success)

        testval = New StringValidator(3, Nothing)
        Assert.AreEqual(True, testval.Validate("abc").Success)
        Assert.AreEqual(True, testval.Validate("abcdef").Success)
        Assert.AreEqual(False, testval.Validate("ab").Success)
        Assert.AreEqual(True, testval.Validate("abcdefg").Success)

        testval = New StringValidator()
        testval.Charset = "abc"
        Assert.AreEqual(True, testval.Validate("abc").Success)
        Assert.AreEqual(True, testval.Validate("abccbbcbbbca").Success)
        Assert.AreEqual(True, testval.Validate("").Success)
        Assert.AreEqual(False, testval.Validate("abcabcabcx").Success)

        testval = New StringValidator()
        testval.RegEx = "(Mr\.|Mrs\.|Miss|Ms\.)"
        Assert.AreEqual(True, testval.Validate("Mr.").Success)
        Assert.AreEqual(True, testval.Validate("Ms.").Success)
        Assert.AreEqual(False, testval.Validate("junk").Success)
        Assert.AreEqual(False, testval.Validate("abMr.").Success)
        Assert.AreEqual(False, testval.Validate("Mr.ab").Success)
    End Sub
End Class
