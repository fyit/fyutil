﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("FYUtil")> 
<Assembly: AssemblyCopyright("")>
<Assembly: AssemblyTrademark("")>

#If DEBUG Then
<Assembly: AssemblyConfiguration("Debug")> 
#Else
<Assembly: AssemblyConfiguration("Release")> 
#End If

<Assembly: AssemblyVersion("3.2.0.0")> 
<Assembly: AssemblyFileVersion("3.2.0.0")> 
