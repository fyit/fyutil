﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



'this class is partial to emulate a namespace
Partial Public Class Database

    Public Class DbParameter
        Public Property Name As String
        Public Property Value As Object
        Public Property Type As System.Data.DbType?
        Public Property MultiValue As Boolean = False
    End Class

    Public Class DbParameterCollection
        Implements IEnumerable

        Protected _params As New List(Of DbParameter)
        Protected _paramLookup As New Dictionary(Of String, DbParameter)

        Public ReadOnly Property Count As Int32
            Get
                Return _params.Count
            End Get
        End Property

        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return _params.GetEnumerator()
        End Function

        Public Sub Add(pName As String, pValue As Object)
            Dim prm As New DbParameter With {.Name = pName, .Value = If(pValue, DBNull.Value)}
            _paramLookup.Add(pName, prm)
            _params.Add(prm)
        End Sub

        Public Sub Add(pName As String, pValue As Object, pType As System.Data.DbType)
            Dim prm As New DbParameter With {.Name = pName, .Value = If(pValue, DBNull.Value), .Type = pType}
            _paramLookup.Add(pName, prm)
            _params.Add(prm)
        End Sub

        Protected Sub AddMultiValue(pName As String, pValue As IEnumerable, pType As System.Data.DbType)
            Dim prm As New DbParameter With {.Name = pName, .Value = pValue, .Type = pType, .MultiValue = True}
            _paramLookup.Add(pName, prm)
            _params.Add(prm)
        End Sub

        Public Sub Add(pParam As DbParameter)
            _paramLookup.Add(pParam.Name, pParam)
            _params.Add(pParam)
        End Sub

        Public Sub Add(pList As DbParameterCollection)
            For Each prm As DbParameter In pList._params
                _paramLookup.Add(prm.Name, prm)
                _params.Add(prm)
            Next
        End Sub


        Public Sub AddString(pName As String, pValue As String, Optional pConvertNullTo As String = "")
            If (pConvertNullTo Is Nothing) Then Throw New Exception("AddString : Null for pConvertNullTo not allowed")
            Add(pName, If(pValue IsNot Nothing, pValue, pConvertNullTo), DbType.AnsiString)
        End Sub
        Public Sub AddStringNullable(pName As String, pValue As String, Optional pEmptyMeansNull As Boolean = True)
            If (pEmptyMeansNull AndAlso String.Equals(pValue, String.Empty)) Then pValue = Nothing
            Add(pName, pValue, DbType.AnsiString)
        End Sub

        Public Sub AddUString(pName As String, pValue As String, Optional pConvertNullTo As String = "")
            If (pConvertNullTo Is Nothing) Then Throw New Exception("AddUString : Null for pConvertNullTo not allowed")
            Add(pName, If(pValue IsNot Nothing, pValue, pConvertNullTo), DbType.String)
        End Sub
        Public Sub AddUStringNullable(pName As String, pValue As String, Optional pEmptyMeansNull As Boolean = True)
            If (pEmptyMeansNull AndAlso String.Equals(pValue, String.Empty)) Then pValue = Nothing
            Add(pName, pValue, DbType.String)
        End Sub


        Public Sub AddBoolean(pName As String, pValue As Boolean)
            Add(pName, pValue, DbType.Boolean)
        End Sub
        Public Sub AddBooleanNullable(pName As String, pValue As Boolean?)
            If (pValue.HasValue) Then
                Add(pName, pValue.Value)
            Else
                Add(pName, Nothing)
            End If
        End Sub


        Public Sub AddInt32(pName As String, pValue As Int32)
            Add(pName, pValue)
        End Sub
        Public Sub AddInt32(pName As String, pValue As Int32?, pConvertNullTo As Int32)
            Add(pName, If(pValue.HasValue, pValue.Value, pConvertNullTo))
        End Sub

        Public Sub AddInt32Nullable(pName As String, pValue As Int32, pValueMeansNull As Int32)
            If (pValue = pValueMeansNull) Then
                Add(pName, Nothing)
            Else
                Add(pName, pValue)
            End If
        End Sub
        Public Sub AddInt32Nullable(pName As String, pValue As Int32?)
            Add(pName, pValue)
        End Sub


        Public Sub AddInt64(pName As String, pValue As Int64)
            Add(pName, pValue)
        End Sub
        Public Sub AddInt64(pName As String, pValue As Int64?, pConvertNullTo As Int64)
            Add(pName, If(pValue.HasValue, pValue.Value, pConvertNullTo))
        End Sub

        Public Sub AddInt64Nullable(pName As String, pValue As Int64, pValueMeansNull As Int64)
            If (pValue = pValueMeansNull) Then
                Add(pName, Nothing)
            Else
                Add(pName, pValue)
            End If
        End Sub
        Public Sub AddInt64Nullable(pName As String, pValue As Int64?)
            Add(pName, pValue)
        End Sub


        Public Sub AddSingle(pName As String, pValue As Single)
            Add(pName, pValue)
        End Sub
        Public Sub AddSingle(pName As String, pValue As Single?, Optional pConvertNullTo As Single = 0)
            Add(pName, If(pValue.HasValue, pValue.Value, pConvertNullTo))
        End Sub

        Public Sub AddSingleNullable(pName As String, pValue As Single, Optional pValueMeansNull As Single = 0)
            If (pValue = pValueMeansNull) Then
                Add(pName, Nothing)
            Else
                Add(pName, pValue)
            End If
        End Sub
        Public Sub AddSingleNullable(pName As String, pValue As Single?)
            Add(pName, pValue)
        End Sub


        Public Sub AddDouble(pName As String, pValue As Double)
            Add(pName, pValue)
        End Sub
        Public Sub AddDouble(pName As String, pValue As Double?, Optional pConvertNullTo As Double = 0)
            Add(pName, If(pValue.HasValue, pValue.Value, pConvertNullTo))
        End Sub

        Public Sub AddDoubleNullable(pName As String, pValue As Double, Optional pValueMeansNull As Double = 0)
            If (pValue = pValueMeansNull) Then
                Add(pName, Nothing)
            Else
                Add(pName, pValue)
            End If
        End Sub
        Public Sub AddDoubleNullable(pName As String, pValue As Double?)
            Add(pName, pValue)
        End Sub


        Public Sub AddDecimal(pName As String, pValue As Decimal)
            Add(pName, pValue)
        End Sub
        Public Sub AddDecimal(pName As String, pValue As Decimal?, Optional pConvertNullTo As Decimal = 0)
            Add(pName, If(pValue.HasValue, pValue.Value, pConvertNullTo))
        End Sub

        Public Sub AddDecimalNullable(pName As String, pValue As Decimal, Optional pValueMeansNull As Decimal = 0)
            If (pValue = pValueMeansNull) Then
                Add(pName, Nothing)
            Else
                Add(pName, pValue)
            End If
        End Sub
        Public Sub AddDecimalNullable(pName As String, pValue As Decimal?)
            Add(pName, pValue)
        End Sub


        Public Sub AddDateTime(pName As String, pValue As DateTime)
            Add(pName, pValue)
        End Sub

        Public Sub AddDateTimeNullable(pName As String, pValue As DateTime?)
            Add(pName, pValue)
        End Sub


        Public Sub AddTimeSpan(pName As String, pValue As TimeSpan)
            Add(pName, pValue)
        End Sub

        Public Sub AddTimeSpanNullable(pName As String, pValue As TimeSpan?)
            Add(pName, pValue)
        End Sub


        Public Sub AddGuid(pName As String, pValue As Guid)
            Add(pName, pValue, DbType.Guid)
        End Sub

        Public Sub AddGuidNullable(pName As String, pValue As Guid?)
            Add(pName, pValue, DbType.Guid)
        End Sub


        Public Sub AddByteArray(pName As String, pValue As Byte())
            If (pValue IsNot Nothing) Then
                Add(pName, pValue, DbType.Binary)
            Else
                Add(pName, New Byte(), DbType.Binary)
            End If
        End Sub
        Public Sub AddByteArrayNullable(pName As String, pValue As Byte(), Optional pEmptyMeansNull As Boolean = True)
            If (pEmptyMeansNull AndAlso (pValue IsNot Nothing) AndAlso (pValue.Length = 0)) Then
                Add(pName, Nothing, DbType.Binary)
            Else
                Add(pName, pValue, DbType.Binary)
            End If
        End Sub

        Public Sub AddRowVersion(pName As String, pValue As UInt64)
            Add(pName, BitConverter.GetBytes(pValue), DbType.Binary)
        End Sub

        Public Sub AddMultiValueInt32(pName As String, pValues As IEnumerable(Of Int32))
            AddMultiValue(pName, pValues, DbType.Int32)
        End Sub

        Public Sub AddMultiValueString(pName As String, pValues As IEnumerable(Of String))
            AddMultiValue(pName, pValues, DbType.String)
        End Sub

    End Class

End Class
