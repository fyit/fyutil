﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Database

    ''' <summary>
    ''' Wraps a .NET DbTransaction object to limit access to database functions
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DbTransaction
        Implements IDisposable

        Friend _connectionInfo As DbConnectionInfo
        Private _transaction As System.Data.IDbTransaction
        Private _connection As System.Data.IDbConnection
        Private _completed As Boolean = False
        Private _guid As Guid


        Friend Event OnComplete(sender As System.Object, e As System.EventArgs)

        Public Sub New(pConnectionInfo As DbConnectionInfo)
            _connectionInfo = pConnectionInfo
            _guid = Guid.NewGuid()
        End Sub

        Public ReadOnly Property Guid As Guid
            Get
                Return _guid
            End Get
        End Property

        Friend Function CreateCommand() As System.Data.IDbCommand
            If (_connection Is Nothing) Then Init()
            Dim cmd As System.Data.IDbCommand = _connection.CreateCommand
            cmd.Transaction = _transaction
            Return cmd
        End Function

        Private Sub Init()
            If (_connection Is Nothing) Then
                _connection = CreateConnection(_connectionInfo)
                _connection.Open()
                _transaction = _connection.BeginTransaction()
                _completed = False
            End If
        End Sub

        Public Sub Rollback()
            If (_transaction IsNot Nothing) Then
                _transaction.Rollback()
                RaiseEvent OnComplete(Me, New System.EventArgs)
                _completed = True
            End If
        End Sub

        Public Sub Commit()
            If (_transaction IsNot Nothing) Then
                _transaction.Commit()
                RaiseEvent OnComplete(Me, New System.EventArgs)
                _completed = True
            End If
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If (_transaction IsNot Nothing) Then
                        If (Not _completed) Then
                            _transaction.Rollback()
                            RaiseEvent OnComplete(Me, New System.EventArgs)
                        End If
                        _transaction.Dispose()
                        _transaction = Nothing
                    End If
                    If (_connection IsNot Nothing) Then
                        _connection.Dispose()
                        _connection = Nothing
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Class
