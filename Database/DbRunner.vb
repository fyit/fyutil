﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports FYUtil.Database

Partial Public Class Database

    Public Class DbRunner

        Private _connInfo As DbConnectionInfo

        Public Sub New(pConnInfo As DbConnectionInfo)
            _connInfo = pConnInfo
        End Sub


#Region "ExecuteCompleteEvent"

        Private Class DbEventWrapper
            Implements IDisposable

            Private _parent As DbRunner
            Private _cmd As IDbCommand
            Private _timer As Stopwatch
            Private _event As ExecuteCompleteData

            Public Sub New(pParent As DbRunner, pCmd As IDbCommand)
                _parent = pParent
                _cmd = pCmd
                _timer = Stopwatch.StartNew()
                _event = New ExecuteCompleteData With {.StartTime = DateTime.UtcNow}
            End Sub

            Private Sub Close()
                _timer.Stop()
                If (_parent._executeCompleteCallback IsNot Nothing) Then
                    'if any subscribers
                    _event.Sql = _cmd.CommandText
                    _event.Parameters = New Dictionary(Of String, Object)
                    Dim summ As New Text.StringBuilder
                    For Each param As IDataParameter In _cmd.Parameters
                        _event.Parameters.Add(param.ParameterName, If(param.Value Is DBNull.Value, Nothing, param.Value))
                        summ.exAppendFormatLine("{0} : {1}", param.ParameterName, param.Value)
                    Next
                    _event.ParameterSummary = summ.ToString
                    _event.DurationMs = Convert.ToInt32(_timer.ElapsedMilliseconds)
                    _parent._executeCompleteCallback.Invoke(_event)
                End If
            End Sub

#Region "IDisposable Support"
            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        Close()
                    End If
                End If
                Me.disposedValue = True
            End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region

        End Class

        Private _executeCompleteCallback As Action(Of ExecuteCompleteData)

        ''' <summary>Event thrown when a database query completes</summary>
        Public Sub SetExecuteCompleteCallback(pCallback As Action(Of ExecuteCompleteData))
            _executeCompleteCallback = pCallback
        End Sub

#End Region


        ''' <summary>
        ''' Configures a native SQL command object with the SQL and parameters stored in the statement
        ''' </summary>
        Protected Sub ConfigureCommand(pCmd As IDbCommand, pStmt As IDbStatement)
            pCmd.CommandText = pStmt.GetSql()
            pCmd.CommandTimeout = pStmt.TimeoutSeconds
            pCmd.CommandType = If(pStmt.IsStoredProcedure, CommandType.StoredProcedure, CommandType.Text)
            If ((pStmt.Parameters IsNot Nothing) AndAlso (pStmt.Parameters.Count > 0)) Then
                For Each p As DbParameter In pStmt.Parameters
                    Dim param = pCmd.CreateParameter()
                    If (p.MultiValue) Then
                        ConfigureMultiValueParam(param, p)
                    Else
                        param.ParameterName = p.Name
                        param.Value = p.Value
                        If (p.Type.HasValue) Then param.DbType = p.Type.Value
                    End If
                    pCmd.Parameters.Add(param)
                Next
            End If
        End Sub

        Protected Sub ConfigureMultiValueParam(pDbParam As IDbDataParameter, pParam As DbParameter)
            If (Not pParam.Type.HasValue) Then Throw New ArgumentException("ConfigureMultiValueParam: Type is required")
            If (TypeOf pDbParam Is SqlClient.SqlParameter) Then
                Dim dbParam = DirectCast(pDbParam, SqlClient.SqlParameter)
                dbParam.ParameterName = pParam.Name
                dbParam.SqlDbType = SqlDbType.Structured
                Dim valsDt As New DataTable()
                Select Case pParam.Type.Value
                    Case DbType.Int32
                        dbParam.TypeName = "dbo.IntList"
                        valsDt.Columns.Add("Value", GetType(Int32))
                    Case DbType.String
                        dbParam.TypeName = "dbo.StrList"
                        valsDt.Columns.Add("Value", GetType(String))
                    Case Else
                        Throw New NotImplementedException("ConfigureMultiValueParam: Type not supported")
                End Select
                If (pParam.Value IsNot Nothing) Then
                    Dim valsColl = DirectCast(pParam.Value, IEnumerable)
                    For Each curVal In valsColl
                        valsDt.Rows.Add(curVal)
                    Next
                End If
                dbParam.Value = valsDt
            Else
                Throw New NotImplementedException("ConfigureMultiValueParam: DbType not supported")
            End If
        End Sub


#Region "DataTable"

        ''' <summary>
        ''' Runs the statement, returning the result as a datatable
        ''' </summary>
        Public Function ExecuteToDataTable(pStmt As IDbStatement) As DataTable
            Dim dt As New DataTable
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        DoFillDataTable(cmd, dt)
                    End Using
                End Using
                conn.Close()
            End Using
            Return dt
        End Function

        ''' <summary>
        ''' Runs the statement, returning the result as a datatable
        ''' </summary>
        Public Function ExecuteToDataTable(pTransaction As DbTransaction, pStmt As IDbStatement) As DataTable
            Dim dt As New DataTable
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    DoFillDataTable(cmd, dt)
                End Using
            End Using
            Return dt
        End Function

        ''' <summary>
        ''' Runs the pager, returning the result as a datatable. The pager specifies the SQL statement and page information for paged results.
        ''' </summary>
        Public Function ExecuteToDataTable(pPager As DbSelectPager) As DataTable
            Dim dt As New DataTable
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pPager.SelectBuilder)
                    cmd.CommandText = pPager.GetSql(_connInfo.DbType)
                    Using New DbEventWrapper(Me, cmd)
                        DoFillDataTable(cmd, dt, pPager:=pPager)
                    End Using
                End Using
                conn.Close()
            End Using
            Return dt
        End Function

        ''' <summary>
        ''' Runs the pager, returning the result as a datatable. The pager specifies the SQL statement and page information for paged results.
        ''' </summary>
        Public Function ExecuteToDataTable(pTransaction As DbTransaction, pPager As DbSelectPager) As DataTable
            Dim dt As New DataTable
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pPager.SelectBuilder)
                cmd.CommandText = pPager.GetSql(pTransaction._connectionInfo.DbType)
                Using New DbEventWrapper(Me, cmd)
                    DoFillDataTable(cmd, dt, pPager:=pPager)
                End Using
            End Using
            Return dt
        End Function

        ''' <summary>
        ''' Fills the datatable with results from execution of the statement.
        ''' Optionally validates the columns and types of the result data against the schema specified in the datatable. Used for strongly typed datatables.
        ''' </summary>
        Public Sub FillDataTable(pStmt As IDbStatement, poDt As DataTable, Optional pValidateSchema As Boolean = False)
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        DoFillDataTable(cmd, poDt, pValidateSchema)
                    End Using
                End Using
                conn.Close()
            End Using
        End Sub

        ''' <summary>
        ''' Fills the datatable with results from execution of the statement.
        ''' Optionally validates the columns and types of the result data against the schema specified in the datatable. Used for strongly typed datatables.
        ''' </summary>
        Public Sub FillDataTable(pTransaction As DbTransaction, pStmt As IDbStatement, poDt As DataTable, Optional pValidateSchema As Boolean = False)
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    DoFillDataTable(cmd, poDt, pValidateSchema)
                End Using
            End Using
        End Sub





        'this function just handles the disposable DataAdapter
        Private Sub DoFillDataTable(pCmd As IDbCommand, pioDt As DataTable, Optional pValidateSchema As Boolean = False, Optional pPager As DbSelectPager = Nothing)
            If (TypeOf pCmd Is SqlClient.SqlCommand) Then
                Using da As New SqlClient.SqlDataAdapter(DirectCast(pCmd, SqlClient.SqlCommand))
                    DoFillDataTableInt(da, pioDt, pValidateSchema, pPager)
                End Using
            ElseIf (TypeOf pCmd Is Odbc.OdbcCommand) Then
                Using da As New Odbc.OdbcDataAdapter(DirectCast(pCmd, Odbc.OdbcCommand))
                    DoFillDataTableInt(da, pioDt, pValidateSchema, pPager)
                End Using
            Else
                Throw New NotImplementedException("Database type not supported")
            End If
        End Sub
        Private Sub DoFillDataTableInt(pDa As IDataAdapter, pioDt As DataTable, pValidateSchema As Boolean, pPager As DbSelectPager)
            If (pValidateSchema) Then
                Dim schemaDs As New DataSet
                Dim schemaDts = pDa.FillSchema(schemaDs, SchemaType.Source)
                Dim errs As String = CompareSchema(schemaDts(0), pioDt)
                If (errs <> "") Then
                    Throw New Exception("Invalid schema: " + errs)
                End If
            End If
            pioDt.TableName = "Table"
            Dim tempDs As New DataSet()
            tempDs.Tables.Add(pioDt)
            pDa.Fill(tempDs)
            If ((pPager IsNot Nothing) AndAlso pPager.CalculateTotals) Then
                'tempDs.Tables.Add("Table1")
                Dim countDt = tempDs.Tables.Item(1)
                pPager.TotalRows = Convert.ToInt32(countDt.Rows(0).Item("__PAGING_ROWCOUNT__"))
                If (pPager.PageSize.HasValue) Then
                    pPager.TotalPages = Convert.ToInt32(Math.Ceiling(pPager.TotalRows.Value / pPager.PageSize.Value))
                Else
                    pPager.TotalPages = 1
                End If
            End If
        End Sub

#End Region



#Region "DataSet"

        ''' <summary>
        ''' Runs the statement, returning the result as a dataset
        ''' </summary>
        Public Function ExecuteToDataSet(pStmt As IDbStatement) As DataSet
            Dim ds As New DataSet
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        DoFillDataSet(cmd, ds)
                    End Using
                End Using
                conn.Close()
            End Using
            Return ds
        End Function

        ''' <summary>
        ''' Runs the statement, returning the result as a dataset
        ''' </summary>
        Public Function ExecuteToDataSet(pTransaction As DbTransaction, pStmt As IDbStatement) As DataSet
            Dim ds As New DataSet
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    DoFillDataSet(cmd, ds)
                End Using
            End Using
            Return ds
        End Function




        Private Sub DoFillDataSet(pCmd As IDbCommand, pioDs As DataSet)
            If (TypeOf pCmd Is SqlClient.SqlCommand) Then
                Using da As New SqlClient.SqlDataAdapter(DirectCast(pCmd, SqlClient.SqlCommand))
                    da.Fill(pioDs)
                End Using
            ElseIf (TypeOf pCmd Is Odbc.OdbcCommand) Then
                Using da As New Odbc.OdbcDataAdapter(DirectCast(pCmd, Odbc.OdbcCommand))
                    da.Fill(pioDs)
                End Using
            Else
                Throw New NotImplementedException("Database type not supported")
            End If
        End Sub

#End Region



#Region "Object"

        ''' <summary>
        ''' Execute the statement, returning a single result row as a strongly typed object.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pStmt"></param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Public Function ExecuteToObject(Of T)(pStmt As IDbStatement, pCopyFunc As Func(Of DbResult, T)) As T
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        Using dbRes As New DbResult(cmd.ExecuteReader(CommandBehavior.SingleRow))
                            If (dbRes.NextRow) Then
                                Return pCopyFunc.Invoke(dbRes)
                            Else
                                Return Nothing
                            End If
                        End Using
                    End Using
                End Using
                conn.Close()
            End Using
        End Function

        ''' <summary>
        ''' Execute the statement, returning a single result row as a strongly typed object.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pTransaction"></param>
        ''' <param name="pStmt"></param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Public Function ExecuteToObject(Of T)(pTransaction As DbTransaction, pStmt As IDbStatement, pCopyFunc As Func(Of DbResult, T)) As T
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    Using dbRes As New DbResult(cmd.ExecuteReader(CommandBehavior.SingleRow))
                        If (dbRes.NextRow) Then
                            Return pCopyFunc.Invoke(dbRes)
                        Else
                            Return Nothing
                        End If
                    End Using
                End Using
            End Using
        End Function

#End Region



#Region "ObjectList"

        ''' <summary>
        ''' Execute the statement, returning the results as a list of strongly typed objects.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pStmt"></param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Public Function ExecuteToObjectList(Of T)(pStmt As IDbStatement, pCopyFunc As Func(Of DbResult, T)) As IList(Of T)
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        Using dbRes As New DbResult(cmd.ExecuteReader())
                            Dim res As New List(Of T)
                            While (dbRes.NextRow)
                                res.Add(pCopyFunc.Invoke(dbRes))
                            End While
                            Return res
                        End Using
                    End Using
                End Using
                conn.Close()
            End Using
        End Function

        ''' <summary>
        ''' Execute the statement, returning the results as a list of strongly typed objects.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pTransaction"></param>
        ''' <param name="pStmt"></param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Public Function ExecuteToObjectList(Of T)(pTransaction As DbTransaction, pStmt As IDbStatement, pCopyFunc As Func(Of DbResult, T)) As IList(Of T)
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    Using dbRes As New DbResult(cmd.ExecuteReader())
                        Dim res As New List(Of T)
                        While (dbRes.NextRow)
                            res.Add(pCopyFunc.Invoke(dbRes))
                        End While
                        Return res
                    End Using
                End Using
            End Using
        End Function

        ''' <summary>
        ''' Execute the pager, returning the results as a list of strongly typed objects.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pPager">Pager to execute. Includes statement and page information for paged results.</param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Public Function ExecuteToObjectList(Of T)(pPager As DbSelectPager, pCopyFunc As Func(Of DbResult, T)) As IList(Of T)
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pPager.SelectBuilder)
                    cmd.CommandText = pPager.GetSql(_connInfo.DbType)
                    Using New DbEventWrapper(Me, cmd)
                        Using dbRes As New DbResult(cmd.ExecuteReader())
                            Dim res As New List(Of T)
                            While (dbRes.NextRow)
                                res.Add(pCopyFunc.Invoke(dbRes))
                            End While
                            If (pPager.CalculateTotals) Then
                                dbRes.NextResultSet()
                                dbRes.NextRow()
                                pPager.TotalRows = dbRes.GetInt32("__PAGING_ROWCOUNT__")
                                If (pPager.PageSize.HasValue) Then
                                    pPager.TotalPages = Convert.ToInt32(Math.Ceiling(pPager.TotalRows.Value / pPager.PageSize.Value))
                                Else
                                    pPager.TotalPages = 1
                                End If
                            End If
                            Return res
                        End Using
                    End Using
                End Using
                conn.Close()
            End Using
        End Function

        ''' <summary>
        ''' Execute the pager, returning the results as a list of strongly typed objects.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pTransaction"></param>
        ''' <param name="pPager">Pager to execute. Includes statement and page information for paged results.</param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Public Function ExecuteToObjectList(Of T)(pTransaction As DbTransaction, pPager As DbSelectPager, pCopyFunc As Func(Of DbResult, T)) As IList(Of T)
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pPager.SelectBuilder)
                cmd.CommandText = pPager.GetSql(pTransaction._connectionInfo.DbType)
                Using New DbEventWrapper(Me, cmd)
                    Using dbRes As New DbResult(cmd.ExecuteReader())
                        Dim res As New List(Of T)
                        While (dbRes.NextRow)
                            res.Add(pCopyFunc.Invoke(dbRes))
                        End While
                        If (pPager.CalculateTotals) Then
                            dbRes.NextResultSet()
                            dbRes.NextRow()
                            pPager.TotalRows = dbRes.GetInt32("__PAGING_ROWCOUNT__")
                            If (pPager.PageSize.HasValue) Then
                                pPager.TotalPages = Convert.ToInt32(Math.Ceiling(pPager.TotalRows.Value / pPager.PageSize.Value))
                            Else
                                pPager.TotalPages = 1
                            End If
                        End If
                        Return res
                    End Using
                End Using
            End Using
        End Function


#End Region


#Region "Action"

        ''' <summary>
        ''' Executes the statement, executing an action on each row returned.
        ''' </summary>
        ''' <param name="pStmt"></param>
        ''' <param name="pAction">Action to execute on each row</param>
        ''' <remarks></remarks>
        Public Sub ExecuteToAction(pStmt As IDbStatement, pAction As Action(Of DbResult))
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        Using dbRes As New DbResult(cmd.ExecuteReader())
                            While (dbRes.NextRow)
                                pAction.Invoke(dbRes)
                            End While
                        End Using
                    End Using
                End Using
                conn.Close()
            End Using
        End Sub

        ''' <summary>
        ''' Executes the statement, executing an action on each row returned.
        ''' </summary>
        ''' <param name="pTransaction"></param>
        ''' <param name="pStmt"></param>
        ''' <param name="pAction">Action to execute on each row</param>
        ''' <remarks></remarks>
        Public Sub ExecuteToAction(pTransaction As DbTransaction, pStmt As IDbStatement, pAction As Action(Of DbResult))
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    Using dbRes As New DbResult(cmd.ExecuteReader())
                        While (dbRes.NextRow)
                            pAction.Invoke(dbRes)
                        End While
                    End Using
                End Using
            End Using
        End Sub

#End Region


#Region "Scalar"

        ''' <summary>
        ''' Execute the statement with a single return value.
        ''' </summary>
        Public Function ExecuteToScalar(pStmt As IDbStatement) As Object
            Dim res As Object
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        res = cmd.ExecuteScalar()
                    End Using
                    If (res Is DBNull.Value) Then res = Nothing
                End Using
                conn.Close()
            End Using
            Return res
        End Function

        ''' <summary>
        ''' Execute the statement with a single return value.
        ''' </summary>
        Public Function ExecuteToScalar(pTransaction As DbTransaction, pStmt As IDbStatement) As Object
            Dim res As Object
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    res = cmd.ExecuteScalar()
                End Using
                If (res Is DBNull.Value) Then res = Nothing
            End Using
            Return res
        End Function

        ''' <summary>
        ''' Execute the statement with a single return value. Casts result to the specified type. Throws an error if the type is incorrect.
        ''' </summary>
        Public Function ExecuteToScalar(Of T)(pStmt As IDbStatement) As T
            Dim res As Object = ExecuteToScalar(pStmt)
            Return DirectCast(res, T)
        End Function

        ''' <summary>
        ''' Execute the statement with a single return value. Casts result to the specified type. Throws an error if the type is incorrect.
        ''' </summary>
        Public Function ExecuteToScalar(Of T)(pTransaction As DbTransaction, pStmt As IDbStatement) As T
            Dim res As Object = ExecuteToScalar(pTransaction, pStmt)
            Return DirectCast(res, T)
        End Function

#End Region


#Region "NonQuery"

        ''' <summary>
        ''' Execute the non-select statement. Returns the number of rows affected
        ''' </summary>
        Public Function ExecuteNonQuery(pStmt As IDbStatement) As Int32
            Dim res As Int32
            Using conn As IDbConnection = CreateConnection(_connInfo)
                conn.Open()
                Using cmd As IDbCommand = conn.CreateCommand()
                    ConfigureCommand(cmd, pStmt)
                    Using New DbEventWrapper(Me, cmd)
                        res = cmd.ExecuteNonQuery()
                    End Using
                End Using
                conn.Close()
            End Using
            Return res
        End Function

        ''' <summary>
        ''' Execute the non-select statement. Returns the number of rows affected
        ''' </summary>
        Public Function ExecuteNonQuery(pTransaction As DbTransaction, pStmt As IDbStatement) As Int32
            Dim res As Int32
            Using cmd As IDbCommand = pTransaction.CreateCommand()
                ConfigureCommand(cmd, pStmt)
                Using New DbEventWrapper(Me, cmd)
                    res = cmd.ExecuteNonQuery()
                End Using
            End Using
            Return res
        End Function

#End Region


    End Class

End Class
