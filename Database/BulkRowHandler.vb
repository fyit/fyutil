﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Database

    Public Class BulkRowHandler
        Implements IDisposable

        Public Event BatchRowsDeleted(pTableName As String, pLastCount As Int32, pTotalCount As Int32)

        Private _connInfo As DbConnectionInfo
        Private _unprocessedRowCount As Int32
        Private _fileStream As IO.FileStream

        Private _textWrapper As Char? = Nothing
        Private _replaceDoubleQuote As String = Nothing

        Private _params As New List(Of Object)

        Private _sqlBulk As SqlClient.SqlBulkCopy
        Private _dataTable As Data.DataTable

        Public Sub New(pConnectionString As String)
            _connInfo = New DbConnectionInfo(pConnectionString, eDbType.SqlServer)
        End Sub

        Public Sub AddRow(pVals As IEnumerable(Of Object))
            If _sqlBulk IsNot Nothing Then WriteToDb(pVals)
            If _fileStream IsNot Nothing Then WriteToFile(pVals)
        End Sub

#Region "File Code"

        Public Sub InitFile(pFilename As String, Optional pWrapDataChar As Char? = Nothing, Optional pReplaceDoubleQuote As String = Nothing)
            _fileStream = New IO.FileStream(pFilename, IO.FileMode.Append)
            _textWrapper = pWrapDataChar
            _replaceDoubleQuote = pReplaceDoubleQuote
        End Sub

        ''' <summary>
        ''' Writes the data to the initialized file stream, and wraps it with the char specified (chr(0) indicates no wrapping char)
        ''' </summary>
        Public Sub WriteToFile(pVals As IEnumerable(Of Object))
            If _fileStream Is Nothing OrElse Not _fileStream.CanWrite Then Throw New Exception("When writing to file, you must prepare the file by calling InitFile()!")
            Dim line = Join((From item In pVals Select PrepareData(item)).ToArray, vbTab) & vbCrLf
            Dim bytes = Text.Encoding.ASCII.GetBytes(line)
            SyncLock _fileStream
                _fileStream.Write(bytes, 0, bytes.Count)
            End SyncLock
        End Sub

        Private Function PrepareData(pData As Object) As String
            Dim data As String
            If pData Is Nothing Then
                data = ""
            Else
                If _replaceDoubleQuote IsNot Nothing Then
                    data = pData.ToString.Replace(Chr(34), _replaceDoubleQuote)
                Else
                    data = pData.ToString
                End If
            End If
            If _textWrapper.HasValue Then
                Return _textWrapper & data & _textWrapper
            Else
                Return data
            End If
        End Function

#End Region

#Region "Bulk Insert Code"

        Public Sub InitInsert(pTableName As String, pFieldNames As String, Optional pBatchSize As Int32 = 10000, Optional pBatchTimeout As Int32 = 300)
            _sqlBulk = New SqlClient.SqlBulkCopy(_connInfo.ConnectionString)
            _sqlBulk.DestinationTableName = pTableName
            _dataTable = New Data.DataTable(pTableName)
            _dataTable.Columns.AddRange((From item In pFieldNames.Split(","c) Select New Data.DataColumn(item.Trim)).ToArray)
            _sqlBulk.BatchSize = pBatchSize
            _sqlBulk.BulkCopyTimeout = pBatchTimeout
        End Sub

        ''' <summary>
        ''' Will store a number of sql insert statements (determined by batch size) and execute them all at one time
        ''' </summary>
        Public Sub WriteToDb(pVals As IEnumerable(Of Object))
            If pVals.Count <> _dataTable.Columns.Count Then Throw New Exception("Bulk Insert vals does not match INSERT statement field count!")
            _dataTable.LoadDataRow(pVals.ToArray, True)

            _unprocessedRowCount += 1
            If _unprocessedRowCount >= _sqlBulk.BatchSize Then Call ExecuteBulkInsert()
        End Sub

        Public Function ExecuteBulkInsert() As Int32
            If _sqlBulk Is Nothing Then Throw New Exception("When inserting, you must prepare the insert statement by calling InitInsert()!")

            If _unprocessedRowCount > 0 Then
                Dim rowsProcessed = _dataTable.Rows.Count
                _sqlBulk.WriteToServer(_dataTable)

                _dataTable.Rows.Clear()
                _unprocessedRowCount = 0

                Return rowsProcessed
            Else
                Return 0
            End If
        End Function

        Private Function ValAsString(pObject As Object) As String
            If pObject Is Nothing Then Return "" Else Return pObject.ToString
        End Function

#End Region

        Public Function BatchDelete(pStmt As DbConditionalStatementBuilder, Optional pBatchSize As Int32 = 10000, Optional pDisableIndexesDuringDelete As IEnumerable(Of String) = Nothing) As Int32
            If Not pStmt.ActionStatement().ToString.ToUpper().Contains("DELETE") Then Throw New Exception("DeleteBatch statement does not appear to be a SQL DELETE statement!")
            If pBatchSize < 1 Then Throw New Exception("DeleteBatch Batch size must be > 0")

            Dim runner As New DbRunner(_connInfo)

            'Extract the TableName from the Action Statement 
            Dim tableName As String = Nothing
            Dim fromIndex = pStmt.ActionStatement.ToString.ToUpper.IndexOf(" FROM ")
            If fromIndex > 0 Then
                tableName = pStmt.ActionStatement.ToString.Substring(fromIndex + 6).Trim()
                If tableName.Contains(" ") Then tableName = tableName.Substring(0, tableName.IndexOf(" "))
            End If
            If String.IsNullOrWhiteSpace(tableName) Then Throw New Exception("DeleteBatch TableName cannot be determined from statement!")

            Dim rowsAffectedTotal As Int32 = 0
            Try
                'Disable non-essential indexes if they exist
                If pDisableIndexesDuringDelete IsNot Nothing AndAlso pDisableIndexesDuringDelete.Count > 0 Then
                    For Each index In pDisableIndexesDuringDelete
                        runner.ExecuteNonQuery(New DbStatementBuilder(String.Format("ALTER INDEX [{0}] ON {1} DISABLE", {index, tableName})) With {.TimeoutSeconds = pStmt.TimeoutSeconds})
                    Next
                End If

                'Convert the Delete statement into a Subset (top) delete statement for the batch
                Dim newActionStatement = pStmt.ActionStatement.ToString().ToUpper.Replace("DELETE", "DELETE TOP (" & pBatchSize & ")")
                pStmt.ActionStatement.Clear()
                pStmt.ActionStatement.Append(newActionStatement)

                Dim stmt As New DbStatementBuilder(pStmt.GetSql)
                stmt.TimeoutSeconds = pStmt.TimeoutSeconds
                For Each param As DbParameter In pStmt.Parameters
                    stmt.Parameters.Add(param)
                Next
                If Not stmt.GetSql.Contains("@@ROWCOUNT") Then stmt.Statement.Append("; SELECT @@ROWCOUNT;")

                'Execute the delete statement batch until rows effected = 0
                Dim rowsAffected As Int32 = 0
                Do
                    rowsAffected = runner.ExecuteToScalar(Of Int32)(stmt)
                    rowsAffectedTotal += rowsAffected

                    RaiseEvent BatchRowsDeleted(tableName, rowsAffected, rowsAffectedTotal)

                    'If the rows from the last delete did not hit the batch size, we know we are done
                    If rowsAffected < pBatchSize Then Exit Do
                Loop Until rowsAffected = 0

            Catch ex As Exception
                If ex.InnerException IsNot Nothing Then Throw ex.InnerException Else Throw ex

            Finally

                'rebuild non-essential indexes if they exist
                If pDisableIndexesDuringDelete IsNot Nothing AndAlso pDisableIndexesDuringDelete.Count > 0 Then
                    For Each index In pDisableIndexesDuringDelete
                        runner.ExecuteNonQuery(New DbStatementBuilder(String.Format("ALTER INDEX [{0}] ON {1} REBUILD", {index, tableName})) With {.TimeoutSeconds = pStmt.TimeoutSeconds})
                    Next
                End If

            End Try

            Return rowsAffectedTotal
        End Function


#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                    If _unprocessedRowCount > 0 Then ExecuteBulkInsert()

                    If _fileStream IsNot Nothing Then
                        _fileStream.Close()
                    End If
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class


End Class
