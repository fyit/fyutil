﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Database

    Public Class SqlScriptRunner

        Public Event RunScriptNotify(ByVal o As Object, ByVal e As RunScriptNotifyEventArgs)

        Public Class RunScriptNotifyEventArgs
            Inherits EventArgs

            Public Property BatchNumber As Int32
            Public Property TotalBatches As Int32
            Public Property Messages As String
        End Class

        Private Class RunScriptMessageTracker
            Public Messages As New Text.StringBuilder

            Public Sub InfoHandler(ByVal o As Object, ByVal e As SqlClient.SqlInfoMessageEventArgs)
                Messages.AppendLine(e.Message)
            End Sub
        End Class

        ''' <summary>
        ''' Split sql script into batches on the "GO" keyword
        ''' </summary>
        ''' <param name="pSql"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function SplitSqlFileIntoBatches(ByVal pSql As String) As IEnumerable(Of String)
            Dim res As New List(Of String)

            'split on GO, surrounded by newlines and optional whitespace
            Dim subscripts() As String = Text.RegularExpressions.Regex.Split(pSql, "\n\s*GO\s*\n")

            'check for GO without preceding CRLF
            If (subscripts(0).StartsWith("GO" + vbCrLf)) Then
                subscripts(0) = subscripts(0).Remove(0, 4)
            End If

            'check for GO without following CRLF
            Dim lastOne = subscripts(subscripts.Length - 1)
            If (lastOne.EndsWith(vbCrLf + "GO")) Then
                lastOne = lastOne.Remove(lastOne.Length - 4, 4)
            End If
            subscripts(subscripts.Length - 1) = lastOne

            For x = 0 To subscripts.Count - 1
                Dim curScript = subscripts(x).Trim
                If (String.IsNullOrWhiteSpace(subscripts(x))) Then Continue For
                res.Add(curScript)
            Next

            Return res
        End Function

        ''' <summary>
        ''' Execute a SQL batch file against specified database
        ''' </summary>
        ''' <param name="pTargetDbConnstring"></param>
        ''' <param name="pScriptFile"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function RunScriptFile(ByVal pTargetDbConnstring As String, ByVal pScriptFile As String) As FYUtil.SuccessAndDescription
            If (Not IO.File.Exists(pScriptFile)) Then Return New SuccessAndDescription(False, "File not found")
            Dim sqlFile = IO.File.ReadAllText(pScriptFile)
            Return RunScript(pTargetDbConnstring, sqlFile)
        End Function

        ''' <summary>
        ''' Execute a SQL batch script against specified database
        ''' </summary>
        ''' <param name="pTargetDbConnstring"></param>
        ''' <param name="pSql">Text of the sql file (1+ batches)</param>
        ''' <param name="pIndividualBatchTimeout">Timeout (in seconds) of a single batch (separated by GO statement)</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function RunScript(ByVal pTargetDbConnstring As String, ByVal pSql As String, Optional ByVal pIndividualBatchTimeout As Int32 = 600) As FYUtil.SuccessAndDescription
            Using conn = Database.CreateConnection(pTargetDbConnstring)
                Try
                    conn.Open()
                Catch ex As Exception
                    Return New FYUtil.SuccessAndDescription(False, ex.Message)
                End Try
                Dim msgTracker As New RunScriptMessageTracker
                Dim messages As New Text.StringBuilder()
                If (conn.GetType() = GetType(SqlClient.SqlConnection)) Then
                    Dim sqlConn = DirectCast(conn, SqlClient.SqlConnection)
                    AddHandler sqlConn.InfoMessage, AddressOf msgTracker.InfoHandler
                End If

                Using cmd = conn.CreateCommand()
                    cmd.CommandType = CommandType.Text
                    cmd.CommandTimeout = pIndividualBatchTimeout
                    Dim batches = SplitSqlFileIntoBatches(pSql)
                    Dim curBatchNum = 1
                    Dim totalBatches = batches.Count
                    Try
                        For Each curScript In batches
                            cmd.CommandText = curScript
                            cmd.ExecuteNonQuery()
                            messages.Append(msgTracker.Messages)
                            RaiseEvent RunScriptNotify(Me, New RunScriptNotifyEventArgs With {.BatchNumber = curBatchNum, .TotalBatches = totalBatches, .Messages = msgTracker.Messages.ToString})
                            msgTracker.Messages.Clear()
                            curBatchNum += 1
                        Next
                        Return New FYUtil.SuccessAndDescription(True, messages.ToString)
                    Catch ex As SqlClient.SqlException
                        Dim maxNum = -1000
                        Dim maxErr As SqlClient.SqlError = Nothing
                        For Each er As SqlClient.SqlError In ex.Errors
                            If (er.Class > maxNum) Then
                                maxErr = er
                                maxNum = er.Class
                            End If
                        Next
                        Dim errMsg As String
                        If (maxErr IsNot Nothing) Then
                            errMsg = String.Format("Error in batch {0}, line {1} : {2}", curBatchNum, maxErr.LineNumber, maxErr.Message)
                        Else
                            errMsg = String.Format("Error in batch {0}, line {1} : {2}", curBatchNum, ex.LineNumber, ex.Message)
                        End If
                        Return New FYUtil.SuccessAndDescription(False, errMsg)
                    Catch ex As Exception
                        Dim errMsg As String
                        errMsg = String.Format("Error in batch {0} : {1}", curBatchNum, ex.Message)
                        Return New FYUtil.SuccessAndDescription(False, errMsg)
                    End Try
                End Using
            End Using
        End Function

    End Class

End Class
