﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



'this class is partial to emulate a namespace
Partial Public Class Database

    Public Class DbConnectionInfo
        Public Property ConnectionString As String
        Public Property DbType As eDbType

        Public Sub New()
        End Sub

        Public Sub New(pConnectionString As String, pDbType As eDbType)
            ConnectionString = pConnectionString
            DbType = pDbType
        End Sub

    End Class

    Public Interface IDbStatement
        ReadOnly Property Parameters As DbParameterCollection
        Property IsStoredProcedure As Boolean
        Property TimeoutSeconds As Int32
        Function GetSql() As String
    End Interface

    Public Class DbStatementBuilder
        Implements IDbStatement

        Protected _parameters As New DbParameterCollection()

        Public Property Statement As New Text.StringBuilder

        Public Sub New()
        End Sub

        Public Sub New(pSql As String)
            SetStatement(pSql)
        End Sub

        Public Sub SetStatement(pSql As String)
            Statement.Clear()
            Statement.Append(pSql)
        End Sub

        Public Function GetSql() As String Implements IDbStatement.GetSql
            Return Statement.ToString()
        End Function

        Public ReadOnly Property Parameters As DbParameterCollection Implements IDbStatement.Parameters
            Get
                Return _parameters
            End Get
        End Property

        Public Property IsStoredProcedure As Boolean = False Implements IDbStatement.IsStoredProcedure
        Public Property TimeoutSeconds As Int32 = 30 Implements IDbStatement.TimeoutSeconds

    End Class

    Public Class DbProcedureCall
        Implements IDbStatement


        Protected _parameters As New DbParameterCollection()

        Public Property SpName As String

        Public Sub New()
        End Sub

        Public Sub New(pSpName As String)
            SpName = pSpName
        End Sub

        Public Function GetSql() As String Implements IDbStatement.GetSql
            Return SpName
        End Function

        Public ReadOnly Property Parameters As DbParameterCollection Implements IDbStatement.Parameters
            Get
                Return _parameters
            End Get
        End Property

        Public Property IsStoredProcedure As Boolean Implements IDbStatement.IsStoredProcedure
            Get
                Return True
            End Get
            Set(value As Boolean)
                If (value = False) Then Throw New ArgumentException("DbProcedureCall: Stored procedures are required")
            End Set
        End Property

        Public Property TimeoutSeconds As Int32 = 30 Implements IDbStatement.TimeoutSeconds

    End Class



    Public Class DbConditionBuilder

        Protected _parameters As DbParameterCollection

        Public Enum eBooleanOperator
            AndOperator = 1
            OrOperator = 2
        End Enum

        Public Property Condition As New Text.StringBuilder

        Public ReadOnly Property Parameters As DbParameterCollection
            Get
                Return _parameters
            End Get
        End Property


        Public Sub New(pStatement As IDbStatement)
            _parameters = pStatement.Parameters
        End Sub

        Public Sub New(pParams As DbParameterCollection)
            _parameters = pParams
        End Sub

        Public Sub New()
            _parameters = New DbParameterCollection
        End Sub


        Public Function GetSql() As String
            Return Condition.ToString
        End Function

        Public Overrides Function ToString() As String
            Return GetSql()
        End Function



        ''' <summary>
        ''' Append directly to the condition string
        ''' </summary>
        Public Sub Append(pToAppend As Object)
            Me.Condition.Append(pToAppend)
        End Sub

        ''' <summary>
        ''' Append directly to the condition string
        ''' </summary>
        Public Sub AppendLine(Optional pToAppend As String = Nothing)
            Me.Condition.AppendLine(pToAppend)
        End Sub

        ''' <summary>
        ''' Append directly to the condition string
        ''' </summary>
        Public Sub AppendFormat(pFormat As String, Arg0 As Object, Optional Arg1 As Object = Nothing, Optional Arg2 As Object = Nothing, Optional Arg3 As Object = Nothing, Optional Arg4 As Object = Nothing, Optional Arg5 As Object = Nothing, Optional Arg6 As Object = Nothing, Optional Arg7 As Object = Nothing, Optional Arg8 As Object = Nothing, Optional Arg9 As Object = Nothing)
            Me.Condition.AppendFormat(pFormat, Arg0, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)
        End Sub

        Public Function Length() As Int32
            Return Me.Condition.Length
        End Function

        ''' <summary>
        ''' Add specified expression to the where clause. Adds WHERE and boolean operators automatically
        ''' </summary>
        ''' <param name="pToAdd">valid boolean expression to add</param>
        ''' <param name="pPrefixOperator">The compositing boolean operator to use, if necessary</param>
        ''' <remarks></remarks>
        Public Sub Add(pToAdd As String, Optional pPrefixOperator As eBooleanOperator = eBooleanOperator.AndOperator)
            If (Condition.Length > 0) Then
                Condition.AppendLine()
                If (pPrefixOperator = eBooleanOperator.AndOperator) Then
                    Condition.Append(" AND ")
                ElseIf (pPrefixOperator = eBooleanOperator.OrOperator) Then
                    Condition.Append(" OR ")
                End If
            End If
            Condition.AppendFormat("( {0} )", pToAdd)
        End Sub

        ''' <summary>
        ''' Add specified condition to the where clause. Adds WHERE and boolean operators automatically
        ''' </summary>
        ''' <param name="pToAdd">condition</param>
        ''' <param name="pPrefixOperator">The compositing boolean operator to use, if necessary</param>
        ''' <remarks></remarks>
        Public Sub Add(pToAdd As DbConditionBuilder, Optional pPrefixOperator As eBooleanOperator = eBooleanOperator.AndOperator)
            If (Condition.Length > 0) Then
                Condition.AppendLine()
                If (pPrefixOperator = eBooleanOperator.AndOperator) Then
                    Condition.Append(" AND ")
                ElseIf (pPrefixOperator = eBooleanOperator.OrOperator) Then
                    Condition.Append(" OR ")
                End If
            End If
            Condition.AppendFormat("( {0} )", pToAdd.GetSql)
            If (Not Me.Parameters.Equals(pToAdd.Parameters)) Then
                For Each param As DbParameter In pToAdd.Parameters
                    Me.Parameters.Add(param)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Wraps parentheses around WHERE clause if it contains content
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Parenthesize()
            If (Condition.Length > 0) Then
                Condition.Insert(0, "(")
                Condition.Insert(1, Environment.NewLine)
                Condition.AppendLine()
                Condition.AppendLine(")")
            End If
        End Sub

        ''' <summary>
        ''' Add parameters and WHERE clause for a multi-value parameter. Does not use table-value parameters or functions.
        ''' </summary>
        ''' <typeparam name="TDataType"></typeparam>
        ''' <param name="pField">Name of field in table</param>
        ''' <param name="pParameterName">Name of parameter, will be used as a base name for multiple parameters if necessary</param>
        ''' <param name="pVals">List of values</param>
        ''' <param name="pUseNotIn">True to make a NOT comparison</param>
        ''' <param name="pPrefixOperator">The compositing boolean operator to use, if necessary</param>
        ''' <remarks></remarks>
        Public Sub AddMvp(Of TDataType)(pField As String, pParameterName As String, pVals As IEnumerable(Of TDataType), Optional pUseNotIn As Boolean = False, Optional pPrefixOperator As eBooleanOperator = eBooleanOperator.AndOperator)
            If ((pVals Is Nothing) OrElse (pVals.Count = 0)) Then
                'don't put the clause in at all
                Return
            Else
                Dim includeNullClause As String = Nothing
                Dim tsi = Utilities.GetTypeSummaryInfo(GetType(TDataType))
                If (tsi.IsNullable AndAlso pVals.Contains(Nothing)) Then
                    includeNullClause = String.Format("{0} IS{1} NULL", pField, If(pUseNotIn, " NOT", ""))
                End If
                If (pVals.Count <= 1) Then
                    If (includeNullClause IsNot Nothing) Then
                        Me.Add(includeNullClause, pPrefixOperator)
                    Else
                        Me.Add(String.Format("{0} {1} {2}", pField, If(pUseNotIn, "<>", "="), pParameterName), pPrefixOperator)
                        Me.Parameters.Add(pParameterName, pVals(0))
                    End If
                ElseIf ((pVals.Count + Me.Parameters.Count) <= 2000) Then
                    'maximum number of parameters in SQL 2000-2008
                    Dim sqlSb As New System.Text.StringBuilder
                    sqlSb.AppendFormat("{0} {1}IN ( ", pField, If(pUseNotIn, "NOT ", ""))
                    Dim delim As String = ""
                    Dim x As Int32 = 1
                    Dim curParam As String
                    For Each curVal As TDataType In pVals
                        If (curVal Is Nothing) Then Continue For
                        curParam = String.Format("{0}__MVP_{1}", pParameterName, x)
                        sqlSb.AppendFormat("{0}{1}", delim, curParam)
                        Me.Parameters.Add(curParam, curVal)
                        delim = ", "
                        x += 1
                    Next
                    sqlSb.Append(" )")
                    If (includeNullClause IsNot Nothing) Then
                        sqlSb.AppendFormat(" OR {0}", includeNullClause)
                    End If
                    Me.Add(sqlSb.ToString, pPrefixOperator)
                Else
                    If (Not tsi.IsNumeric) Then Throw New ArgumentException("AddMvpWhere: List is too large for string MVP")

                    'construct a big dynamic SQL statement
                    Dim sqlSb As New System.Text.StringBuilder
                    sqlSb.AppendFormat("{0} {1}IN ( ", pField, If(pUseNotIn, "NOT ", ""))
                    Dim delim As String = ""
                    For Each curVal As TDataType In pVals
                        If (curVal Is Nothing) Then Continue For
                        Dim curValStr As String = ""
                        If (curVal Is Nothing) Then
                            curValStr = "NULL"
                        ElseIf (tsi.IsEnum) Then
                            'enum converts to a string as the element name
                            curValStr = Convert.ToInt32(curVal).ToString
                        Else
                            curValStr = curVal.ToString
                        End If
                        sqlSb.AppendFormat("{0}{1}", delim, curValStr)
                        delim = ", "
                    Next
                    sqlSb.Append(" )")
                    If (includeNullClause IsNot Nothing) Then
                        sqlSb.AppendFormat(" OR {0}", includeNullClause)
                    End If
                    Me.Add(sqlSb.ToString, pPrefixOperator)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Add an IS NULL condition to where clause
        ''' </summary>
        ''' <param name="pField">Name of field in table</param>
        ''' <param name="pIsNull">Whether to use IS NULL or IS NOT NULL</param>
        ''' <param name="pPrefixOperator">The compositing boolean operator to use, if necessary</param>
        ''' <remarks></remarks>
        Public Sub AddIsNull(pField As String, pIsNull As Boolean?, Optional pPrefixOperator As eBooleanOperator = eBooleanOperator.AndOperator)
            If (Not pIsNull.HasValue) Then
                'don't put the clause in at all
                Return
            Else
                Me.Add(String.Format("{0} IS{1} NULL", pField, If(pIsNull.Value, "", " NOT")), pPrefixOperator)
            End If
        End Sub

        ''' <summary>
        ''' Add an IS NOT NULL condition to where clause
        ''' </summary>
        ''' <param name="pField">Name of field in table</param>
        ''' <param name="pIsNotNull">Whether to use IS NOT NULL or IS NULL</param>
        ''' <param name="pPrefixOperator">The compositing boolean operator to use, if necessary</param>
        ''' <remarks></remarks>
        Public Sub AddIsNotNull(pField As String, pIsNotNull As Boolean?, Optional pPrefixOperator As eBooleanOperator = eBooleanOperator.AndOperator)
            If (Not pIsNotNull.HasValue) Then
                'don't put the clause in at all
                Return
            Else
                Me.Add(String.Format("{0} IS{1} NULL", pField, If(pIsNotNull.Value, " NOT", "")), pPrefixOperator)
            End If
        End Sub

        ''' <summary>
        ''' Add parameters and WHERE clause for a date range.
        ''' </summary>
        ''' <param name="pField">Name of field in table</param>
        ''' <param name="pParameterName">Name of parameter, will be used as a base name for multiple parameters if necessary</param>
        ''' <param name="pDateFrom">The start date/time of the range</param>
        ''' <param name="pDateTo">The end date/time of the range</param>
        ''' <param name="pDateOnly">Interpret source date/times as date-only</param>
        ''' <param name="pParameterTimeZone">The timezone of the supplied parameters</param>
        ''' <param name="pDbTimeZone">The timezone of values stored in the database</param>
        ''' <param name="pPrefixOperator">The compositing boolean operator to use, if necessary</param>
        ''' <remarks></remarks>
        Public Sub AddDateRange(pField As String, pParameterName As String, pDateFrom As DateTime?, pDateTo As DateTime?, Optional pDateOnly As Boolean = False, Optional pParameterTimeZone As TimeZoneInfo = Nothing, Optional pDbTimeZone As TimeZoneInfo = Nothing, Optional pPrefixOperator As eBooleanOperator = eBooleanOperator.AndOperator)
            If ((pDbTimeZone IsNot Nothing) AndAlso (pParameterTimeZone Is Nothing)) Then Throw New ArgumentException("ParameterTimeZone is required if DbTimeZone is specified")

            If ((Not pDateFrom.HasValue) AndAlso (Not pDateTo.HasValue)) Then
                'don't put the clause in at all
                Return
            Else
                Dim dates = FYUtil.Utilities.TranslateDateRange(pDateFrom, pDateTo, pDateOnly, pParameterTimeZone, pDbTimeZone)

                Dim sqlSb As New System.Text.StringBuilder
                If (dates.Item1.HasValue) Then
                    Dim paramFrom = String.Format("{0}__From", pParameterName)
                    sqlSb.AppendFormat("{0} >= {1}", pField, paramFrom)
                    Me.Parameters.AddDateTime(paramFrom, dates.Item1.Value)
                End If
                If (dates.Item2.HasValue) Then
                    If (sqlSb.Length > 0) Then sqlSb.Append(" AND ")
                    Dim paramTo = String.Format("{0}__To", pParameterName)
                    sqlSb.AppendFormat("{0} < {1}", pField, paramTo)
                    Me.Parameters.AddDateTime(paramTo, dates.Item2.Value)
                End If
                Me.Add(sqlSb.ToString, pPrefixOperator)
            End If
        End Sub

    End Class


    Public MustInherit Class DbConditionalStatementBuilderBase
        Implements IDbStatement

        Protected _parameters As New DbParameterCollection()

        Public Property WhereClause As New DbConditionBuilder(Me)

        Public MustOverride Function GetSql() As String Implements IDbStatement.GetSql

        Public ReadOnly Property Parameters As DbParameterCollection Implements IDbStatement.Parameters
            Get
                Return _parameters
            End Get
        End Property

        Public Property IsStoredProcedure As Boolean = False Implements IDbStatement.IsStoredProcedure
        Public Property TimeoutSeconds As Int32 = 30 Implements IDbStatement.TimeoutSeconds

    End Class



    Public Class DbConditionalStatementBuilder
        Inherits DbConditionalStatementBuilderBase

        Public Property ActionStatement As New Text.StringBuilder

        Public Sub New()
        End Sub

        Public Sub New(pActionSql As String)
            ActionStatement.Append(pActionSql)
        End Sub

        Public Sub SetActionStatement(pSql As String)
            ActionStatement.Clear()
            ActionStatement.Append(pSql)
        End Sub

        Public Overrides Function GetSql() As String
            Dim out As New Text.StringBuilder()
            out.Append(ActionStatement.ToString)
            If (WhereClause.Length > 0) Then
                out.AppendLine(" WHERE ")
                out.AppendLine(WhereClause.ToString)
            Else
                Throw New Exception("DbConditionalStatementBuilder: Conditional statement must include a condition")
            End If

            Return out.ToString
        End Function

    End Class



    Public Class DbSelectBuilder
        Inherits DbConditionalStatementBuilderBase

        Public Property Top As Int32? = Nothing
        Public Property Distinct As Boolean = False
        Public Property FieldsClause As New Text.StringBuilder
        Public Property FromClause As New Text.StringBuilder
        Public Property GroupClause As New Text.StringBuilder
        Public Property OrderClause As New Text.StringBuilder

        Public Sub New()
        End Sub

        Public Sub New(pFrom As String, pFields As String)
            FromClause.Append(pFrom)
            FieldsClause.Append(pFields)
        End Sub

        Public Sub AddToFieldsClause(pToAdd As String)
            If (FieldsClause.Length > 0) Then FieldsClause.Append(", ")
            FieldsClause.Append(pToAdd)
        End Sub

        Public Sub AddToGroupClause(pToAdd As String)
            If (GroupClause.Length > 0) Then GroupClause.Append(", ")
            GroupClause.Append(pToAdd)
        End Sub

        Public Sub AddToOrderClause(pToAdd As String)
            If (OrderClause.Length > 0) Then OrderClause.Append(", ")
            OrderClause.Append(pToAdd)
        End Sub

        Public Overrides Function GetSql() As String
            Dim out As New Text.StringBuilder()
            out.Append("SELECT")
            If (Me.Distinct) Then out.Append(" DISTINCT")
            If (Top.HasValue) Then out.AppendFormat(" TOP {0}", Top.Value)
            out.AppendLine()
            out.AppendLine(FieldsClause.ToString)
            out.AppendLine("FROM ")
            out.AppendLine(FromClause.ToString)
            If (WhereClause.Length > 0) Then
                out.AppendLine("WHERE ")
                out.AppendLine(WhereClause.ToString)
            End If
            If (GroupClause.Length > 0) Then
                out.AppendLine("GROUP BY ")
                out.AppendLine(GroupClause.ToString)
            End If
            If (OrderClause.Length > 0) Then
                out.AppendLine("ORDER BY ")
                out.AppendLine(OrderClause.ToString)
            End If

            Return out.ToString
        End Function

    End Class



    Public Class DbSelectPager

        Public Sub New()
        End Sub

        Public Sub New(pSelectBuilder As DbSelectBuilder, pPageNumber As Int32)
            SelectBuilder = pSelectBuilder
            PageNumber = pPageNumber
            PageSize = pSelectBuilder.Top
            pSelectBuilder.Top = Nothing
        End Sub

        Public Sub New(pSelectBuilder As DbSelectBuilder, pPageNumber As Int32, pPageSize As Int32)
            SelectBuilder = pSelectBuilder
            PageNumber = pPageNumber
            PageSize = pPageSize
            pSelectBuilder.Top = Nothing
        End Sub

        Friend Property SelectBuilder As DbSelectBuilder
        Public Property PageNumber As Int32
        Public Property PageSize As Int32?

        Public Property CalculateTotals As Boolean
        Public Property TotalRows As Int32?
        Public Property TotalPages As Int32?



        Friend Function GetSql(pDbType As eDbType) As String
            If (SelectBuilder.OrderClause.Length = 0) Then Throw New ArgumentException("DbSelectPager: ORDER clause required")
            If (Me.PageNumber <= 0) Then Throw New ArgumentException("DbSelectPager: Invalid page number")

            If ((PageNumber = 1) OrElse (PageSize Is Nothing)) Then
                SelectBuilder.Top = PageSize
                Dim out As New Text.StringBuilder(SelectBuilder.GetSql)
                SelectBuilder.Top = Nothing

                If (CalculateTotals) Then
                    'add count
                    Dim origOrderClause = SelectBuilder.OrderClause.ToString()
                    SelectBuilder.OrderClause.Clear()

                    Dim origFieldsClause = SelectBuilder.FieldsClause.ToString()
                    SelectBuilder.FieldsClause.Clear()
                    SelectBuilder.FieldsClause.Append("COUNT(*) AS __PAGING_ROWCOUNT__")

                    out.Append(";")
                    out.AppendLine(SelectBuilder.GetSql)

                    'reset builder back
                    SelectBuilder.FieldsClause.Clear()
                    SelectBuilder.FieldsClause.Append(origFieldsClause)
                    SelectBuilder.OrderClause.Clear()
                    SelectBuilder.OrderClause.Append(origOrderClause)
                End If

                Return out.ToString
            Else
                If (pDbType = eDbType.SqlServer) Then
                    'modify builder to support paging
                    Dim origOrderClause = SelectBuilder.OrderClause.ToString()
                    SelectBuilder.OrderClause.Clear()

                    Dim origFieldsClause = SelectBuilder.FieldsClause.ToString()
                    SelectBuilder.AddToFieldsClause(String.Format("ROW_NUMBER() OVER (ORDER BY {0}) AS __PAGING_ROWNUMBER__", origOrderClause))


                    Dim out As New Text.StringBuilder()
                    out.AppendLine("WITH __PAGING_CTE__ AS (")

                    out.AppendLine(SelectBuilder.GetSql())

                    out.AppendLine(") SELECT * FROM __PAGING_CTE__")
                    out.AppendFormat("WHERE __PAGING_ROWNUMBER__ BETWEEN {0} AND {1}", (((PageNumber - 1) * PageSize) + 1), (PageNumber * PageSize))
                    out.AppendLine()

                    If (CalculateTotals) Then
                        'add count
                        SelectBuilder.FieldsClause.Clear()
                        SelectBuilder.FieldsClause.Append("COUNT(*) AS __PAGING_ROWCOUNT__")
                        out.Append(";")
                        out.AppendLine(SelectBuilder.GetSql)
                    End If

                    'reset builder back
                    SelectBuilder.FieldsClause.Clear()
                    SelectBuilder.FieldsClause.Append(origFieldsClause)
                    SelectBuilder.OrderClause.Clear()
                    SelectBuilder.OrderClause.Append(origOrderClause)

                    Return out.ToString()
                Else
                    Throw New NotImplementedException("DbSelectPager: DbType not supported")
                End If
            End If
        End Function

    End Class

End Class
