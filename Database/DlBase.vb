﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Imports FYUtil.Database

Partial Public Class Database

    ''' <summary>
    ''' This is a base class for a DL base class. Create a subclass as a base DL for your application.
    ''' Create a subclass under that for each table.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class DlBase

        Private _connectionInfo As DbConnectionInfo
        Private _dbRunner As DbRunner
        Private _executeCompleteCallback As Action(Of ExecuteCompleteData)

        Protected Sub New(pConnInfo As DbConnectionInfo)
            _connectionInfo = pConnInfo
            _dbRunner = New DbRunner(_connectionInfo)
        End Sub

        Private Function GetConnectionInfo() As DbConnectionInfo
            Return _connectionInfo
        End Function

        Protected Sub SetExecuteCompleteCallback(pCallback As Action(Of ExecuteCompleteData))
            _dbRunner.SetExecuteCompleteCallback(pCallback)
        End Sub

#Region "Transaction"

        Private _transaction As DbTransaction = Nothing

        ''' <summary>
        ''' Begins a new transaction for this DL object. All database activity within this scope will be in the transaction.
        ''' </summary>
        Public Function BeginTransaction() As DbTransaction
            If (_transaction IsNot Nothing) Then Throw New Exception("DlBase: Duplicate transaction")
            _transaction = New DbTransaction(GetConnectionInfo)
            AddHandler _transaction.OnComplete, AddressOf TransactionOwner_OnComplete
            SaveTransactionToContext(_transaction)
            Return _transaction
        End Function

        ''' <summary>
        ''' Joins this DL object to a transaction already in progress. All database activity within this scope will be in the transaction.
        ''' </summary>
        Public Sub JoinTransaction(pTransaction As DbTransaction)
            If (pTransaction.Equals(_transaction)) Then Return

            If (_transaction IsNot Nothing) Then Throw New Exception("DlBase: Duplicate transaction")
            If (pTransaction._connectionInfo.ConnectionString <> GetConnectionInfo().ConnectionString) Then Throw New Exception("DlBase: Transaction must be on the same database")

            _transaction = pTransaction
            AddHandler _transaction.OnComplete, AddressOf Transaction_OnComplete
        End Sub

        'currently checks for untransacted activity within scope of a transaction
        'could auto-join here, if necessary
        Private Sub CheckTransaction()
            If ((_preventNonTransactedInScope) AndAlso (_transaction Is Nothing)) Then
                Dim trans = LoadTransactionFromContext()
                If (trans IsNot Nothing) Then Throw New Exception("Non-transacted activity detected within the scope of a transaction")
            End If

        End Sub

        Private Sub Transaction_OnComplete(sender As System.Object, e As System.EventArgs)
            If (sender.Equals(_transaction)) Then
                RemoveHandler _transaction.OnComplete, AddressOf Transaction_OnComplete
                _transaction = Nothing
            End If
        End Sub

        Private Sub TransactionOwner_OnComplete(sender As System.Object, e As System.EventArgs)
            If (sender.Equals(_transaction)) Then
                RemoveHandler _transaction.OnComplete, AddressOf TransactionOwner_OnComplete
                ClearTransactionFromContext(_transaction.Guid)
                _transaction = Nothing
            End If
        End Sub

#End Region


#Region "ContextTransaction"

        Private Shared _contextTransactionStore As New Dictionary(Of Guid, DbTransaction)
        Private Const CONTEXT_TRANSACTION_SLOT_BASE = "FYUtil.Database.DlBase.Transaction"
        Private _contextTransactionSlotName As String = CONTEXT_TRANSACTION_SLOT_BASE
        Private _autoJoinTransactionInScope As Boolean = False
        Private _preventNonTransactedInScope As Boolean = False

        ''' <summary>
        ''' Attaches to an open transaction on the context, if present.
        ''' Causes any new transactions to be attached to the context.
        ''' </summary>
        Protected Sub SetTransactionMode(pPartitionName As String, pAutoJoinTransactionInScope As Boolean, pPreventNonTransactedInScope As Boolean)
            _contextTransactionSlotName = String.Format("{0}.{1}", CONTEXT_TRANSACTION_SLOT_BASE, pPartitionName)
            _autoJoinTransactionInScope = pAutoJoinTransactionInScope
            _preventNonTransactedInScope = pPreventNonTransactedInScope

            If (_autoJoinTransactionInScope) Then
                Dim trans = LoadTransactionFromContext()
                If (trans IsNot Nothing) Then JoinTransaction(trans)
            End If
        End Sub


        Private Sub SaveTransactionToContext(pTransaction As DbTransaction)
            SyncLock _contextTransactionStore
                If (_contextTransactionStore.ContainsKey(pTransaction.Guid)) Then Throw New Exception("Transaction already in context store")
                _contextTransactionStore.Item(pTransaction.Guid) = pTransaction

                'could save on CallContext
                Runtime.Remoting.Messaging.CallContext.LogicalSetData(_contextTransactionSlotName, pTransaction.Guid)

                'could save on Thread
                'Dim slot = System.Threading.Thread.GetNamedDataSlot(_contextTransactionSlotName)
                'System.Threading.Thread.SetData(slot, pTransaction.Guid)
            End SyncLock
        End Sub

        Private Function LoadTransactionFromContext() As DbTransaction
            SyncLock _contextTransactionStore
                Dim guidObj As Object = Nothing
                'could save on CallContext
                guidObj = Runtime.Remoting.Messaging.CallContext.LogicalGetData(_contextTransactionSlotName)

                'could save on Thread
                'Dim slot = System.Threading.Thread.GetNamedDataSlot(_contextTransactionSlotName)
                'guidObj = System.Threading.Thread.GetData(slot)

                If (guidObj Is Nothing) Then Return Nothing

                Dim guid = DirectCast(guidObj, Guid)
                If (_contextTransactionStore.ContainsKey(guid)) Then Return _contextTransactionStore.Item(guid)
                Return Nothing
            End SyncLock
        End Function

        Private Sub ClearTransactionFromContext(pGuid As Guid)
            SyncLock _contextTransactionStore
                If (_contextTransactionStore.ContainsKey(pGuid)) Then _contextTransactionStore.Remove(pGuid)

                'could save on CallContext
                Runtime.Remoting.Messaging.CallContext.FreeNamedDataSlot(_contextTransactionSlotName)

                'could save on Thread
                'System.Threading.Thread.FreeNamedDataSlot(_contextTransactionSlotName)
            End SyncLock
        End Sub

#End Region


#Region "Execute"

        ''' <summary>
        ''' Runs the statement, returning the result as a dataset
        ''' </summary>
        Protected Function ExecuteToDataSet(pStmt As IDbStatement) As DataSet
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToDataSet(pStmt)
            Else
                Return _dbRunner.ExecuteToDataSet(_transaction, pStmt)
            End If
        End Function


        ''' <summary>
        ''' Runs the statement, returning the result as a datatable
        ''' </summary>
        Protected Function ExecuteToDataTable(pStmt As IDbStatement) As DataTable
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToDataTable(pStmt)
            Else
                Return _dbRunner.ExecuteToDataTable(_transaction, pStmt)
            End If
        End Function


        ''' <summary>
        ''' Runs the pager, returning the result as a datatable. The pager specifies the SQL statement and page information for paged results.
        ''' </summary>
        Protected Function ExecuteToDataTable(pPager As DbSelectPager) As DataTable
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToDataTable(pPager)
            Else
                Return _dbRunner.ExecuteToDataTable(_transaction, pPager)
            End If
        End Function


        ''' <summary>
        ''' Fills the datatable with results from execution of the statement.
        ''' Optionally validates the columns and types of the result data against the schema specified in the datatable. Used for strongly typed datatables.
        ''' </summary>
        Protected Sub FillDataTable(pStmt As IDbStatement, poDt As DataTable, Optional pValidateSchema As Boolean = False)
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                _dbRunner.FillDataTable(pStmt, poDt, pValidateSchema)
            Else
                _dbRunner.FillDataTable(_transaction, pStmt, poDt, pValidateSchema)
            End If
        End Sub



        ''' <summary>
        ''' Execute the statement, returning a single result row as a strongly typed object.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pStmt"></param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Protected Function ExecuteToObject(Of T)(pStmt As IDbStatement, pCopyFunc As Func(Of DbResult, T)) As T
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToObject(Of T)(pStmt, pCopyFunc)
            Else
                Return _dbRunner.ExecuteToObject(Of T)(_transaction, pStmt, pCopyFunc)
            End If
        End Function

        ''' <summary>
        ''' Execute the statement, returning the results as a list of strongly typed objects.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pStmt"></param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Protected Function ExecuteToObjectList(Of T)(pStmt As IDbStatement, pCopyFunc As Func(Of DbResult, T)) As IList(Of T)
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToObjectList(Of T)(pStmt, pCopyFunc)
            Else
                Return _dbRunner.ExecuteToObjectList(Of T)(_transaction, pStmt, pCopyFunc)
            End If
        End Function

        ''' <summary>
        ''' Execute the pager, returning the results as a list of strongly typed objects.
        ''' </summary>
        ''' <typeparam name="T">Type of object to return</typeparam>
        ''' <param name="pPager">Pager to execute. Includes statement and page information for paged results.</param>
        ''' <param name="pCopyFunc">Function which translates a single row to an instance of the object type</param>
        ''' <returns>An object of the specified type</returns>
        ''' <remarks></remarks>
        Protected Function ExecuteToObjectList(Of T)(pPager As DbSelectPager, pCopyFunc As Func(Of DbResult, T)) As IList(Of T)
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToObjectList(Of T)(pPager, pCopyFunc)
            Else
                Return _dbRunner.ExecuteToObjectList(Of T)(_transaction, pPager, pCopyFunc)
            End If
        End Function


        ''' <summary>
        ''' Executes the statement, executing an action on each row returned.
        ''' </summary>
        ''' <param name="pStmt"></param>
        ''' <param name="pAction">Action to execute on each row</param>
        ''' <remarks></remarks>
        Protected Sub ExecuteToAction(pStmt As IDbStatement, pAction As Action(Of DbResult))
            'CheckTransaction()
            If (_transaction Is Nothing) Then
                _dbRunner.ExecuteToAction(pStmt, pAction)
            Else
                _dbRunner.ExecuteToAction(_transaction, pStmt, pAction)
            End If
        End Sub


        ''' <summary>
        ''' Execute the statement with a single return value.
        ''' </summary>
        Protected Function ExecuteToScalar(pStmt As IDbStatement) As Object
            CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToScalar(pStmt)
            Else
                Return _dbRunner.ExecuteToScalar(_transaction, pStmt)
            End If
        End Function

        ''' <summary>
        ''' Execute the statement with a single return value. Casts result to the specified type. Throws an error if the type is incorrect.
        ''' </summary>
        Protected Function ExecuteToScalar(Of T)(pStmt As IDbStatement) As T
            CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteToScalar(Of T)(pStmt)
            Else
                Return _dbRunner.ExecuteToScalar(Of T)(_transaction, pStmt)
            End If
        End Function


        ''' <summary>
        ''' Execute the non-select statement. Returns the number of rows affected
        ''' </summary>
        Protected Function ExecuteNonQuery(pStmt As IDbStatement) As Int32
            CheckTransaction()
            If (_transaction Is Nothing) Then
                Return _dbRunner.ExecuteNonQuery(pStmt)
            Else
                Return _dbRunner.ExecuteNonQuery(_transaction, pStmt)
            End If
        End Function

#End Region


#Region "Utility"

        ''' <summary>
        ''' Generate a fields clause using a fields enum
        ''' </summary>
        ''' <typeparam name="TFieldsEnum">type parameter for the enum of field names</typeparam>
        ''' <param name="pAlias">optional alias to attach to each field</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetSqlFields(Of TFieldsEnum As Structure)(Optional pAlias As String = Nothing) As String
            Dim aliasPrefix = If(String.IsNullOrEmpty(pAlias), "", pAlias + ".")
            Dim flds = FYUtil.Utilities.EnumToDict(GetType(TFieldsEnum))
            Dim res As New Text.StringBuilder
            Dim delim = ""
            For Each fld In flds.Values
                res.AppendFormat("{0}{1}[{2}]", delim, aliasPrefix, fld)
                delim = ", "
            Next

            Return res.ToString
        End Function

        ''' <summary>
        ''' Generate a fields clause using a list of items from a fields enum
        ''' </summary>
        ''' <typeparam name="TFieldsEnum">type parameter for the enum of field names</typeparam>
        ''' <param name="pFieldEnumItems">list of items from an enum of field names</param>
        ''' <param name="pAlias">optional alias to attach to each field</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetSqlFields(Of TFieldsEnum As Structure)(pFieldEnumItems As IEnumerable(Of TFieldsEnum), Optional pAlias As String = Nothing) As String
            Dim aliasPrefix = If(String.IsNullOrEmpty(pAlias), "", pAlias + ".")
            Dim res As New Text.StringBuilder
            Dim delim = ""
            For Each fld In pFieldEnumItems
                res.AppendFormat("{0}{1}[{2}]", delim, aliasPrefix, fld)
                delim = ", "
            Next

            Return res.ToString
        End Function

#End Region

    End Class

End Class
