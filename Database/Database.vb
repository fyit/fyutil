﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



'this class is partial to emulate a namespace
Partial Public Class Database

    Public Enum eDbType
        NotSet = 0
        Odbc = 1
        SqlServer = 2
        SqlServerCe = 3
    End Enum


    Public Class ExecuteCompleteData
        Public DbName As String
        Public Sql As String
        Public Parameters As IDictionary(Of String, Object)
        Public ParameterSummary As String
        Public StartTime As DateTime
        Public DurationMs As Int32
    End Class

#Region "DbInfo"

    ''' <summary>Verify that the database is the correct model, version, and environment</summary>
    Public Shared Function VerifyDbInfo(pConnectionInfo As DbConnectionInfo, pModelName As String, pModelVersion As String, pEnvironmentEnum As eEnvironment) As FYUtil.SuccessAndDescription
        Return VerifyDbInfo(pConnectionInfo, pModelName, pModelVersion, pEnvironmentEnum.ToString)
    End Function

    ''' <summary>Verify that the database is the correct model, version, and environment</summary>
    Public Shared Function VerifyDbInfo(pConnectionInfo As DbConnectionInfo, pModelName As String, pModelVersion As String, pEnvironment As String) As FYUtil.SuccessAndDescription
        Try
            Dim dbInfo = GetDbInfo(pConnectionInfo)
            If (dbInfo.ModelName.Equals(pModelName, StringComparison.OrdinalIgnoreCase) AndAlso
               dbInfo.ModelVersion.Equals(pModelVersion, StringComparison.OrdinalIgnoreCase) AndAlso
               dbInfo.Environment.Equals(pEnvironment, StringComparison.OrdinalIgnoreCase)) Then
                Return SuccessAndDescription.Ok
            Else
                Dim EnvInfo = String.Format("{0}.{1}.{2}", dbInfo.ModelName, dbInfo.ModelVersion, dbInfo.Environment)
                Dim EnvExp = String.Format("{0}.{1}.{2}", pModelName, pModelVersion, pEnvironment)
                Return SuccessAndDescription.NotOk(String.Format("Expected DB Info: {0} but found: {1}", EnvExp, EnvInfo))
            End If
        Catch ex As Exception
            Return SuccessAndDescription.NotOk(ex.Message)
        End Try
    End Function

    Public Class DbInfo
        Public ModelName As String
        Public ModelVersion As String
        Public Environment As String
        Public EnvironmentEnum As eEnvironment
    End Class

    ''' <summary>Returns the database model information</summary>
    Public Shared Function GetDbInfo(pConnectionInfo As DbConnectionInfo) As DbInfo
        Dim runner As New DbRunner(pConnectionInfo)
        Dim rows As IList(Of DbInfo) = Nothing
        Dim errMsg = ""
        Try
            Dim stmt As New DbProcedureCall("usp_GetDbInfo")
            rows = runner.ExecuteToObjectList(Of DbInfo)(stmt, AddressOf CreateDbInfoObject)
        Catch ex1 As Exception
            errMsg = ex1.Message
            Try
                Dim stmt As New DbStatementBuilder("SELECT * FROM DbInfo")
                rows = runner.ExecuteToObjectList(Of DbInfo)(stmt, AddressOf CreateDbInfoObject)
            Catch ex2 As Exception
                If (Not ex2.Message.Equals(errMsg)) Then errMsg += ", " + ex2.Message
                Throw New Exception(String.Format("GetDbInfo: Unable to locate DbInfo ({0})", errMsg))
            End Try
        End Try

        If (rows.Count = 1) Then
            Return rows(0)
        ElseIf (rows.Count = 0) Then
            Throw New Exception("GetDbInfo: DbInfo missing")
        Else
            Throw New Exception("GetDbInfo: DbInfo duplicate")
        End If
    End Function

    Protected Shared Function CreateDbInfoObject(pData As DbResult) As DbInfo
        Dim obj As New DbInfo
        obj.ModelName = pData.GetString("ModelName")
        obj.ModelVersion = pData.GetString("ModelVersion")
        obj.Environment = pData.GetString("Environment")
        obj.EnvironmentEnum = General.DetermineEnvironment(obj.Environment)
        Return obj
    End Function

#End Region

#Region "Connection"

    Friend Shared Function CreateConnection(pConnectionInfo As DbConnectionInfo) As IDbConnection
        Select Case pConnectionInfo.DbType
            Case eDbType.SqlServer
                Return New SqlClient.SqlConnection(pConnectionInfo.ConnectionString)
            Case eDbType.Odbc
                Return New Odbc.OdbcConnection(pConnectionInfo.ConnectionString)
            Case eDbType.SqlServerCe
                Throw New Exception("SqlServerCe not supported")
                'Return New SqlServerCe.SqlCeConnection(pConnectionInfo.ConnectionString)
            Case Else
                Return CreateConnection(pConnectionInfo.ConnectionString)
        End Select
    End Function

    Friend Shared Function CreateConnection(pConnectionString As String) As IDbConnection
        'TBD: differentiate dbms by connection string?
        Dim conn As IDbConnection = New SqlClient.SqlConnection(pConnectionString)
        Return conn
    End Function

    Public Shared Function CreateConnectionStringBuilder(pConnectionString As String) As Common.DbConnectionStringBuilder
        'TBD: differentiate dbms by connection string?
        Return New SqlClient.SqlConnectionStringBuilder(pConnectionString)
    End Function

    Public Shared Function GetDatabaseNameFromConnectionString(pConnectionString As String) As String
        'TBD: differentiate dbms by connection string?
        Dim csb = New SqlClient.SqlConnectionStringBuilder(pConnectionString)
        Return csb.InitialCatalog
    End Function

    Public Shared Function SetDatabaseNameInConnectionString(pConnectionString As String, pDbName As String) As String
        'TBD: differentiate dbms by connection string?
        Dim csb = New SqlClient.SqlConnectionStringBuilder(pConnectionString)
        csb.InitialCatalog = pDbName
        Return csb.ToString
    End Function

    Public Shared Function RemovePasswordFromConnectionString(pConnectionString As String) As String
        'TBD: differentiate dbms by connection string?
        Dim csb = New SqlClient.SqlConnectionStringBuilder(pConnectionString)
        If (Not String.IsNullOrWhiteSpace(csb.Password)) Then csb.Password = ""
        Return csb.ToString
    End Function

    Public Shared Function SetPasswordInConnectionString(pConnectionString As String, pPassword As String) As String
        'TBD: differentiate dbms by connection string?
        Dim csb = New SqlClient.SqlConnectionStringBuilder(pConnectionString)
        csb.Password = pPassword
        Return csb.ToString
    End Function

#End Region

    ''' <summary>
    ''' Compares the columns and types defined in each datatable, returning errors on any differences
    ''' </summary>
    Public Shared Function CompareSchema(pDtActual As DataTable, pDtExpected As DataTable) As String
        Dim res As New Text.StringBuilder

        'If (pDtActual.Columns.Count <> pDtExpected.Columns.Count) Then
        '    Return String.Format("Incorrect column count, found {0}, expected {1}", pDtActual.Columns.Count, pDtExpected.Columns.Count)
        'End If

        Dim actCols As New Dictionary(Of String, DataColumn)
        For Each col As DataColumn In pDtActual.Columns
            actCols.Add(col.ColumnName, col)
        Next

        For Each colex As DataColumn In pDtExpected.Columns
            If (pDtActual.Columns.Contains(colex.ColumnName)) Then
                Dim colact As DataColumn = pDtActual.Columns(colex.ColumnName)
                actCols.Remove(colact.ColumnName)
                If (colex.DataType.FullName <> colact.DataType.FullName) Then
                    res.AppendLine(String.Format("Incorrect data type on column '{0}', found {1}, expected {2}", colex.ColumnName, colact.DataType.FullName, colex.DataType.FullName))
                End If
            Else
                res.AppendLine(String.Format("Missing column, expected '{0}'", colex.ColumnName))
            End If
        Next

        For Each col As DataColumn In actCols.Values
            res.AppendLine(String.Format("Extra column, '{0}'", col.ColumnName))
        Next

        Return res.ToString
    End Function



End Class
