﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Database

    Public Class ModelUpdater

        Public Event StartedProcessing(ByVal pScript As String, ByVal pCurrent As Int32, ByVal pTotal As Int32)
        Public Event FinishedProcessing(ByVal pScript As String, ByVal pCurrent As Int32, ByVal pTotal As Int32)

        Private Const VERSION_STRING_LENGTH = 4 'if this is changed, it must be changed everywhere

        Private _modelName As String
        Private _connInfo As DbConnectionInfo
        Private _dbRunner As DbRunner
        Private _getVersionSql As String
        Private _setVersionSql As String

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pModelName">Name of the db model. Used to identify applicable scripts</param>
        ''' <param name="pConnString">sql connection string for target db</param>
        ''' <param name="pGetVersionSql">sql script to obtain current version from target db</param>
        ''' <param name="pSetVersionSql">sql script to set new version on target db. accepts params @OldVersion, @NewVersion, @ModelName</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal pModelName As String, ByVal pConnString As String, ByVal pGetVersionSql As String, ByVal pSetVersionSql As String)
            _modelName = pModelName
            _connInfo = New DbConnectionInfo(pConnString, eDbType.SqlServer)
            _dbRunner = New DbRunner(_connInfo)
            _getVersionSql = pGetVersionSql
            _setVersionSql = pSetVersionSql
        End Sub

        Public Function DoAllUpdates(ByVal pScriptDir As String) As FYUtil.SuccessAndDescription
            Dim upgrades As Dictionary(Of String, UpgradeScript) = GetAvailableUpgrades(_modelName, pScriptDir)
            Dim curVersion As String = GetVersion()
            While (upgrades.ContainsKey(curVersion))
                Dim upg = upgrades.Item(curVersion)
                Dim runner = New FYUtil.Database.SqlScriptRunner
                AddHandler runner.RunScriptNotify, AddressOf DoUpdates_RunScriptNotify
                Dim res As FYUtil.SuccessAndDescription = runner.RunScriptFile(_connInfo.ConnectionString, upg.Filename)
                If (res.Success) Then
                    SetVersion(upg.FromVer, upg.ToVer)
                Else
                    Return New SuccessAndDescription(False, String.Format("Error upgrading from version {0} to {1} : {2}", upg.FromVer, upg.ToVer, res.Description))
                End If
                curVersion = GetVersion()
            End While
            Return New FYUtil.SuccessAndDescription(True, "")
        End Function

        Private Sub DoUpdates_RunScriptNotify(ByVal o As Object, ByVal e As FYUtil.Database.SqlScriptRunner.RunScriptNotifyEventArgs)
            'FYUtil.Logging.LogInformational(String.Format("Running batch {0} of {1}", e.BatchNumber, e.TotalBatches))
            'FYUtil.Logging.LogInformational(e.Messages)
        End Sub

        Private Function GetVersion() As String
            Dim stmt As New DbStatementBuilder(_getVersionSql)
            Return _dbRunner.ExecuteToScalar(Of String)(stmt)
        End Function

        Private Sub SetVersion(ByVal pOldVersion As String, ByVal pNewVersion As String)
            Dim stmt As New DbStatementBuilder(_setVersionSql)
            stmt.Parameters.AddString("@NewVersion", pNewVersion)
            stmt.Parameters.AddString("@OldVersion", pOldVersion)
            stmt.Parameters.AddString("@ModelName", _modelName)
            Dim res = _dbRunner.ExecuteNonQuery(stmt)
            If (res <> 1) Then Throw New Exception("Error updating database version")
        End Sub

        Private Class UpgradeScript
            Public Property FromVer As String
            Public Property ToVer As String
            Public Property Filename As String
        End Class

        Private Function GetAvailableUpgrades(ByVal pModelName As String, ByVal pSqlScriptPath As String) As Dictionary(Of String, UpgradeScript)
            Dim res As New Dictionary(Of String, UpgradeScript)

            Dim upgradeDir As New IO.DirectoryInfo(pSqlScriptPath)
            Dim prefix = String.Format("{0}.", pModelName)
            Dim suffix = ".Upgrade.sql"
            Dim fromToSeparator = ".to."
            Dim migScripts = upgradeDir.GetFiles(String.Format("{0}*{1}", prefix, suffix))
            Dim toVers As New List(Of String)

            For Each fil In migScripts
                Dim newObj = New UpgradeScript
                newObj.Filename = fil.FullName

                Dim fromAndTo = fil.Name.Replace(prefix, "").Replace(suffix, "")
                Dim sepPos = fromAndTo.IndexOf(fromToSeparator)
                If (sepPos = -1) Then Throw New Exception("Error in upgrade script filename")
                newObj.FromVer = fromAndTo.Substring(0, sepPos)
                sepPos += fromToSeparator.Length
                newObj.ToVer = fromAndTo.Substring(sepPos, fromAndTo.Length - sepPos)

                If ((newObj.FromVer.Length <> VERSION_STRING_LENGTH) OrElse (newObj.ToVer.Length <> VERSION_STRING_LENGTH)) Then Throw New Exception(String.Format("Invalid version strings found {0} to {1}", newObj.FromVer, newObj.ToVer))
                If (res.ContainsKey(newObj.FromVer)) Then Throw New Exception(String.Format("Duplicate upgrade script found from version {0}", newObj.FromVer))
                If (toVers.Contains(newObj.ToVer)) Then Throw New Exception(String.Format("Duplicate upgrade script found to version {0}", newObj.ToVer))
                If (newObj.FromVer >= newObj.ToVer) Then Throw New Exception("Error in version numbers found")
                toVers.Add(newObj.ToVer)
                res.Add(newObj.FromVer, newObj)
            Next

            Return res
        End Function

    End Class

End Class
