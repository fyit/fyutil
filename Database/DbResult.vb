﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Partial Public Class Database

    ''' <summary>
    ''' Wraps a DataReader object to present a single row of data to a consumer
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DbResult
        Implements IDisposable

        Protected _myReader As IDataReader

        Friend Sub New(pReader As IDataReader)
            Me._myReader = pReader
        End Sub

        Friend Function NextRow() As Boolean
            Return _myReader.Read()
        End Function

        Friend Function NextResultSet() As Boolean
            Return _myReader.NextResult()
        End Function



#Region "Value"

        ''' <summary>gets specified column as Object, null if dbnull</summary>
        Protected Function _GetValue(pColumnNumber As Int32) As Object
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetValue(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Object, null if dbnull</summary>
        Public Function GetValue(pColumnName As String) As Object
            Return _GetValue(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Object, null if dbnull</summary>
        Public Function GetValue(pColumnNameEnum As [Enum]) As Object
            Return _GetValue(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets all columns as Object, nulls if dbnull</summary>
        Public Function GetValues() As IList(Of Object)
            Dim res(_myReader.FieldCount) As Object
            _myReader.GetValues(res)
            Dim resList = res.ToList
            For Each fval In resList
                If (fval Is DBNull.Value) Then fval = Nothing
            Next
            Return resList
        End Function

        ''' <summary>gets all column names</summary>
        Public Function GetNames() As IList(Of String)
            Dim res As New List(Of String)
            For x As Int32 = 0 To _myReader.FieldCount - 1
                res.Add(_myReader.GetName(x))
            Next
            Return res
        End Function

#End Region


#Region "String"

        ''' <summary>gets specified column as String, empty string if dbnull</summary>
        Protected Function _GetString(pColumnNumber As Int32) As String
            If (_myReader.IsDBNull(pColumnNumber)) Then Return "" Else Return _myReader.GetString(pColumnNumber)
        End Function
        ''' <summary>gets specified column as String, empty string if dbnull</summary>
        Public Function GetString(pColumnName As String) As String
            Return _GetString(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as String, empty string if dbnull</summary>
        Public Function GetString(pColumnNameEnum As [Enum]) As String
            Return _GetString(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Byte"

        ''' <summary>gets specified column as Byte, 0 if dbnull</summary>
        Protected Function _GetByte(pColumnNumber As Int32) As Byte
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0 Else Return _myReader.GetByte(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Byte, 0 if dbnull</summary>
        Public Function GetByte(pColumnName As String) As Byte
            Return _GetByte(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Byte, 0 if dbnull</summary>
        Public Function GetByte(pColumnNameEnum As [Enum]) As Byte
            Return _GetByte(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Byte, pNullValue if dbnull</summary>
        Protected Function _GetByte(pColumnNumber As Int32, pNullValue As Byte) As Byte
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetByte(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Byte, pNullValue if dbnull</summary>
        Public Function GetByte(pColumnName As String, pNullValue As Byte) As Byte
            Return _GetByte(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Byte, pNullValue if dbnull</summary>
        Public Function GetByte(pColumnNameEnum As [Enum], pNullValue As Byte) As Byte
            Return _GetByte(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Byte</summary>
        Protected Function _GetByteNullable(pColumnNumber As Int32) As Byte?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetByte(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Byte</summary>
        Public Function GetByteNullable(pColumnName As String) As Byte?
            Return _GetByteNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Byte</summary>
        Public Function GetByteNullable(pColumnNameEnum As [Enum]) As Byte?
            Return _GetByteNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Int16"

        ''' <summary>gets specified column as Int16, 0 if dbnull</summary>
        Protected Function _GetInt16(pColumnNumber As Int32) As Int16
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0 Else Return _myReader.GetInt16(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Int16, 0 if dbnull</summary>
        Public Function GetInt16(pColumnName As String) As Int16
            Return _GetInt16(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Int16, 0 if dbnull</summary>
        Public Function GetInt16(pColumnNameEnum As [Enum]) As Int16
            Return _GetInt16(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Int16, pNullValue if dbnull</summary>
        Protected Function _GetInt16(pColumnNumber As Int32, pNullValue As Int16) As Int16
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetInt16(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Int16, pNullValue if dbnull</summary>
        Public Function GetInt16(pColumnName As String, pNullValue As Int16) As Int16
            Return _GetInt16(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Int16, pNullValue if dbnull</summary>
        Public Function GetInt16(pColumnNameEnum As [Enum], pNullValue As Int16) As Int16
            Return _GetInt16(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Int16</summary>
        Protected Function _GetInt16Nullable(pColumnNumber As Int32) As Int16?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetInt16(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Int16</summary>
        Public Function GetInt16Nullable(pColumnName As String) As Int16?
            Return _GetInt16Nullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Int16</summary>
        Public Function GetInt16Nullable(pColumnNameEnum As [Enum]) As Int16?
            Return _GetInt16Nullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Int32"

        ''' <summary>gets specified column as Int32, 0 if dbnull</summary>
        Protected Function _GetInt32(pColumnNumber As Int32) As Int32
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0 Else Return _myReader.GetInt32(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Int32, 0 if dbnull</summary>
        Public Function GetInt32(pColumnName As String) As Int32
            Return _GetInt32(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Int32, 0 if dbnull</summary>
        Public Function GetInt32(pColumnNameEnum As [Enum]) As Int32
            Return _GetInt32(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Int32, pNullValue if dbnull</summary>
        Protected Function _GetInt32(pColumnNumber As Int32, pNullValue As Int32) As Int32
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetInt32(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Int32, pNullValue if dbnull</summary>
        Public Function GetInt32(pColumnName As String, pNullValue As Int32) As Int32
            Return _GetInt32(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Int32, pNullValue if dbnull</summary>
        Public Function GetInt32(pColumnNameEnum As [Enum], pNullValue As Int32) As Int32
            Return _GetInt32(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Int32</summary>
        Protected Function _GetInt32Nullable(pColumnNumber As Int32) As Int32?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetInt32(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Int32</summary>
        Public Function GetInt32Nullable(pColumnName As String) As Int32?
            Return _GetInt32Nullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Int32</summary>
        Public Function GetInt32Nullable(pColumnNameEnum As [Enum]) As Int32?
            Return _GetInt32Nullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Int64"

        ''' <summary>gets specified column as Int64, 0 if dbnull</summary>
        Protected Function _GetInt64(pColumnNumber As Int32) As Int64
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0 Else Return _myReader.GetInt64(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Int64, 0 if dbnull</summary>
        Public Function GetInt64(pColumnName As String) As Int64
            Return _GetInt64(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Int64, 0 if dbnull</summary>
        Public Function GetInt64(pColumnNameEnum As [Enum]) As Int64
            Return _GetInt64(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Int64, pNullValue if dbnull</summary>
        Protected Function _GetInt64(pColumnNumber As Int32, pNullValue As Int64) As Int64
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetInt64(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Int64, pNullValue if dbnull</summary>
        Public Function GetInt64(pColumnName As String, pNullValue As Int64) As Int64
            Return _GetInt64(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Int64, pNullValue if dbnull</summary>
        Public Function GetInt64(pColumnNameEnum As [Enum], pNullValue As Int64) As Int64
            Return _GetInt64(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Int64</summary>
        Protected Function _GetInt64Nullable(pColumnNumber As Int32) As Int64?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetInt64(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Int64</summary>
        Public Function GetInt64Nullable(pColumnName As String) As Int64?
            Return _GetInt64Nullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Int64</summary>
        Public Function GetInt64Nullable(pColumnNameEnum As [Enum]) As Int64?
            Return _GetInt64Nullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Single"

        ''' <summary>gets specified column as Single, 0 if dbnull</summary>
        Protected Function _GetSingle(pColumnNumber As Int32) As Single
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0.0 Else Return _myReader.GetFloat(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Single, 0 if dbnull</summary>
        Public Function GetSingle(pColumnName As String) As Single
            Return _GetSingle(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Single, 0 if dbnull</summary>
        Public Function GetSingle(pColumnNameEnum As [Enum]) As Single
            Return _GetSingle(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Single, pNullValue if dbnull</summary>
        Protected Function _GetSingle(pColumnNumber As Int32, pNullValue As Single) As Single
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetFloat(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Single, pNullValue if dbnull</summary>
        Public Function GetSingle(pColumnName As String, pNullValue As Single) As Single
            Return _GetSingle(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Single, pNullValue if dbnull</summary>
        Public Function GetSingle(pColumnNameEnum As [Enum], pNullValue As Single) As Single
            Return _GetSingle(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Single</summary>
        Protected Function _GetSingleNullable(pColumnNumber As Int32) As Single?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetFloat(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Single</summary>
        Public Function GetSingleNullable(pColumnName As String) As Single?
            Return _GetSingleNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Single</summary>
        Public Function GetSingleNullable(pColumnNameEnum As [Enum]) As Single?
            Return _GetSingleNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Double"

        ''' <summary>gets specified column as Double, 0 if dbnull</summary>
        Protected Function _GetDouble(pColumnNumber As Int32) As Double
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0.0 Else Return _myReader.GetDouble(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Double, 0 if dbnull</summary>
        Public Function GetDouble(pColumnName As String) As Double
            Return _GetDouble(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Double, 0 if dbnull</summary>
        Public Function GetDouble(pColumnNameEnum As [Enum]) As Double
            Return _GetDouble(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Double, pNullValue if dbnull</summary>
        Protected Function _GetDouble(pColumnNumber As Int32, pNullValue As Double) As Double
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetDouble(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Double, pNullValue if dbnull</summary>
        Public Function GetDouble(pColumnName As String, pNullValue As Double) As Double
            Return _GetDouble(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Double, pNullValue if dbnull</summary>
        Public Function GetDouble(pColumnNameEnum As [Enum], pNullValue As Double) As Double
            Return _GetDouble(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Double</summary>
        Protected Function _GetDoubleNullable(pColumnNumber As Int32) As Double?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetDouble(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Double</summary>
        Public Function GetDoubleNullable(pColumnName As String) As Double?
            Return _GetDoubleNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Double</summary>
        Public Function GetDoubleNullable(pColumnNameEnum As [Enum]) As Double?
            Return _GetDoubleNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Decimal"

        ''' <summary>gets specified column as Decimal, 0 if dbnull</summary>
        Protected Function _GetDecimal(pColumnNumber As Int32) As Decimal
            If (_myReader.IsDBNull(pColumnNumber)) Then Return 0 Else Return _myReader.GetDecimal(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Decimal, 0 if dbnull</summary>
        Public Function GetDecimal(pColumnName As String) As Decimal
            Return _GetDecimal(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Decimal, 0 if dbnull</summary>
        Public Function GetDecimal(pColumnNameEnum As [Enum]) As Decimal
            Return _GetDecimal(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Decimal, pNullValue if dbnull</summary>
        Protected Function _GetDecimal(pColumnNumber As Int32, pNullValue As Decimal) As Decimal
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetDecimal(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Decimal, pNullValue if dbnull</summary>
        Public Function GetDecimal(pColumnName As String, pNullValue As Decimal) As Decimal
            Return _GetDecimal(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Decimal, pNullValue if dbnull</summary>
        Public Function GetDecimal(pColumnNameEnum As [Enum], pNullValue As Decimal) As Decimal
            Return _GetDecimal(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Decimal</summary>
        Protected Function _GetDecimalNullable(pColumnNumber As Int32) As Decimal?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetDecimal(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Decimal</summary>
        Public Function GetDecimalNullable(pColumnName As String) As Decimal?
            Return _GetDecimalNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Decimal</summary>
        Public Function GetDecimalNullable(pColumnNameEnum As [Enum]) As Decimal?
            Return _GetDecimalNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "DateTime"

        ''' <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        Protected Function _GetDateTime(pColumnNumber As Int32) As DateTime
            If (_myReader.IsDBNull(pColumnNumber)) Then Return DateTime.MinValue Else Return _myReader.GetDateTime(pColumnNumber)
        End Function
        ''' <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        Public Function GetDateTime(pColumnName As String) As DateTime
            Return _GetDateTime(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        Public Function GetDateTime(pColumnNameEnum As [Enum]) As DateTime
            Return _GetDateTime(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as DateTime, pNullValue if dbnull</summary>
        Protected Function _GetDateTime(pColumnNumber As Int32, pNullValue As DateTime) As DateTime
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetDateTime(pColumnNumber)
        End Function
        ''' <summary>gets specified column as DateTime, pNullValue if dbnull</summary>
        Public Function GetDateTime(pColumnName As String, pNullValue As DateTime) As DateTime
            Return _GetDateTime(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as DateTime, pNullValue if dbnull</summary>
        Public Function GetDateTime(pColumnNameEnum As [Enum], pNullValue As DateTime) As DateTime
            Return _GetDateTime(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable DateTime</summary>
        Protected Function _GetDateTimeNullable(pColumnNumber As Int32) As DateTime?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetDateTime(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable DateTime</summary>
        Public Function GetDateTimeNullable(pColumnName As String) As DateTime?
            Return _GetDateTimeNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable DateTime</summary>
        Public Function GetDateTimeNullable(pColumnNameEnum As [Enum]) As DateTime?
            Return _GetDateTimeNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Time"

        ''' <summary>gets specified column as DateTime, MinValue if dbnull</summary>
        Protected Function _GetTimeSpan(pColumnNumber As Int32) As TimeSpan
            If (_myReader.IsDBNull(pColumnNumber)) Then Return TimeSpan.MinValue Else Return DirectCast(_myReader.GetValue(pColumnNumber), TimeSpan)
        End Function
        ''' <summary>gets specified column as TimeSpan, MinValue if dbnull</summary>
        Public Function GetTimeSpan(pColumnName As String) As TimeSpan
            Return _GetTimeSpan(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as TimeSpan, MinValue if dbnull</summary>
        Public Function GetTimeSpan(pColumnNameEnum As [Enum]) As TimeSpan
            Return _GetTimeSpan(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as TimeSpan, pNullValue if dbnull</summary>
        Protected Function _GetTimeSpan(pColumnNumber As Int32, pNullValue As TimeSpan) As TimeSpan
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return DirectCast(_myReader.GetValue(pColumnNumber), TimeSpan)
        End Function
        ''' <summary>gets specified column as TimeSpan, pNullValue if dbnull</summary>
        Public Function GetTimeSpan(pColumnName As String, pNullValue As TimeSpan) As TimeSpan
            Return _GetTimeSpan(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as TimeSpan, pNullValue if dbnull</summary>
        Public Function GetTimeSpan(pColumnNameEnum As [Enum], pNullValue As TimeSpan) As TimeSpan
            Return _GetTimeSpan(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable TimeSpan</summary>
        Protected Function _GetTimeSpanNullable(pColumnNumber As Int32) As TimeSpan?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return DirectCast(_myReader.GetValue(pColumnNumber), TimeSpan)
        End Function
        ''' <summary>gets specified column as nullable TimeSpan</summary>
        Public Function GetTimeSpanNullable(pColumnName As String) As TimeSpan?
            Return _GetTimeSpanNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable TimeSpan</summary>
        Public Function GetTimeSpanNullable(pColumnNameEnum As [Enum]) As TimeSpan?
            Return _GetTimeSpanNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Boolean"

        ''' <summary>gets specified column as Boolean, False if dbnull</summary>
        Protected Function _GetBoolean(pColumnNumber As Int32) As Boolean
            If (_myReader.IsDBNull(pColumnNumber)) Then Return False Else Return _myReader.GetBoolean(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Boolean, False if dbnull</summary>
        Public Function GetBoolean(pColumnName As String) As Boolean
            Return _GetBoolean(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Boolean, False if dbnull</summary>
        Public Function GetBoolean(pColumnNameEnum As [Enum]) As Boolean
            Return _GetBoolean(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as nullable Boolean</summary>
        Protected Function _GetBooleanNullable(pColumnNumber As Int32) As Boolean?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetBoolean(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Boolean</summary>
        Public Function GetBooleanNullable(pColumnName As String) As Boolean?
            Return _GetBooleanNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Boolean</summary>
        Public Function GetBooleanNullable(pColumnNameEnum As [Enum]) As Boolean?
            Return _GetBooleanNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "Guid"

        ''' <summary>gets specified column as Guid, null if dbnull</summary>
        Protected Function _GetGuid(pColumnNumber As Int32) As Guid
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetGuid(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Guid, null if dbnull</summary>
        Public Function GetGuid(pColumnName As String) As Guid
            Return _GetGuid(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Guid, null if dbnull</summary>
        Public Function GetGuid(pColumnNameEnum As [Enum]) As Guid
            Return _GetGuid(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function


        ''' <summary>gets specified column as Guid, pNullValue if dbnull</summary>
        Protected Function _GetGuid(pColumnNumber As Int32, pNullValue As Guid) As Guid
            If (_myReader.IsDBNull(pColumnNumber)) Then Return pNullValue Else Return _myReader.GetGuid(pColumnNumber)
        End Function
        ''' <summary>gets specified column as Guid, pNullValue if dbnull</summary>
        Public Function GetGuid(pColumnName As String, pNullValue As Guid) As Guid
            Return _GetGuid(_myReader.GetOrdinal(pColumnName), pNullValue)
        End Function
        ''' <summary>gets specified column as Guid, pNullValue if dbnull</summary>
        Public Function GetGuid(pColumnNameEnum As [Enum], pNullValue As Guid) As Guid
            Return _GetGuid(_myReader.GetOrdinal(pColumnNameEnum.ToString), pNullValue)
        End Function


        ''' <summary>gets specified column as nullable Guid</summary>
        Protected Function _GetGuidNullable(pColumnNumber As Int32) As Guid?
            If (_myReader.IsDBNull(pColumnNumber)) Then Return Nothing Else Return _myReader.GetGuid(pColumnNumber)
        End Function
        ''' <summary>gets specified column as nullable Guid</summary>
        Public Function GetGuidNullable(pColumnName As String) As Guid?
            Return _GetGuidNullable(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as nullable Guid</summary>
        Public Function GetGuidNullable(pColumnNameEnum As [Enum]) As Guid?
            Return _GetGuidNullable(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "ByteArray"

        ''' <summary>gets specified column as Byte array, null if dbnull</summary>
        Protected Function _GetByteArray(pColumnNumber As Int32) As Byte()
            If (_myReader.IsDBNull(pColumnNumber)) Then
                Return Nothing
            Else
                Dim tempDat As Byte() = DirectCast(_myReader.Item(pColumnNumber), Byte())
                Dim result(tempDat.Length - 1) As Byte
                tempDat.CopyTo(result, 0)
                Return result

                'this code was for serialized.. wasn't completely working
                'Dim bufSize = 5
                'Dim buf(bufSize - 1) As Byte
                'Using strm As New IO.MemoryStream()
                '    Dim curOffset = 0
                '    Dim bytesRead = _myReader.GetBytes(pColumnNumber, curOffset, buf, 0, bufSize)

                '    Do While bytesRead = bufSize
                '        strm.Write(buf, 0, Convert.ToInt32(bytesRead)) 'NOTE: bufSize cannot exceed size of Int32

                '        curOffset += bufSize
                '        bytesRead = _myReader.GetBytes(pColumnNumber, curOffset, buf, 0, bufSize)
                '    Loop
                '    strm.Write(buf, 0, Convert.ToInt32(bytesRead))

                '    If (strm.Length > Int32.MaxValue) Then Throw New Exception("GetByteArray: Source data exceeds maximum size of byte array")

                '    Dim result(Convert.ToInt32(strm.Length - 1)) As Byte
                '    strm.GetBuffer().CopyTo(result, 0)
                '    Return result
                'End Using
            End If
        End Function
        ''' <summary>gets specified column as Byte array, null if dbnull</summary>
        Public Function GetByteArray(pColumnName As String) As Byte()
            Return _GetByteArray(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified column as Byte array, null if dbnull</summary>
        Public Function GetByteArray(pColumnNameEnum As [Enum]) As Byte()
            Return _GetByteArray(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region


#Region "RowVersion"

        ''' <summary>gets specified rowversion column as UInt64</summary>
        Protected Function _GetRowVersion(pColumnNumber As Int32) As UInt64
            Dim tempDat As Byte() = DirectCast(_myReader.Item(pColumnNumber), Byte())
            Return BitConverter.ToUInt64(tempDat, 0)
        End Function
        ''' <summary>gets specified rowversion column as UInt64</summary>
        Public Function GetRowVersion(pColumnName As String) As UInt64
            Return _GetRowVersion(_myReader.GetOrdinal(pColumnName))
        End Function
        ''' <summary>gets specified rowversion column as UInt64</summary>
        Public Function GetRowVersion(pColumnNameEnum As [Enum]) As UInt64
            Return _GetRowVersion(_myReader.GetOrdinal(pColumnNameEnum.ToString))
        End Function

#End Region



#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If (Me._myReader IsNot Nothing) Then
                        Me._myReader.Dispose()
                    End If
                End If
            End If
            Me.disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Friend Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Class
