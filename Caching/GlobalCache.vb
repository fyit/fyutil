﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace Caching

    Public Delegate Function PartitioningFunctionDelegate() As String

    Public Class GlobalCache
        Private Shared _cache As New Runtime.Caching.MemoryCache(Guid.NewGuid.ToString)

        Public Shared Sub Add(pPartition As String, pBaseKey As String, pKey As String, pValue As Object, pExpiration As TimeSpan)
            SyncLock _cache
                Dim fullKey = GenerateFullKey(pPartition, pBaseKey, pKey)
                _cache.Add(fullKey, pValue, DateTime.UtcNow.Add(pExpiration))
            End SyncLock
        End Sub

        Public Shared Sub Add(pPartition As String, pBaseKey As String, pKey As String, pValue As Object, pExpirationPolicy As Runtime.Caching.CacheItemPolicy)
            SyncLock _cache
                Dim fullKey = GenerateFullKey(pPartition, pBaseKey, pKey)
                _cache.Add(fullKey, pValue, pExpirationPolicy)
            End SyncLock
        End Sub

        Public Shared Sub Remove(pPartition As String, pBaseKey As String, pKey As String)
            SyncLock _cache
                Dim fullKey = GenerateFullKey(pPartition, pBaseKey, pKey)
                If _cache.Contains(fullKey) Then
                    _cache.Remove(fullKey)
                End If
            End SyncLock
        End Sub

        Public Shared Function Exists(pPartition As String, pBaseKey As String, pKey As String) As Boolean
            SyncLock _cache
                Return _cache.Item(GenerateFullKey(pPartition, pBaseKey, pKey)) IsNot Nothing
            End SyncLock
        End Function

        Public Shared Function Item(pPartition As String, pBaseKey As String, pKey As String) As Object
            SyncLock _cache
                Return _cache.Item(GenerateFullKey(pPartition, pBaseKey, pKey))
            End SyncLock
        End Function

        Public Shared Sub Clear(pPartition As String, pBaseKey As String)
            SyncLock _cache
                For Each tmpKey As String In FindSimilarFullKeys(pPartition, pBaseKey)
                    _cache.Remove(tmpKey)
                Next
            End SyncLock
        End Sub

        Public Shared Function GetUniqueBaseKey() As String
            SyncLock _cache
                Return Guid.NewGuid.ToString
            End SyncLock
        End Function

        Public Shared Function AllItems() As IDictionary(Of String, Object)
            Return _cache.ToDictionary(Function(i) i.Key, Function(i) i.Value)
        End Function

        Public Shared Function AllItemCount() As Int32
            Return _cache.Count
        End Function


        Private Shared Function GenerateFullKey(pPartition As String, pBaseKey As String, pKey As String) As String
            Return String.Format("{0}.{1}.{2}", {pPartition, pBaseKey, pKey})
        End Function

        Private Shared Function GeneratePartialKey(pPartition As String, pBaseKey As String) As String
            Return String.Format("{0}.{1}.", {pPartition, pBaseKey})
        End Function

        Private Shared Function FindSimilarFullKeys(pPartition As String, pBaseKey As String) As IEnumerable(Of String)
            Dim allkeys = _cache.Select(Function(i) i.Key).ToList
            Return From k In allkeys Where k.StartsWith(GeneratePartialKey(pPartition, pBaseKey))
        End Function

    End Class

End Namespace
