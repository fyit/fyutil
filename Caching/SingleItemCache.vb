﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace Caching

    Public Class SingleItemCache(Of TPayload)
        Implements IDisposable

        Private Const SINGLE_ITEM_KEY As String = "SingleItemCacheKey"
        Private _myName As String
        Private _myBaseKey As String
        Private _defaultTimeout As TimeSpan
        Private _partitioningFunction As PartitioningFunctionDelegate

        Public Sub New(pName As String, pPartitioningFunction As PartitioningFunctionDelegate, pDefaultTimeout As TimeSpan)
            If (pPartitioningFunction Is Nothing) Then Throw New Exception("Partitioning function must be initialized!")

            _myName = pName
            _myBaseKey = String.Format("{0}.{1}", _myName, GlobalCache.GetUniqueBaseKey)
            _partitioningFunction = pPartitioningFunction
            _defaultTimeout = pDefaultTimeout
        End Sub

        Public Sub New(pName As String, pTimeout As TimeSpan)
            Me.New(pName, Function() "", pTimeout)
        End Sub

        Public Function Exists() As Boolean
            Return GlobalCache.Item(_partitioningFunction.Invoke(), _myBaseKey, SINGLE_ITEM_KEY) IsNot Nothing
        End Function

        Public Overridable Function GetValue() As TPayload
            Return DirectCast(GlobalCache.Item(_partitioningFunction.Invoke(), _myBaseKey, SINGLE_ITEM_KEY), TPayload)
        End Function

        Public Sub SetValue(pValue As TPayload)
            GlobalCache.Add(_partitioningFunction.Invoke(), _myBaseKey, SINGLE_ITEM_KEY, pValue, _defaultTimeout)
        End Sub

        Public Sub SetValue(pValue As TPayload, pTimeout As TimeSpan)
            GlobalCache.Add(_partitioningFunction.Invoke(), _myBaseKey, SINGLE_ITEM_KEY, pValue, pTimeout)
        End Sub

        Public Sub SetValue(pValue As TPayload, pExpirationPolicy As Runtime.Caching.CacheItemPolicy)
            GlobalCache.Add(_partitioningFunction.Invoke(), _myBaseKey, SINGLE_ITEM_KEY, pValue, pExpirationPolicy)
        End Sub

        Public Sub Clear()
            GlobalCache.Clear(_partitioningFunction.Invoke(), _myBaseKey)
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    Clear()
                End If
            End If
            Me.disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
