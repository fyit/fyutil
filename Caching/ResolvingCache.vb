﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace Caching

    Public Class ResolvingCache(Of TKey, TPayload)
        Inherits FYUtil.Caching.MultiItemCache(Of TKey, TPayload)

        Private _resolver As Func(Of TKey, TPayload) = Nothing
        Private _multiResolver As Func(Of IEnumerable(Of TKey), IDictionary(Of TKey, TPayload)) = Nothing

        Public Sub New(pName As String, pPayloadResolver As Func(Of TKey, TPayload), pTimeout As TimeSpan)
            Me.New(pName, pPayloadResolver, Nothing, Function() "", pTimeout)
        End Sub

        Public Sub New(pName As String, pPayloadResolver As Func(Of TKey, TPayload), pPartitioningFunction As PartitioningFunctionDelegate, pTimeout As TimeSpan)
            Me.New(pName, pPayloadResolver, Nothing, pPartitioningFunction, pTimeout)
        End Sub

        Public Sub New(pName As String, pMultiPayloadResolver As Func(Of IEnumerable(Of TKey), IDictionary(Of TKey, TPayload)), pTimeout As TimeSpan)
            Me.New(pName, Nothing, pMultiPayloadResolver, Function() "", pTimeout)
        End Sub

        Public Sub New(pName As String, pMultiPayloadResolver As Func(Of IEnumerable(Of TKey), IDictionary(Of TKey, TPayload)), pPartitioningFunction As PartitioningFunctionDelegate, pTimeout As TimeSpan)
            Me.New(pName, Nothing, pMultiPayloadResolver, pPartitioningFunction, pTimeout)
        End Sub

        Private Sub New(pName As String, pPayloadResolver As Func(Of TKey, TPayload), pMultiPayloadResolver As Func(Of IEnumerable(Of TKey), IDictionary(Of TKey, TPayload)), pPartitioningFunction As PartitioningFunctionDelegate, pTimeout As TimeSpan)
            MyBase.New(pName, pPartitioningFunction, pTimeout)
            _resolver = pPayloadResolver
            _multiResolver = pMultiPayloadResolver
        End Sub

        Public Overrides Function Item(pId As TKey) As TPayload
            If Not Exists(pId) Then
                If (_resolver IsNot Nothing) Then
                    Dim curVal As TPayload = _resolver.Invoke(pId)
                    If (curVal IsNot Nothing) Then MyBase.Add(pId, curVal)
                Else
                    Call Items({pId})
                End If
            End If

            If (MyBase.Exists(pId)) Then Return MyBase.Item(pId) Else Return Nothing
        End Function

        Public Function Items(pKeys As IEnumerable(Of TKey)) As IDictionary(Of TKey, TPayload)
            If ((pKeys Is Nothing) OrElse (pKeys.Count = 0)) Then Return New Dictionary(Of TKey, TPayload)
            pKeys = pKeys.Distinct().ToList

            Dim toResolve As New List(Of TKey)
            Dim results As New Dictionary(Of TKey, TPayload)
            For Each key As TKey In pKeys
                If (MyBase.Exists(key)) Then results.Add(key, MyBase.Item(key)) Else toResolve.Add(key)
            Next

            If (toResolve.Count > 0) Then
                If (_multiResolver IsNot Nothing) Then
                    Dim resolved As IDictionary(Of TKey, TPayload) = _multiResolver.Invoke(toResolve)
                    For Each key As TKey In toResolve
                        Dim curVal As TPayload = Nothing
                        If (resolved.ContainsKey(key)) Then curVal = resolved.Item(key)
                        If (curVal IsNot Nothing) Then MyBase.Add(key, curVal)
                        results.Add(key, curVal)
                    Next
                Else
                    'need to look them up one by one
                    For Each key As TKey In toResolve
                        Dim curVal As TPayload = _resolver.Invoke(key)
                        If (curVal IsNot Nothing) Then MyBase.Add(key, curVal)
                        results.Add(key, curVal)
                    Next
                End If
            End If

            Return results
        End Function

    End Class

End Namespace