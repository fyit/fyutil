@echo off
set config=Debug
set destdir=Build
echo Ready to build %config% ...
Tools\AssemblyInfoUtil.exe -file:SharedAssemblyInfo.vb

set msbuild_exe="C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe"
if exist %msbuild_exe% goto FoundMsbuild
set msbuild_exe="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe"
if exist %msbuild_exe% goto FoundMsbuild
echo msbuild not found
goto Error
:FoundMsbuild

%msbuild_exe% /t:Build /p:Configuration=%config% FYUtil.sln
if ERRORLEVEL 1 goto Error

md %destdir%
copy Common\bin\%config%\FYUtil.Common.* %destdir%
copy Caching\bin\%config%\FYUtil.Caching.* %destdir%
copy FlatFiles\bin\%config%\FYUtil.FlatFiles.* %destdir%
copy Database\bin\%config%\FYUtil.Database.* %destdir%
copy Web\bin\%config%\FYUtil.Web.* %destdir%
copy WinForms\bin\%config%\FYUtil.WinForms.* %destdir%
copy Pdf\bin\%config%\FYUtil.Pdf.* %destdir%
copy Zip\bin\%config%\FYUtil.Zip.* %destdir%
copy Components\*.* %destdir%

echo Done.
pause

goto End
:Error
echo Error!
pause
:End
