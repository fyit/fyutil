﻿Namespace WinForms.UserControls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EnhancedComboBox
        Inherits System.Windows.Forms.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.BaseComboBox = New System.Windows.Forms.ComboBox()
            Me.SuspendLayout()
            '
            'BaseComboBox
            '
            Me.BaseComboBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.BaseComboBox.FormattingEnabled = True
            Me.BaseComboBox.Location = New System.Drawing.Point(0, 0)
            Me.BaseComboBox.Name = "BaseComboBox"
            Me.BaseComboBox.Size = New System.Drawing.Size(120, 21)
            Me.BaseComboBox.TabIndex = 0
            '
            'EnhancedComboBox
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.BaseComboBox)
            Me.Name = "EnhancedComboBox"
            Me.Size = New System.Drawing.Size(120, 22)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents BaseComboBox As System.Windows.Forms.ComboBox

    End Class

End Namespace