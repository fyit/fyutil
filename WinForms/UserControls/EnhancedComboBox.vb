﻿Namespace WinForms.UserControls

    Public Class EnhancedComboBox

        Public Shadows Event TextChanged(sender As Object, e As System.EventArgs)
        Public Shadows Event SelectedIndexChanged(sender As Object, e As System.EventArgs)

        Private _pendingFindData As String = Nothing

        Public Property ClearInvalidText As Boolean = False
        Public Property BeepWhenInvalid As Boolean = True
        Public Property SearchKeyValue As Boolean = False

        Private _dataSource As List(Of KeyValuePair(Of String, String))
        Public Property DataSource As List(Of KeyValuePair(Of String, String))
            Get
                Return _dataSource
            End Get
            Set(value As List(Of KeyValuePair(Of String, String)))
                _dataSource = value
                Me.BaseComboBox.DataSource = _dataSource
            End Set
        End Property

        Public Property SelectedKVP() As KeyValuePair(Of String, String)?
            Get
                If Me.BaseComboBox.SelectedItem IsNot Nothing Then Return DirectCast(Me.BaseComboBox.SelectedItem, KeyValuePair(Of String, String)) Else Return Nothing
            End Get
            Set(value As KeyValuePair(Of String, String)?)
                Me.BaseComboBox.SelectedItem = value
            End Set
        End Property

        Public Property SelectedKey As String
            Get
                If Me.IsValid Then Return Me.SelectedKVP.Value.Key Else Return Nothing
            End Get
            Set(value As String)
                Me.BaseComboBox.SelectedValue = value
                If Me.BaseComboBox.SelectedItem Is Nothing Then Me.BaseComboBox.Text = value
            End Set
        End Property

        Public ReadOnly Property IsValidOrEmpty() As Boolean
            Get
                If Me.BaseComboBox.Items.Count = 0 Then
                    Return True
                ElseIf Me.BaseComboBox.SelectedItem Is Nothing Then
                    Return (String.IsNullOrWhiteSpace(Me.BaseComboBox.Text))
                End If
                Return IsValid()
            End Get
        End Property

        Public ReadOnly Property IsValid() As Boolean
            Get
                If Me.BaseComboBox.Items.Count > 0 AndAlso Me.BaseComboBox.SelectedItem IsNot Nothing AndAlso
                        (Me.BaseComboBox.Text = DirectCast(Me.BaseComboBox.SelectedItem, KeyValuePair(Of String, String)).Value) Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public Sub New()
            Try
                ' This call is required by the designer.
                InitializeComponent()

                ' Add any initialization after the InitializeComponent() call.
                Me.BaseComboBox.ValueMember = "Key"
                Me.BaseComboBox.DisplayMember = "Value"
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try

        End Sub

        Private _resetCBO As Boolean = False

        Private Sub BaseComboBox_Click(sender As Object, e As System.EventArgs) Handles BaseComboBox.Click
            Try
                Me.BaseComboBox.Select(0, Me.BaseComboBox.Text.Length)
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles BaseComboBox.SelectionChangeCommitted
            Try
                Application.DoEvents()
                SendKeys.Send("{HOME}")
                _resetCBO = True
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_DropDown(sender As Object, e As System.EventArgs) Handles BaseComboBox.DropDown
            Try
                Application.DoEvents()
                If Me.BaseComboBox.Items.Count <= 1 OrElse _resetCBO Then
                    Dim existingItem = Me.BaseComboBox.SelectedItem
                    Me.BaseComboBox.DataSource = _dataSource
                    Me.BaseComboBox.Refresh()
                    If existingItem IsNot Nothing Then Me.BaseComboBox.SelectedItem = existingItem
                End If
                _resetCBO = False
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles BaseComboBox.KeyDown
            Try
                If {Windows.Forms.Keys.Back, Windows.Forms.Keys.Delete}.Contains(e.KeyCode) AndAlso
                    (String.IsNullOrWhiteSpace(Me.BaseComboBox.Text) OrElse
                         Me.BaseComboBox.SelectionLength = Me.BaseComboBox.Text.Length OrElse
                         Me.BaseComboBox.Text.Length = 1
                         ) Then
                    Me.BaseComboBox.DataSource = _dataSource
                    Me.BaseComboBox.Text = ""
                End If
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles BaseComboBox.KeyUp
            Try
                _pendingFindData = Nothing
                SyncLock Me.BaseComboBox
                    'Debug.Print(e.KeyCode.ToString & " -- [" & Me.BaseComboBox.Text & "]")

                    If e.KeyValue = Windows.Forms.Keys.Return Then
                        If Me.BaseComboBox.SelectedItem IsNot Nothing Then
                            Me.BaseComboBox.Text = Me.SelectedKVP.Value.Value
                        End If
                    ElseIf {Windows.Forms.Keys.Back, Windows.Forms.Keys.Escape, Windows.Forms.Keys.Space, Windows.Forms.Keys.Delete}.Contains(e.KeyCode) OrElse FYUtil.Utilities.CHARSET_ALPHANUMERIC.Contains(Chr(e.KeyValue)) Then
                        'All ALphanumeric, and the backspace, delete, escape, space, delete 
                        Dim origText = Me.BaseComboBox.Text
                        If String.IsNullOrWhiteSpace(origText) OrElse e.KeyCode = Windows.Forms.Keys.Escape Then
                            Me.BaseComboBox.DataSource = _dataSource
                        Else
                            Dim items = FilteredList(origText)
                            If items.Count = 0 Then items = _dataSource
                            Me.BaseComboBox.DataSource = items
                        End If
                        If e.KeyCode = Windows.Forms.Keys.Escape Then
                            Me.BaseComboBox.DroppedDown = False
                        Else
                            If Not Me.BaseComboBox.DroppedDown Then Me.BaseComboBox.DroppedDown = True
                        End If
                        Me.BaseComboBox.Text = origText
                        Me.BaseComboBox.Select(Me.BaseComboBox.Text.Length, 0)
                    End If
                End SyncLock
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_GotFocus(sender As Object, e As System.EventArgs) Handles BaseComboBox.GotFocus
            Try
                If Me.BaseComboBox.Items.Count = 0 Then Me.BaseComboBox.DataSource = _dataSource
                Me.BaseComboBox.Select(0, Me.BaseComboBox.Text.Length)
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_LostFocus(sender As Object, e As System.EventArgs) Handles BaseComboBox.LostFocus
            Try
                If Not IsValidOrEmpty Then
                    If BeepWhenInvalid Then Beep()
                    If ClearInvalidText Then Me.ClearSelection()
                End If
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Sub BaseComboBox_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BaseComboBox.MouseUp
            Try
                'If this is the first time this was opened and there's pending find data that didn't match up to a single item - search the items and open the drop down
                If _pendingFindData IsNot Nothing Then
                    Me.BaseComboBox.Text = _pendingFindData

                    BaseComboBox_KeyUp(sender, New Windows.Forms.KeyEventArgs(Keys.Space))
                    _pendingFindData = Nothing
                End If
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Public Sub AddMultiColumnDataSource(pItems As IEnumerable(Of String()), Optional pColWidths() As Int32 = Nothing)
            Try
                Dim tmpDataSource = New List(Of KeyValuePair(Of String, String))
                Dim maxLen = 0
                For Each itemArray In pItems
                    Dim key = itemArray(0)
                    Dim value = ""
                    If pColWidths IsNot Nothing AndAlso pColWidths.Length >= 0 Then
                        value = itemArray(1).PadRight(pColWidths(0), " "c)
                    Else
                        value = itemArray(1)
                    End If
                    For i = 2 To itemArray.Length - 1
                        value &= " | "
                        If pColWidths IsNot Nothing AndAlso pColWidths.Length >= i Then
                            value &= itemArray(i).PadRight(pColWidths(i - 1), " "c)
                        Else
                            value &= itemArray(i)
                        End If
                    Next
                    tmpDataSource.Add(New KeyValuePair(Of String, String)(key, value))
                    If Len(key) + Len(value) > maxLen Then maxLen = Len(key) + Len(value)
                Next

                If maxLen > 0 Then
                    With Me.BaseComboBox
                        .DropDownWidth = maxLen * 8
                        .Font = New System.Drawing.Font("Lucida Console", Me.BaseComboBox.Font.Size)
                    End With
                End If

                Me.DataSource = tmpDataSource
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Public Sub SetByValue(pValue As String, Optional pSelectFirstMatch As Boolean = False)
            Try
                Me.BaseComboBox.Text = pValue
                Dim matches = From item In FilteredList(pValue) Order By (If(item.Value.ToUpper.StartsWith(pValue.ToUpper), 0, 1)), item.Value
                If matches.Count = 1 OrElse pSelectFirstMatch Then
                    Dim match = matches.FirstOrDefault
                    If Not String.IsNullOrWhiteSpace(match.Key) Then
                        Me.BaseComboBox.Text = match.Value
                        Me.BaseComboBox.SelectedItem = match
                    End If
                Else
                    _pendingFindData = pValue
                End If
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Public Sub ClearSelection()
            Try
                _pendingFindData = Nothing
                Me.BaseComboBox.DataSource = _dataSource
                Me.BaseComboBox.SelectedIndex = -1
                Me.BaseComboBox.Text = ""
            Catch ex As Exception
                MsgBox(ex.Message + ex.StackTrace)
            End Try
        End Sub

        Private Function FilteredList(pFilter As String, Optional pLookInKey As Boolean = False) As List(Of KeyValuePair(Of String, String))
            Return (From item In _dataSource
                            Where item.Value.ToUpper.Contains(pFilter.ToUpper) _
                            OrElse ((pLookInKey OrElse SearchKeyValue) And item.Key.ToUpper.Contains(pFilter.ToUpper))
                        ).ToList
        End Function

        Private Sub TextChangedHandler(sender As Object, e As System.EventArgs) Handles BaseComboBox.TextChanged
            RaiseEvent TextChanged(sender, e)
        End Sub

        Private Sub SelectedIndexChangedHandler(sender As Object, e As System.EventArgs) Handles BaseComboBox.SelectedIndexChanged
            RaiseEvent SelectedIndexChanged(sender, e)
        End Sub

    End Class

End Namespace
