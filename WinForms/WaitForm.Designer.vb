﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class WaitFormPrv
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.ProgressBar = New System.Windows.Forms.ProgressBar()
            Me.StatusLabel = New System.Windows.Forms.Label()
            Me.tmrRefresh = New System.Windows.Forms.Timer(Me.components)
            Me.SuspendLayout()
            '
            'ProgressBar
            '
            Me.ProgressBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ProgressBar.BackColor = System.Drawing.SystemColors.Control
            Me.ProgressBar.Location = New System.Drawing.Point(13, 13)
            Me.ProgressBar.Name = "ProgressBar"
            Me.ProgressBar.Size = New System.Drawing.Size(251, 23)
            Me.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous
            Me.ProgressBar.TabIndex = 0
            '
            'StatusLabel
            '
            Me.StatusLabel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.StatusLabel.Location = New System.Drawing.Point(13, 43)
            Me.StatusLabel.Name = "StatusLabel"
            Me.StatusLabel.Size = New System.Drawing.Size(251, 14)
            Me.StatusLabel.TabIndex = 1
            Me.StatusLabel.Text = "Please Wait..."
            '
            'tmrRefresh
            '
            Me.tmrRefresh.Interval = 500
            '
            'WaitFormPrv
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.BackColor = System.Drawing.SystemColors.Info
            Me.ClientSize = New System.Drawing.Size(276, 66)
            Me.ControlBox = False
            Me.Controls.Add(Me.StatusLabel)
            Me.Controls.Add(Me.ProgressBar)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Name = "WaitFormPrv"
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
        Friend WithEvents StatusLabel As System.Windows.Forms.Label
        Friend WithEvents tmrRefresh As System.Windows.Forms.Timer
    End Class

End Namespace
