﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CustomMessageBoxPrv
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.lblPrompt = New System.Windows.Forms.Label()
            Me.tblButtons = New System.Windows.Forms.TableLayoutPanel()
            Me.Button0 = New System.Windows.Forms.Button()
            Me.Button1 = New System.Windows.Forms.Button()
            Me.Button2 = New System.Windows.Forms.Button()
            Me.Button3 = New System.Windows.Forms.Button()
            Me.Button4 = New System.Windows.Forms.Button()
            Me.Button5 = New System.Windows.Forms.Button()
            Me.Button6 = New System.Windows.Forms.Button()
            Me.Button7 = New System.Windows.Forms.Button()
            Me.Button8 = New System.Windows.Forms.Button()
            Me.Button9 = New System.Windows.Forms.Button()
            Me.Button10 = New System.Windows.Forms.Button()
            Me.Button11 = New System.Windows.Forms.Button()
            Me.Button12 = New System.Windows.Forms.Button()
            Me.Button13 = New System.Windows.Forms.Button()
            Me.Button14 = New System.Windows.Forms.Button()
            Me.tblButtons.SuspendLayout()
            Me.SuspendLayout()
            '
            'lblPrompt
            '
            Me.lblPrompt.AutoSize = True
            Me.lblPrompt.Location = New System.Drawing.Point(9, 9)
            Me.lblPrompt.Name = "lblPrompt"
            Me.lblPrompt.Size = New System.Drawing.Size(56, 13)
            Me.lblPrompt.TabIndex = 0
            Me.lblPrompt.Text = "[lblPrompt]"
            '
            'tblButtons
            '
            Me.tblButtons.AutoSize = True
            Me.tblButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.tblButtons.ColumnCount = 5
            Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
            Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
            Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
            Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
            Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
            Me.tblButtons.Controls.Add(Me.Button0, 0, 0)
            Me.tblButtons.Controls.Add(Me.Button1, 1, 0)
            Me.tblButtons.Controls.Add(Me.Button2, 2, 0)
            Me.tblButtons.Controls.Add(Me.Button3, 3, 0)
            Me.tblButtons.Controls.Add(Me.Button4, 4, 0)
            Me.tblButtons.Controls.Add(Me.Button5, 0, 1)
            Me.tblButtons.Controls.Add(Me.Button6, 1, 1)
            Me.tblButtons.Controls.Add(Me.Button7, 2, 1)
            Me.tblButtons.Controls.Add(Me.Button8, 3, 1)
            Me.tblButtons.Controls.Add(Me.Button9, 4, 1)
            Me.tblButtons.Controls.Add(Me.Button10, 0, 2)
            Me.tblButtons.Controls.Add(Me.Button11, 1, 2)
            Me.tblButtons.Controls.Add(Me.Button12, 2, 2)
            Me.tblButtons.Controls.Add(Me.Button13, 3, 2)
            Me.tblButtons.Controls.Add(Me.Button14, 4, 2)
            Me.tblButtons.Location = New System.Drawing.Point(1, 57)
            Me.tblButtons.Name = "tblButtons"
            Me.tblButtons.Padding = New System.Windows.Forms.Padding(5, 0, 5, 0)
            Me.tblButtons.RowCount = 3
            Me.tblButtons.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.tblButtons.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.tblButtons.RowStyles.Add(New System.Windows.Forms.RowStyle())
            Me.tblButtons.Size = New System.Drawing.Size(205, 87)
            Me.tblButtons.TabIndex = 1
            '
            'Button0
            '
            Me.Button0.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button0.AutoSize = True
            Me.Button0.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button0.Location = New System.Drawing.Point(8, 3)
            Me.Button0.Name = "Button0"
            Me.Button0.Size = New System.Drawing.Size(33, 23)
            Me.Button0.TabIndex = 0
            Me.Button0.Text = "Btn"
            Me.Button0.UseVisualStyleBackColor = True
            '
            'Button1
            '
            Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button1.AutoSize = True
            Me.Button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button1.Location = New System.Drawing.Point(47, 3)
            Me.Button1.Name = "Button1"
            Me.Button1.Size = New System.Drawing.Size(33, 23)
            Me.Button1.TabIndex = 1
            Me.Button1.Text = "Btn"
            Me.Button1.UseVisualStyleBackColor = True
            '
            'Button2
            '
            Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button2.AutoSize = True
            Me.Button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button2.Location = New System.Drawing.Point(86, 3)
            Me.Button2.Name = "Button2"
            Me.Button2.Size = New System.Drawing.Size(33, 23)
            Me.Button2.TabIndex = 2
            Me.Button2.Text = "Btn"
            Me.Button2.UseVisualStyleBackColor = True
            '
            'Button3
            '
            Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button3.AutoSize = True
            Me.Button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button3.Location = New System.Drawing.Point(125, 3)
            Me.Button3.Name = "Button3"
            Me.Button3.Size = New System.Drawing.Size(33, 23)
            Me.Button3.TabIndex = 3
            Me.Button3.Text = "Btn"
            Me.Button3.UseVisualStyleBackColor = True
            '
            'Button4
            '
            Me.Button4.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button4.AutoSize = True
            Me.Button4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button4.Location = New System.Drawing.Point(164, 3)
            Me.Button4.Name = "Button4"
            Me.Button4.Size = New System.Drawing.Size(33, 23)
            Me.Button4.TabIndex = 4
            Me.Button4.Text = "Btn"
            Me.Button4.UseVisualStyleBackColor = True
            '
            'Button5
            '
            Me.Button5.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button5.AutoSize = True
            Me.Button5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button5.Location = New System.Drawing.Point(8, 32)
            Me.Button5.Name = "Button5"
            Me.Button5.Size = New System.Drawing.Size(33, 23)
            Me.Button5.TabIndex = 5
            Me.Button5.Text = "Btn"
            Me.Button5.UseVisualStyleBackColor = True
            '
            'Button6
            '
            Me.Button6.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button6.AutoSize = True
            Me.Button6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button6.Location = New System.Drawing.Point(47, 32)
            Me.Button6.Name = "Button6"
            Me.Button6.Size = New System.Drawing.Size(33, 23)
            Me.Button6.TabIndex = 6
            Me.Button6.Text = "Btn"
            Me.Button6.UseVisualStyleBackColor = True
            '
            'Button7
            '
            Me.Button7.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button7.AutoSize = True
            Me.Button7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button7.Location = New System.Drawing.Point(86, 32)
            Me.Button7.Name = "Button7"
            Me.Button7.Size = New System.Drawing.Size(33, 23)
            Me.Button7.TabIndex = 7
            Me.Button7.Text = "Btn"
            Me.Button7.UseVisualStyleBackColor = True
            '
            'Button8
            '
            Me.Button8.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button8.AutoSize = True
            Me.Button8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button8.Location = New System.Drawing.Point(125, 32)
            Me.Button8.Name = "Button8"
            Me.Button8.Size = New System.Drawing.Size(33, 23)
            Me.Button8.TabIndex = 8
            Me.Button8.Text = "Btn"
            Me.Button8.UseVisualStyleBackColor = True
            '
            'Button9
            '
            Me.Button9.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button9.AutoSize = True
            Me.Button9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button9.Location = New System.Drawing.Point(164, 32)
            Me.Button9.Name = "Button9"
            Me.Button9.Size = New System.Drawing.Size(33, 23)
            Me.Button9.TabIndex = 9
            Me.Button9.Text = "Btn"
            Me.Button9.UseVisualStyleBackColor = True
            '
            'Button10
            '
            Me.Button10.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button10.AutoSize = True
            Me.Button10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button10.Location = New System.Drawing.Point(8, 61)
            Me.Button10.Name = "Button10"
            Me.Button10.Size = New System.Drawing.Size(33, 23)
            Me.Button10.TabIndex = 10
            Me.Button10.Text = "Btn"
            Me.Button10.UseVisualStyleBackColor = True
            '
            'Button11
            '
            Me.Button11.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button11.AutoSize = True
            Me.Button11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button11.Location = New System.Drawing.Point(47, 61)
            Me.Button11.Name = "Button11"
            Me.Button11.Size = New System.Drawing.Size(33, 23)
            Me.Button11.TabIndex = 11
            Me.Button11.Text = "Btn"
            Me.Button11.UseVisualStyleBackColor = True
            '
            'Button12
            '
            Me.Button12.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button12.AutoSize = True
            Me.Button12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button12.Location = New System.Drawing.Point(86, 61)
            Me.Button12.Name = "Button12"
            Me.Button12.Size = New System.Drawing.Size(33, 23)
            Me.Button12.TabIndex = 12
            Me.Button12.Text = "Btn"
            Me.Button12.UseVisualStyleBackColor = True
            '
            'Button13
            '
            Me.Button13.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button13.AutoSize = True
            Me.Button13.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button13.Location = New System.Drawing.Point(125, 61)
            Me.Button13.Name = "Button13"
            Me.Button13.Size = New System.Drawing.Size(33, 23)
            Me.Button13.TabIndex = 13
            Me.Button13.Text = "Btn"
            Me.Button13.UseVisualStyleBackColor = True
            '
            'Button14
            '
            Me.Button14.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.Button14.AutoSize = True
            Me.Button14.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.Button14.Location = New System.Drawing.Point(164, 61)
            Me.Button14.Name = "Button14"
            Me.Button14.Size = New System.Drawing.Size(33, 23)
            Me.Button14.TabIndex = 14
            Me.Button14.Text = "Btn"
            Me.Button14.UseVisualStyleBackColor = True
            '
            'CustomMessageBoxPrv
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.AutoSize = True
            Me.ClientSize = New System.Drawing.Size(243, 153)
            Me.ControlBox = False
            Me.Controls.Add(Me.tblButtons)
            Me.Controls.Add(Me.lblPrompt)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Name = "CustomMessageBoxPrv"
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Message"
            Me.tblButtons.ResumeLayout(False)
            Me.tblButtons.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents lblPrompt As System.Windows.Forms.Label
        Friend WithEvents tblButtons As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents Button0 As System.Windows.Forms.Button
        Friend WithEvents Button1 As System.Windows.Forms.Button
        Friend WithEvents Button2 As System.Windows.Forms.Button
        Friend WithEvents Button3 As System.Windows.Forms.Button
        Friend WithEvents Button4 As System.Windows.Forms.Button
        Friend WithEvents Button5 As System.Windows.Forms.Button
        Friend WithEvents Button6 As System.Windows.Forms.Button
        Friend WithEvents Button7 As System.Windows.Forms.Button
        Friend WithEvents Button8 As System.Windows.Forms.Button
        Friend WithEvents Button9 As System.Windows.Forms.Button
        Friend WithEvents Button10 As System.Windows.Forms.Button
        Friend WithEvents Button11 As System.Windows.Forms.Button
        Friend WithEvents Button12 As System.Windows.Forms.Button
        Friend WithEvents Button13 As System.Windows.Forms.Button
        Friend WithEvents Button14 As System.Windows.Forms.Button
    End Class

End Namespace
