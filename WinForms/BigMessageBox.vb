﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

    Friend Class BigMessageBoxPrv

        Private Sub BigMessageBoxPrv_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
            Me.Focus()
            Me.BringToFront()
        End Sub

        Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Close()
        End Sub

        Public Function DisplayMessage(ByVal pMessage As String, ByVal pPrompt As String, ByVal pTitle As String) As DialogResult
            Me.txtMessage.Text = pMessage
            Me.lblPrompt.Text = pPrompt
            Me.Text = pTitle
            Me.ShowDialog()
            Return Me.DialogResult
        End Function

        Private Sub BigMessageBoxPrv_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
            Me.btnOk.Focus()
        End Sub
    End Class

    Public Class BigMessageBox

        Private Sub New()
            'no public instantiation
        End Sub

        Public Shared Function Show(ByVal pMessage As String, Optional ByVal pPrompt As String = "", Optional ByVal pTitle As String = "Message", Optional pMessageBoxOptions As MessageBoxOptions = Nothing) As DialogResult
            If (pMessageBoxOptions Is Nothing) Then pMessageBoxOptions = New MessageBoxOptions
            Using frm As New BigMessageBoxPrv
                If pMessageBoxOptions.Height.HasValue Then frm.Height = pMessageBoxOptions.Height.Value
                If pMessageBoxOptions.Width.HasValue Then frm.Width = pMessageBoxOptions.Width.Value
                If pMessageBoxOptions.TopMost Then frm.TopMost = True
                If pMessageBoxOptions.ReadOnlyMessage Then frm.txtMessage.ReadOnly = True
                Return frm.DisplayMessage(pMessage, pPrompt, pTitle)
            End Using
        End Function

    End Class

End Namespace
