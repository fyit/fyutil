﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

	Public Class WaitCursorHandler
		Implements IDisposable

		Private Shared _waitCount As Int32 = 0
		Private Shared _lockTarget As New Object

#Region "IDisposable Support"
		Private disposedValue As Boolean ' To detect redundant calls

		' IDisposable
		Protected Overridable Sub Dispose(ByVal disposing As Boolean)
			If Not Me.disposedValue Then
				If disposing Then
					SyncLock _lockTarget
						_waitCount -= 1
						If (_waitCount = 0) Then
							System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
						End If
					End SyncLock
				End If

				' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
				' TODO: set large fields to null.
			End If
			Me.disposedValue = True
		End Sub

		' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
		'Protected Overrides Sub Finalize()
		'    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
		'    Dispose(False)
		'    MyBase.Finalize()
		'End Sub

		' This code added by Visual Basic to correctly implement the disposable pattern.
		Public Sub Dispose() Implements IDisposable.Dispose
			' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
			Dispose(True)
			GC.SuppressFinalize(Me)
		End Sub
#End Region

		Sub New()
			SyncLock _lockTarget
				System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
				_waitCount += 1
			End SyncLock
		End Sub
	End Class

End Namespace
