﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

Public Class LoginForm

    Public Property Username As String = ""
    Public Property Password As String = ""

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Username = txtUsername.Text
        Me.Password = txtPassword.Text
        Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Close()
    End Sub

        Public Function Display(Optional ByVal pUsername As String = "", Optional ByVal pPassword As String = "", Optional ByVal pPrompt As String = "") As Windows.Forms.DialogResult
            txtUsername.Text = pUsername
            txtPassword.Text = pPassword
            If (String.IsNullOrWhiteSpace(pPrompt)) Then
                lblPrompt.Text = ""
                Me.Height = 130
            Else
                lblPrompt.Text = pPrompt
                Me.Height = 130 + lblPrompt.Height
            End If
            Dim res = Me.ShowDialog()
            If (res = Windows.Forms.DialogResult.OK) Then
                Me.Username = txtUsername.Text
                Me.Password = txtPassword.Text
            End If
            Return res
        End Function

        Private Sub LoginForm_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
            If (String.IsNullOrWhiteSpace(txtUsername.Text)) Then
                txtUsername.Focus()
            Else
                txtPassword.Focus()
            End If
        End Sub

    Private Sub txtPassword_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPassword.KeyUp
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            Call btnLogin_Click(Nothing, Nothing)
            e.Handled = True
        End If
    End Sub

        Private Sub txtUsername_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsername.KeyUp
            If e.KeyCode = Windows.Forms.Keys.Enter Then
                txtPassword.Focus()
                e.Handled = True
            End If
        End Sub

    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.BringToFront()
        Me.Focus()
    End Sub
End Class

End Namespace
