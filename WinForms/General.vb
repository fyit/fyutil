﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

    Public Class General

#Region "Form Geometry"

        ''' <summary>
        ''' Restores a form's geometry (size, position, state) using the information stored in the referenced string created by GetFormGeometry
        ''' </summary>
        ''' <param name="pForm"></param>
        ''' <param name="pGeometryData"></param>
        ''' <remarks></remarks>
        Public Shared Sub RestoreFormGeometry(pForm As Windows.Forms.Form, pGeometryData As String, Optional pAllowMinimized As Boolean = False)
            If (String.IsNullOrWhiteSpace(pGeometryData)) Then Return

            Dim posVals = pGeometryData.Split(","c)
            'stored as top,left,height,width,state
            If ((posVals.Count > 0) AndAlso IsNumeric(posVals(0))) Then pForm.Top = Convert.ToInt32(posVals(0))
            If ((posVals.Count > 1) AndAlso IsNumeric(posVals(1))) Then pForm.Left = Convert.ToInt32(posVals(1))
            If ((posVals.Count > 2) AndAlso IsNumeric(posVals(2))) Then pForm.Height = Convert.ToInt32(posVals(2))
            If ((posVals.Count > 3) AndAlso IsNumeric(posVals(3))) Then pForm.Width = Convert.ToInt32(posVals(3))

            'ensure top left corner is visible on the screen
            Dim tlc As New Point(pForm.Left, pForm.Top)
            Dim scr = Windows.Forms.Screen.FromPoint(tlc)
            If (Not scr.WorkingArea.Contains(tlc)) Then
                pForm.Top = scr.WorkingArea.Top
                pForm.Left = scr.WorkingArea.Left
            End If

            'reset state
            If (posVals.Count > 4) Then
                Dim st As Windows.Forms.FormWindowState = pForm.WindowState
                [Enum].TryParse(Of Windows.Forms.FormWindowState)(posVals(4), st)
                If ((st = Windows.Forms.FormWindowState.Minimized) AndAlso (Not pAllowMinimized)) Then st = Windows.Forms.FormWindowState.Normal
                pForm.WindowState = st
            End If
        End Sub

        ''' <summary>
        ''' Creates a string representing a form's geometry (size, position, state) for use in RestoreFormGeometry later
        ''' </summary>
        ''' <param name="pForm"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFormGeometry(pForm As Windows.Forms.Form) As String
            'stored as top,left,height,width,state
            If (pForm.WindowState = Windows.Forms.FormWindowState.Normal) Then
                Return String.Format("{0},{1},{2},{3},{4}", pForm.Top, pForm.Left, pForm.Height, pForm.Width, pForm.WindowState)
            Else
                Return String.Format("{0},{1},{2},{3},{4}", pForm.RestoreBounds.Top, pForm.RestoreBounds.Left, pForm.RestoreBounds.Height, pForm.RestoreBounds.Width, pForm.WindowState)
            End If
        End Function

#End Region

    End Class

End Namespace
