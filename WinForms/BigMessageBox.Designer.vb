﻿'  Copyright 2013 For Your Information Technologies
'
'  Licensed under the Apache License, Version 2.0 (the "License");
'  you may not use this file except in compliance with the License.
'  You may obtain a copy of the License at
'
'  http://www.apache.org/licenses/LICENSE-2.0
'
'  Unless required by applicable law or agreed to in writing, software
'  distributed under the License is distributed on an "AS IS" BASIS,
'  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'  See the License for the specific language governing permissions and
'  limitations under the License. 



Namespace WinForms

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BigMessageBoxPrv
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.lblPrompt = New System.Windows.Forms.Label()
            Me.txtMessage = New System.Windows.Forms.TextBox()
            Me.btnOk = New System.Windows.Forms.Button()
            Me.SuspendLayout()
            '
            'lblPrompt
            '
            Me.lblPrompt.AutoSize = True
            Me.lblPrompt.Location = New System.Drawing.Point(9, 9)
            Me.lblPrompt.Name = "lblPrompt"
            Me.lblPrompt.Size = New System.Drawing.Size(56, 13)
            Me.lblPrompt.TabIndex = 0
            Me.lblPrompt.Text = "[lblPrompt]"
            '
            'txtMessage
            '
            Me.txtMessage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.txtMessage.Location = New System.Drawing.Point(4, 38)
            Me.txtMessage.Multiline = True
            Me.txtMessage.Name = "txtMessage"
            Me.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.txtMessage.Size = New System.Drawing.Size(285, 125)
            Me.txtMessage.TabIndex = 1
            Me.txtMessage.WordWrap = False
            '
            'btnOk
            '
            Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.btnOk.Location = New System.Drawing.Point(214, 169)
            Me.btnOk.Name = "btnOk"
            Me.btnOk.Size = New System.Drawing.Size(75, 23)
            Me.btnOk.TabIndex = 2
            Me.btnOk.Text = "Ok"
            Me.btnOk.UseVisualStyleBackColor = True
            '
            'BigMessageBoxPrv
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(292, 198)
            Me.ControlBox = False
            Me.Controls.Add(Me.btnOk)
            Me.Controls.Add(Me.txtMessage)
            Me.Controls.Add(Me.lblPrompt)
            Me.Name = "BigMessageBoxPrv"
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Message"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents lblPrompt As System.Windows.Forms.Label
        Friend WithEvents txtMessage As System.Windows.Forms.TextBox
        Friend WithEvents btnOk As System.Windows.Forms.Button
    End Class

End Namespace
