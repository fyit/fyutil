﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestApp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstTestMethods = New System.Windows.Forms.ComboBox()
        Me.btnRunTest = New System.Windows.Forms.Button()
        Me.txtResults = New System.Windows.Forms.TextBox()
        Me.btnCopyLog = New System.Windows.Forms.Button()
        Me.btnClearLog = New System.Windows.Forms.Button()
        Me.lstLogLevel = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lstTestMethods
        '
        Me.lstTestMethods.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstTestMethods.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.lstTestMethods.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstTestMethods.FormattingEnabled = True
        Me.lstTestMethods.Location = New System.Drawing.Point(3, 10)
        Me.lstTestMethods.Name = "lstTestMethods"
        Me.lstTestMethods.Size = New System.Drawing.Size(556, 22)
        Me.lstTestMethods.TabIndex = 0
        '
        'btnRunTest
        '
        Me.btnRunTest.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRunTest.Location = New System.Drawing.Point(565, 9)
        Me.btnRunTest.Name = "btnRunTest"
        Me.btnRunTest.Size = New System.Drawing.Size(47, 21)
        Me.btnRunTest.TabIndex = 1
        Me.btnRunTest.Text = "Run"
        Me.btnRunTest.UseVisualStyleBackColor = True
        '
        'txtResults
        '
        Me.txtResults.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtResults.Font = New System.Drawing.Font("Courier New", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResults.Location = New System.Drawing.Point(3, 39)
        Me.txtResults.Multiline = True
        Me.txtResults.Name = "txtResults"
        Me.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtResults.Size = New System.Drawing.Size(609, 334)
        Me.txtResults.TabIndex = 2
        Me.txtResults.WordWrap = False
        '
        'btnCopyLog
        '
        Me.btnCopyLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCopyLog.Location = New System.Drawing.Point(3, 377)
        Me.btnCopyLog.Name = "btnCopyLog"
        Me.btnCopyLog.Size = New System.Drawing.Size(44, 23)
        Me.btnCopyLog.TabIndex = 3
        Me.btnCopyLog.Text = "Copy"
        Me.btnCopyLog.UseVisualStyleBackColor = True
        '
        'btnClearLog
        '
        Me.btnClearLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClearLog.Location = New System.Drawing.Point(53, 377)
        Me.btnClearLog.Name = "btnClearLog"
        Me.btnClearLog.Size = New System.Drawing.Size(44, 23)
        Me.btnClearLog.TabIndex = 4
        Me.btnClearLog.Text = "Clear"
        Me.btnClearLog.UseVisualStyleBackColor = True
        '
        'lstLogLevel
        '
        Me.lstLogLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.lstLogLevel.FormattingEnabled = True
        Me.lstLogLevel.Location = New System.Drawing.Point(162, 379)
        Me.lstLogLevel.Name = "lstLogLevel"
        Me.lstLogLevel.Size = New System.Drawing.Size(71, 21)
        Me.lstLogLevel.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(128, 383)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Log:"
        '
        'frmTestApp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(618, 402)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstLogLevel)
        Me.Controls.Add(Me.btnClearLog)
        Me.Controls.Add(Me.btnCopyLog)
        Me.Controls.Add(Me.txtResults)
        Me.Controls.Add(Me.btnRunTest)
        Me.Controls.Add(Me.lstTestMethods)
        Me.Name = "frmTestApp"
        Me.Text = "TestApp"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstTestMethods As System.Windows.Forms.ComboBox
    Friend WithEvents btnRunTest As System.Windows.Forms.Button
    Friend WithEvents txtResults As System.Windows.Forms.TextBox
    Friend WithEvents btnCopyLog As System.Windows.Forms.Button
    Friend WithEvents btnClearLog As System.Windows.Forms.Button
    Friend WithEvents lstLogLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
