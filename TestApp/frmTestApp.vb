﻿Public Class frmTestApp

    Private _myTestMethods As ITestMethods = New TestMethods
    Private _testList As New Dictionary(Of String, TestMethodDelegate)
    Private _logLevel As FYUtil.Logging.eLogLevel = FYUtil.Logging.eLogLevel.All

    Private Sub frmTestApp_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        FYUtil.Logging.MasterLoggingLevel = FYUtil.Logging.eLogLevel.All
        AddHandler FYUtil.Logging.LogEvent, AddressOf FYUtil_Logging_LogEvent
        FYUtil.Logging.IncludeExtendedData = True
        FYUtil.Logging.ExecutionTimer.EnableTraceLogging = True
        FYUtil.Logging.ExecutionTimer.EnableAggregation = True
        FYUtil.Logging.ExecutionTimer.KeepHistoryCount = 10

        lstTestMethods.Items.Add(" -- ALL TESTS -- ")
        Dim meths As Reflection.MethodInfo() = _myTestMethods.GetType().GetMethods((Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public Or Reflection.BindingFlags.DeclaredOnly))
        For Each meth As Reflection.MethodInfo In meths
            If (meth.ReturnType <> GetType(System.Void)) Then Continue For
            If (meth.GetParameters().Length > 0) Then Continue For
            If (meth.Name.Equals("InitTestEnvironment")) Then Continue For
            If (meth.Name.Equals("CleanupTestEnvironment")) Then Continue For
            _testList.Item(meth.Name) = DirectCast([Delegate].CreateDelegate(GetType(TestMethodDelegate), _myTestMethods, meth), TestMethodDelegate)
            lstTestMethods.Items.Add(meth.Name)
        Next

        Dim logLevels As Dictionary(Of Int32, String) = FYUtil.Utilities.EnumToDict(GetType(FYUtil.Logging.eLogLevel))
        For Each lv As String In logLevels.Values
            lstLogLevel.Items.Add(lv)
        Next


        Dim defMethName = GetSetting("TestApp", If(_myTestMethods.TestCollectionName, ""), "DefaultTestMethod", "")
        If (lstTestMethods.Items.Contains(defMethName)) Then
            lstTestMethods.SelectedItem = defMethName
        ElseIf (lstTestMethods.Items.Count > 1) Then
            lstTestMethods.SelectedIndex = 1
        End If

        Dim defLogLevel = GetSetting("TestApp", If(_myTestMethods.TestCollectionName, ""), "LogLevel", "Warning")
        Try
            _logLevel = DirectCast([Enum].Parse(GetType(FYUtil.Logging.eLogLevel), defLogLevel), FYUtil.Logging.eLogLevel)
        Catch ex As Exception
            _logLevel = FYUtil.Logging.eLogLevel.Warning
        End Try
        lstLogLevel.SelectedItem = _logLevel.ToString
    End Sub

    Private Sub frmTestApp_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        WriteLineToLog("Start InitTestEnvironment...")
        _myTestMethods.InitTestEnvironment()
        WriteLineToLog("End InitTestEnvironment.")
    End Sub

    Private Sub frmTestApp_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        WriteLineToLog("Start CleanupTestEnvironment...")
        _myTestMethods.CleanupTestEnvironment()
        WriteLineToLog("End CleanupTestEnvironment.")
    End Sub

    Private Sub lstLogLevel_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstLogLevel.SelectedIndexChanged
        _logLevel = DirectCast([Enum].Parse(GetType(FYUtil.Logging.eLogLevel), lstLogLevel.SelectedItem.ToString), FYUtil.Logging.eLogLevel)
        SaveSetting("TestApp", If(_myTestMethods.TestCollectionName, ""), "LogLevel", _logLevel.ToString)
    End Sub

    Private Sub btnRunTest_Click(sender As System.Object, e As System.EventArgs) Handles btnRunTest.Click
        If (lstTestMethods.SelectedIndex = 0) Then
            ClearLog()
            For Each tst As String In _testList.Keys
                DoRunTest(_testList.Item(tst))
                WriteLineToLog("---------------------------------------------------------")
            Next
        ElseIf (_testList.ContainsKey(lstTestMethods.Text)) Then
            ClearLog()
            DoRunTest(_testList.Item(lstTestMethods.Text))
            SaveSetting("TestApp", If(_myTestMethods.TestCollectionName, ""), "DefaultTestMethod", lstTestMethods.Text)
        End If
    End Sub

    Private Sub btnCopyLog_Click(sender As System.Object, e As System.EventArgs) Handles btnCopyLog.Click
        Windows.Forms.Clipboard.SetText(txtResults.Text)
    End Sub

    Private Sub btnClearLog_Click(sender As System.Object, e As System.EventArgs) Handles btnClearLog.Click
        ClearLog()
    End Sub



    Private Sub DoRunTest(pFunc As TestMethodDelegate)
        If (pFunc Is Nothing) Then Return
        Try
            btnRunTest.Enabled = False
            WriteLineToLog(String.Format("Start '{0}' at {1:HH:mm:ss.ff} ...", pFunc.Method.Name, DateTime.Now))
            WriteLineToLog("")
            pFunc.Invoke()
        Catch ex As Exception
            FYUtil.Logging.LogError(ex)
        Finally
            WriteLineToLog("")
            WriteLineToLog(String.Format("End '{0}' at {1:HH:mm:ss.ff}.", pFunc.Method.Name, DateTime.Now))
            btnRunTest.Enabled = True
        End Try
    End Sub

    Private Sub FYUtil_Logging_LogEvent(pEventData As FYUtil.Logging.EventData)
        If (pEventData.Severity < _logLevel) Then Return
        If (pEventData.Severity >= FYUtil.Logging.eSeverity.Warning) Then
            WriteLineToLog(String.Format("*{0}* || {1}", pEventData.Severity, pEventData.Message))
            If (Not String.IsNullOrEmpty(pEventData.StackTrace)) Then
                WriteToLog(pEventData.StackTrace)
                WriteLineToLog("")
            End If
        Else
            WriteLineToLog(pEventData.Message)
        End If
    End Sub

    Private Sub WriteToLog(ByVal pMessage As String)
        If (Me.InvokeRequired) Then
            Dim meth As New Action(Of String)(AddressOf WriteToLog)
            Me.Invoke(meth, pMessage)
        Else
            Dim atBottom As Boolean = (txtResults.SelectionStart = txtResults.Text.Length)
            txtResults.Text += pMessage
            If (atBottom) Then
                txtResults.SelectionStart = txtResults.Text.Length
                txtResults.ScrollToCaret()
            End If
            Me.Refresh()
        End If
    End Sub
    Private Sub WriteLineToLog(ByVal pMessage As String)
        WriteToLog(pMessage + Environment.NewLine)
    End Sub
    Private Sub ClearLog()
        If (Me.InvokeRequired) Then
            Dim meth As New Action(AddressOf ClearLog)
            Me.Invoke(meth)
        Else
            txtResults.Text = String.Empty
            Me.Refresh()
        End If
    End Sub

End Class

Public Interface ITestMethods
    Sub InitTestEnvironment()
    Sub CleanupTestEnvironment()
    Property TestCollectionName As String
End Interface

Public Delegate Sub TestMethodDelegate()
