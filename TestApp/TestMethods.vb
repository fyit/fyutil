﻿Imports FYUtil.ExtensionMethods

Public Class TestMethods
    Implements ITestMethods

    Public Property TestCollectionName As String = "FYUtil" Implements ITestMethods.TestCollectionName


    Public Sub InitTestEnvironment() Implements ITestMethods.InitTestEnvironment
    End Sub

    Public Sub CleanupTestEnvironment() Implements ITestMethods.CleanupTestEnvironment
    End Sub




    Sub Test_VerifyDbInfo()
        Dim ci As New FYUtil.Database.DbConnectionInfo("Server=.;Database=TestDb;UID=sa;Password=Password1;", FYUtil.Database.eDbType.SqlServer)
        Dim reply = FYUtil.Database.VerifyDbInfo(ci, "TestDb", "5106", FYUtil.eEnvironment.Test)
        FYUtil.Logging.LogTrace(String.Format("VerifyDbInfo: {0},{1}", reply.Success.ToString, reply.Description))
    End Sub

    Sub Test_Symmetric()
        Dim toEnc = "aaaabbbbccccddddeeeeffffgggghhhh"
        Dim key24 = "asefhoaihgpaiosernpoi9aj"
        Dim key32 = "asefhoaihgpaiosernpoi9ajne8dnqpf"
        Dim encrypted, decrypted As String

        FYUtil.Logging.LogTrace(String.Format("Random password: {0}", FYUtil.Security.Symmetric.GenerateEncryptionKey(192)))


        '3DES
        FYUtil.Logging.LogTrace("3DES")

        encrypted = FYUtil.Security.Symmetric.EncryptTripleDesToBase64(toEnc, key24)
        FYUtil.Logging.LogTrace(String.Format("3DES encrypt, key={0}  clear={1}  enc={2}", key24, toEnc, encrypted))

        decrypted = FYUtil.Security.Symmetric.DecryptTripleDesBase64ToString(encrypted, key24)
        FYUtil.Logging.LogTrace(String.Format("3DES decrypt, key={0}  clear={1}  enc={2}", key24, decrypted, encrypted))



        'AES
        FYUtil.Logging.LogTrace("AES")

        encrypted = FYUtil.Security.Symmetric.EncryptAesToBase64(toEnc, key32)
        FYUtil.Logging.LogTrace(String.Format("AES encrypt, key={0}  clear={1}  enc={2}", key32, toEnc, encrypted))

        decrypted = FYUtil.Security.Symmetric.DecryptAesBase64ToString(encrypted, key32)
        FYUtil.Logging.LogTrace(String.Format("AES decrypt, key={0}  clear={1}  enc={2}", key32, decrypted, encrypted))



    End Sub

    Sub Test_Encryption()
        'old encryption lib
        'Dim toProcess As New FYUtil.Security.Encryption.Data("test string")
        'Dim key As New FYUtil.Security.Encryption.Data("asefhoaihgpaiosernpoi9aj")
        'Dim processed As FYUtil.Security.Encryption.Data

        'Dim hasher As New FYUtil.Security.Encryption.Hash(FYUtil.Security.Encryption.Hash.Provider.SHA1)
        'processed = hasher.Calculate(toProcess)
        ''should be 661295c9cbf9d6b2f6428414504a8deed3020641
        'FYUtil.Logging.LogVerbose(String.Format("SHA1: {0} = {1}", toProcess.Text, processed.Hex))

        'Dim encrypter As New FYUtil.Security.Encryption.Symmetric(FYUtil.Security.Encryption.Symmetric.Provider.TripleDES, useDefaultInitializationVector:=True)
        'processed = encrypter.Encrypt(toProcess, key)
        'FYUtil.Logging.LogVerbose(String.Format("3DES, key={0}: {1} = {2}", key.Hex, toProcess.Text, processed.Base64))

        'Dim encrypted2 = ""
    End Sub

    Public Sub Logging()

        Dim msg = FYUtil.Logging.CreateTraceMessage("hello")

        FYUtil.Logging.MasterLoggingLevel = FYUtil.Logging.eLogLevel.All
        FYUtil.Logging.AppName = "FYUtil.TestApp"
        FYUtil.Logging.AppEnvironment = "Dev"
        FYUtil.Logging.EnableEmailLogSink(FYUtil.Logging.eLogLevel.Error, "tmatous@gmail.com", "errors@test.com", Nothing)
        FYUtil.Logging.EnableFileLogSink(FYUtil.Logging.eLogLevel.Warning, "C:\FYUtil.TestApp.log")
        FYUtil.Logging.IncludeExtendedData = True
        FYUtil.Logging.FileCreationMode = FYUtil.Logging.eFileCreationMode.OneFilePerRun
        FYUtil.Logging.FileArchivePeriod = TimeSpan.FromMinutes(5)
        AddHandler FYUtil.Logging.LogMessageEvent, AddressOf Log_Handler

        'already being initialized by TestApp
        'FYUtil.Logging.Init(pIgnoreAppConfigSettings:=True)


        'send some duplicate emails
        FYUtil.Logging.EmailFloodProtectionBlockPeriod = TimeSpan.FromSeconds(2)
        FYUtil.Logging.LogUnconditionalEmail("message 1", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 1", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 2", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 1", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 2", FYUtil.Logging.eSeverity.Info)
        Threading.Thread.Sleep(2000)
        FYUtil.Logging.LogUnconditionalEmail("message 1", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 1", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 2", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 1", FYUtil.Logging.eSeverity.Info)
        FYUtil.Logging.LogUnconditionalEmail("message 2", FYUtil.Logging.eSeverity.Info)



        FYUtil.Logging.MasterLoggingLevel = FYUtil.Logging.eLogLevel.All
        FYUtil.Logging.LogTrace("test log message (verbose)", "mycat")
        FYUtil.Logging.LogInformational("test log message (info)")
        FYUtil.Logging.LogWarning("test log message (warning)")
        FYUtil.Logging.LogError("test log message (error)", "othercat")
        FYUtil.Logging.LogDebugFull("test log message (error)")
        If (FYUtil.Logging.IsTraceLevel) Then FYUtil.Logging.LogTrace("test trace")

        FYUtil.Logging.MasterLoggingLevel = FYUtil.Logging.eLogLevel.Error
        FYUtil.Logging.LogTrace("test log message (verbose)")
        FYUtil.Logging.LogInformational("test log message (info)")
        FYUtil.Logging.LogWarning("test log message (warning)")
        FYUtil.Logging.LogError("test log message (error)")
        FYUtil.Logging.LogMessage("test log message (fatal)", FYUtil.Logging.eSeverity.Fatal, "mycategory")

        FYUtil.Logging.ExecutionTimer.EnableTraceLogging = True
        FYUtil.Logging.ExecutionTimer.EnableAggregation = True
        FYUtil.Logging.ExecutionTimer.KeepHistoryCount = 10

        FYUtil.Logging.MasterLoggingLevel = FYUtil.Logging.eLogLevel.Debug
        Using New FYUtil.Logging.ExecutionTimer("Do Stuff 1")
            Threading.Thread.Sleep(100)
        End Using
        Using New FYUtil.Logging.ExecutionTimer("Do Stuff 2")
            For i As Int32 = 1 To 10
                Using New FYUtil.Logging.ExecutionTimer("Do Substuff 2")
                    Threading.Thread.Sleep(100 * i)
                End Using
            Next
        End Using
        If (FYUtil.Logging.IsTraceLevel) Then FYUtil.Logging.LogTrace("test trace 2")

        Dim agg = FYUtil.Logging.ExecutionTimer.GetAggregate("Do Stuff 2")

        FYUtil.Logging.MasterLoggingLevel = FYUtil.Logging.eLogLevel.Warning
        Using New FYUtil.Logging.ExecutionTimer("test4")
            Threading.Thread.Sleep(100)
        End Using
        If (FYUtil.Logging.IsTraceLevel) Then FYUtil.Logging.LogTrace("test trace 3")
    End Sub

    Private Sub Log_Handler(pMessage As String)
        Dim hold = pMessage
    End Sub

    Private Sub Test_Random()
        FYUtil.Logging.LogTrace("Rand 5:" + FYUtil.Utilities.RandomString(5))
    End Sub


#Region "EnumString"

    Public Class esBatchStatus
        Inherits FYUtil.EnumString(Of esBatchStatus)

        Public Sub New()
        End Sub

        Protected Sub New(ByVal pStringValue As String, ByVal pDescription As String)
            MyBase.New(pStringValue, pDescription)
        End Sub

        'Implemented as property 
        Public Shared Property NotSet As New esBatchStatus("", "My NotSet")
        Public Shared Property Pending As New esBatchStatus("P", "My Pending")
        Public Shared Property Rejected As New esBatchStatus("R", "My Rejected")
        Public Shared Property Accepted As New esBatchStatus("A", "My Accepted")
    End Class

    Public Class esOtherStatus
        Inherits FYUtil.EnumString(Of esOtherStatus)

        Public Sub New()
        End Sub

        Public Sub New(ByVal pStringValue As String)
            MyBase.New(pStringValue)
        End Sub

        'Implemented as Methods
        Public Shared NotSet As New esOtherStatus("")
        Public Shared Pending As New esOtherStatus("P")
        Public Shared Rejected As New esOtherStatus("R")
        Public Shared Accepted As New esOtherStatus("A")
    End Class

    Sub Test_EnumString()
        Dim tmp22 = esBatchStatus.GetByCode("P")
        Dim tmp21 = esBatchStatus.GetByCode("UNKNOWN")

        FYUtil.Logging.LogTrace("LookupByAny by Code: " & esBatchStatus.LookupByAny("P").Description)
        FYUtil.Logging.LogTrace("LookupByAny by Name: " & esBatchStatus.LookupByAny("Pending").Description)
        FYUtil.Logging.LogTrace("LookupByAny by Description: " & esBatchStatus.LookupByAny("My Pending").Description)

        FYUtil.Logging.LogTrace("Unknown code ToString: " & tmp21.ToString)
        FYUtil.Logging.LogTrace("Known code (Pending) ToString: " & tmp22.ToString)

        FYUtil.Logging.LogTrace("Using Contains(expects true): " & {esBatchStatus.Accepted, esBatchStatus.Pending}.Contains(tmp22))
        FYUtil.Logging.LogTrace("Using Contains(expects false): " & {esBatchStatus.Accepted, esBatchStatus.Rejected}.Contains(tmp22))

        Dim dict As New Dictionary(Of esBatchStatus, String)
        dict.Add(tmp22, "Temp 22 (Pending)")

        FYUtil.Logging.LogTrace("Checking in dictionary by EnumString object as key (expects true)" & dict.ContainsKey(esBatchStatus.Pending))
        FYUtil.Logging.LogTrace("Checking in dictionary by EnumString object as key (expects false)" & dict.ContainsKey(esBatchStatus.Accepted))

        Dim tmp22Ser = FYUtil.Serialization.SerializeObject(tmp22, FYUtil.Serialization.eSerializer.XmlContract)
        FYUtil.Logging.LogTrace("Serialized: " & tmp22Ser)

        Dim tmp22_deserialized = FYUtil.Serialization.DeserializeObject(Of esBatchStatus)(tmp22Ser, FYUtil.Serialization.eSerializer.XmlContract)
        FYUtil.Logging.LogTrace("Deserialized 1: " & tmp22_deserialized.Code & "." & tmp22_deserialized.Description)

        Dim tmp23 = esBatchStatus.GetByCode("P") 'Get a copy for comparisons
        Dim tmp23Ser = FYUtil.Serialization.SerializeObject(tmp23, FYUtil.Serialization.eSerializer.XmlContract)
        Dim tmp23_deserialized = FYUtil.Serialization.DeserializeObject(Of esBatchStatus)(tmp23Ser, FYUtil.Serialization.eSerializer.XmlContract)
        FYUtil.Logging.LogTrace("Deserialized 2: " & tmp23_deserialized.Code & "." & tmp23_deserialized.Description)

        FYUtil.Logging.LogTrace("Comparing Deserialized Objects: " & (tmp22_deserialized = tmp23_deserialized).ToString)

        FYUtil.Logging.LogTrace("Pending Name: " & tmp22.Name())

        Dim tmp As esBatchStatus = esBatchStatus.Accepted
        FYUtil.Logging.LogTrace("Accepted = Rejected: " & (tmp = esBatchStatus.Rejected).ToString)
        FYUtil.Logging.LogTrace("Accepted = Accepted: " & (tmp = esBatchStatus.Accepted).ToString)

        FYUtil.Logging.LogTrace("Accetpted Details: " & esBatchStatus.Accepted.ToString & " (" & esBatchStatus.Accepted.Code & ")")
        FYUtil.Logging.LogTrace("Get code P:" & esBatchStatus.GetByCode("P").Description)

        FYUtil.Logging.LogTrace("Missing Code (with default) = FindByCode(String.Empty) = " & (esBatchStatus.GetByCode("X", esBatchStatus.NotSet) = esBatchStatus.GetByCode("")).ToString)

        Dim tmp2 = esBatchStatus.GetByCode("X", esBatchStatus.NotSet)
        FYUtil.Logging.LogTrace("Compare NotSet: " & (esBatchStatus.NotSet = tmp2).ToString)

        Dim tmp3 = esBatchStatus.GetByCode("X", esBatchStatus.NotSet)
        Dim tmp4 = esBatchStatus.GetByCode("X", Nothing)
        FYUtil.Logging.LogTrace("Compare Default NotSet with Default Nothing: " & (tmp3 = tmp4).ToString)

        'tmp4 = eOtherStatus.Pending
        'tmp4 = Temp.eBatchStatus.Pending

        Console.Write("Compare to base classs was ")
        If esBatchStatus.NotSet = esBatchStatus.GetByCode("") Then
            FYUtil.Logging.LogTrace("True")
        Else
            FYUtil.Logging.LogTrace("False")
        End If

        Dim tmp5 = esBatchStatus.NotSet
        FYUtil.Logging.LogTrace("Compare NotSets of same objects one explicit, one derived: " & (tmp3 = tmp5).ToString)

        Dim sw1 = Stopwatch.StartNew()
        For i As Int32 = 0 To 1000000
            Dim outString = tmp.Code
            Dim outDescription = tmp.Description
        Next
        FYUtil.Logging.LogTrace("Time to do 1,000,000 StringValue lookups: " & sw1.ElapsedMilliseconds.ToString & " ms")

        sw1.Reset()
        For i As Int32 = 0 To 1000000
            Dim outObj1 = esBatchStatus.GetByCode("P")
            Dim outObj2 = esBatchStatus.GetByCode("MISSING", esBatchStatus.NotSet)
        Next
        FYUtil.Logging.LogTrace("Time to do 1,000,000 Obj lookups of existing and non-existing values: " & sw1.ElapsedMilliseconds.ToString & " ms")

        FYUtil.Logging.LogTrace("exToString: " & tmp.exToString)

        Dim tmpNothing As esBatchStatus = Nothing
        FYUtil.Logging.LogTrace("exToString: " & tmpNothing.exToString)

        FYUtil.Logging.LogTrace("NotSet < Accepted: " & CStr(esBatchStatus.NotSet < esBatchStatus.Accepted))
        FYUtil.Logging.LogTrace("NotSet >= Accepted: " & CStr(esBatchStatus.NotSet >= esBatchStatus.Accepted))
        FYUtil.Logging.LogTrace("Accepted > Rejected: " & CStr(esBatchStatus.Accepted > esBatchStatus.Rejected))
        FYUtil.Logging.LogTrace("Accepted <= Pending: " & CStr(esBatchStatus.Accepted <= esBatchStatus.Pending))

        FYUtil.Logging.LogTrace("esBatchStatus BindableList Element Count: " & esBatchStatus.ToBindableList().Count)
        FYUtil.Logging.LogTrace("esOtherStatus BindableList Element Count: " & esOtherStatus.ToBindableList().Count)

        FYUtil.Logging.LogTrace("exToCode: " & tmp22.Code)

        Dim serializedString = FYUtil.Serialization.SerializeObject(tmp21, FYUtil.Serialization.eSerializer.XmlContract)
        FYUtil.Logging.LogTrace("Serialized " & tmp21.Description & " (" & tmp21.Code & "): " & serializedString)
        Dim deserializedObj As esBatchStatus = FYUtil.Serialization.DeserializeObject(Of esBatchStatus)(serializedString, FYUtil.Serialization.eSerializer.XmlContract)
        FYUtil.Logging.LogTrace("Deserialized: " & deserializedObj.Description & " (" & deserializedObj.Code & ")")

        Console.ReadLine()
    End Sub

#End Region

    Sub Test_Login()
        Using frm As New FYUtil.WinForms.LoginForm
            If (frm.Display("test", "junk") <> Windows.Forms.DialogResult.OK) Then
                FYUtil.Logging.LogTrace("Canceled")
                Return
            End If
            FYUtil.Logging.LogTrace(String.Format("{0}, {1}", frm.Username, frm.Password))
        End Using
    End Sub

    Sub Test_LoginForm()
        Using frm As New FYUtil.WinForms.LoginForm
            Dim prmpt = "asdf a sdf asd f asd f sdf g sdf gsdfg sdfkljhglskdjfgh lksjd flkjsd flgkj sdflkj gslkdjf glksj dflgkj hsldfkjh glskjdf glk sjdfg"
            frm.Display("un", "", prmpt)
            frm.Display("", "", "")
        End Using

        'Dim frm2 = New FYUtil.WinForms.SecurityKeyForm
        'frm2.ShowDialog()
        'FYUtil.Logging.LogVerbose("User Entered Security Key: " & frm2.SecurityKey)

        'FYUtil.Logging.LogVerbose("User Entered Security Key (shared): " & FYUtil.WinForms.SecurityKeyForm.PromptForSecurityKey())
    End Sub

    Sub Test_PromptForms()
        Dim prmpt = "This is prompt text. Blah blah blah. This is prompt text. Blah blah blah. This is prompt text. Blah blah blah. This is prompt text. Blah blah blah. This is prompt text. Blah blah blah. This is prompt text. Blah blah blah."
        Dim str = ""
        Dim str2 = ""
        Dim i = 0
        Dim dic As New Dictionary(Of Int32, String)
        dic.Add(1, "test")
        dic.Add(2, "test2")
        dic.Add(3, "test3")
        dic.Add(4, "test4")
        dic.Add(5, "test5")

        FYUtil.WinForms.PromptForm.PromptForStringFromList(str, {"a", "b"}, prmpt, pAllowUserEntry:=True, pValidator:=New FYUtil.StringValidator With {.MaxLength = 3, .MinLength = 3})
        FYUtil.WinForms.PromptForm.PromptForValueFromList(Of Int32)(i, dic)

        Dim lst As New List(Of Int32)
        lst.Add(3)
        lst.Add(5)
        FYUtil.WinForms.PromptForm.PromptForValuesFromList(Of Int32)(lst, dic)

        FYUtil.WinForms.PromptForm.PromptForSingleLineString(str)
        FYUtil.WinForms.PromptForm.PromptForMultilineString(str)
        FYUtil.WinForms.PromptForm.PromptForNumber(i)
        FYUtil.WinForms.PromptForm.PromptForValueFromList(Of Int32)(i, dic)
        FYUtil.WinForms.PromptForm.PromptForSingleLineString(str, "Enter an e-mail", "", New FYUtil.EmailValidator(8, 20))


        Dim dt = DateTime.Today
        Dim dic2 As New Dictionary(Of DateTime, String)
        dic2.Add(DateTime.Today, "today")
        dic2.Add(DateTime.Today.AddDays(5), "next week")
        FYUtil.WinForms.PromptForm.PromptForValueFromList(Of DateTime)(dt, dic2)

        Debug.Print("pause")
    End Sub

    Public Class TestEx
        Public Property junk As Int32
    End Class
    Sub Test_Extensions()
        Dim dc As New Dictionary(Of String, String)
        dc.Add("def", "junk")
        Dim val = dc.exValueOrDefault("abc")
        val = dc.exValueOrDefault("abc", "mydef")
        val = dc.exValueOrDefault("def")

        Dim dc2 As New Dictionary(Of String, Int32)
        Dim val2 = dc2.exValueOrDefault("abc")

        Dim dc3 As New Dictionary(Of String, DateTime)
        Dim val3 = dc3.exValueOrDefault("abc")

        Dim dc4 As New Dictionary(Of String, DateTime?)
        Dim val4 = dc4.exValueOrDefault("abc")

        Dim dc5 As New Dictionary(Of String, TestEx)
        Dim val5 = dc5.exValueOrDefault("abc")

        Dim dt As DateTime
        dt = DateTime.Now.AddMinutes(-10)
        FYUtil.Logging.LogTrace(dt.exToRelativeDateString())
        dt = DateTime.Now.AddDays(-30)
        FYUtil.Logging.LogTrace(dt.exToRelativeDateString())
        dt = DateTime.Now.AddDays(-400)
        FYUtil.Logging.LogTrace(dt.exToRelativeDateString())
        dt = DateTime.Now.AddDays(400)
        FYUtil.Logging.LogTrace(dt.exToRelativeDateString())
    End Sub

    Sub Test_ExecuteSqlScript()
        Dim cs = "Server=.;Database=TestDb;Trusted_Connection=true"
        Dim sqlSb As New Text.StringBuilder
        sqlSb.AppendLine("PRINT 'Start'")
        sqlSb.AppendLine("select count(*) from TestTable")
        sqlSb.AppendLine("GO")
        sqlSb.AppendLine("PRINT 'next 1'")
        sqlSb.AppendLine("select count(*) from TestTable")
        'sqlSb.AppendLine("select notfound from TestTable")
        sqlSb.AppendLine("GO")
        sqlSb.AppendLine("PRINT 'next 2'")
        sqlSb.AppendLine("select count(*) from TestTable")
        sqlSb.AppendLine("PRINT 'next 2.1'")
        sqlSb.AppendLine("PRINT 'next 2.2'")
        sqlSb.AppendLine("RAISERROR('doh1', 1, 1)")
        sqlSb.AppendLine("RAISERROR('doh2', 13, 1)")
        sqlSb.AppendLine("PRINT 'next 2.3'")
        sqlSb.AppendLine("GO")
        sqlSb.AppendLine("PRINT 'next 3';")
        sqlSb.AppendLine("select count(*) from TestTable")
        sqlSb.AppendLine("GO")

        Dim runner As New FYUtil.Database.SqlScriptRunner
        AddHandler runner.RunScriptNotify, AddressOf Test_ExecuteSqlScript_Handler
        Dim res = runner.RunScript(cs, sqlSb.ToString)
        FYUtil.Logging.LogTrace(res.Description)
    End Sub

    Sub Test_ExecuteSqlScript_Handler(ByVal o As Object, ByVal e As FYUtil.Database.SqlScriptRunner.RunScriptNotifyEventArgs)
        Console.Write(String.Format("Batch {0} of {1} : {2}", e.BatchNumber, e.TotalBatches, e.Messages))
    End Sub

    Sub Test_TempFileManager()
        Using mgr1 As New FYUtil.TempFileManager("c:\junk\crap", "test")
            Dim tmp1 = mgr1.GetTempFilename("Temp1", "txt")
            IO.File.WriteAllText(tmp1, "Junk")

            Dim tmp2 = mgr1.GetTempFilename("Temp2", "txt")
            IO.File.WriteAllText(tmp2, "Junk")
        End Using

        Dim mgr2 As New FYUtil.TempFileManager
        Dim tmp3 = mgr2.GetTempFilename("Temp3", "txt")
        IO.File.WriteAllText(tmp3, "Junk")


        Using mgr3 As New FYUtil.TempFileManager()
            Dim tmp1 = mgr3.AddTempFilename("junk123.txt")
            IO.File.WriteAllText(tmp1, "Junk")
            Dim tmp2 = mgr3.AddTempFilename("\junk4.txt")
            IO.File.WriteAllText(tmp2, "Junk")
        End Using

        Debug.Print(Now().ToString)
    End Sub

    Sub Test_FixedWidth()
        Dim tmp As New FYUtil.FlatFiles.FixedWidthFile
        tmp.ColumnWidths = {7, 35, 35, 3, 35, 35, 35, 35, 2, 11, 25, 25, 80, 25, 3, 25, 3, 25, 3, 25, 3, 25, 3, 22, 22, 5, 35, 22, 1, 35, 1}.ToList
        tmp.ColumnNames = {"NCPDPID", "StoreNumber", "ReferenceNumberAlt1", "ReferenceNumberAlt1Qualifier", "StoreName", "AddressLine1", "AddressLine2", "City", "State", "Zip", "PhonePrimary", "Fax", "Email", "PhoneAlt1", "PhoneAlt1Qualifier", "PhoneAlt2", "PhoneAlt2Qualifier", "PhoneAlt3", "PhoneAlt3Qualifier", "PhoneAlt4", "PhoneAlt4Qualifier", "PhoneAlt5", "PhoneAlt5Qualifier", "ActiveStartTime", "ActiveEndTime", "ServiceLevel", "PartnerAccount", "LastModifiedDate", "TwentyFourOurFlag", "CrossStreet", "RecordChange"}.ToList
        tmp.Import("d:\temp\23f8da9a-f17d-46ed-bcf3-b1b226a5b7de.txt")

        FYUtil.Logging.LogTrace("First Row name: " & tmp.GetElement(0, "StoreName"))
        FYUtil.Logging.LogTrace("Last Row name: " & tmp.GetElement(8132, "StoreName"))

    End Sub

    Sub Test_CommandLine(ByVal pArgs As String())
        Dim args = FYUtil.CommandLine.GetArguments(pArgs)
    End Sub


    Sub Test_PDFPrefill()
        Dim dat As New Dictionary(Of String, String)
        dat.Item("txtTest") = "Testy Testerson"
        dat.Item("chkTest") = "True"
        dat.Item("optTest") = "optVal2"
        dat.Item("txtTestLarge") = "This is a longer sentence to show what data would look like in a larger textbox.  This is a longer sentence to show what data would look like in a larger textbox.  This is a longer sentence to show what data would look like in a larger textbox.  This is a longer sentence to show what data would look like in a larger textbox.  "
        dat.Item("cboTest") = "OPT1"
        dat.Item("lstTest") = "OPT1"

        Dim filename = "D:\Projects\FyUtil\Pdf\TestFiles\Fyit_Testing.pdf"
        Dim output = "D:\Projects\FyUtil\Pdf\TestFiles\Fyit_Testing_out.pdf"
        FYUtil.Pdf.FillForm(dat, filename, output)

        Process.Start(output)
    End Sub


    Sub Test_ImageToPdf()
        Dim outputFile = "d:\temp\dump.pdf"
        Dim imageFile = "d:\temp\fyitlogo.jpg"
        FYUtil.Pdf.ConvertImageToPdf(imageFile, outputFile)
        Process.Start(outputFile)
    End Sub

    Sub Test_DynamicPDF()
        Const FONT_SIZE = 14

        Dim outputFile = "d:\temp\dump.pdf"
        Dim imageFile = "d:\temp\fyitlogo.jpg"
        Dim helpImageFile = "d:\temp\help.png"
        Dim html = "<div style=""font-size: 6px;""><div>Test html</div><div>Test html <span>blah</span></div><div>Test html asdf asdfsfgh dfjh efgh dfgh dfth drth dfghdfgj dfj xdfth dfj dxftjhdth dfj dyj dfgj dj</div><div>Test html</div><div>Test html end</div><table><tr><td>test1</td><td>test 2</td></tr></table><span>junky</span></div>"

        Dim opt = New FYUtil.Pdf.PageSettings() With {
            .Width = FYUtil.Pdf.InchToPoint(5),
            .Height = Nothing,
            .MarginLeft = FYUtil.Pdf.InchToPoint(0.1),
            .MarginRight = FYUtil.Pdf.InchToPoint(0.1),
            .MarginTop = FYUtil.Pdf.InchToPoint(0.2),
            .MarginBottom = FYUtil.Pdf.InchToPoint(0.2)
        }

        Dim tbl As New FYUtil.Pdf.DynamicTable() With {.Options = New FYUtil.Pdf.TableSettings() With {
                                                            .Border = 0}}
        tbl.ColumnWidths = {60.0F, 180.0F, 60.0F}.ToList
        Dim row = New FYUtil.Pdf.DynamicTable.Row()
        row.Cells.Add(New FYUtil.Pdf.DynamicTable.Cell(FYUtil.Pdf.DynamicTable.Cell.eDataType.Text, "Date", New FYUtil.Pdf.CellSettings() With {.BorderBottom = 1}))
        row.Cells.Add(New FYUtil.Pdf.DynamicTable.Cell(FYUtil.Pdf.DynamicTable.Cell.eDataType.Text, "Product", New FYUtil.Pdf.CellSettings() With {.BorderBottom = 1}))
        row.Cells.Add(New FYUtil.Pdf.DynamicTable.Cell(FYUtil.Pdf.DynamicTable.Cell.eDataType.Text, "Cost", New FYUtil.Pdf.CellSettings() With {.BorderBottom = 1}))
        tbl.Rows.Add(row)

        For i = 1 To 4
            row = New FYUtil.Pdf.DynamicTable.Row()
            For j = 1 To 3
                Dim cellOpt As New FYUtil.Pdf.CellSettings()
                Dim cellText As String = ""
                Select Case j
                    Case 1
                        cellOpt.HorizontalAlignment = FYUtil.Pdf.eHorizontalAlignment.Center
                        cellOpt.VerticalAlignment = FYUtil.Pdf.eVerticalAlignment.Top
                        cellOpt.FontSize = 16
                        cellText = String.Format(New Date(2013, i, j).ToShortDateString)
                    Case 2
                        cellOpt.HorizontalAlignment = FYUtil.Pdf.eHorizontalAlignment.Left
                        cellOpt.VerticalAlignment = FYUtil.Pdf.eVerticalAlignment.Top
                        cellOpt.FontSize = 16
                        cellText = String.Format("Test: {0}, {1}, but this is longer description", {i, j})
                    Case 3
                        cellOpt.HorizontalAlignment = FYUtil.Pdf.eHorizontalAlignment.Right
                        cellOpt.VerticalAlignment = FYUtil.Pdf.eVerticalAlignment.Top
                        cellOpt.FontSize = 16
                        cellText = String.Format("{0:c}", {10 * i * j / 13})
                End Select
                Dim cell = New FYUtil.Pdf.DynamicTable.Cell(FYUtil.Pdf.DynamicTable.Cell.eDataType.Text, cellText, cellOpt)
                row.Cells.Add(cell)
            Next
            tbl.Rows.Add(row)
        Next

        Using pdf = New FYUtil.DynamicPdf(outputFile, opt)
            'pdf.AddImage(imageFile, New FYUtil.DynamicPdf.ImageSettings() With {.Alignment = FYUtil.DynamicPdf.eHorizontalAlignment.Left})

            pdf.AddImage(imageFile, New FYUtil.Pdf.ImageSettings() With {.Alignment = FYUtil.Pdf.eHorizontalAlignment.Center,
                                                                         .ScaleWidth = FYUtil.Pdf.InchToPoint(4.6)})
            pdf.AddText("Test text.  Just wanted to add something here.", New FYUtil.Pdf.TextSettings() With {.SpacingAfter = 15})
            'pdf.AddHtml(html)
            'pdf.AddImage(imageFile, New FYUtil.DynamicPdf.ImageSettings() With {.Alignment = FYUtil.DynamicPdf.eAlignment.Right})

            pdf.AddTable(tbl)

            pdf.AddText("Signature:", New FYUtil.Pdf.TextSettings() With {.SpacingBefore = 30})
            pdf.AddHorizontalLine(New FYUtil.Pdf.LineSettings() With {.SpacingBefore = 8,
                                                                            .SpacingAfter = 5,
                                                                            .LeftOffest = 15})

            pdf.AddText("101-12345678",
                         New FYUtil.Pdf.TextSettings() With {.FontSize = FONT_SIZE * 1.4,
                                                                    .SpacingBefore = 50,
                                                                    .SpacingAfter = 20,
                                                                    .HorizontalAlignment = FYUtil.Pdf.eHorizontalAlignment.Center,
                                                                    .FontPath = "D:\Projects\Optics\WebSite\Content\IDAutomationHC39M.ttf"})

            For x = 0 To 500 Step 50
                For y = 0 To 500 Step 10
                    pdf.AddFixedPositionText(String.Format("{0},{1}", {x, y}), New FYUtil.Pdf.FixedPosition() With {.X = x, .Y = y}, Nothing)
                Next
            Next

            For x = 0 To 500 Step 100
                For y = 0 To 500 Step 50
                    pdf.AddImage(helpImageFile, New FYUtil.Pdf.ImageSettings() With {.FixedPositionInfo = New FYUtil.Pdf.FixedPosition() With {.X = x, .Y = y}})
                Next
            Next

        End Using

        Process.Start(outputFile)
    End Sub

    Sub Test_CustomMsgBox()
        Dim res As String
        Dim longMsg As String = ""
        For x As Int32 = 1 To 10
            longMsg += String.Format("Testing #{0} abcd efgh ijkl mnop qrst uvwx yz ", x)
        Next
        Dim msg As String = "Do you wish to finish now?"
        res = FYUtil.WinForms.CustomMessageBox.Show(msg, {"Long button blah blah blah", "Later", "Cancel"}, "Prompt")
        res = FYUtil.WinForms.CustomMessageBox.Show(longMsg, {"Ok", "Cancel"}, "Prompt")
        res = FYUtil.WinForms.CustomMessageBox.Show("Simple message", {"Ok"})
        res = FYUtil.WinForms.CustomMessageBox.ShowTopMost("", {"Ok"}, "TopMost")
    End Sub

    Sub Test_BigMsgBox()
        Dim msg As String = "This is my message"
        Dim msgOptions = New FYUtil.WinForms.MessageBoxOptions() With {.Width = 800, .Height = 600, .ReadOnlyMessage = True}
        FYUtil.WinForms.BigMessageBox.Show(msg, "Status: Ok", "Test Big Message", msgOptions)
    End Sub

    Sub Test_ZipAndEmail()
        Using tfm = New FYUtil.TempFileManager("d:\temp")
            Dim filename As String = tfm.GetTempFilename("testfile", "txt")

            FYUtil.Logging.LogTrace("Generating temp file: " & filename)

            Dim rndgen = New Random(CInt(Right(DateTime.Now.Ticks.ToString, 8)))
            IO.File.WriteAllText(filename, "Test file - test data ")
            For i = 0 To 1000
                Dim sb As New Text.StringBuilder
                For j = 0 To 80
                    Dim ascVal = rndgen.Next(64, 90)
                    If ascVal = 64 Then
                        sb.Append(" ")
                    Else
                        sb.Append(Chr(ascVal))
                    End If
                Next
                IO.File.AppendAllText(filename, sb.ToString & vbCrLf)
            Next

            Dim zipFilename = FYUtil.Zip.IonicZip.GetZipFileName(filename)
            FYUtil.Logging.LogTrace("Zipping to: " & zipFilename)

            Call FYUtil.Zip.IonicZip.ZipFile(filename)

            Dim origFileInfo = New IO.FileInfo(filename)
            Dim zipFileInfo = New IO.FileInfo(zipFilename)

            FYUtil.Logging.LogTrace("Original File Size = " & origFileInfo.Length)
            FYUtil.Logging.LogTrace("Zip File Size = " & zipFileInfo.Length)

            FYUtil.Logging.LogTrace("Emailing log file to jmccombie@gmail.com")
            Dim attachment As New Net.Mail.Attachment(zipFilename)
            FYUtil.Email.Send("devel", "testapp@gmail.com", "jmccombie@gmail.com", "Test Send Zipped Log File", "Test Send Zipped Log File", Nothing, Nothing, Nothing, {attachment})

            tfm.AddTempFilename(zipFilename)
        End Using
        FYUtil.Logging.LogTrace("Orig & Zip files should be deleted...")
    End Sub

#Region "Threads"

    Sub Threads_DoLater()
        FYUtil.Threading.DelayDo(AddressOf Threads_DoLater_Func, TimeSpan.FromSeconds(5))
    End Sub

    Private Sub Threads_DoLater_Func()
        MsgBox("Threads_DoLater")
    End Sub
    Private _wt As FYUtil.Threading.WorkThreadBehavior
    Sub WorkThreadStart()
        If (_wt Is Nothing) Then
            FYUtil.Logging.LogInformational("Starting WorkThread")
            _wt = New FYUtil.Threading.WorkThreadBehavior(AddressOf WorkThreadWork)
            _wt.Interval = TimeSpan.FromSeconds(10)
            _wt.Startup()
        End If
    End Sub
    Sub WorkThreadStop()
        If (_wt IsNot Nothing) Then
            FYUtil.Logging.LogInformational("Stopping WorkThread")
            _wt.Shutdown()
            _wt = Nothing
        End If
    End Sub
    Private Sub WorkThreadWork()
        FYUtil.Logging.LogTrace(String.Format("WorkThreadWork: {0}", DateTime.Now))
    End Sub
#End Region


#Region "DbResult"


    Sub DbResult()

    End Sub

#End Region

    Sub Webservices()
        Dim x = FYUtil.WebServices.CreateClientBinding(FYUtil.WebServices.eServiceSecurityMode.SslEncryptionWithUsernameAuth)
    End Sub

    Sub ObjectLabelPair()
        Dim olp As New FYUtil.ObjectLabelPair(12, "twelve")
        Dim x As Int32 = FYUtil.ObjectLabelPair.GetPayload(Of Int32)(olp)
        FYUtil.Logging.LogInformational(x.ToString)
        olp = Nothing
        x = FYUtil.ObjectLabelPair.GetPayload(Of Int32)(olp)
        FYUtil.Logging.LogInformational(x.ToString)
        x = FYUtil.ObjectLabelPair.GetPayload(Of Int32)(olp, -100)
        FYUtil.Logging.LogInformational(x.ToString)
        Dim y As Int32? = FYUtil.ObjectLabelPair.GetPayload(Of Int32?)(olp)
        FYUtil.Logging.LogInformational(y.ToString)

        Dim olp2 As New FYUtil.ObjectLabelPair("hey", "there")
        Dim s As String = FYUtil.ObjectLabelPair.GetPayload(Of String)(olp2)
        FYUtil.Logging.LogInformational(s)
        olp2 = Nothing
        s = FYUtil.ObjectLabelPair.GetPayload(Of String)(olp2)
        FYUtil.Logging.LogInformational(s)
    End Sub

#Region "DbStatement"

    Sub DbStatement_Image()
        Dim cs = "Server=(local);Database=JunkTemp;Trusted_Connection=true;"
        Dim runner = New FYUtil.Database.DbRunner(New FYUtil.Database.DbConnectionInfo With {.ConnectionString = cs})

        Dim stmt As New FYUtil.Database.DbStatementBuilder("INSERT INTO testtable (Id, testimage) VALUES (@Id, @testimage)")
        stmt.Parameters.AddInt32("@Id", 0)
        stmt.Parameters.Add("@testimage", Nothing, DbType.Binary)
        runner.ExecuteNonQuery(stmt)
    End Sub

    Sub DbStatement_MVP()
        Dim cs = "Server=(local);Database=TestDb;Trusted_Connection=true;"
        Dim runner = New FYUtil.Database.DbRunner(New FYUtil.Database.DbConnectionInfo With {.ConnectionString = cs})

        Dim stmt As New FYUtil.Database.DbSelectBuilder("NullsTest", "*")

        stmt.WhereClause.AddMvp("intnull", "@ints", {1, 2, 3})
        stmt.WhereClause.AddMvp("strnull", "@strs", {"asdf", "asdfdf", "erhsth"})
        stmt.WhereClause.AddMvp("dtnull", "@dts", {DateTime.Now}, pUseNotIn:=True, pPrefixOperator:=FYUtil.Database.DbConditionBuilder.eBooleanOperator.OrOperator)

        FYUtil.Logging.LogInformational(stmt.GetSql)

        Dim dt = runner.ExecuteToDataTable(stmt)

        FYUtil.Logging.LogInformational(dt.Rows.Count.ToString)
    End Sub

    Sub DbStatement_TVP()
        Dim cs = "Server=(local)\SQLEXPRESS;Database=TestDb;Trusted_Connection=true;"
        Dim runner = New FYUtil.Database.DbRunner(New FYUtil.Database.DbConnectionInfo With {.ConnectionString = cs})

        Dim stmt As New FYUtil.Database.DbSelectBuilder("TestTable", "*")

        stmt.WhereClause.Add("[id] IN (SELECT Value FROM @mytvp)")
        stmt.Parameters.AddMultiValueInt32("@mytvp", {1, 2, 3, 4, 5})

        FYUtil.Logging.LogInformational(stmt.GetSql)

        Dim dt = runner.ExecuteToDataTable(stmt)

        FYUtil.Logging.LogInformational(dt.Rows.Count.ToString)



        Dim stmt2 As New FYUtil.Database.DbSelectBuilder("TestTable", "*")

        stmt2.WhereClause.Add("[name] IN (SELECT Value FROM @mytvp)")
        stmt2.Parameters.AddMultiValueString("@mytvp", {"stgj", "d", "ghmk"})

        FYUtil.Logging.LogInformational(stmt2.GetSql)

        Dim dt2 = runner.ExecuteToDataTable(stmt2)

        FYUtil.Logging.LogInformational(dt2.Rows.Count.ToString)



        Dim stmt3 As New FYUtil.Database.DbSelectBuilder("TestTable", "*")

        stmt3.WhereClause.Add("(SELECT COUNT(*) FROM @mytvp) = 0 OR [id] IN (SELECT Value FROM @mytvp)")
        stmt3.Parameters.AddMultiValueString("@mytvp", {})

        FYUtil.Logging.LogInformational(stmt3.GetSql)

        Dim dt3 = runner.ExecuteToDataTable(stmt3)

        FYUtil.Logging.LogInformational(dt3.Rows.Count.ToString)
    End Sub

    Sub DbStatement_NULL()
        Dim cs = "Server=(local)\SQLEXPRESS;Database=TestDb;Trusted_Connection=true;"
        Dim runner = New FYUtil.Database.DbRunner(New FYUtil.Database.DbConnectionInfo With {.ConnectionString = cs})

        Dim stmt As New FYUtil.Database.DbSelectBuilder("NullsTest", "*")

        stmt.WhereClause.Add("strnull = @par1")
        stmt.Parameters.AddString("@par1", "asdf")
        stmt.WhereClause.Add("strnull = @par1null")
        stmt.Parameters.AddStringNullable("@par1null", Nothing)

        FYUtil.Logging.LogInformational(stmt.GetSql)

        Dim dt = runner.ExecuteToDataTable(stmt)

        FYUtil.Logging.LogInformational(dt.Rows.Count.ToString)
    End Sub

    Sub DbStatement_SubCondition()
        'Dim cs = "Server=(local)\SQLEXPRESS;Database=TestDb;Trusted_Connection=true;"
        'Dim runner = New FYUtil.Database.DbRunner(New FYUtil.Database.DbConnectionInfo With {.ConnectionString = cs})

        Dim stmt As New FYUtil.Database.DbSelectBuilder("TestTable", "*")

        Dim subWhere As New FYUtil.Database.DbConditionBuilder()
        subWhere.AddMvp("[id]", "@id", {1, 2, 3})
        subWhere.AddIsNull("[id]", True, FYUtil.Database.DbConditionBuilder.eBooleanOperator.OrOperator)

        stmt.WhereClause.Add("name = @name")
        stmt.Parameters.AddString("@name", "asdf")
        stmt.WhereClause.Add(subWhere)

        FYUtil.Logging.LogInformational(stmt.GetSql)

        'Dim dt = runner.ExecuteToDataTable(stmt)

        'FYUtil.Logging.LogInformational(dt.Rows.Count.ToString)
    End Sub

    Sub DbTransaction()
        Dim ci As New FYUtil.Database.DbConnectionInfo("Server=(local)\SQLEXPRESS;Database=TestDb;Trusted_Connection=true;", FYUtil.Database.eDbType.SqlServer)
        Dim runner = New FYUtil.Database.DbRunner(ci)

        For idx As Int32 = 1 To 10
            Dim insStmt As New FYUtil.Database.DbStatementBuilder("insert into transtest (junk) values (null)")
            runner.ExecuteNonQuery(insStmt)
        Next


        Dim countStmt As New FYUtil.Database.DbStatementBuilder("select count(*) from transtest")
        Dim deleteStmt As New FYUtil.Database.DbStatementBuilder("delete from transtest")
        FYUtil.Logging.LogInformational(String.Format("Initial count: {0}", runner.ExecuteToScalar(Of Int32)(countStmt)))

        Using trans As New FYUtil.Database.DbTransaction(ci)
            runner.ExecuteNonQuery(trans, deleteStmt)
        End Using
        FYUtil.Logging.LogInformational(String.Format("Implicit rollback: {0}", runner.ExecuteToScalar(Of Int32)(countStmt)))

        Using trans As New FYUtil.Database.DbTransaction(ci)
            Dim stmt As New FYUtil.Database.DbStatementBuilder("delete from transtest")
            runner.ExecuteNonQuery(trans, deleteStmt)
            FYUtil.Logging.LogInformational(String.Format("In transaction: {0}", runner.ExecuteToScalar(Of Int32)(trans, countStmt)))
            trans.Rollback()
        End Using
        FYUtil.Logging.LogInformational(String.Format("Explicit rollback: {0}", runner.ExecuteToScalar(Of Int32)(countStmt)))

        Using trans As New FYUtil.Database.DbTransaction(ci)
            Dim stmt As New FYUtil.Database.DbStatementBuilder("delete from transtest")
            runner.ExecuteNonQuery(trans, deleteStmt)
            FYUtil.Logging.LogInformational(String.Format("In transaction: {0}", runner.ExecuteToScalar(Of Int32)(trans, countStmt)))
            trans.Commit()
        End Using
        FYUtil.Logging.LogInformational(String.Format("Commit: {0}", runner.ExecuteToScalar(Of Int32)(countStmt)))

    End Sub

#End Region

    Sub AppSettings()
        Dim str As String
        str = FYUtil.AppSettingsHelper.GetString("teststr")
        FYUtil.Logging.LogInformational(String.Format("str: {0}", str))
        str = FYUtil.AppSettingsHelper.GetString("notexist")
        FYUtil.Logging.LogInformational(String.Format("str: {0}", str))
        str = FYUtil.AppSettingsHelper.GetString("notexist", "notfound")
        FYUtil.Logging.LogInformational(String.Format("str: {0}", str))

        Dim bl As Boolean?
        bl = FYUtil.AppSettingsHelper.GetBoolean("testbool")
        FYUtil.Logging.LogInformational(String.Format("bool: {0}", bl))
        bl = FYUtil.AppSettingsHelper.GetBoolean("noconvert")
        FYUtil.Logging.LogInformational(String.Format("bool: {0}", bl))
        bl = FYUtil.AppSettingsHelper.GetBoolean("notexist")
        FYUtil.Logging.LogInformational(String.Format("bool: {0}", bl))
        bl = FYUtil.AppSettingsHelper.GetBoolean("notexist", True)
        FYUtil.Logging.LogInformational(String.Format("bool: {0}", bl))

        Dim inn As Int32?
        inn = FYUtil.AppSettingsHelper.GetInt32("testint")
        FYUtil.Logging.LogInformational(String.Format("int: {0}", inn))
        inn = FYUtil.AppSettingsHelper.GetInt32("noconvert")
        FYUtil.Logging.LogInformational(String.Format("int: {0}", inn))
        inn = FYUtil.AppSettingsHelper.GetInt32("notexist")
        FYUtil.Logging.LogInformational(String.Format("int: {0}", inn))
        inn = FYUtil.AppSettingsHelper.GetInt32("notexist", 12)
        FYUtil.Logging.LogInformational(String.Format("int: {0}", inn))

        Dim en As DayOfWeek?
        en = FYUtil.AppSettingsHelper.GetEnum(Of DayOfWeek)("testenum")
        FYUtil.Logging.LogInformational(String.Format("enum: {0}", en))
        en = FYUtil.AppSettingsHelper.GetEnum(Of DayOfWeek)("noconvert")
        FYUtil.Logging.LogInformational(String.Format("enum: {0}", en))
        en = FYUtil.AppSettingsHelper.GetEnum(Of DayOfWeek)("notexist")
        FYUtil.Logging.LogInformational(String.Format("enum: {0}", en))
        en = FYUtil.AppSettingsHelper.GetEnum(Of DayOfWeek)("notexist", DayOfWeek.Thursday)
        FYUtil.Logging.LogInformational(String.Format("enum: {0}", en))

    End Sub

    Sub MySettings()
        Dim settings As FYUtil.MySettings
        Dim str As String

        settings = New FYUtil.MySettings(FYUtil.MySettings.eSource.AppConfig)

        str = settings.GetString("teststr1")
        FYUtil.Logging.LogInformational(String.Format("str1: {0}", str))
        str = settings.GetString("teststr2")
        FYUtil.Logging.LogInformational(String.Format("str2: {0}", str))


        settings = New FYUtil.MySettings("app.junk.config")

        str = settings.GetString("teststr1")
        FYUtil.Logging.LogInformational(String.Format("str1: {0}", str))
        str = settings.GetString("teststr2")
        FYUtil.Logging.LogInformational(String.Format("str2: {0}", str))

    End Sub

    Sub Paging_Selects()
        Dim connInfo As New FYUtil.Database.DbConnectionInfo With {
         .ConnectionString = "Server=.\SQLEXPRESS;Database=TestDb;UID=sa;Password=Password1;",
         .DbType = FYUtil.Database.eDbType.SqlServer
        }
        Dim runner = New FYUtil.Database.DbRunner(connInfo)

        Dim dt As DataTable
        Dim stmt As New FYUtil.Database.DbSelectBuilder("TestTable", "id,name")
        stmt.AddToOrderClause("name asc")
        Dim pager As New FYUtil.Database.DbSelectPager(stmt, 1)
        pager.CalculateTotals = True
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 1
            dt = runner.ExecuteToDataTable(pager)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, dt.Rows.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 2
            dt = runner.ExecuteToDataTable(pager)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, dt.Rows.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 200
            dt = runner.ExecuteToDataTable(pager)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, dt.Rows.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 201
            dt = runner.ExecuteToDataTable(pager)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, dt.Rows.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 202
            dt = runner.ExecuteToDataTable(pager)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, dt.Rows.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 200000
            dt = runner.ExecuteToDataTable(pager)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, dt.Rows.Count))
        End Using
    End Sub

    Sub Paging_Selects_Objects()
        Dim connInfo As New FYUtil.Database.DbConnectionInfo With {
         .ConnectionString = "Server=.;Database=TestDb;UID=sa;Password=Password1;",
         .DbType = FYUtil.Database.eDbType.SqlServer
        }
        Dim runner = New FYUtil.Database.DbRunner(connInfo)

        Dim objList As IList(Of Tuple(Of Int32, String))
        Dim stmt As New FYUtil.Database.DbSelectBuilder("TestTable", "id, name")
        stmt.AddToOrderClause("name asc")
        Dim pager As New FYUtil.Database.DbSelectPager(stmt, 1)
        pager.CalculateTotals = True
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 1
            objList = runner.ExecuteToObjectList(Of Tuple(Of Int32, String))(pager, AddressOf Paging_Selects_Objects_CreateObject)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, objList.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 2
            objList = runner.ExecuteToObjectList(Of Tuple(Of Int32, String))(pager, AddressOf Paging_Selects_Objects_CreateObject)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, objList.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 200
            objList = runner.ExecuteToObjectList(Of Tuple(Of Int32, String))(pager, AddressOf Paging_Selects_Objects_CreateObject)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, objList.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 201
            objList = runner.ExecuteToObjectList(Of Tuple(Of Int32, String))(pager, AddressOf Paging_Selects_Objects_CreateObject)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, objList.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 202
            objList = runner.ExecuteToObjectList(Of Tuple(Of Int32, String))(pager, AddressOf Paging_Selects_Objects_CreateObject)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, objList.Count))
        End Using
        Using New FYUtil.Logging.ExecutionTimer("get page")
            pager.PageNumber = 200000
            objList = runner.ExecuteToObjectList(Of Tuple(Of Int32, String))(pager, AddressOf Paging_Selects_Objects_CreateObject)
            FYUtil.Logging.LogInformational(String.Format("Page: {0}, rows:{1}", pager.PageNumber, objList.Count))
        End Using
    End Sub

    Private Function Paging_Selects_Objects_CreateObject(pData As FYUtil.Database.DbResult) As Tuple(Of Int32, String)
        Return New Tuple(Of Int32, String)(pData.GetInt32("id"), pData.GetString("name"))
    End Function

    Sub CSV()
        Using conn As New SqlClient.SqlConnection("Server=.;Database=TestDb;UID=sa;Password=Password1;")
            conn.Open()
            Using cmd = conn.CreateCommand()
                cmd.CommandText = "select * from TestTable"
                Using rdr = cmd.ExecuteReader()
                    FYUtil.FlatFiles.CsvFile.DataReaderToCsvFile(rdr, True, "d:\temp\test1.csv")
                End Using
                cmd.CommandText = "select * from TestTable2"
                Using rdr = cmd.ExecuteReader()
                    FYUtil.FlatFiles.CsvFile.DataReaderToCsvFile(rdr, True, "d:\temp\test2.csv")
                End Using
            End Using
        End Using
    End Sub

    Sub ExtractPDFPages()
        Dim sourceFile = ""
        Using frm As New System.Windows.Forms.OpenFileDialog()
            frm.Filter = "pdf|*.pdf"
            frm.ShowDialog()
            sourceFile = frm.FileName
        End Using

        If IO.File.Exists(sourceFile) Then
            Dim destPath = IO.Path.GetDirectoryName(sourceFile)
            For x = 1 To FYUtil.Pdf.PageCount(sourceFile)
                Dim destFile = IO.Path.Combine(destPath, String.Format("ExtractPages_{0}.pdf", x))
                FYUtil.Pdf.ExtractPages(sourceFile, destFile, {x})
                FYUtil.Logging.LogInformational(destFile + " saved.")
            Next
        End If
    End Sub


#Region "CodedEnum"

    Private Enum eTestEnum
        Male
        Female
        NotSure
        asdf
    End Enum

    Private Enum eTestCodedEnum
        <FYUtil.CodedEnum.CodedEnumValue("M", "male desc")>
        Male = 1

        <FYUtil.CodedEnum.CodedEnumValue("F")>
        Female = 2

        <FYUtil.CodedEnum.CodedEnumValue("NS", "Not Sure")>
        NotSure = 0

        <FYUtil.CodedEnum.Hidden()>
        hiddenVal = 99

        <FYUtil.CodedEnum.Category("cat1")>
            cat1val = 4

        <FYUtil.CodedEnum.Category("cat2")>
            cat2val = 5

        <FYUtil.CodedEnum.Category({"cat1", "cat2"})>
        cat1and2val = 6

        <ComponentModel.Description("compdesc")>
            compdescval = 8

        plainEnumVal = 3
    End Enum

    Private Enum eBrokenEnum
        BrokenEnum0 = 1

        <FYUtil.CodedEnum.CodedEnumValue("1")>
        BrokenEnum1 = 2

        <FYUtil.CodedEnum.CodedEnumValue("xx")>
        BrokenEnum2 = 3

        <FYUtil.CodedEnum.CodedEnumValue("XX")>
        BrokenEnum3 = 4
    End Enum

    Sub CodedEnums()
        Using New FYUtil.Logging.ExecutionTimer("first one")
            CodedEnumsPrint(eTestEnum.Female)
        End Using
        Using New FYUtil.Logging.ExecutionTimer("second one")
            CodedEnumsPrint(eTestEnum.NotSure)
        End Using
        Using New FYUtil.Logging.ExecutionTimer("first one")
            CodedEnumsPrint(eTestCodedEnum.Female)
        End Using
        Using New FYUtil.Logging.ExecutionTimer("second one")
            CodedEnumsPrint(eTestCodedEnum.NotSure)
        End Using

        FYUtil.Logging.LogInformational(String.Format("eTestEnum code {0}: {1}", "M", FYUtil.CodedEnum.GetByCode(Of eTestEnum)("M")))
        FYUtil.Logging.LogInformational(String.Format("eTestCodedEnum code {0}: {1}", "M", FYUtil.CodedEnum.GetByCode(Of eTestCodedEnum)("M")))
        For Each it In FYUtil.CodedEnum.GetCodeLabelList(Of eTestCodedEnum)()
            FYUtil.Logging.LogInformational(String.Format("[list of eTestCodedEnum] {0}: {1}", it.Key, it.Value))
        Next
        For Each it In FYUtil.CodedEnum.GetCodeLabelList(Of eTestCodedEnum)(pIncludeHidden:=True)
            FYUtil.Logging.LogInformational(String.Format("[list of eTestCodedEnum with hidden] {0}: {1}", it.Key, it.Value))
        Next
        For Each it In FYUtil.CodedEnum.GetCodeLabelList(Of eTestCodedEnum)(pCategory:="cat1")
            FYUtil.Logging.LogInformational(String.Format("[list of eTestCodedEnum, cat1] {0}: {1}", it.Key, it.Value))
        Next

        Try
            FYUtil.Logging.LogInformational(String.Format("[shouldn't get here] eBrokenEnum code {0}: {1}", "3", FYUtil.CodedEnum.GetByCode(Of eBrokenEnum)("3")))
        Catch ex As Exception
            FYUtil.Logging.LogInformational(String.Format("eBrokenEnum is broken ({0})", ex.Message))
        End Try
    End Sub
    Private Sub CodedEnumsPrint(pEnumVal As System.Enum)
        FYUtil.Logging.LogInformational(String.Format("{0}: {1} ({2})", pEnumVal, FYUtil.CodedEnum.GetCode(pEnumVal), FYUtil.CodedEnum.GetLabel(pEnumVal)))
    End Sub

#End Region


    Private Class ExToActHandler

        Public Property MyTitle As String

        Public Sub DoStuff(pName As String)
            FYUtil.Logging.LogInformational(String.Format("Do stuff ({0}): {1}", MyTitle, pName))
        End Sub
    End Class

    Sub DbExecuteToAction()
        Dim connInfo As New FYUtil.Database.DbConnectionInfo("Server=.;Database=TestDb;UID=sa;Password=Password1;", FYUtil.Database.eDbType.SqlServer)
        Dim runner = New FYUtil.Database.DbRunner(connInfo)
        Dim stmt As New FYUtil.Database.DbSelectBuilder("Junk1", "*")
        runner.ExecuteToAction(stmt, Sub(pData As FYUtil.Database.DbResult)
                                         FYUtil.Logging.LogInformational("ExecuteToAction: " + pData.GetString("Name"))
                                     End Sub)

        Dim hnd As New ExToActHandler With {.MyTitle = "myhandler"}
        runner.ExecuteToAction(stmt, Sub(pData As FYUtil.Database.DbResult)
                                         hnd.DoStuff(pData.GetString("Name"))
                                     End Sub)
    End Sub

    Sub TZs()
        For Each tz As TimeZoneInfo In TimeZoneInfo.GetSystemTimeZones()
            FYUtil.Logging.LogInformational(tz.Id)
        Next
        Dim tz1 = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
        Dim tz2 = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")
        If (tz1.Equals(tz2)) Then FYUtil.Logging.LogInformational("Timezones same")
        FYUtil.Logging.LogInformational(FYUtil.TimeZoneCodes.GetCode(tz1))
    End Sub



#Region "Cache"

    Private Class CacheTestObj
        Inherits FYUtil.IntKeyBaseObject

        Public Overrides Property UniqueId As Integer
        Public Property Fk As Int32
        Public Property Name As String
    End Class

    Private Shared _curPartition As String = "test1"
    Private Shared Function GetPartition() As String
        Return _curPartition
    End Function

    'Private Shared _cacheTestObjs As IList(Of CacheTestObj) = {New CacheTestObj With {.UniqueId = 1, .Fk = 1, .Name = "test1"},
    '                                                    New CacheTestObj With {.UniqueId = 2, .Fk = 1, .Name = "test2"},
    '                                                    New CacheTestObj With {.UniqueId = 3, .Fk = 2, .Name = "test3"},
    '                                                    New CacheTestObj With {.UniqueId = 4, .Fk = 2, .Name = "test4"},
    '                                                    New CacheTestObj With {.UniqueId = 5, .Fk = 2, .Name = "test5"},
    '                                                    New CacheTestObj With {.UniqueId = 6, .Fk = 2, .Name = "test6"},
    '                                                    New CacheTestObj With {.UniqueId = 7, .Fk = 2, .Name = "test7"},
    '                                                    New CacheTestObj With {.UniqueId = 8, .Fk = 3, .Name = "test8"},
    '                                                    New CacheTestObj With {.UniqueId = 9, .Fk = 7, .Name = "test9"}}

    'Private Shared Function GetCacheTestObjs(pIds As IEnumerable(Of Int32)) As IList(Of CacheTestObj)
    '    FYUtil.Logging.LogInformational("Resolve: " + Join((From i In pIds Select CStr(i)).ToArray))
    '    System.Threading.Thread.Sleep(1000)
    '    Return (From ct In _cacheTestObjs Where pIds.Contains(ct.UniqueId) Select ct).ToList
    'End Function

    'Private Shared Function GetCacheTestObjsByFk(pFk As Int32) As IList(Of CacheTestObj)
    '    FYUtil.Logging.LogInformational("Resolve Rel: " + pFk.ToString)
    '    System.Threading.Thread.Sleep(1000)
    '    Return (From ct In _cacheTestObjs Where ct.Fk = pFk Select ct).ToList
    'End Function

    'Private Shared ReadOnly ACache As New FYUtil.Caching.ResolvingCacheSBO(Of Int32, CacheTestObj)("ACache", AddressOf GetCacheTestObjs, AddressOf GetPartition)
    'Private Shared ReadOnly ARelCache As New FYUtil.Caching.ResolvingRelationshipCacheSBO(Of Int32, Int32, CacheTestObj)("ARelCache", AddressOf GetCacheTestObjsByFk, ACache)

    'Sub Caching()
    '    Dim itm As CacheTestObj
    '    Dim itms As IEnumerable(Of CacheTestObj)

    '    _curPartition = "test1"
    '    itm = ACache.LoadItem(1) 'load new one, slow
    '    itm = ACache.LoadItem(1) 'reload, fast
    '    itm = ACache.LoadItem(2) 'load new
    '    itm = ACache.LoadItem(99) 'load missing

    '    itms = ACache.LoadItems({1, 2}).Values 'load 2 already loaded

    '    'error
    '    itms = ACache.LoadItems({1, 2, 3}).Values 'includes one not loaded

    '    itms = ARelCache.LoadItemsByForeignKey(1).Values 'load new rel with populated items
    '    itms = ARelCache.LoadItemsByForeignKey(2).Values 'load new rel with unpopulated items
    '    itms = ARelCache.LoadItemsByForeignKey(1).Values 'reload existing rel
    '    itms = ARelCache.LoadItemsByForeignKey(99).Values 'load empty rel

    '    'new partition
    '    _curPartition = "test2"
    '    itm = ACache.LoadItem(1) 'new again, slow
    '    itm = ACache.LoadItem(1) 'reload, fast
    '    itm = ACache.LoadItem(2) 'new again

    '    _curPartition = "test1"
    '    'all previously loaded
    '    itm = ACache.LoadItem(1)
    '    itm = ACache.LoadItem(2)

    'End Sub

#End Region


    Private Enum eMvpTest
        test
        junk
        blah
    End Enum

    Sub StatementMVP()
        'int list
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            stmt.WhereClause.AddMvp("num", "@num", {0, 1, 2, 3})
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

        'nullable int list
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            Dim lst As IEnumerable(Of Int32?) = {1, 2, 3, Nothing}
            stmt.WhereClause.AddMvp("numnull", "@numnull", lst)
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

        'nullable int list without null
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            Dim lst As IEnumerable(Of Int32?) = {1, 2, 3}
            stmt.WhereClause.AddMvp("numnonull", "@numnonull", lst)
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

        'string list
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            stmt.WhereClause.AddMvp("junk", "@junk", {"a", "b"})
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

        'nullable string list
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            stmt.WhereClause.AddMvp("junk", "@junk", {"a", "b", Nothing})
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

        'enum list
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            stmt.WhereClause.AddMvp("enum", "@enum", {eMvpTest.junk, eMvpTest.blah, eMvpTest.test})
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

        'nullable enum list
        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder
            Dim lst As IEnumerable(Of eMvpTest?) = {eMvpTest.junk, eMvpTest.blah, eMvpTest.test, Nothing}
            stmt.WhereClause.AddMvp("enumnull", "@enumnull", lst)
            FYUtil.Logging.LogInformational(stmt.GetSql)
        End With

    End Sub

    Sub StatementDateRange()
        Dim ETZ As TimeZoneInfo = FYUtil.TimeZoneCodes.GetTimeZoneInfo("ET")
        Dim PTZ As TimeZoneInfo = FYUtil.TimeZoneCodes.GetTimeZoneInfo("PT")
        Dim UTCTZ = TimeZoneInfo.Utc

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", DateTime.Parse("1/1/2000"), DateTime.Parse("1/3/2000"), pDateOnly:=True)
            Dim testDesc = "Local, DateOnly 1/1/2000 - 1/3/2000"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", DateTime.Parse("1/1/2000"), DateTime.Parse("1/3/2000"), pDateOnly:=False)
            Dim testDesc = "Local, 1/1/2000 - 1/3/2000, without DateOnly"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", DateTime.Parse("1/1/2000 10:30 AM"), DateTime.Parse("1/1/2000 1:30 PM"), pDateOnly:=False)
            Dim testDesc = "Local, 10:30 AM - 1:30 PM"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", Nothing, DateTime.Parse("1/5/2000"), pDateOnly:=True)
            Dim testDesc = "No start date, DateOnly 1/5/2000"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", DateTime.Parse("1/1/2000 10:30 AM"), DateTime.Parse("1/1/2000 1:30 PM"), pDateOnly:=False, pParameterTimeZone:=ETZ, pDbTimeZone:=TimeZoneInfo.Utc)
            Dim testDesc = "User in ET, DB in UTC, 10:30 AM - 1:30 PM"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", DateTime.Parse("1/1/2000"), DateTime.Parse("1/3/2000"), pDateOnly:=True, pParameterTimeZone:=ETZ, pDbTimeZone:=TimeZoneInfo.Utc)
            Dim testDesc = "User in ET, DB in UTC, DateOnly 1/1/2000 - 1/3/2000"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        With Nothing
            Dim stmt As New FYUtil.Database.DbSelectBuilder("Test", "*")
            stmt.WhereClause.AddDateRange("DateCreated", "@DateCreated", DateTime.Now, Nothing, pParameterTimeZone:=ETZ)
            Dim testDesc = "User in ET, DB in UTC, Now"
            FYUtil.Logging.LogInformational(String.Format("{1}: {0}{2}{0}Parameters:{0}{3}", Environment.NewLine, testDesc, stmt.GetSql.Replace(vbCrLf, " "), StatementParameterSummary(stmt)))
        End With

        Dim dt As DateTime
        dt = DateTime.Parse("2010-01-15")
        FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from ET to UTC: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, ETZ, UTCTZ)))
        FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from UTC to ET: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, UTCTZ, ETZ)))
        FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from ET to PT: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, ETZ, PTZ)))

        dt = DateTime.Now.Date
        FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from ET to UTC: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, ETZ, UTCTZ)))
        'error converting a local time FROM UTC... leave this as an error
        'FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from UTC to ET: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, UTCTZ, ETZ)))
        FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from ET to PT: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, ETZ, PTZ)))

        dt = DateTime.UtcNow.Date
        'error converting a UTC time FROM local time... leave this as an error
        'FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from ET to UTC: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, ETZ, UTCTZ)))
        FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from UTC to ET: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, UTCTZ, ETZ)))
        'error converting a UTC time FROM local time... leave this as an error
        'FYUtil.Logging.LogInformational(String.Format("Convert {0}({1}) from ET to PT: {2}", dt, dt.Kind, FYUtil.Utilities.ConvertDateTime(dt, ETZ, PTZ)))

    End Sub

    Private Function StatementParameterSummary(pStmt As FYUtil.Database.IDbStatement) As String
        Dim res As New Text.StringBuilder
        For Each p As FYUtil.Database.DbParameter In pStmt.Parameters
            res.AppendFormat("{0} ({1}/{2}): {3}", p.Name, p.Type, p.Value.GetType().Name, p.Value)
            res.AppendLine()
        Next
        Return res.ToString
    End Function


    Sub TzCodes()
        Dim localCode = FYUtil.TimeZoneCodes.GetCode(TimeZoneInfo.Local)
        FYUtil.Logging.LogInformational(String.Format("Local timezone: {0} ({1})", localCode, FYUtil.TimeZoneCodes.GetDescription(localCode)))
        Dim dt1 = DateTime.Parse("1/20/2000 10:00 AM")
        Dim dt2 = DateTime.Parse("6/20/2000 10:00 AM")
        For Each tzkvp In FYUtil.TimeZoneCodes.GetCodeDescriptionList
            Dim tz = FYUtil.TimeZoneCodes.GetTimeZoneInfo(tzkvp.Key)
            FYUtil.Logging.LogInformational(String.Format("{0} : {1}  ->  {2}", tzkvp.Value, dt1, TimeZoneInfo.ConvertTime(dt1, TimeZoneInfo.Local, tz)))
            FYUtil.Logging.LogInformational(String.Format("{0} : {1}  ->  {2}", tzkvp.Value, dt2, TimeZoneInfo.ConvertTime(dt2, TimeZoneInfo.Local, tz)))
        Next
    End Sub

    Private g_usd As FYUtil.MultiUserSessionData
    Sub SessionDataTest()
        g_usd = New FYUtil.MultiUserSessionData
        g_usd.AddSession("abc")
        g_usd.SetCurrentSession("abc")
        g_usd.Write("test", "junk")
        FYUtil.Logging.LogInformational(String.Format("SessionDataTest: {0}", g_usd.Read("test")))
        g_usd.SaveToContext()
        Dim thrd As New Threading.Thread(AddressOf SessionDataTestThread)
        thrd.Start()
    End Sub


    Private Sub SessionDataTestThread()
        FYUtil.Logging.LogInformational(String.Format("SessionDataTestThread: {0}", g_usd.Read("test")))
    End Sub

    Public Enum eSerTest
        Junk
        Test
        Blah
    End Enum
    Public Class SerTestObj
        Public Property Id As Int32
        Public Property Name As String
        Public Property ANum As Int32?
        Public Property AnEnum As eSerTest
        Public Property ADate As DateTime
        Public Property ABool As Boolean
        Public Property AFlt As Single
    End Class
    Sub Serialization()
        Dim serializer = FYUtil.Serialization.eSerializer.Readable

        Dim obj As New SerTestObj With {.Id = 5, .Name = "test", .AnEnum = eSerTest.Junk, .ADate = DateTime.Now, .AFlt = -5.3, .ABool = True}
        Dim objSer = FYUtil.Serialization.SerializeObject(obj, serializer)
        Dim objBack = FYUtil.Serialization.DeserializeObject(Of SerTestObj)(objSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, objSer))

        Dim dt = DateTime.Now
        Dim dtSer = FYUtil.Serialization.SerializeObject(dt, serializer)
        Dim dtBack = FYUtil.Serialization.DeserializeObject(Of DateTime)(dtSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, dtSer))

        Dim dtn As DateTime? = DateTime.Now
        Dim dtnSer = FYUtil.Serialization.SerializeObject(dtn, serializer)
        Dim dtnBack = FYUtil.Serialization.DeserializeObject(Of DateTime?)(dtnSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, dtnSer))

        Dim lst = {1, 2, 3}.ToList
        Dim lstSer = FYUtil.Serialization.SerializeObject(lst, serializer)
        Dim lstBack = FYUtil.Serialization.DeserializeObject(Of List(Of Int32))(lstSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, lstSer))

        Dim str = "test string~!@#$%^&*()_+=-`[]{}\|'""?/.,<>"
        Dim strSer = FYUtil.Serialization.SerializeObject(str, serializer)
        Dim strBack = FYUtil.Serialization.DeserializeObject(Of String)(strSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, strSer))

        Dim nint As Int32? = 5
        Dim nintSer = FYUtil.Serialization.SerializeObject(nint, serializer)
        Dim nintBack = FYUtil.Serialization.DeserializeObject(Of Int32?)(nintSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, nintSer))

        Dim nint2 As Int32? = Nothing
        Dim nint2Ser = FYUtil.Serialization.SerializeObject(nint2, serializer)
        Dim nint2Back = FYUtil.Serialization.DeserializeObject(Of Int32?)(nint2Ser, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, nint2Ser))

        Dim en = eSerTest.Blah
        Dim enSer = FYUtil.Serialization.SerializeObject(en, serializer)
        Dim enBack = FYUtil.Serialization.DeserializeObject(Of eSerTest)(enSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, enSer))

        Dim enn As eSerTest? = eSerTest.Blah
        Dim ennSer = FYUtil.Serialization.SerializeObject(enn, serializer)
        Dim ennBack = FYUtil.Serialization.DeserializeObject(Of eSerTest?)(ennSer, serializer)
        FYUtil.Logging.LogInformational(String.Format("Serialized using {0}: {1}", serializer, ennSer))
    End Sub

#Region "RowVersion"

    Private Class RowVersionObject
        Public Property Id As Int32
        Public Property Name As String
        Public Property RowVer As UInt64
    End Class

    Sub RowVersion()
        Dim ci As New FYUtil.Database.DbConnectionInfo("Server=.;Database=TestDb;UID=sa;Password=Password1;", FYUtil.Database.eDbType.SqlServer)
        Dim runner = New FYUtil.Database.DbRunner(ci)
        Dim stmt As New FYUtil.Database.DbSelectBuilder("RowVerTest", "*")
        Dim rvos = runner.ExecuteToObjectList(Of RowVersionObject)(stmt, AddressOf RowVersionAction)
        For Each rvo In rvos
            FYUtil.Logging.LogInformational(String.Format("ID: {0}, Name: {1}, RV: {2}", rvo.Id, rvo.Name, rvo.RowVer))
        Next

        Dim targetRvo = rvos.Item(0)
        Dim updStmt As New FYUtil.Database.DbStatementBuilder("UPDATE RowVerTest SET Name = @Name WHERE Id = @Id AND RowVer = @RowVer")
        updStmt.Parameters.AddInt32("@Id", targetRvo.Id)
        updStmt.Parameters.AddString("@Name", If(targetRvo.Name.Length > 30, Left(targetRvo.Name, 10), targetRvo.Name + "."))
        updStmt.Parameters.AddRowVersion("@RowVer", targetRvo.RowVer)

        Dim rowsUpdated = runner.ExecuteNonQuery(updStmt)
        FYUtil.Logging.LogInformational(String.Format("Correct RowVer, updated: {0} rows", rowsUpdated))

        rowsUpdated = runner.ExecuteNonQuery(updStmt)
        FYUtil.Logging.LogInformational(String.Format("Outdated RowVer, updated: {0} rows", rowsUpdated))
    End Sub
    Private Function RowVersionAction(pData As FYUtil.Database.DbResult) As RowVersionObject
        Dim res As New RowVersionObject
        res.Id = pData.GetInt32("Id")
        res.Name = pData.GetString("Name")
        res.RowVer = pData.GetRowVersion("RowVer")
        Return res
    End Function

#End Region


#Region "Reflection"

    Public Class ReflTestClass
        Public Property Id As Int32
        Public Property Name As String
        Public TestField As String
        Public Property cls As New ReflTestClass2

        Public Sub New()
        End Sub

        Public Sub New(pId As Int32)
            Id = pId
        End Sub

        Public Function TestFunc1(pInput As String) As String
            Return String.Format("Called TestFunc1; pInput: {0}", pInput)
        End Function

        Public Function TestFunc1(pInput As String, pInput2 As String) As String
            Return String.Format("Called TestFunc1; pInput: {0}, pInput2: {1}", pInput, pInput2)
        End Function

        Public Function TestFunc2() As String
            Return String.Format("Called TestFunc2; Name: {0}", Name)
        End Function

    End Class


    Public Class ReflTestClass2
        Public Property num1 As Int32
        Public Property str1 As String
        Public Property str2 As String
        Public fldnum1 As Int32
        Public fldstr1 As String
        Public Property junk1 As Int32
        Public Property junk2 As Int32?
        Public Property junk3 As String
        Public Property dec1 As Decimal
        Public Property dt1 As DateTime
        Public Property ts1 As TimeSpan
        Public Property arr1 As New List(Of Int32)
    End Class

    Public Class ReflTestClass3
        Public Property num1 As Int32
        Public Property str1 As String
        Public Property STR2 As String
        Public fldnum1 As Int32
        Public fldstr1 As String
        Public Property junk1 As Int32?
        Public Property junk2 As Int32
        Public Property junk3 As Int32
    End Class


    Sub SetObjectProperty()
        Dim tst As New ReflTestClass
        FYUtil.Utilities.SetObjectProperty(tst, "Id", 5)
        FYUtil.Utilities.SetObjectPropertyPath(tst, "cls.str1", "abc")
        FYUtil.Logging.LogTrace(FYUtil.Utilities.GetObjectProperty(tst, "Id").ToString)
        FYUtil.Logging.LogTrace(FYUtil.Utilities.GetObjectPropertyPath(tst, "cls.str1").ToString)
        'FYUtil.Utilities.SetObjectProperty(tst, "Id", "err")
    End Sub

    Sub CopyObject()
        Dim obj1 As New ReflTestClass2 With {.num1 = 5, .str1 = "test", .str2 = "test2", .fldnum1 = 11, .fldstr1 = "junk", .arr1 = {1, 2, 3}.ToList, .junk1 = 8, .junk2 = 9, .junk3 = "jnk"}
        Dim new1 = FYUtil.Utilities.CopyObject(obj1, FYUtil.Utilities.eHandleRefTypes.CopyReference)
        Dim new2 As New ReflTestClass2
        FYUtil.Utilities.CopyObjectInto(obj1, new2, FYUtil.Utilities.eHandleRefTypes.Ignore)
        Dim new3 As New ReflTestClass3
        FYUtil.Utilities.CopyObjectInto(obj1, new3)

        obj1.str1 = "new test"
        obj1.arr1.Add(55)
    End Sub

    Sub CallObjectMethod()
        Dim testObj As New ReflTestClass With {.Name = "testObj"}
        Dim tf1res1 = testObj.TestFunc1("abc")
        Dim tf1res2 = FYUtil.Utilities.CallObjectMethod(Of String)(testObj, "TestFunc1", {"abc"})
        FYUtil.Logging.LogImportant(String.Format("Test 1, equal: {0}", (tf1res1 = tf1res2)))

        Dim tf12res1 = testObj.TestFunc1("abc", "def")
        Dim tf12res2 = FYUtil.Utilities.CallObjectMethod(Of String)(testObj, "TestFunc1", {"abc", "def"})
        FYUtil.Logging.LogImportant(String.Format("Test 2, equal: {0}", (tf12res1 = tf12res2)))

        Dim tf2res1 = testObj.TestFunc2()
        Dim tf2res2 = FYUtil.Utilities.CallObjectMethod(Of String)(testObj, "TestFunc2", Nothing)
        FYUtil.Logging.LogImportant(String.Format("Test 3, equal: {0}", (tf2res1 = tf2res2)))

    End Sub


#End Region



    Sub FormatString()
        Dim dat1 = New With {.test1 = "abc", .test2 = 123}
        FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("test1: {test1}, test2: {test2}", dat1))

        'throws error
        'Dim dat2 = New With {.test1 = "abc"}
        'FYUtil.Logging.LogInformational(FYUtil.Utilities.FormatString("test1: {test1}, test2: {test2}", dat2))

        Dim dat3 = New Dictionary(Of String, Object)
        dat3.Add("test1", "abc")
        dat3.Add("test2", 123)
        FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("test1: {test1}, test2: {test2}", dat3))

        Dim dat4 = New With {.test1 = "abc{test2}def", .test2 = 123}
        FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("test1: {test1}, test2: {test2}", dat4))

        Dim dat5 = New With {.test1 = "abc", .test2 = 123, .test3 = DateTime.Now}
        FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("test1: {test1}, test2: {test2}, test3: {test3}, test2: {test2}, test3: {test3:yyyyMMdd}", dat5))

        Dim dat6 = New With {.test1 = "abc", .test2 = 123}
        FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("{test1} {{}} {{test1{test2}", dat6))
        FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("{{{{{test1} {test2}}}", dat6))

        'Dim dat7 = New With {.test1 = "abc"}
        'FYUtil.Logging.LogInformational(FYUtil.FormatString.Format("{test1} {test2}", dat7))

        Dim success As FYUtil.SuccessAndDescription
        Dim fs = ""

        fs = ""
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

        fs = "{abc}junk{def}"
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

        fs = "{{this is a test {junk}}}"
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

        fs = "{num:C2}"
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

        fs = "{crap stuff}"
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

        fs = "this is a {bad test"
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

        fs = "this is a bad test}"
        success = FYUtil.FormatString.Validate(fs)
        FYUtil.Logging.LogInformational(String.Format("String: {0}, valid: {1} ({2})", fs, success.Success, success.Description))

    End Sub


    Sub TestDurationToEnglish()
        TestDateDiff(Now.AddSeconds(-1), Now)
        TestDateDiff(Now.AddSeconds(-3.2), Now)
        TestDateDiff(Now.AddSeconds(-32.5667), Now)
        TestDateDiff(Now.AddSeconds(-95.343), Now)
        TestDateDiff(Now.AddMinutes(-1.2135), Now)
        TestDateDiff(Now.AddMinutes(-1.7512), Now)
        TestDateDiff(Now.AddMinutes(-45.00991), Now)
        TestDateDiff(Now.AddMinutes(-51.6), Now)
        TestDateDiff(Now.AddMinutes(-133.27), Now)
        TestDateDiff(Now.AddHours(-18.11234122), Now)
        TestDateDiff(Now.AddDays(-3.2), Now)
        TestDateDiff(Now.AddDays(-36.2345345), Now)
        TestDateDiff(Now.AddDays(-342.345634562), Now)
        TestDateDiff(Now.AddDays(-99.345432), Now)
        TestDateDiff(Now.AddMonths(2).AddMinutes(-75), Now)
        TestDateDiff(Now.AddYears(-1).AddDays(45.3454), Now)
        TestDateDiff(Now.AddYears(-3).AddDays(9.3333333), Now)
    End Sub

    Sub TestDateDiff(pDatetime As DateTime, pDatetime2 As DateTime)
        FYUtil.Logging.LogInformational(String.Format("User Friendly - Date1: {0}, Date2: {1} ==> {2}", {pDatetime, pDatetime2, FYUtil.Utilities.UserFriendlyDuration(pDatetime, pDatetime2)}))
        FYUtil.Logging.LogInformational(String.Format("Approximate   - Date1: {0}, Date2: {1} ==> {2}", {pDatetime, pDatetime2, FYUtil.Utilities.ApproximateDuration(pDatetime, pDatetime2)}))
    End Sub


    Private Class NodeTest
        Implements IComparable(Of NodeTest)

        Public Property Id As Int32
        Public Property ParentId As Int32?
        Public Property Name As String
        Public Property SortOrder As String
        Public Property PathName As String

        Public Overrides Function ToString() As String
            Return Name
        End Function

        Public Function CompareTo(other As NodeTest) As Integer Implements IComparable(Of NodeTest).CompareTo
            Return Me.SortOrder.CompareTo(other.SortOrder)
        End Function

    End Class


    Sub TreeNode()
        Dim nodes = {
            New NodeTest With {.Id = 210, .ParentId = 200, .Name = "node2.1", .SortOrder = "0210"},
            New NodeTest With {.Id = 220, .ParentId = 200, .Name = "node2.2", .SortOrder = "0220"},
            New NodeTest With {.Id = 221, .ParentId = 220, .Name = "node2.2.1", .SortOrder = "0221"},
            New NodeTest With {.Id = 230, .ParentId = 200, .Name = "node2.3", .SortOrder = "0230"},
            New NodeTest With {.Id = 120, .ParentId = 100, .Name = "node1.2", .SortOrder = "0120"},
            New NodeTest With {.Id = 110, .ParentId = 100, .Name = "node1.1", .SortOrder = "0110"},
            New NodeTest With {.Id = 121, .ParentId = 120, .Name = "node1.2.1", .SortOrder = "0121"},
            New NodeTest With {.Id = 122, .ParentId = 120, .Name = "node1.2.2", .SortOrder = "0122"},
            New NodeTest With {.Id = 100, .ParentId = Nothing, .Name = "node1", .SortOrder = "0100"},
            New NodeTest With {.Id = 200, .ParentId = Nothing, .Name = "node2", .SortOrder = "0200"},
            New NodeTest With {.Id = 300, .ParentId = Nothing, .Name = "node3", .SortOrder = "0300"}
        }.ToList
        nodes.Sort()

        Dim trees = FYUtil.TreeNode(Of NodeTest).CreateTree(nodes, Function(n) n.Id, Function(n) n.ParentId).ToList
        trees.Item(0).Traverse(Sub(n) n.Value.PathName = If(n.IsRoot, n.Value.Name, String.Format("{0} > {1}", n.Parent.Value.PathName, n.Value.Name)))
        trees.Item(0).TraverseValues(Sub(n) Debug.Print(n.PathName))
    End Sub

    Public Enum wsmTest
        StringData
        IntData
        ListData
    End Enum

    Public Shared UserSessionData As New FYUtil.Web.WebUserSessionData
    Public Shared SessionManager As New FYUtil.Web.WebSessionManager(Of wsmTest)(UserSessionData)

    Sub WebSessionManager()
        Throw New NotImplementedException("This test needs written")
        SessionManager.ReadSessionAndCookie(Of Int32)(wsmTest.IntData, 0)
    End Sub

    Sub EmbeddedResources()
        Dim rsrcs = FYUtil.EmbeddedResources.GetResourceNames
        FYUtil.Logging.LogInformational(String.Format("Resources: {0}", String.Join(", ", rsrcs)))
        Dim filedata = FYUtil.EmbeddedResources.GetResourceText("TextFile1.txt")
        FYUtil.Logging.LogInformational(String.Format("TextFile1.txt: {0}", filedata))

        Dim jpgfil = FYUtil.EmbeddedResources.ExtractResourceToAssemblyPath("tricycle.jpg")
        Process.Start(jpgfil)

        Dim txtfil = FYUtil.EmbeddedResources.ExtractResourceToAssemblyPath("TextFile1.txt")
        Process.Start(txtfil)

        FYUtil.Serialization.SerializeObjectReadable("test")
    End Sub


End Class
